package Karte_osm;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
use strict;
use warnings;
use CGI ':standard';
use Lib::Config;
use Mod::Buttons;
use Mod::DBtank;
use Mod::APIfunc;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
 my $node_meta = shift;
 my $users_dms = shift || "";
 my $mode = shift || "";
 my $varenv = shift;
 my $users_sharee = shift || { c_id => 0 };
 my $return = shift || "";

 my $q = new CGI;
 my $but = new Buttons;
 my $dbt = new DBtank;
 my $apif = new APIfunc;

 my $api_file = "/var/www/copri4/shareeconf/apikeys.cfg";
 my $aconf = Config::General->new($api_file);
 my %apikeyconf = $aconf->getall;

 my $api_return = { authcookie => '' };
 my $coo = $q->cookie(-name=>'domcookie') || $R::sessionid;
 ($api_return,$users_sharee) = $apif->auth_verify($q,$coo,"");

 my $man_gps = $q->escapeHTML($R::man_gps) || "";
 my $initMap = $dbt->{website}->{$varenv->{syshost}}->{initMap};
 my $map_zoom = $dbt->{website}->{$varenv->{syshost}}->{map_zoom} || "10";
 my $project = "all";
 my $uri_request = $dbt->{primary}->{sharee_primary}->{primaryApp};

  if($dbt->{website}->{$varenv->{syshost}}->{merchant_id}){
   $api_return->{authcookie} = $dbt->{website}->{$varenv->{syshost}}->{merchant_id} if(!$api_return->{authcookie});
   $project = $dbt->{website}->{$varenv->{syshost}}->{project} if($dbt->{website}->{$varenv->{syshost}}->{project});
   $uri_request = $dbt->{website}->{$varenv->{syshost}}->{operatorWeb};
   print $q->div({-class=>"content2_contact"},"Hier können Lastenfahrräder gemietet werden:"),"\n"; 
  }elsif($dbt->{operator}->{$varenv->{dbname}}->{merchant_id}){
   $api_return->{authcookie} = $dbt->{operator}->{$varenv->{dbname}}->{merchant_id} if(!$api_return->{authcookie});
   $initMap = $dbt->{operator}->{$varenv->{dbname}}->{initMap} if($dbt->{operator}->{$varenv->{dbname}}->{initMap});
   $map_zoom = $dbt->{operator}->{$varenv->{dbname}}->{map_zoom} if($dbt->{operator}->{$varenv->{dbname}}->{map_zoom});
   $project = $dbt->{operator}->{$varenv->{dbname}}->{project} if($dbt->{operator}->{$varenv->{dbname}}->{project});
   $uri_request = $dbt->{operator}->{$varenv->{dbname}}->{operatorDMS};
   print $q->div({-style=>'padding-top:40px;'},""),"\n"; 
   print $q->start_form(),"\n";
   print $q->div({-style=>'text-align:left;'},$but->singlesubmit1("set_gps","set_gps","",""),$q->textfield(-name=>'man_gps', -value=>"$man_gps", -override=>1, -placeholder=>'latitude, longitude',-size=>30)),"\n";
   print $q->end_form,"\n";
  }elsif($dbt->{primary}->{$varenv->{dbname}}->{merchant_id}){
   $api_return->{authcookie} = $dbt->{primary}->{$varenv->{dbname}}->{merchant_id} if(!$api_return->{authcookie});
   $initMap = $dbt->{primary}->{$varenv->{dbname}}->{initMap} if($dbt->{primary}->{$varenv->{dbname}}->{initMap});
   $uri_request = $dbt->{primary}->{$varenv->{dbname}}->{primaryDMS};
   print $q->div({-style=>'padding-top:40px;'},""),"\n"; 
   print $q->start_form(),"\n";
   print $q->div({-style=>'text-align:left;'},$but->singlesubmit1("set_gps","set_gps","",""),$q->textfield(-name=>'man_gps', -value=>"$man_gps", -override=>1, -placeholder=>'latitude, longitude',-size=>30)),"\n";
   print $q->end_form,"\n";
  }
 
  my $manlat = "";
  my $manlng = "";
  $manlat = $1 if($man_gps =~ /^(\d+\.\d+)/);
  $manlng = $1 if($man_gps =~ /(\d+\.\d+)$/);

  my $icon_green = "Open_Green.png";
  my $icon_red = "Open_Red.png";
  my $icon_blue = "Open_Blue.png";
  my $mapheight = "800px;";
  if($project eq "Bayern"){
   $icon_green = "LastenradBay_Standortpfeil_gruen_03a.png";
   $icon_red = "LastenradBay_Standortpfeil_rot_03a.png";
   $icon_blue = "LastenradBay_Standortpfeil_blau_03a.png";
   $mapheight = "800px;" if($varenv->{syshost} !~ /shareedms-/);
  }

 $users_sharee->{c_id} = "0" if(!$users_sharee->{c_id});
 $initMap =~ s/\s//g;
 my ($lat,$lng) = split(/,/,$initMap);

 print "<div style='background-color:#cccccc;width:100%;'>\n";

print<<EOF
<link rel="stylesheet" href="https://unpkg.com/leaflet\@1.8.0/dist/leaflet.css"
   integrity="sha512-hoalWLoI8r4UszCkZ5kL8vayOGVae1oxXe/2A4AO6J9+580uKHDO3JdHb7NzwwzK5xr/Fs0W40kiNHxM9vyTtQ=="
   crossorigin=""/>

<script src="https://unpkg.com/leaflet\@1.8.0/dist/leaflet.js"
   integrity="sha512-BB3hKbKWOc9Ez/TAwyWxNXeoV9c1v6FIeYiBieIWkpLjauysF18NzgR1MBNBXf8/KABdlkX68nAhlwcDFLGPCQ=="
   crossorigin=""></script>

<style>
  #map { 
   height: $mapheight 
   width: 100%;
  }
</style>

EOF
;

 print "<div id='map'></div>\n";
  
print<<EOF 
 <script>

 var map = L.map('map', { scrollWheelZoom: false }).setView([$lat, $lng], $map_zoom);
  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

var icon_green = L.icon({
    iconUrl: '$varenv->{metahost}/img/$icon_green',
    iconSize: [37, 37],
    iconAnchor: [20, 37],
    popupAnchor: [-2, -36]
    //shadowUrl: 'marker-shadow.png',
    //shadowSize: [68, 95],
    //shadowAnchor: [22, 94]
});
var icon_red = L.icon({
    iconUrl: '$varenv->{metahost}/img/$icon_red',
    iconSize: [37, 37],
    iconAnchor: [20, 37],
    popupAnchor: [-2, -36] 
});

Promise.all([
  fetch(
    "$uri_request/GBFSout?request=stations_available&authcookie=$api_return->{authcookie}"
  ),
  fetch("$uri_request/GBFSout?request=bikes_available&authcookie=$api_return->{authcookie}")
]).then(async ([response1, response2]) => {
  const responseData1 = await response1.json();
  const responseData2 = await response2.json();

  const data1 = responseData1.data.stations;
  const data2 = responseData2.data.vehicles;

 const layerGroup = L.featureGroup().addTo(map);

  data1.forEach(({ lat, lon, name, address, station_id: stationId }) => {
    let bikesonstation = '';
    console.log('Station: ' + stationId , name);

    data2.forEach(({ station_id, vehicle_name, vehicle_id }) => {
     if(typeof station_id !== 'undefined' && station_id == stationId){
        bikesonstation = bikesonstation.concat('&bull; Verfügbar: ' + vehicle_name + ' ' + vehicle_id + '<br/>');
     }
    });
    console.log(bikesonstation);

    //bike_array.forEach((bike, index) => {
        //console.log('xxxx ' + index + ':' + bike);
    //});

    if(bikesonstation){
     layerGroup.addLayer(
      L.marker([lat, lon], { icon:icon_green }).bindPopup(
        `<b>Mietradstation: \${name} \${stationId} </b><br/>
         \${bikesonstation}
        `
      )
     );
    }else{
     layerGroup.addLayer(
      L.marker([lat, lon], { icon:icon_red }).bindPopup(
        `<b>Mietradstation: \${name} \${stationId} </b><br/>
        `
      )
     );

    }
  });

   if('$manlat' && '$manlng'){
     layerGroup.addLayer(
      L.marker([$manlat, $manlng]).bindPopup(
        '$manlat, $manlng'
      )
     );
    }

  map.fitBounds(layerGroup.getBounds());
});

    </script>
EOF
;

print "</div>\n";

#print $q->div({-class=>"content2_legende", -style=>'font-weight:bold;'}, "&bull; Ist ein Lastenrad an einer Mietradstation verfügbar?"),"\n";
print $q->div({-class=>"content2_legende"}, $q->img({-style=>'height:37px;', -src=>"$varenv->{metahost}/img/$icon_green"}), "Lastenrad verfügbar"),"\n";
print $q->div({-class=>"content2_legende"}, $q->img({-style=>'height:37px;', -src=>"$varenv->{metahost}/img/$icon_red"}), "Lastenrad nicht verfügbar"),"\n";
#print $q->div({-class=>"content2_legende"}, "&nbsp;"),"\n";

 print "$initMap|$varenv->{dbname}|$api_return->{authcookie}|$users_sharee->{c_id}" if($users_dms->{u_id} == $dbt->{copri_conf}->{superu_id});

 #print "$uri_request/GBFSout?request=stations_available&authcookie=$api_return->{authcookie}";

}
1;

