package MandantConf;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
#
use strict;
use warnings;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI ':standard';
use Lib::Config;
use Mod::Buttons;
use Mod::Libenz;
use Mod::Libenzdb;
use Mod::DBtank;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
 my $node_meta = shift;
 my $users_dms = shift;
 my $u_group = shift;
 my $return = shift || "";

  my $q = new CGI;
  my $cf = new Config;
  my $lb = new Libenz;
  my $db = new Libenzdb;
  my $dbt = new DBtank;
  my $but = new Buttons;
  my %ib = $but->ibuttons();
  my %varenv = $cf->envonline();
  my $script = $q->script_name();
  my $path_info = $q->path_info();
  my $path = $path_info;
  #with meta_host, 
  if("$varenv{metahost}"){
   $path = "$script" . "$path_info";
   $script="";
  }
  $path =~ s/\/user|\/manager|\/admin//;
  my $lang = "de";
  my $dbh = "";
  my $dbh_primary = $dbt->dbconnect_extern("sharee_primary");
  my $users_dms_primary = { u_id => 0 };
  $users_dms_primary = $dbt->select_users($dbh_primary,$users_dms->{u_id},"and int02 >= 1");

  my $rel = {};
  my $ct = {};
  if($node_meta->{ct_table} eq "contentuser"){
   #Firma alias Kunden-Faktura and Operator-Faktura config
   if(($node_meta->{template_id} =~ /196|201/) && $users_dms_primary->{int02} >= 1){
    $rel = $db->get_rel4tpl($node_meta->{main_id},$lang,"","$node_meta->{template_id}","","",">0");
    $ct = $db->get_content1("contentuser",$rel->{content_id});
   }elsif($node_meta->{template_id} == 197 && $users_dms->{int08} >= 1){
    $rel = $db->get_rel4tpl($node_meta->{main_id},$lang,"","197","","",">0");
    $ct = $db->get_content1("contentuser",$rel->{content_id});
   }else{
    $return = "failure::Abbruch. Keine Zugriffsberechtigung";
   }
  }

  my $tpl = $db->get_tpl($rel->{template_id});
  my @tpl_order = split /,/,$tpl->{tpl_order};
  my $u_name = $dbt->sys_username($dbh,$ct->{owner});
  $ct->{mtime} = $lb->time4de($ct->{mtime},"1");

  print "<div id='Content4conf'>";
  print $q->div({-class=>"copri_header",-style=>"background-color:#cccccc;"},"Pfad: $path");
  print $q->div({-style=>'background-color:silver;height:10px;'},""),"\n";
  print $q->start_form();

  #Content
  print $q->start_table({-border=>'0', -width=>'100%',-align=>'center', -cellpadding=>'3', -cellspacing=>'0'});
  if($return !~ /failure/){
   print $q->Tr();
   if($R::config2edit){
    print "<td class='ct_editline'>";
    print $q->hidden(-name=>'owner',-override=>'1', -value=>"$users_dms->{u_id}");
    print $q->hidden(-name=>'c_id',-override=>'1', -value=>"$ct->{c_id}");
    print $q->hidden(-name=>'rel_id',-override=>'1', -value=>"$rel->{rel_id}");
    print $q->hidden(-name=>'main_id',-override=>'1', -value=>"$rel->{main_id}");
    print $but->singlesubmit7("rel_edit","save_user","$ib{save}");
    print "</td>\n";
   }else{
     print "<td class='ct_editline'>";
     print $q->a({-class=>"editnav",-href=>"$script$path?config2edit=1",-title=>'bearbeiten'}, $q->img({-src=>"$varenv{metahost}/glyphicons/glyphicons-151-edit.png"}));
     print "</td>\n";
   }
   print $q->td({-style=>"background-color:white;padding-right:10px;border-bottom: 1px solid silver;text-align:right;font-size:12px;"}, "$u_name / $ct->{mtime}");
  }
  my $i=0;
  foreach (@tpl_order){
   $i++;
   my ($key,$des,$size,$unit) = split /=/,$_;
   $size = "60" if($key =~ /ct_name|txt|img/ && !$size);
   $ct->{$key} = $q->unescapeHTML("$ct->{$key}");
   $des .= " ($key)" if($users_dms->{u_id} eq $varenv{superu_id});
   my $value = "";
   if($R::config2edit){
      $ct->{$key} = $lb->newline($ct->{$key},"","1");
      if($size eq "area"){
        $value = "<textarea class='etxtarea' name='$key' rows=4 cols=80>$ct->{$key}</textarea>";
      }elsif($size =~ /\w\+\w/){
	my ($a,$b) = split /\+/,$size;
	my $a_checked;
	my $b_checked;
	$a_checked = "checked" if($ct->{$key} eq "$a" || !$ct->{$key});
	$b_checked = "checked" if($ct->{$key} eq "$b");
        $value = "$a <input type='radio' name='$key' value='$a' $a_checked> $b <input type='radio' name='$key' value='$b' $b_checked>";
      }elsif($key =~ /txt/){
          $value = "<input class='etxt' type='text' name='$key' value='$ct->{$key}' size=$size maxlength=200>";
      }elsif($key =~ /int/){
        $value = "<input class='etxt' type='text' name='$key' value='$ct->{$key}' size=6 maxlength=10>";
      }elsif($key =~ /img/){
        $value = "<input class='etxt' type='text' name='$key' value='$ct->{$key}' size=$size maxlength=200>";
      }
   }else{
	$value = $lb->newline($ct->{$key},"","");
   }
   print $q->Tr();
   if($key =~ /ct_name/){
     print $q->td({-class=>'tdescr2'}, ""),"\n";
     print $q->td({-class=>'tdval2'},"$dbt->{operator}->{$varenv{dbname}}->{title} | $dbt->{operator}->{$varenv{dbname}}->{oprefix} | $dbt->{operator}->{$varenv{dbname}}->{database}->{dbname}"),"\n";
   }else{
     print $q->td({-class=>'tdescr2'},"$des"),"\n";
     print $q->td({-class=>'tdval2'}, "$value $unit"),"\n";
   }
  }
  print $q->end_table;
 print $q->end_form; 
 print "</div>";

 return $return;
}
1;
