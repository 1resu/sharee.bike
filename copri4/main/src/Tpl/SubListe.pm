package SubListe;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
#
#uncomment for perl -cw src/Tpl/Liste3.pm
#use lib qw(/var/www/copri4/shareedms-primary/src);
#
use strict;
use warnings;
use POSIX;
use CGI;
use URI::Escape;
use Encode;
use Lib::Config;
use Mod::Buttons;
use Mod::Libenz;
use Mod::Libenzdb;
use Mod::DBtank;
use Mod::APIfunc;
use Storable;
use Data::Dumper;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
 my $node_meta = shift;
 my $users_dms = shift;
 my $u_group = shift;
 my $return = shift || "";

  my $q = new CGI;
  my @keywords = $q->param;
  my $keycount = scalar(@keywords);
  my $time = time;
  my $now_db = strftime("%d.%m.%Y %H:%M:%S",localtime(time));
  my $cf = new Config;
  my $lb = new Libenz;
  my $db = new Libenzdb;
  my $dbt = new DBtank;
  my $apif = new APIfunc;
  my $but = new Buttons;
  my %varenv = $cf->envonline();
  my $path = $q->path_info();
  my $coo = $q->cookie(-name=>'domcookie') || "";
  my $lang = "de";
  my $dbh = "";

  my $parent_node4rel = $db->get_node4rel($node_meta->{parent_id},"","","null");
  my %ib = $but->ibuttons();
  my $opdir_dms = "$dbt->{copri_conf}->{basedir}/$dbt->{operator}->{$varenv{dbname}}->{dir_dms}" || "";   

  my $message = "";
  my $s_owner_id = "";
  #my $s_u_name = "";
  my $searchref = {};
  my $channel_map = $dbt->channel_map();
  my $mapref = {};
  my $ct_users = $dbt->users_map($dbh,$mapref);#get serviceAPP and DMS users from contentadr

  my @_users = ("");
  foreach my $id (sort { $channel_map->{$a} cmp $channel_map->{$b} } keys (%$channel_map)){
     push (@_users, "$id:$channel_map->{$id}");
     if($R::s_owner && $channel_map->{$id} eq $R::s_owner){
        $searchref->{owner} = $id;
	#$s_u_name = $channel_map->{$id};
     }
  }

   foreach my $ctu_id (sort { $ct_users->{$a}->{txt01} cmp $ct_users->{$b}->{txt01} } keys (%$ct_users)){
     push (@_users, "$ct_users->{$ctu_id}->{c_id}:$ct_users->{$ctu_id}->{txt01}");
     if($ct_users->{$ctu_id}->{ct_name} && ($ct_users->{$ctu_id}->{txt01} =~ /$R::s_owner/i) || ($ct_users->{$ctu_id}->{c_id} eq $searchref->{owner})){
        $searchref->{owner} = $ct_users->{$ctu_id}->{c_id};
	#$s_u_name = $ct_users->{$ctu_id}->{txt01};
     }
   }

  my $limit = $R::limit || $varenv{limit};
  my $offset = $R::offset || "0";
  if($node_meta->{int10} && $node_meta->{int10} < $varenv{limit}){
        $offset = 0;
  }
  #backward | forward
    if($R::go eq "backward_list"){
        $offset -= $limit if($offset >= $limit);
    }elsif($R::go eq "forward_list"){
        $offset += $limit;
    }

  my $date = "";
  my $start_chck=0;
  my $end_chck=0;
  my $last_year;
  if($R::s_start_mtime){
    ($date,$start_chck) = $lb->checkdate($R::s_start_mtime) if($R::s_start_mtime !~ "%"); 
    $message .= ">>> Datum Eingabefehler: $date <<<" if($start_chck);
    my ($c_dd,$c_mm,$c_yy) = split(/\./,$date);
    $last_year = $c_yy if("$c_yy" eq "2011");
  }
  if($R::s_end_mtime){
    ($date,$end_chck) = $lb->checkdate($R::s_end_mtime) if($R::s_end_mtime !~ "%");
    $message .= ">>> Datum Eingabefehler: $date <<<" if($end_chck);
    my ($c_dd,$c_mm,$c_yy) = split(/\./,$date);
  }

  #because of search we need parents
  my $main_ids = $parent_node4rel->{main_id};
  my $tpl_ids = $parent_node4rel->{template_id};

  my $table = "content";
  $searchref->{table_pos} = "contentpos";
  if($node_meta->{tpl_id} == 199){
    $table = "contentuser";
    $searchref->{table_pos} = "";
    $searchref->{template_id_pos} = "";
    $main_ids = $node_meta->{main_id};
    $tpl_ids = $node_meta->{tpl_id};
  }
  elsif($node_meta->{tpl_id} >= 400 && $node_meta->{tpl_id} < 500){
    $table = "content";
    $searchref->{table_pos} = "contentpos";
    $searchref->{template_id_pos} = "$node_meta->{tpl_id}";
    $tpl_ids = "205";
    $node_meta->{tpl_order} .= ",txt10=Redistribution" if($node_meta->{tpl_id} != 499);
  }
  elsif($node_meta->{tpl_id} > 600 && $node_meta->{tpl_id} < 700){
    $table = "contentadr";
    $searchref->{table_pos} = "contentadrpos";
    $searchref->{template_id_pos} = "$node_meta->{tpl_id}";
    $main_ids = $node_meta->{main_id};
    $tpl_ids = $node_meta->{tpl_id};
  }
  elsif($node_meta->{tpl_id} == 198){
    my $dbh_primary = $dbt->dbconnect_extern("sharee_primary");
    my $users_dms_primary = { u_id => 0 };
    $users_dms_primary = $dbt->select_users($dbh_primary,$users_dms->{u_id},"and int03=2");

    #only if user is also a primary DMS user with invoice rw
    if(!$users_dms_primary->{u_id} || $users_dms_primary->{int03} != 2){	
      $node_meta->{tpl_order} =~ s/int02=[\w\s\+\&=]+,//;
      $node_meta->{tpl_order} =~ s/int03=[\w\s\+\&=]+,//;
    }

    $table = "contentadr";
    $searchref->{table_pos} = "users";
    #$searchref->{template_id_pos} = "$node_meta->{tpl_id}";
    $tpl_ids = 202;
print<<EOF
    <style>
     .ui-autocomplete {
     text-align:left;
     background:#eeeeee;
     border:1px solid silver;
  }
  </style>

  <script>
  \$(function() {       
      \$('#json_selectadr').autocomplete({
            source: '/ajax_json?mandant_id=$dbt->{shareedms_conf}->{parent_id}&table=contentadr',
            minLength: 2,
            select: function(event, ui) {
                    \$('#vorname_name').val(ui.item.vorname_name);
                    \$('#c_idadr').val(ui.item.c_id);
            }
       });
     

  });
  </script>
EOF
;

  }
  my @tpl_order = split /,/,$node_meta->{tpl_order};
  my $rows = 0;
  my $scol = "mtime";
  #table columne check for col_sort
  if($users_dms->{col_sort}){
   my $tinfo = $db->table_info($table);
   foreach (keys(%$tinfo)){
     $scol = "$users_dms->{col_sort}" if("$users_dms->{col_sort}" eq "$tinfo->{$_}->{attname}");
   }
   $db->users_up("col_sort","0",$users_dms->{owner}) if("$scol" ne "$users_dms->{col_sort}");
  }

  $scol = "change" if($node_meta->{template_id} == 198);

  #print Dumper($node_meta);
  my $hashfile = "$varenv{logdir}/$users_dms->{u_id}-$searchref->{table_pos}-searchhash";
  my $ct4rel = {};
  #print Dumper($node_meta);
  #print "if(!$start_chck && !$end_chck && $main_ids && $tpl_ids){";
  if(!$start_chck && !$end_chck && $main_ids && $tpl_ids){
      #$rows = $db->count_content($searchref->{table_pos},"$main_ids","$tpl_ids");

        #collect search keys. 
        foreach my $postkey (@keywords){
          foreach(@tpl_order){
            my ($key,$val,$size) = split /=/,$_;
            if($postkey =~ /s_$key|s_start_$key|s_end_$key/){
                my $val = $q->param($postkey);
                $postkey =~ s/^s_//; 
                $searchref->{$postkey} = $val; 
            }
          }
        }
        #trying to save hashref
	#if($R::detail_search && ref($searchref) eq "HASH"){
	#  store $searchref, $hashfile;
	#}elsif($keycount > 0 && !$R::detail_search && -f $hashfile && !$R::col_sort && !$R::sort_updown){
	#  $searchref = {};
	#  $searchref = retrieve($hashfile);
	#}
	#print Dumper($searchref);
	my $export = "";
	my $todo = "";
	my $ck4ex = "";
	#only if permission read
	if(($node_meta->{ct_table} eq "users" && $users_dms->{int07} >= 1) || ($node_meta->{ct_table} eq "contentadrpos" && $users_dms->{int02} >= 1) || ($node_meta->{ct_table} eq "contentpos" && $users_dms->{int01} >= 1) || ($node_meta->{ct_table} eq "contentuser" && $users_dms->{int08} >= 1)){
          $ct4rel = $db->search_content3($searchref,$table,$dbt->{shareedms_conf}->{parent_id},$node_meta,$users_dms->{u_id},$lang,"$main_ids","$tpl_ids","","",$time,"",$scol,$users_dms->{sort_updown},$offset,$limit,$export,$todo,$ck4ex,"");
	}else{
	  $return = "failure::Abbruch. Keine Zugriffsberechtigung";
	}
  }

  print "<div id='Content4sublist'>\n";
  my $header_style = "";
  $header_style = "border:2px solid #9f1f0e;" if($message);
  print $q->div({-class=>"copri_header",-style=>"background-color:$node_meta->{bg_color};"},"$path",$q->span({-style=>"$header_style"},"$message"));
  print $q->div({-style=>'background-color:silver;height:10px;'},""),"\n";

  print $q->start_form(-name=>'searchform'),"\n";
  print $q->hidden(-name=>'offset', -value=>"$offset"),"\n";
  print $q->hidden(-name=>'main_id', -value=>"$node_meta->{main_id}"),"\n";
  print $q->hidden(-name=>'mode', -value=>"manager"),"\n";
  print $q->hidden(-name=>'owner', -value=>"$users_dms->{u_id}"),"\n";
  print $q->hidden(-name=>'template_id', -value=>"$node_meta->{template_id}"),"\n";

  my $hstyle = "width:20px;background-color:$node_meta->{bg_color};border-right: solid thin gray;border-bottom: solid thin gray;";
  print $q->start_table({ -style=>'clear:both;', -border=>'0', -width=>'100%',-align=>'center', -cellpadding=>'0', -cellspacing=>'0'}),"\n";

  #new_edit and search (disabled for Statistik, Service-Config and DMS-Account view)
  if($node_meta->{tpl_id} !~ /195|198|199/){
   my $edit="rel_edit";
   my $new_key="service_done";

   my $search = "search";
   print $q->Tr(),"\n";
   print $q->td({-style=>"background-color:silver;$hstyle"},$but->singlesubmit1("detail_search","$search","","")),"\n";
   print $q->td({-style=>"$hstyle"},"&nbsp;"),"\n";

   #1. Search-fields
   my $s_val = "";
   foreach(@tpl_order){
     my ($key,$val,$size) = split /=/,$_;
     $size = 15 if($size =~ /area/);
     $size = 10 if($size =~ /time/);
     $size = 2 if($size =~ /checkbox/);

     #$s_val = $searchref->{$key} if($searchref->{$key});
     #print "$key=$searchref->{$key}|";
     if($key =~ /node|txt|int|ct_name|_id|barcode|sort|public/){
      if($key =~ /_id|barcode|int04/){
      	print $q->td({-class=>"search_line", -style=>'width:100px;'},$q->textfield(-class=>'stxt',-name=>"s_$key",-size=>"4",-default=>"$s_val",-maxlength=>40, -autofocus=>1)),"\n";
      }else{
      	print $q->td({-class=>"search_line"},$q->textfield(-class=>'stxt',-name=>"s_$key",-size=>"$size",-default=>"$s_val",-maxlength=>40)),"\n";
      }
     }elsif($key =~ /owner/){
        print $q->td({-class=>'search_line', -style=>'width:10px;'},$but->selector("s_$key","","$s_val",@_users)),"\n";
     }
     my $s_mtime; my $e_mtime;
     if($key eq "mtime"){
      $s_mtime = $searchref->{start_mtime};
      $e_mtime = $searchref->{end_mtime};
     }
     if($key eq "date_time"){
      $s_mtime = $searchref->{start_date_time};
      $e_mtime = $searchref->{end_date_time};
     }

     print $q->td({-nowrap=>1,-class=>"search_line_date"},$q->textfield(-id=>'datepicker1',-class=>'etxt',-name=>"s_start_$key",-default=>"$s_mtime",-size=>"$size",-maxlength=>10),"-",$q->textfield(-id=>'datepicker2',-class=>'etxt',-name=>"s_end_$key",-default=>"$e_mtime",-size=>"$size", -maxlength=>10)),"\n" if($key =~ /time/);
   }#end Search-fields
  }

  #2. Tableheader
  print $q->Tr();
  my $i=0;
    if($node_meta->{tpl_id} == 198){
      my $edit="base_edit";
      my $new_key="new_dmsusers";

      print "<th style='$hstyle;width:250px;'>\n"; 
      print $but->singlesubmit2glyph("$edit","$new_key","$ib{$new_key}","background-color:$node_meta->{bg_color};"),"\n";
      print $q->hidden(-id=>'c_idadr', -name=>"c_idadr", -override=>'1'),"\n";
      print $q->hidden(-id=>'vorname_name', -name=>"vorname_name", -override=>'1'),"\n";
      print $q->textfield(-style=>'border:1px solid grey;height:25px;width:80%;',-id=>"json_selectadr",-name=>"json_selectadr", -placeholder=>'Neuer DMS-Account', -value=>""),"\n";

      print "</th>\n";
    }elsif($node_meta->{tpl_id} !~ /195|199/){
      #print $q->th({-style=>""},"&nbsp;"),"\n";
      my $sort_up = "up";
      my $sort_down = "down";
      $sort_up = "<b>$sort_up</b>" if($users_dms->{sort_updown} eq "up");
      $sort_down = "<b>$sort_down</b>" if($users_dms->{sort_updown} eq "down");
      print $q->th($q->a({-class=>"sortnav",-href=>"?sort_updown=up\&offset=$offset\&limit=$limit",-title=>'Aufsteigend sortieren'},"$sort_up"),"|",$q->a({-class=>"sortnav",-href=>"?sort_updown=down\&offset=$offset\&limit=$limit",-title=>'Absteigend sortieren'},"$sort_down")),"\n";
      print $q->th({-style=>""},"&nbsp;"),"\n";
    }

  my $j = 0;
  foreach (@tpl_order){
   my ($key,$val,$size,$interval) = split /=/,$_;
   $val .= " ($key)" if($users_dms->{u_id} eq $varenv{superu_id});

   my $divstyle = "";
   if($node_meta->{tpl_id} == 199 && $val =~ /int\d+/){
     $j++;
     my $val_head = $val;
     $val = "Wartung $j";
     $val .= " ($val_head)" if($users_dms->{u_id} eq $varenv{superu_id});
     $divstyle = "width:100px;white-space:nowrap;";
   }
   if($size =~ /checkbox/){
     	$size = 2;
	$divstyle = "width:20px;white-space:nowrap;overflow:hidden;";
   }
   my $sort_title="| $val";

   if($node_meta->{tpl_id} !~ /198|199/){
     $val = "<b>$val</b>" if($key eq $users_dms->{col_sort});
     print $q->th({-style=>'padding:5px 0'},$q->div({-style=>"$divstyle"},$q->a({-class=>"sortnav",-href=>"?col_sort=$key\&offset=$offset\&limit=$limit",-title=>"$val"},"$val"))),"\n" if($key ne "u_id");
   }else{
     print $q->th({-style=>'padding:5px 0'},$q->div({-style=>"$divstyle"},"$val")),"\n" if($key ne "u_id");
   }
  }#end Tableheader

  my $nr=0;
  my $tdclass = "tdtxt";
  my $tdstyle = "text-align:left;";

  #Statistic file view
  if($node_meta->{tpl_id} == 195){
   if( -d "$opdir_dms/csv"){
    my @pdfs = $lb->read_dirfiles("$opdir_dms/csv","\.ZIP","file","");
    @pdfs = reverse(@pdfs);
    foreach (@tpl_order){
      my ($key,$val,$size) = split /=/,$_;
      if($key eq "pdf01"){
       	  foreach(@pdfs){
	 	print $q->Tr(),"\n";
        	print $q->td({-class=>"$tdclass"},$q->a({-class=>'linknav3',-href=>"$varenv{wwwhost}/FileOut?file=$_&sessionid=$coo", -target=>'_default', -title=>'Download',-type=>'application/octet-stream'}, $q->img({-src=>"$varenv{metahost}/glyphicons/file-any.png", -style=>'width:25px;'}), "$_")),"\n";
       	  }
	}
      }
    }
  }
  #BIG LOOP loop content table
  else{
   $users_dms->{sort_updown} = "down" if(!$users_dms->{sort_updown});
   foreach my $id (sort {
    if($users_dms->{sort_updown} eq "down"){
        if ($scol =~ /barcode|int/) {
                $ct4rel->{$b}->{$scol} <=> $ct4rel->{$a}->{$scol}
        }else{
                $ct4rel->{$b}->{$scol} cmp $ct4rel->{$a}->{$scol}
        }
    }else{
        if ($scol =~ /barcode|int/) {
                $ct4rel->{$a}->{$scol} <=> $ct4rel->{$b}->{$scol}
        }else{
                $ct4rel->{$a}->{$scol} cmp $ct4rel->{$b}->{$scol}
        }
    }
  } keys(%$ct4rel)){

   my $set_style = "";
    $nr++;
    #Tablecontent (parameter)
    print $q->Tr(),"\n";

    if($node_meta->{tpl_id} != 198 && $node_meta->{tpl_id} != 199){
      print $q->td({-class=>'tdtxt',-style=>"$set_style"},""),"\n";
      print $q->td({-class=>'tdint',-style=>"$set_style"},""),"\n";
    }
    my $k=0;
    foreach (@tpl_order){
      my ($key,$val,$size) = split /=/,$_;
      $size = 15 if($size =~ /area/);
      $size = 2 if($size =~ /checkbox/);
      $tdclass = "tdtxt";
      $tdstyle = "text-align:left;";
      if($size =~ /\w\+\w/){
	$size = 5;
      }elsif($key =~ /barcode|c_id|ct_name|int|state|sort|public/){
        $tdclass = "tdint";
        $tdstyle = "text-align:right;max-width:8em;$size px;";
      }
      $ct4rel->{$id}->{$key} = $lb->time4de($ct4rel->{$id}->{$key},"1") if($key =~ /time|change/);
      $ct4rel->{$id}->{$key} = $q->unescapeHTML($ct4rel->{$id}->{$key}) if($ct4rel->{$id}->{$key});
      $ct4rel->{$id}->{$key} = $lb->newline($ct4rel->{$id}->{$key},"","") if($ct4rel->{$id}->{$key});
      if($key eq "owner"){
   	my $u_name = $ct4rel->{$id}->{owner};
     	foreach my $ctu_id (keys (%$ct_users)){
          if($channel_map->{$u_name}){
           $u_name = $channel_map->{$u_name};
          }elsif($ct4rel->{$id}->{owner} eq $ct_users->{$ctu_id}->{c_id}){
           $u_name = $ct_users->{$ctu_id}->{txt01};
          }
       }
        #$u_name = $ct_users->{$ct4rel->{$id}->{$key}}->{txt01} || $ct4rel->{$id}->{$key};
        print $q->td({-class=>"$tdclass",-style=>"$tdstyle $set_style"},"$u_name"),"\n";

      }elsif($key eq "ct_name" && $node_meta->{ct_table} eq "contentuser" && $node_meta->{tpl_id} == 199){
	my $spec_style = "";
	$spec_style = "min-width:140px;";
	print $q->td({-class=>"$tdclass",-style=>"$tdstyle $spec_style"},$q->a({-class=>"linknav3",-href=>"?node2edit=editpart\&c_id=$ct4rel->{$id}->{c_id}",-title=>"edit"},"$ct4rel->{$id}->{$key}")),"\n";

      }elsif($key eq "barcode" && $node_meta->{ct_table} eq "contentadrpos"){
	print $q->td({-class=>"$tdclass",-style=>"$tdstyle $set_style"},$q->a({-class=>"linknav3",-href=>"$varenv{wwwhost}/DMS/Waren/?detail_search=1\&s_barcode=$ct4rel->{$id}->{barcode}",-title=>"zum Rad "},"$ct4rel->{$id}->{barcode}")),"\n";
 
      }elsif($key eq "u_id" && $node_meta->{ct_table} eq "users"){
	my $adref = {
          table           => "contentadr",
          fetch           => "one",
          template_id     => "202",
          c_id          => "$ct4rel->{$id}->{$key}",
        };
        my $ctadr = $dbt->fetch_record($dbh,$adref);

	print $q->td({-class=>"$tdclass",-style=>"$tdstyle $set_style width:300px;"},$q->a({-class=>"linknav3",-href=>"?node2edit=editpart\&u_id=$ct4rel->{$id}->{u_id}",-title=>"edit"},"$ctadr->{txt01} ($ct4rel->{$id}->{$key})")),"\n";
      }elsif($key eq "txt08" && $node_meta->{ct_table} eq "contentadrpos"){
	my $subject = "sharee.bike feedback";
	my $body = "Hallo $ct4rel->{$id}->{txt01},\%0A\%0Avielen Dank für Ihre Nachricht \"$ct4rel->{$id}->{txt02}\"";
        print $q->td({-class=>"$tdclass",-style=>"$tdstyle $set_style"},$q->a({-class=>"editnav",-href=>"mailto:$ct4rel->{$id}->{$key}?subject=$subject&body=$body"},$q->span({-class=>"bi bi-envelope"}," $ct4rel->{$id}->{$key}"))),"\n";
      }elsif($key =~ /int0[1-8]/ && $node_meta->{ct_table} eq "users"){
        print $q->td({-class=>"$tdclass",-style=>"$tdstyle $set_style",-nowrap=>1},"$dbt->{copri_conf}->{permission}->{$ct4rel->{$id}->{$key}}"),"\n";
      }elsif($key =~ /int09/ && $node_meta->{ct_table} eq "users"){
        print $q->td({-class=>"$tdclass",-style=>"$tdstyle $set_style",-nowrap=>1},"$dbt->{copri_conf}->{access}->{$ct4rel->{$id}->{$key}}"),"\n";
      }elsif($key =~ /txt\d+/ && $node_meta->{tpl_id} == 199){
	my $tpl_desc = $ct4rel->{$id}->{$key};
	$tpl_desc =~ s/^int\d+=//;
	$tpl_desc =~ s/=checkbox=(\d+)/\<br \/\>$1 Tage Intervall/;
	$tpl_desc =~ s/=1/\<br \/\>Typ weich/;
	$tpl_desc =~ s/=2/\<br \/\>Typ hart/;
	my $spec_style = "";
	$spec_style = "min-width:140px;" if($tpl_desc);
        print $q->td({-class=>"$tdclass",-style=>"$tdstyle $spec_style"},"$tpl_desc"),"\n";
      }else{
        print $q->td({-class=>"$tdclass",-style=>"$tdstyle $set_style"},"$ct4rel->{$id}->{$key}"),"\n";
      }
     }
    }
   }

   print $q->end_table;

   my $offset_nr = $offset + $nr;
   print $q->div({-style=>'float:left;padding:0.5em;'},"Zeile: $offset - $offset_nr");

   #backward | forward
   print "<div style='padding:0.5em;'>\n";
   print $q->a({-class=>"linknav1",-href=>"?go=backward_list;offset=$offset;limit=$limit",-title=>'backward'}," &larr; ") if($offset >= $limit);
   print $q->a({-class=>"linknav1",-href=>"?go=forward_list;offset=$offset;limit=$limit",-title=>'forward'}," &rarr; ") if($nr >= $limit-10); #if($rows > $limit && $nr > 0);
   print "</div>\n";

  print $q->end_form,"\n";
  print "</div>\n";
  my $debug = "(ct_table: $node_meta->{ct_table} | main_id: $node_meta->{main_id}  | template_id: $node_meta->{template_id})";
  print $q->div({-style=>'position:fixed;bottom:1%;font-size:13px;'},"$debug"),"\n" if($users_dms->{u_id} eq $varenv{superu_id});
  
  return $return;
}

1;
