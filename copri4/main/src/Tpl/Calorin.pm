package Calorin;
#
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
use strict;
use warnings;
use POSIX;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI ':standard';
use Scalar::Util qw(looks_like_number);
use Calendar::Simple;
use Date::Calc qw(:all);
use Lib::Config;
use Mod::Buttons;
use Mod::Libenz;
use Mod::Callib;
use Mod::DBtank;
use Mod::APIfunc;
use Mod::Pricing;
use Data::Dumper;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
 my ($node_meta,$users_dms,$u_group,$return) = @_;
 my $q = new CGI;
 my $cf = new Config;
 my $lb = new Libenz;
 my $but = new Buttons;
 my $cb = new Callib;
 my $apif = new APIfunc;
 my $dbt = new DBtank;
 my $pri = new Pricing;

 my $lang = "de";
 my @tpl_order = ('c_id=ID=5=Datensatz ID (nur intern)','date_time=Mietzeit','txt08=Name=15','int06=Start Station=5','int04=End Station=5','barcode=Bike Nr.=5','int12=Flotte=select','int10=Rental State=select','int20=Lock State=select','owner=User=select','txt23=Comment=15');

 my %varenv = $cf->envonline();
 my %ib = $but->ibuttons();
 my $script = $q->script_name();
  my $path_info = $q->path_info();
  my $path = $path_info;
  #with meta_host, 
  if("$varenv{metahost}"){
   $path = "$script" . "$path_info";
   $script="";
  }

 $path =~ s/\/user|\/manager|\/admin//;
 my $now_time = strftime "%Y-%m-%d %H:%M", localtime;

 my $dbh_primary = $dbt->dbconnect_extern("sharee_primary");
 my $users_dms_primary = { u_id => 0 };
 $users_dms_primary = $dbt->select_users($dbh_primary,$users_dms->{u_id},"and int03=2");

 my $table = "contenttrans";
 my $u_name = $q->escapeHTML("$R::u_name");
 my $dbh = "";

  my $channel_map = $dbt->channel_map();
  my $mapref = {};
  my $ct_users = $dbt->users_map($dbh,$mapref);#get serviceAPP and DMS users from contentadr

  my @_users = (":user");
  foreach my $id (sort { $channel_map->{$a} cmp $channel_map->{$b} } keys (%$channel_map)){
     push (@_users, "$id:$channel_map->{$id}");
     if($channel_map->{$id} eq $R::s_owner){
        #$searchref->{owner} = $id;
        #$s_u_name = $channel_map->{$id};
     }
  }

  if(1==1){
   foreach my $ctu_id (sort { $ct_users->{$a}->{txt01} cmp $ct_users->{$b}->{txt01} } keys (%$ct_users)){
     push (@_users, "$ct_users->{$ctu_id}->{c_id}:$ct_users->{$ctu_id}->{txt01}");
     if($ct_users->{$ctu_id}->{ct_name} && ($ct_users->{$ctu_id}->{txt01} =~ /$R::s_owner/i)){
        #$searchref->{owner} = $ct_users->{$ctu_id}->{c_id};
        #$s_u_name = $ct_users->{$ctu_id}->{txt01};
     }
   }
  }

  my $bnode = { template_id => 205,#Leihrad_liste
                 parent_id => 200013,#Waren
                 fetch  => "all",
                 keyfield => "main_id",
                 };
  my $bike_nodes = $dbt->fetch_rel4tpl4nd($dbh,$bnode);

 my $pref_ct = {
     table => "contenttrans",
     fetch => "all",
     keyfield => "c_id",
     template_id => "IN::(209,218)",
 };

 my $ct4rel = { c_id => 0 };
 $ct4rel = $dbt->fetch_record($dbh,$pref_ct);

 my @months = $cb->monthmap();

 my $hh;my $mm;
 my $day = strftime "%d", localtime;
 my $day_today = $day;
 my $mon = strftime "%m", localtime;
 my $mon_today = $mon;
 my $year = strftime "%Y", localtime;
 my $year_today = $year;
 ($year,$mon,$day,$hh,$mm) = $lb->split_date($users_dms->{cal_start}) if($users_dms->{cal_start});

 print $q->end_form;
 print $q->start_form();

 print "<div id='Content4cal'>\n";

  my $start_date_time = $R::start_date_time;
  my $end_date_time = $R::end_date_time; 

  $start_date_time = "01.$mon.$year" if(!$start_date_time);
  

  my $c_date; my $start_chck=0;my $end_chck=0;my $message;
  if($start_date_time){
    ($start_date_time,$start_chck) = $lb->checkdate($start_date_time) if($start_date_time ne "%");
    print $q->div({-style=>'background-color:white;color:red;'},"&rarr; Datum Eingabefehler: $start_date_time <<<") if($end_chck);
  }
  if($end_date_time){
    ($end_date_time,$end_chck) = $lb->checkdate($end_date_time) if($end_date_time ne "%");
    print $q->div({-style=>'background-color:white;color:red;'},"&rarr; Datum Eingabefehler: $end_date_time <<<") if($end_chck);
  }

  if(!$end_date_time){
	  my $days4month = Days_in_Month($year,$mon);
	  $end_date_time = "$days4month.$mon.$year";
	  $end_date_time = $cb->datetime_defaults($end_date_time,"dummy",$lang); 
  }


  my $limit = $R::limit || $varenv{limit} * 2;#max. 160 * 2
  my $offset = $R::offset || "0";
  if($R::detail_search){
	$limit = 10000;
	$offset = 0;
  }
  #backward | forward
    if($R::go eq "backward_list"){
        $offset -= $limit if($offset >= $limit);
    }elsif($R::go eq "forward_list"){
        $offset += $limit;
    }

  my $c_id = $R::cttpos_id || $R::c_id || "";
  my $cttpos = {};
  $R::ct_ct_name =~ s/\#//;
  my $search = {
		table		=> "$node_meta->{ct_table}",
		limit		=> $q->escapeHTML($limit),
		offset		=> $q->escapeHTML($offset),
	};

  if($R::ct_id && looks_like_number($R::ct_id)){
     	$search->{ct_id} = $R::ct_id;
  }elsif($c_id &&  looks_like_number($c_id)){
	$search->{cttpos_id} = $c_id;
  }else{
     	$search->{start_date_time} = "$start_date_time";
     	$search->{end_date_time} = "$end_date_time";
     	$search->{cttpos_id} = $q->escapeHTML("$R::cttpos_id");
	$search->{txt06} = $q->escapeHTML("$R::txt06");
	$search->{txt08} = $q->escapeHTML("$R::txt08");
	$search->{txt23} = $q->escapeHTML("$R::txt23");
	$search->{int04} = $q->escapeHTML("$R::int04");
	$search->{int06} = $q->escapeHTML("$R::int06");
	$search->{int12} = $q->escapeHTML("$R::int12");
	$search->{int13} = $q->escapeHTML("$R::int13");
	$search->{cp_ct_name} = $q->escapeHTML("$R::cp_ct_name");
	$search->{int10} = $q->escapeHTML("$R::int10");
	$search->{int20} = $q->escapeHTML("$R::int20");
	$search->{barcode} = $q->escapeHTML("$R::barcode");
	$search->{ct_txt06} = $q->escapeHTML("$R::ct_txt06");#PLZ
	$search->{ct_ct_name} = $q->escapeHTML("$R::ct_ct_name");
	$search->{owner} = $q->escapeHTML("$R::owner");
  }
  
  if(!$start_chck && !$end_chck){
    if($node_meta->{ct_table} eq "contenttranspos"){
      $cttpos = $dbt->collect_transpos($dbh,$search);
    }elsif($node_meta->{ct_table} eq "contenttheftpos"){
      $cttpos = $dbt->collect_theftpos($dbh,$search);
    }
  }

  my $pref_cc = {
     table => "content",
     fetch => "all",
     keyfield => "c_id",
     template_id => "205",
  };

  my $ct4rel_part = { c_id => 0 };
  $ct4rel_part = $dbt->fetch_record($dbh,$pref_cc);

    print $q->div({-class=>"copri_header",-style=>"background-color:$node_meta->{bg_color};"},"$path", $q->span({-style=>"padding:4px 10px;color:white;"},
  " $months[$mon -1] $year",
    $q->a({-class=>"linknav",-style=>"padding:0 0.5em;",-title=>"Monat zurück",-href=>"?cal_delta_start=0:-1:0"}," &larr; "),
    $q->a({-class=>"linknav",-style=>"padding:0 0.5em;",-title=>"Monat aktuell",-href=>"?cal_today=1"}," &bull; "), 
    $q->a({-class=>"linknav",-style=>"padding:0 0.5em;",-title=>"Monat vorwärts",-href=>"?cal_delta_start=0:1:0"}," &rarr; "),
    "$message"
    )),"\n";

  my $sort_up = "up";
  my $sort_down = "down";
  $users_dms->{cal_sort_updown} = "down" if(!$users_dms->{cal_sort_updown});
  $sort_up = "<b>$sort_up</b>" if($users_dms->{cal_sort_updown} eq "up");
  $sort_down = "<b>$sort_down</b>" if($users_dms->{cal_sort_updown} eq "down");

  print $q->div({-style=>'background-color:silver;height:10px;'},""),"\n";

  my $hstyle = "border-right: solid thin gray;border-bottom: solid thin gray;";
  my $search = "search";
  my $edit="ct_trans";
  my $new_key="new_transdate";

  print $q->start_table({ -style=>"width:100%;", -border=>'0',-align=>'left', -cellpadding=>'1', -cellspacing=>'0'});
  print $q->Tr();
  print $q->td({-style=>"background-color:silver;$hstyle;width:30px;padding:1px 10px;"},$but->singlesubmit1("detail_search","$search")),"\n";

  
  #1. Search-fields
  my $h=3;
  print "<td class='search_line'>\n";
  #print $q->a({-class=>"sortnav",-href=>"?cal_sort_updown=up\&offset=$offset\&limit=$limit",-title=>'Aufsteigend sortieren'},"$sort_up"),"|",$q->a({-class=>"sortnav",-href=>"?cal_sort_updown=down\&offset=$offset\&limit=$limit",-title=>'Absteigend sortieren'},"$sort_down"),"\n";
  foreach(@tpl_order){
    #$h++;
    my ($key,$des,$size) = split /=/,$_;
    my $valxx = $q->param("$_") || "";
    #$valxx = "" if($R::base_edit eq "save_pos" && $key ne "barcode");#empty search fields, select only bike number

    if($key =~ /time/){
      $size="10px";
      print $q->textfield(-id=>'datepicker1',-class=>'etxt',-name=>"start_$key",-override=>'1', -default=>"$start_date_time",-size=>"$size",-maxlength=>20), "-", $q->textfield(-id=>'datepicker2',-class=>'etxt',-name=>"end_$key",-override=>'1',-default=>"$end_date_time",-size=>"$size",-maxlength=>20),"\n";
    }
    elsif($key =~ /owner/){
        print $but->selector_class("$key","eselect","width:80px;",$valxx,@_users),"\n";
    }
    elsif($key =~ /int10/ && "$size" eq "select"){# && $node_meta->{tpl_id} == 205){#bike_state
        my @_lock_valxx = (":$des");
        while (my ($key, $value) = each %{ $dbt->{copri_conf}->{bike_state} }) {
          push @_lock_valxx, "$key:$value";#[1:available] ...
        }
        print $but->selector_class("$key","eselect","",$valxx,@_lock_valxx),"\n";
    }
    elsif($key =~ /int12/ && "$size" eq "select"){# && $node_meta->{tpl_id} == 210){#Flotte bike_group (bikenode.main_id)
          my @_valxx = (":$des");
          foreach my $rid (sort { $bike_nodes->{$a}->{node_name} cmp $bike_nodes->{$b}->{node_name} } keys (%$bike_nodes)){
            push (@_valxx, "$bike_nodes->{$rid}->{main_id}:$bike_nodes->{$rid}->{node_name} ($bike_nodes->{$rid}->{main_id})");
          }
          print $but->selector_class("$key","eselect","width:100px;",$valxx,@_valxx),"\n";
    }
    elsif($key =~ /int20/ && "$size" eq "select"){# && $node_meta->{tpl_id} == 205){#lock_state locked/unlocked
        my @_lock_valxx = (":$des");
        while (my ($key, $value) = each %{ $dbt->{copri_conf}->{lock_state} }) {
          push @_lock_valxx, "$key:$value";#[2:unlocked]
        }
        print $but->selector_class("$key","eselect","width:100px;",$valxx,@_lock_valxx),"\n";
    }
    else{
      $size="9px" if($key =~ /int/);
      if($key =~ /ct_name|barcode/){
        print $q->textfield(-class=>'stxt2',-name=>"$key",-default=>"$valxx",-size=>"$size",-override=>'1',-maxlength=>40, -placeholder=>"$des",-autofocus=>1),"\n";
      }else{
        print $q->textfield(-class=>'stxt2',-name=>"$key",-default=>"$valxx",-size=>"$size",-override=>'1', -maxlength=>40, -placeholder=>"$des"),"\n";
      }
    }
  }
  print "</td>\n";


  my ($daymarker,$raster_mmpx,$day4month) = $lb->month_line($users_dms);

  print $q->Tr();
  print $q->td({-class=>'element',-style=>"height:1.5em;padding:0;border:0px solid green;",-colspan=>"$h",-nowrap=>"1"},"$day4month"),"\n";
  ###
  
  ###Calendar Content
  my $nx=0;
  my $set_style="";
  my $scale_color = $node_meta->{bg_color};
  my $edit="ct_trans";
  my $save_key="save";
  my $delete_key="delete";
  my $itime="";
  my $start_time="";
  my $end_time="";
  my ($year_st,$mon_st,$day_st,$hh_st,$mm_st);
  my ($year_en,$mon_en,$day_en,$hh_en,$mm_en);

  my $nr=0; 
  my $i= $offset || 0; 
  my $c_left = "14px;";
  foreach my $pid (sort { $cttpos->{$b}->{c_id} <=> $cttpos->{$a}->{c_id} } keys(%$cttpos)){

      $nr++;
      $nx++;
      $i++;
      $c_left = "7px;" if($i >= 10);
      $c_left = "0px;" if($i >= 100);
      $set_style = "background-color:#fcfdfb;";
      $set_style = "background-color:#f4f1ee;" if($nx %= 2);
      $set_style = "background-color:#b6dbe3;" if($R::c_id && $R::c_id == $cttpos->{$pid}->{c_id});

      my $trans_style = "padding:0 5px;border: 2px solid #f7ae37;";
      my $stamm_style = "padding:0 5px;border: 2px solid #98c13b;";
      my $part_style = "padding:0 5px;border: 2px solid #dcd77f;";
   
      my ($ct_name,$ct_txt00,$ct_txt01,$ct_phone,$c_id4trans,$tpl_id4trans,$u_name);
	my $id = $cttpos->{$pid}->{ct_id};
	if($ct4rel->{$id}->{c_id} == $cttpos->{$pid}->{ct_id}){
	  $ct_name = $ct4rel->{$id}->{ct_name};
	  $ct_txt00 = $ct4rel->{$id}->{txt00};
	  $ct_txt01 = $ct4rel->{$id}->{txt01};
	  $ct_phone = $ct4rel->{$id}->{txt07};
	  $c_id4trans = $ct4rel->{$id}->{c_id};
	  $tpl_id4trans = $ct4rel->{$id}->{template_id};
	  $trans_style .= "background-color:#f7ae37;" if($ct4rel->{$id}->{c_id} == $users_dms->{c_id4trans});
	}
      if($cttpos->{$pid}->{start_time}){
        ($year_st,$mon_st,$day_st,$hh_st,$mm_st) = $lb->split_date($cttpos->{$pid}->{start_time});
      }else{
        ($year_st,$mon_st,$day_st,$hh_st,$mm_st) = $lb->split_date($cttpos->{$pid}->{itime});
      }
      if($cttpos->{$pid}->{end_time}){
        ($year_en,$mon_en,$day_en,$hh_en,$mm_en) = $lb->split_date($cttpos->{$pid}->{end_time});
      }else{
        ($year_en,$mon_en,$day_en,$hh_en,$mm_en) = $lb->split_date($cttpos->{$pid}->{itime});
      }

      #if($year_st && $mon_st && $day_st && $hh_st && $mm_st && $year_en && $mon_en && $day_en && $hh_en && $mm_en){
      if(1==1){

	#generate px for rent scale
	my $start_nr = $year_st . $mon_st . $day_st . $hh_st . $mm_st;
	my $end_nr = $year_en . $mon_en . $day_en . $hh_en . $mm_en;
	my $day_stpx = 0;
	my $rent_day_px = 0;
	my $time_style="";;
	if($start_nr <= $end_nr){
          ($day_stpx,$rent_day_px) = $lb->rent_scale($users_dms,$year_st,$mon_st,$day_st,$hh_st,$mm_st,$year_en,$mon_en,$day_en,$hh_en,$mm_en);
	}else{
	  $time_style="color:red;";
	}

	$itime = $lb->time4de($cttpos->{$pid}->{itime},1);
	if($cttpos->{$pid}->{start_time}){
	   $start_time = $lb->time4de($cttpos->{$pid}->{start_time},1);
	   $end_time = $lb->time4de($cttpos->{$pid}->{end_time},1);
	}else{
	   $start_time = $lb->time4de($cttpos->{$pid}->{itime},1);
	   $end_time = $lb->time4de($cttpos->{$pid}->{itime},1);
	}

		my $u_name = $cttpos->{$pid}->{owner} || "";
    		my $u_name_end = $cttpos->{$pid}->{owner_end} || "";
     		foreach my $ctu_id (keys (%$ct_users)){
       		  if($u_name && $channel_map->{$u_name}){
        		$u_name = $channel_map->{$u_name};
       		  }elsif($cttpos->{$pid}->{owner} eq $ct_users->{$ctu_id}->{c_id}){
        		$u_name = $ct_users->{$ctu_id}->{txt01};
       		  }
       		  if($u_name_end && $channel_map->{$u_name_end}){
        		$u_name_end = $channel_map->{$u_name_end};
       		  }elsif($cttpos->{$pid}->{owner_end} eq $ct_users->{$ctu_id}->{c_id}){
        		$u_name_end = $ct_users->{$ctu_id}->{txt01};
       		  }
     		 }
		 $u_name_end = "-" if(!$u_name_end);


	#if($ct_name){
	if(1==1){
	  print "<Tr><td class='element' colspan='$h' style='$set_style;0.91em;padding:0.3em 0 0 0em;'>\n";

	    my $pos_hash = $cttpos->{$pid};
	    my $pos_details = "";
	    foreach my $did (sort keys (%{$pos_hash})){
	    	    $pos_details .= $did . " = " . $pos_hash->{$did} . "</br>" if($pos_hash->{$did});
	    }
	    my $pos_id = "ID $cttpos->{$pid}->{c_id}";
	    my $user_device = "";
	    #if($users_dms->{u_id} == $dbt->{copri_conf}->{bike_state}->{superu_id}){
	    if($users_dms->{u_id} && $users_dms->{u_id} =~ /1842|5781|11765|21618|38883/){
	      $pos_id = $q->div({-class=>"popup",-onclick=>"toggle_box('$pid')"},"$cttpos->{$pid}->{c_id}", $q->span({-class=>"popuptext",-id=>"$pid"},"$pos_details"));
	      $user_device = "";
	      $user_device .= " &rarr; lock charge $cttpos->{$pid}->{int14} %" if($cttpos->{$pid}->{int14});
	      my $txt26 = $cttpos->{$pid}->{txt26};
	      $txt26 = substr($cttpos->{$pid}->{txt26},0,25) if(length($cttpos->{$pid}->{txt26}) > 20);
	      $user_device .= " &rarr; $txt26" if($txt26);
	      my $txt21 = $cttpos->{$pid}->{txt21};
	      $txt21 = substr($cttpos->{$pid}->{txt21},0,50) . " ..." if(length($cttpos->{$pid}->{txt21}) > 50);
	      #$user_device .= " &rarr; $txt21" if($txt21);

	    }

	    my $bikenr = $cttpos->{$pid}->{barcode} || $cttpos->{$pid}->{ct_name};

	    my $status = "$dbt->{copri_conf}->{bike_state}->{$cttpos->{$pid}->{int10}}" || "state failure";
	    $status = "<span style='color:#c63e3e;'>$dbt->{copri_conf}->{bike_state}->{$cttpos->{$pid}->{int10}}</span>" if($cttpos->{$pid}->{int10} == 2 || $cttpos->{$pid}->{int10} == 3);
	    if($cttpos->{$pid}->{int10} ne $ct4rel_part->{$cttpos->{$pid}->{cc_id}}->{int10}){
		    $status = "<span style='border: 1px solid #c63e3e;'>$dbt->{copri_conf}->{bike_state}->{$cttpos->{$pid}->{int10}}</span>";
	    }
	    my $lock_state = "lock failure" if(!$cttpos->{$pid}->{int20});
	    $lock_state = "locked" if($cttpos->{$pid}->{int20} == 1);
	    $lock_state = "<span style='color:#c63e3e;'>unlocked</span>" if($cttpos->{$pid}->{int20} == 2);
	    $lock_state = "<span style='color:#c63e3e;'>locking</span>" if($cttpos->{$pid}->{int20} == 3);
	    $lock_state = "<span style='color:#c63e3e;'>unlocking</span>" if($cttpos->{$pid}->{int20} == 4);
	    if($cttpos->{$pid}->{int20} ne $ct4rel_part->{$cttpos->{$pid}->{cc_id}}->{int20}){
		    $lock_state = "<span style='border: 1px solid #c63e3e;'>$dbt->{copri_conf}->{lock_state}->{$cttpos->{$pid}->{int20}}</span>";
	    }


	    my $track_info = "";
	    my $co2saving = "";
	    if($cttpos->{$pid}->{int26}){
		$co2saving = $pri->co2calc($cttpos->{$pid});
		$cttpos->{$pid}->{int26} =~ s/\./,/;
	      	$track_info = "&rarr; - $co2saving kg CO&sup2; ($cttpos->{$pid}->{int26} km)";
	    }
            	
	    my $edit_pos = $q->a({-class=>"editboo",-href=>"?base_edit=transpos\&c_id=$cttpos->{$pid}->{c_id}\&owner=$users_dms->{u_id}",-title=>"Position bearbeiten ($cttpos->{$pid}->{c_id})"},$q->span({-class=>"bi bi-file-earmark-text-fill"}));
            $bikenr = $q->a({-class=>"linknav3",-style=>"$stamm_style",-href=>"/DMS/Waren/?detail_search=1&s_barcode=$cttpos->{$pid}->{barcode}",-title=>"Rad im Warenstamm"},"$bikenr") if($cttpos->{$pid}->{cc_id});

	    if($node_meta->{ct_table} eq "contenttranspos"){
	    	my $start_station = "$cttpos->{$pid}->{int06}";
	    	my $end_station = "$cttpos->{$pid}->{int04}";
	    	$start_station = $q->a({-class=>"linknav3",-style=>"",-href=>"/DMS/Waren/?detail_search=1&s_int04=$cttpos->{$pid}->{int06}",-title=>"Rad Warenstamm nach Station filtern"},"$cttpos->{$pid}->{int06}") if($ct4rel_part->{$cttpos->{$pid}->{cc_id}}->{rel_id});
	    	$end_station = $q->a({-class=>"linknav3",-style=>"",-href=>"/DMS/Waren/?detail_search=1&s_int04=$cttpos->{$pid}->{int04}",-title=>"Rad Warenstamm nach Station filtern"},"$cttpos->{$pid}->{int04}") if($ct4rel_part->{$cttpos->{$pid}->{cc_id}}->{rel_id});

  		my $comment = $cttpos->{$pid}->{txt23};
	        $comment = substr($cttpos->{$pid}->{txt23},0,50) . " ..." if(length($cttpos->{$pid}->{txt23}) > 50);

		my $kunde = "$cttpos->{$pid}->{txt08}";
		if($users_dms_primary->{u_id} && $users_dms_primary->{int03} == 2){
            	  $kunde = $q->a({-class=>"linknav3",-style=>"$part_style",-href=>"/DMS/Kunden/?detail_search=1&s_c_id=$cttpos->{$pid}->{ca_id}",-title=>"Kunde im Kundenstamm"},"$cttpos->{$pid}->{txt08} ($cttpos->{$pid}->{ca_id})");#2021-05-24 saves kd name
		}


		#Gebühren und Gutscheine
		if($cttpos->{$pid}->{template_id} && $cttpos->{$pid}->{template_id} =~ /224|229/){
            	  print $q->div({-style=>"float:left;margin-left:$c_left"}, "$i) $edit_pos $pos_id &rarr; $start_time &rarr; Nr. $bikenr &rarr; $kunde &rarr; $u_name/$u_name_end"),"\n";
		}else{
		  print $q->div({-style=>"float:left;margin-left:$c_left"}, "$i) $edit_pos $pos_id &rarr; <span style='$time_style'>$start_time – $end_time</span> &rarr; $kunde &rarr; Start Station $start_station &rarr; End Station $end_station &rarr; Bike $bikenr $status $lock_state $track_info &rarr; $u_name/$u_name_end"),"\n";
		  #print $q->div({-style=>"float:left;margin-left:$c_left"}, "$i) $edit_pos $pos_id &rarr; Start $start_time Station $start_station &rarr; End $end_time Station $end_station &rarr; Bike $bikenr $status $lock_state $track_info &rarr; $kunde &rarr; $u_name/$u_name_end"),"\n";
		}

		if($users_dms_primary->{u_id} && $users_dms_primary->{int03} == 2){
	    	  print $q->div({-style=>'float:left;margin-left:1em;'}, "&rarr; Faktura", $q->a({-class=>"linknav3",-style=>"$trans_style",-href=>"/DMS/Faktura?ct_trans=open\&c_id4trans=$c_id4trans\&tpl_id4trans=$tpl_id4trans\&kind_of_trans=Faktura\&owner=$users_dms->{owner}",-title=>"Faktura Terminal öffnen"},"\#$ct_name")),"\n" if($c_id4trans && $tpl_id4trans); 
		  print $q->div({-style=>'float:left;margin-left:1em;'}, "$user_device"),"\n"; 
		}
		print $q->div({-style=>'float:left;margin-left:1em;'}, " &rarr; $comment"),"\n" if($comment); 

    	   }elsif($node_meta->{ct_table} eq "contenttheftpos"){
		my $speed = 0;
	       	if($cttpos->{$pid}->{int07} && $cttpos->{$pid}->{int07} > 0){
		  $speed = $cttpos->{$pid}->{int07} * 1.852;
		  $speed = $lb->round_half($speed);
		}
		my $event_type = "";
		my $meter = $cttpos->{$pid}->{int08};
		$meter =~ s/\./\,/;
		$event_type = "Diebstahlalarm" if($cttpos->{$pid}->{int01});
 		if($cttpos->{$pid}->{int02} && $cttpos->{$pid}->{txt06}){
		  my $gps2card = $q->a({-class=>"linknav3",-href=>"/DMS/Karte?man_gps=$cttpos->{$pid}->{txt06}\&owner=$users_dms->{owner}",-title=>"Karte öffnen"}, "$cttpos->{$pid}->{txt06}");
		  $event_type = "Alarm movement &rarr; GPS $gps2card &rarr; speed $speed km/h &rarr; distance $meter m ";
		}
            	print $q->div({-style=>"float:left;margin-left:$c_left"}, "$i) $pos_id &rarr; $end_time &rarr; Bike $bikenr &rarr; $event_type &rarr; $u_name"),"\n";

	   }

          print $q->div({-style=>"position:absolute;margin-left:$daymarker;border-right: solid thin #009899;height:1.5em;"},"&nbsp;"),"\n" if("$mon" eq "$mon_today");
          print $q->div({-style=>"position:static;margin-left:$day_stpx;width:$rent_day_px;height:1.5em;background-color:$scale_color;"},"&nbsp;"),"\n";
	  print "</td></Tr>\n";
	}
      }
     }

  print $q->end_table;
  ###

 print "</div>\n";

   my $offset_nr = $offset + $nr;

   #backward | forward
   print $q->div({-style=>'float:left;padding:0.5em;'}, "Zeile: $offset - $offset_nr"),"\n";
   print "<div style='float:left;padding:0.5em;'>\n";
   print $q->a({-class=>"linknav1",-href=>"?go=backward_list;offset=$offset;limit=$limit",-title=>'backward'}," &larr; ") if($offset >= $limit);
   print $q->a({-class=>"linknav1",-href=>"?go=forward_list;offset=$offset;limit=$limit",-title=>'forward'}," &rarr;") if($nr >= $limit-10);
   print "</div>\n";

 print $q->end_form;
}
1;

