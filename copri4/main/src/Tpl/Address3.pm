package Address3;
#
##
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
use strict;
use warnings;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI ':standard';
use Lib::Config;
use Mod::Buttons;
use Mod::Libenz;
use Mod::Libenzdb;
use Mod::DBtank;
use Tpl::TransPositionen;
use Tpl::TransInvoices;
use Data::Dumper;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
  my $node_meta = shift;
  my $users_dms = shift;
  my $return = shift || "";

  my $q = new CGI;
  my $cf = new Config;
  my $lb = new Libenz;
  my $db = new Libenzdb;
  my $dbt = new DBtank;
  my $but = new Buttons;
  my $transp = new TransPositionen;
  my $trin = new TransInvoices;
  my %varenv = $cf->envonline();
  my $lang = "de";
  my %ib = $but->ibuttons();
  my $line_count1 = 0;
  my $line_count2 = 0;
  my $dbh = "";

  #Kunden Faktura ex Firma
  #my $ctf = $db->get_content1("contentuser",$dbt->{shareedms_conf}->{parent_id});
  my $ctf = { c_id => 0 };
  my $pref_cu = {
    table => "contentuser",
    fetch => "one",
    c_id  => $dbt->{shareedms_conf}->{parent_id},
  };
  $ctf = $dbt->fetch_tablerecord($dbh,$pref_cu);

  my $ctt = { c_id => 0 };
  my $pref = {
        table           => "contenttrans",
        fetch           => "one",
	c_id		=> $users_dms->{c_id4trans},
  };
  $ctt = $dbt->fetch_record($dbh,$pref);

  my $ctadr = { c_id => 0 };
  my $rel_adr = { rel_id => 0 };
  my $c_idadr = $ctt->{int10} || "";#c_id orig from contentadr

  if($node_meta->{tpl_id} == 208){
    $ctadr = $db->get_content1("contentuser","2");
  }else{
    $ctadr = $db->get_content1("contentadr",$c_idadr);
    $rel_adr = $db->get_rel4tpl("","$lang","$c_idadr","202");
  }

  if(("$R::trans2edit" eq "client" && $R::c_idadr) || ($R::ct_trans eq "add_transadr")){
	$ctadr = $db->get_content1("contentadr",$R::c_idadr);
	$c_idadr = $ctadr->{c_id};
	$ctt->{txt01} = $ctadr->{txt01};
	$ctt->{txt03} = $ctadr->{txt03};
	$ctt->{txt06} = $ctadr->{txt06};
	$ctt->{txt07} = $ctadr->{txt07};
	$ctt->{txt08} = $ctadr->{txt08};
  }

print<<EOF
    <style>
     .ui-autocomplete {
     text-align:left;
     background:#eeeeee;
     border:1px solid silver;
  }
  </style>

  <script>
  \$(function() {       
      \$('#json_selectadr').autocomplete({
            source: '/ajax_json?main_id=$dbt->{shareedms_conf}->{parent_id}&table=contentadr',
            minLength: 2,
            select: function(event, ui) {
                    \$('#vorname_name').val(ui.item.vorname_name);
                    \$('#c_idadr').val(ui.item.c_id);
            }
       });
     

  });
  </script>
EOF
;
 
  my $main_id = $ctt->{main_id};
  my $set_main_id = $dbt->{shareedms_conf}->{invoice};


  print "\n<div style='position:absolute;text-align:left;background-color:white;width:100%;'>\n";
  if(!$ctt->{c_id}){
    $lb->failure3("Das Formular wurde gelöscht bzw. ist nicht vorhanden");   
  }
  
  #######Verkauf Header
  print $q->start_form(),"\n";
  print $q->hidden(-name=>'printer_id', -override=>'1', -value=>"PDF");
  print "\n<div style='background-color:#a7a18f;height:25px;text-align:left;width:100%;' nowrap=1>\n";
    #submit Header

    my $invoice_time = "";
    if($ctt->{invoice_time}){#since 2020-03-16 will be set on Printpreview
      $invoice_time = $lb->time4de($ctt->{invoice_time},1);
    }else{
      $invoice_time = $lb->time4de($ctt->{mtime},1);
    }

    my $channel_map = $dbt->channel_map();
    my $buchen_users = { txt01 => "" };
    if($channel_map->{$ctt->{owner}}){
      $buchen_users = { txt01 => "$channel_map->{$ctt->{owner}}" };
    }else{
      $buchen_users = $db->get_content1("contentadr",$ctt->{owner});
    }

    $set_main_id=$main_id if($ctt->{main_id} > "300000");
    print $q->hidden(-name=>'set_main_id', -override=>'1', -value=>"$set_main_id");
    print $q->span({-style=>'margin:0em 0.5em 0 0.5em;padding:0.5em 3em;background-color:white;border:solid thin gray;position:absolute;left:3px;'}, "$ctt->{txt00}");
    
    print "<span style='margin:0 0.2em;position:absolute;left:180px;'>\n";
    print $q->b({-style=>'padding:0 1em;'}, "\# $ctt->{ct_name}"), "\n";
    if($ctt->{ct_name} =~ /\d+/ && $ctt->{txt00} eq "Storno"){
      print $but->singlesubmit3("ct_trans","set_workflow2invoice","","",""),"\n";
      print $q->hidden(-name=>'set_main_id4workflow', -override=>'1', -value=>"300008"), "\n";
    }
    if($ctt->{ct_name} =~ /\d+/ && $ctt->{txt00} eq "Rechnung" && $ctt->{int10} != 2){#only for client invoice
      print $but->singlesubmit3("ct_trans","set_workflow2storno","","",""), "\n";
      print $q->hidden(-name=>'set_main_id4workflow', -override=>'1', -value=>"300009"), "\n";
    }
    if($ctt->{ct_name} =~ /\d+/ && $ctt->{txt00} eq "Abrechnung"){
      print $but->singlesubmit3("ct_trans","set_accounting2invoice","","",""), "\n";
      print $q->hidden(-name=>'set_main_id4workflow', -override=>'1', -value=>"300008"), "\n";
    }

    print "</span>\n";

    if(!$ctt->{int01} || $ctt->{ct_name} =~ /-/){
      print $q->span({-style=>'margin:0 0.2em;position:absolute;right:3px;'}, $but->singlesubmit3("ct_trans","print_pdf","","","")," $buchen_users->{txt01} / $invoice_time ", $but->singlesubmit7("ct_trans","remove_chk4rel","$ib{remove_chk4rel}","")),"\n";
      print $q->hidden(-name=>'c_id', -override=>'1', -value=>"$ctt->{content_id}");
      print $q->hidden(-name=>'template_id', -override=>'1', -value=>"$ctt->{template_id}");
      print $q->hidden(-name=>'main_id', -override=>'1', -value=>"$ctt->{main_id}");
      print $q->hidden(-name=>'rel_id', -override=>'1', -value=>"$ctt->{rel_id}");
    }else{
       print $q->span({-style=>'margin:0 0.2em;position:absolute;right:3px;'}, $but->singlesubmit3("ct_trans","print_pdf","","","")," $buchen_users->{txt01} / $invoice_time "),"\n";
    } 
  print "</div>\n"; 

  print $q->hidden(-name=>'owner', -override=>'1', -value=>"$users_dms->{owner}");
  print $q->hidden(-name=>'c_idadr', -override=>'1', -value=>"$c_idadr");
  print $q->hidden(-name=>'offset', -override=>'1', -value=>"$R::offset");
  print $q->hidden(-name=>'limit', -override=>'1', -value=>"$R::limit");
  print $q->hidden(-name=>'relids', -override=>'1', -value=>"$R::relids");
  print $q->hidden(-name=>'ct_name4workflow', -override=>1, -value=>"$ctt->{ct_name}");
  print $q->hidden(-name=>'c_id4trans', -override=>'1', -value=>"$ctt->{c_id}");
  print $q->hidden(-name=>'tpl_id4trans', -override=>'1', -value=>"$ctt->{template_id}");
  print $q->hidden(-name=>'close_time', -override=>'1', -value=>"$ctt->{close_time}");

  print $q->end_form,"\n";
  print $q->div({-style=>'position:fixed;bottom:2%;right:1%;z-index:10;font-size:13px;'}," (c_id: $ctt->{c_id} | rel_id: $ctt->{rel_id} | tpl_id: $ctt->{tpl_id})"),"\n" if($users_dms->{u_id} eq $varenv{superu_id});
  ##########
  

  #Form for AdressData
  print $q->start_form(-name=>'addressform'),"\n";
  #Big table
  print $q->start_table({-class=>'list', -border=>'0', -width=>'100%',-align=>'left', -cellpadding=>'3', -cellspacing=>'0'}),"\n";


  print "<tr><td width='50%' style='font-size:1em;'>\n"; 
  print $q->start_table({-class=>'list', -height=>'10em',-border=>'0', -width=>'100%',-align=>'left', -cellpadding=>'0', -cellspacing=>'0'}),"\n";
  ###Content #Edit Address --> template=Adressenliste
  $ctt->{txt01} = $q->unescapeHTML("$ctt->{txt01}");
  my $int05 = "";
  $int05 = "(manuell)" if($ctt->{int05});

  if($users_dms->{u_id} && ($R::trans2edit eq "client" || $R::ct_trans eq "add_transadr")){
    #my @_anrede = ("","Frau","Herr","Firma");
    print $q->hidden(-name=>'c_id4trans', -override=>'1', -value=>"$ctt->{c_id}"),"\n";
    print $q->hidden(-name=>'int10', -override=>'1', -value=>"$c_idadr"),"\n";
    print $q->hidden(-name=>'c_idadr', -override=>'1', -value=>"$c_idadr"),"\n";
    print $q->hidden(-name=>'rel_id', -override=>'1', -value=>"$ctt->{rel_id}"),"\n";

    print $q->Tr(),"\n";  
    print "<td class='tdescr4' nowrap>";
    print $but->singlesubmit7("ct_trans","save_adr","$ib{save_adr}","","","ebutton"),"\n";
    #print $q->span($q->a({-class=>"ebutton3",-href=>'javascript:history.back()'}, " back "));
    print "</td>\n";
    print $q->td({-class=>'tdval4',-colspan=>'2'},"Kunden ID $c_idadr"),"\n"; 
    $ctt->{txt01} = $lb->newline($ctt->{txt01},"","1");
    print $q->Tr(),"\n"; 
    print $q->td({-class=>'tdescr4'},"Vorname Name<br />Zusatz"),"\n";
    print $q->td({-class=>'tdval4',-colspan=>'2'},$q->textarea(-class=>'autos',-style=>'border:1px solid #ededed;background-color: #ededed;', -name=>'txt01', -default=>"$ctt->{txt01}", -rows=>1, -columns=>38)),"\n";
    print $q->Tr(),"\n";   
    print $q->td({-class=>'tdescr4'},"Straße Nr."),"\n";
    print $q->td({-class=>'tdval4',-colspan=>'2',-nowrap=>1}, $q->textfield(-class=>'etxt',-name=>'txt03', -default=>"$ctt->{txt03}", -size=>'34', maxlength=>'45')),"\n";
    print $q->Tr(),"\n"; 
    print $q->td({-class=>'tdescr4'},"PLZ Ort"),"\n";
    print $q->td({-class=>'tdval4',-colspan=>'2'}, $q->textfield(-class=>'etxt',-name=>'txt06', -default=>"$ctt->{txt06}", -size=>'34', maxlength=>'45')),"\n";
    print $q->Tr(),"\n"; 
    print $q->td({-class=>'tdescr4'},"eMail"),"\n";
    print $q->td({-class=>'tdval4',-colspan=>'2'}, $q->textfield(-class=>'etxt',-name=>'txt08', -default=>"$ctt->{txt08}", -size=>'34', maxlength=>'45')),"\n";
    print $q->Tr(),"\n";
    print $q->td({-class=>'tdescr4'},"Telefon"),"\n";
    print $q->td({-class=>'tdval4',-colspan=>'2'}, $q->textfield(-class=>'etxt',-name=>'txt07', -default=>"$ctt->{txt07}", -size=>'34', maxlength=>'40')),"\n";
    print $q->Tr(),"\n";
    print $q->td({-class=>'tdescr4'},"Leistungsdatum"),"\n";
    print $q->td({-class=>'tdval4',-colspan=>'2'}, $q->textfield(-class=>'etxt',-name=>'txt20', -default=>"$ctt->{txt20}", -size=>'34', maxlength=>'60'),$but->checkbox("1","int05","$ctt->{int05}"),"manuell"),"\n";
    print $q->hidden(-name=>"int05",-override=>1,-value=>"");

  }elsif($users_dms->{u_id}){
    print $q->Tr(),"\n";  
    if(!$ctt->{close_time} && (!$ctt->{int10} || $ctt->{int10} != 2)){
      print "<td class='tdescr4' style='width:8em;' nowrap>"; 
      print $but->singlesubmit2glyph("trans2edit","client","Kunden bearbeiten","background-color:white;"),"\n";
      print $q->hidden(-name=>'c_id4trans', -override=>'1', -value=>"$ctt->{c_id}"),"\n";
      #from json_selectadr
      print $q->hidden(-id=>'c_idadr', -name=>"c_idadr", -override=>'1'),"\n";
      print $q->hidden(-id=>'vorname_name', -name=>"vorname_name", -override=>'1'),"\n";
      print $q->textfield(-style=>'border:1px solid silver;vertical-align: top;',-id=>"json_selectadr",-name=>"json_selectadr", -value=>""),"\n";
      print "</td>\n";
    }else{
      print $q->td({-class=>'tdescr4'},"&nbsp;"),"\n";
    }

     #print $q->td({-class=>'tdval4'},"$ctt->{txt02}"),"\n";
     if($c_idadr && $rel_adr->{rel_id}){
      my $vde = "";
      $vde = " Vde $ctadr->{int12}" if($ctadr->{int12});
      print $q->td({-class=>'tdval4',-colspan=>'2'},$q->span({-style=>"background-color:#dcd77f;"},$q->a({-class=>"linknav",-href=>"/DMS/Kunden?node2edit=editpart\&mode=manager\&rel_id=$rel_adr->{rel_id}\&tpl_id=202",-title=>"Kunden Stammdaten öffnen"}," Kunden ID $c_idadr ")), $q->span({-style=>"color:red;padding-left:10px;"}," $vde")),"\n"; 
     }
     $ctt->{txt01} = $lb->newline($ctt->{txt01},"","");
     print $q->Tr(),"\n"; $line_count1++;   
     print $q->td({-class=>'tdescr4'},"&nbsp;"),"\n";
     print $q->td({-class=>'tdval4',-colspan=>2},"$ctt->{txt01}"),"\n"; 
     print $q->Tr(),"\n"; $line_count1++;  
     print $q->td({-class=>'tdescr4'},"&nbsp;"),"\n";
     print $q->td({-class=>'tdval4',-colspan=>2},"$ctt->{txt03}"),"\n"; 
     print $q->Tr(),"\n"; $line_count1++;  
     print $q->td({-class=>'tdescr4'},"&nbsp;"),"\n";
     print $q->td({-class=>'tdval4',-colspan=>2},"$ctt->{txt06}"),"\n"; 
     print $q->Tr(),"\n"; $line_count1++;  
     print $q->td({-class=>'tdescr4'},"&nbsp;"),"\n";
     print $q->td({-class=>'tdval4',-colspan=>2},"$ctt->{txt08}"),"\n"; 
     print $q->Tr(),"\n";
     print $q->td({-class=>'tdescr4'},"&nbsp;"),"\n";
     print $q->td({-class=>'tdval4',-colspan=>2},"$ctt->{txt07}"),"\n";
     print $q->Tr(),"\n";$line_count1++;
     print $q->td({-class=>'tdescr4'},"&nbsp;"),"\n";
     print $q->td({-class=>'tdval4',-colspan=>2},"$ctt->{txt20} $int05"),"\n";
  }
  print $q->end_table;
  print "</td>\n";

  print "<td width='50%' style='font-size:1em;'>\n"; 
  print $q->start_table({-class=>'list', -border=>'0', -width=>'100%',-align=>'left', -cellpadding=>'0', -cellspacing=>'0'}),"\n";

  #if operator accounting/invoice
  if($node_meta->{tpl_id} == 208 || $ctt->{int10} == 2){
    my $tplop = $dbt->get_tpl($dbh,"196");#Operator-Faktura
    my @tplop_order = split /,/,$tplop->{tpl_order};
    foreach(@tplop_order){
     my ($key,$val,$size,$unit) = split /=/,$_;
     if($key =~ /int(\d+)/){
	#take fee values used by creating operator accounting invoice
	my $count_key = $1 + 20;#cu.int to ctt.int
	my $ctt_key = "int" . $count_key;
	$ctt->{$ctt_key} =~ s/\./,/;
       	print $q->Tr(),"\n";
       	print $q->td({-class=>'tdescr4', -style=>'width:50%;'},"$val"),"\n";
	if($ctt->{$ctt_key}){
	  $ctt->{$ctt_key} =~ s/\./,/;
       	  print $q->td({-class=>'tdval4'},"$ctt->{$ctt_key} $unit"),"\n";
	}else{
       	  print $q->td({-class=>'tdval4'},""),"\n";
	}
     }
    }
  }else{
    my $pay_sequence = " | sequencenr: $ctt->{int18}" if($ctt->{int18});
    my @_paymentstate = split(/\|/,$dbt->{shareedms_conf}->{payment_state});
    my $kind_of_payment = "fehlt";
    $kind_of_payment = "$_paymentstate[0]" if($ctadr->{int03} == 1);
    $kind_of_payment = "$_paymentstate[1]" if($ctadr->{int03} == 2);
    print $q->Tr(),"\n";
    print $q->td({-class=>'tdescr4'},"Payone Zahlungsart"),"\n";
    print $q->td({-class=>'tdval4'},"$kind_of_payment | aus Kunden Stammdaten"),"\n";
    print $q->Tr(),"\n";
    print $q->td({-class=>'tdescr4'},"Payone TXID"),"\n";
    print $q->td({-class=>'tdval4'},"$ctt->{txt16}"),"\n";
    print $q->Tr(),"\n";
    print $q->td({-class=>'tdescr4'},"Payone Saldo"),"\n";
    print $q->td({-class=>'tdval4'},"$ctt->{int16} $pay_sequence"),"\n";
    print $q->Tr(),"\n";
    print $q->td({-class=>'tdescr4'},"Payone Referenz"),"\n";
    print $q->td({-class=>'tdval4'},"$ctt->{txt25}"),"\n";

    #check payone status
    if($ctt->{txt28} =~ /error/i){
     print $q->Tr(),"\n";
     print $q->td({-class=>'tdescr4'},"payone error"),"\n";
     print $q->td({-class=>'tdval4'},"(transaction) $ctt->{txt28}"),"\n";
    }
  }
  print $q->end_table;
  print "</td></tr>\n"; 
  
  ###end
  print $q->end_form,"\n";
  

  ###Content #Edit Parts
  print "<tr><td colspan='5' style='font-size:1em;'>\n"; 

   #operator invoices else position accounting
   if($ctt->{template_id} == 208 || $ctt->{int10} == 2){
     $line_count2 = $trin->tpl($node_meta,$users_dms,$set_main_id,$ctt,$return);
   }else{
     $line_count2 = $transp->tpl($node_meta,$users_dms,$set_main_id,$ctt,$return);
   }

  print "</td></tr>\n"; 
  ###end Edit Parts 

  #Text & Vorbelegungen
  my $tplf = $db->get_tpl("201");#Firma tpl
  my @tplf_order = split /,/,$tplf->{tpl_order};


  print $q->start_form(),"\n";
  print $q->hidden(-name=>'c_id4trans', -override=>'1', -value=>"$ctt->{c_id}");
  print $q->hidden(-name=>'set_main_id', -override=>'1', -value=>"$set_main_id"),"\n";

  #Text save area
  print "<tr><td colspan='5' style='font-size:1.1em;padding-top:1em;'>\n"; 
  print $q->start_table({-class=>'list', -border=>'0', -width=>'100%',-align=>'left', -cellpadding=>'0', -cellspacing=>'0'}),"\n";


  print $q->Tr(),"\n"; 
  print $q->td({-class=>'tdval5',-colspan=>"2"},$q->span({-style=>'font-weight:bold;'},"Internas und Bearbeitungstatus")),"\n";
  print $q->td({-class=>'tdval5',-colspan=>"2"},$q->span({-style=>'font-weight:bold;'},"PDF Formular Texte "),$q->span({-style=>'color:silver;font-weight:normal;'}," ( für den Kunden sichtbar )")),"\n";
  if($dbt->{shareedms_conf}->{order_state}){
      print $q->Tr(),"\n";  
      my @_orderstate = split(/\|/,$dbt->{shareedms_conf}->{order_state});
      print $q->td({-class=>'tdval5',-colspan=>2},$but->selector("txt22","180px",$ctt->{txt22},@_orderstate)),"\n";
  }
  $ctt->{txt23} = $q->unescapeHTML("$ctt->{txt23}") if($ctt->{txt23});
  print $q->Tr(),"\n";
  print $q->td({-class=>'tdval5',-colspan=>'2'},$q->textarea(-class=>'etxt',-name=>'txt23', -default=>"$ctt->{txt23}", -rows=>6, -columns=>65)),"\n";

  $ctt->{txt12} = $q->unescapeHTML($ctt->{txt12}) || "";
  print $q->td({-class=>'tdval5',-colspan=>'2'},$q->textarea(-class=>'etxt',-name=>'txt12', -default=>"$ctt->{txt12}", -rows=>6, -columns=>65)),"\n";

  print $q->Tr(),"\n"; $line_count1++; 
  my @line_txt12 = split(/\n/,$ctt->{txt12});
  $line_count1 += scalar(@line_txt12);

  #Vorbelegung-Text
  my $formular_text = "";
  my $set_text_id;  
  #at first empty checkbox definition (txt21='txt61,txt62,...')
  print $q->hidden(-name=>"off_txt21",-override=>1,-value=>"");
  foreach(@tplf_order){
    my ($key,$des,$size) = split /=/,$_;
    if($key =~ /txt6/ && $ctf->{$key}){
      $ctf->{$key} = $q->unescapeHTML("$ctf->{$key}"); 
      my $substrtxt = $lb->sub4txt($ctf->{$key},"0","14");
      my $check="";
      $check=1 if($ctt->{txt21} && $ctt->{txt21} =~ /$key/);
      $formular_text .= " [ $key $substrtxt..." . $but->checkbox("$key","$key","$check","$ctf->{$key}") . " ]  ";
      if($ctt->{txt21} && $ctt->{txt21} =~ /$key/){
        my @line_key = split(/\n/,$ctf->{$key});
        $line_count1 += scalar(@line_key);
        $ctf->{$key} = $lb->newline($ctf->{$key},"","");
        print $q->Tr(); $line_count1++;
        print $q->td({-class=>'tdval5',-colspan=>2},"$ctf->{$key}"),"\n";
      }
    }
  }
  ###

  print $q->Tr(),"\n";
  if($ctt->{close_time}){
    print $q->td({-class=>'tdval5',-colspan=>2}, $but->singlesubmit7("ct_trans","save_text_internas","$ib{save_text}","","","ebutton"),"$formular_text"),"\n";
  }else{
    print $q->td({-class=>'tdval5',-colspan=>2}, $but->singlesubmit7("ct_trans","save_text","$ib{save_text}","","","ebutton"),"$formular_text"),"\n";
  }
  print $q->end_table,"\n";
  print "</td></tr>\n"; 

  print $q->hidden(-name=>'c_id4trans', -override=>'1', -value=>"$ctt->{c_id}"),"\n";
  print $q->hidden(-name=>'set_main_id', -override=>'1', -value=>"$set_main_id"),"\n";

  print $q->end_form,"\n";

 print $q->end_table,"\n";
 ###end Big
 
 if($ctt->{barcode}){
  my $pref_rel = {
   table     => "contenttrans",
   fetch     => "all",
   keyfield  => "c_id",
   barcode   => $ctt->{barcode},
   c_id      => "!=::$ctt->{c_id}",
  };
  my $ctt_rel = "";
  $ctt_rel = $dbt->fetch_record($dbh,$pref_rel);

  if(ref($ctt_rel) eq "HASH"){
   print "<div style='padding: 0 10px 15px 5px;'>Relationen\n";
   foreach my $id (sort { $ctt_rel->{$b}->{c_id} <=> $ctt_rel->{$a}->{c_id} } keys (%$ctt_rel)){
        print $q->span({-style=>"background-color:#f7ae37"}, $q->a({-class=>"linknav",-href=>"/$dbt->{shareedms_conf}->{parent_node}/Faktura/$ctt_rel->{$id}->{node_name}?ct_trans=open\&c_id4trans=$ctt_rel->{$id}->{c_id}\&tpl_id4trans=$ctt_rel->{$id}->{template_id}\&owner=$users_dms->{u_id}",-title=>"Faktura Terminal öffnen"},"[ $ctt_rel->{$id}->{txt00}  #$ctt_rel->{$id}->{ct_name} ]")),"\n";
   }
   print "</div>\n";
  }
 }

 $line_count2 = "0" . "$line_count2" if($line_count2 < 10); 
 my $line_count = "$line_count1.$line_count2";
 #print $q->div({style=>'font-size:0.81em;'},"line to print -> address+text: $line_count1 | table: $line_count2"),"\n";
 print "</div>\n";
 $db->update_content4change2("contenttrans",$ctt->{c_id},"$line_count","int04") if($ctt->{c_id});
 return $line_count;
}
1;
