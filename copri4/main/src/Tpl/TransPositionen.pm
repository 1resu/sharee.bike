package TransPositionen;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
#
use strict;
use warnings;
use POSIX;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI ':standard';
use DateTime;
use DateTime::Format::Pg;
use Date::Calc::Object qw(:ALL);
use Scalar::Util qw(looks_like_number);
use Data::Dumper;
use Lib::Config;
use Mod::Buttons;
use Mod::Libenz;
use Mod::Libenzdb;
use Mod::DBtank;
use Mod::APIfunc;
use Mod::Pricing;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
  my $self = shift;
  my $node_meta = shift;
  my $users_dms = shift;
  my $set_main_id = shift;
  my $ctt = shift;
  my $return = shift || "";

  my $q = new CGI;
  my $cf = new Config;
  my $lb = new Libenz;
  my $db = new Libenzdb;
  my $dbt = new DBtank;
  my $apif = new APIfunc;
  my $but = new Buttons;
  my $pri = new Pricing;
  my %varenv = $cf->envonline();
  my %ib = $but->ibuttons();
  my $today = strftime "%d.%m.%Y",localtime;
  my $now_dt = strftime "%Y-%m-%d %H:%M:%S", localtime;
  my $channel_map = $dbt->channel_map();
  my $coo = $q->cookie(-name=>'domcookie') || "";
  my $dbh = "";

  my $mapref = {};
  my $ct_users = $dbt->users_map($dbh,$mapref);#get serviceAPP and DMS users from contentadr
  my $line_count2 = 0; 
  my $tc=0; 
  my $txt20 = $R::txt20 || $ctt->{txt20} || "";#Leistungsdatum
  my $int05 = $R::int05 || $ctt->{int05} || "";#manuell
  my $rows = 0;
  my $spart_ct_name = $R::spart_ct_name || "";
  my $c_idpos = $R::c_idpos || $R::pos_id || "";
  my $cttpos = { c_id => 0 };

  my $ctf = { c_id => 0 };
  my $pref_cu = {
    table => "contentuser",
    fetch => "one",
    c_id  => $dbt->{shareedms_conf}->{parent_id},
  };
  $ctf = $dbt->fetch_tablerecord($dbh,$pref_cu);

  my $ctadr = $db->get_content1("contentadr",$ctt->{int10});
  my $buchen_mtime = $lb->time4de($ctt->{mtime});
  my $vibuchen_mtime = "";
  $vibuchen_mtime = "payone post " . $lb->time4de($ctt->{pay_time},1) . " . " if($ctt->{pay_time});

  my @tpl_order = ("txt01=Beschreibung","ct_name=Nummer","date_time=timerange","int03=Menge (Miet - Gratis Zeit)","int02=Preis","int07=Rabatt","int04=Gesamt");
  my $tplf = $db->get_tpl("201");#Kunden-Faktura, ex Firma
  my @tplf_order = split /,/,$tplf->{tpl_order};

  ($cttpos,$rows) = $dbt->collect_contentpos($dbh,"contenttrans",$ctt->{content_id});

  foreach my $id (keys(%$cttpos)){
   if(ref($cttpos->{$id}) eq "HASH"){ 
    if($cttpos->{$id}->{int26}){
      @tpl_order = ("txt01=Beschreibung","ct_name=Nummer","date_time=timerange","int26=Einsparung","int03=Menge (Miet - Gratis Zeit)","int02=Preis","int07=Rabatt","int04=Gesamt");
    }
    #enhanced tariff
    #if($cttpos->{$id}->{int35}){
    #  @tpl_order = ("txt01=Beschreibung","ct_name=Nummer","date_time=timerange","int26=Einsparung","int35=Mietzeit Menge","int03=Artikel Menge","int02=Preis","int07=Rabatt","int04=Gesamt");
    #}
   }else{
      $lb->failure3("Der selekt einer Verkaufsposition ist fehlgeschlagen, errocode ($ctt->{content_id} | $ctt->{template_id} | $users_dms->{c_id4trans}). Bitte admin kontaktieren");
      exit;
   }
  }


print<<EOF
    <style>
     .ui-autocomplete {
     text-align:left;
     font-size:14px;
     background:#eeeeee;
     border:1px solid silver;
  }
  </style>

  <script>
  \$(function() {       
      \$('#json_select').autocomplete({
            source: '/ajax_json?main_id=$dbt->{shareedms_conf}->{parent_id}&table=content',
            minLength: 2,
	    select: function(event, ui) {
                    \$('#spart_ct_name').val(ui.item.spart_ct_name);
                    \$('#c_id').val(ui.item.c_id);
            }
       });
  });
  </script>
EOF
;


  print $q->start_table({-class=>'list', -border=>'0', -width=>'100%',-align=>'left', -cellpadding=>'3', -cellspacing=>'0'});

  #Parts Header
  print $q->start_form(-name=>'spartform');

  $c_idpos = $1 if($return && $return =~ /pos_id=(\d+)/);
  print $q->hidden(-name=>'trans2edit', -value=>"transpos", -override=>'1'),"\n";
  print $q->hidden(-name=>'offset', -override=>'1', -value=>"$R::offset"),"\n" if($R::offset);
  print $q->hidden(-name=>'limit', -override=>'1', -value=>"$R::limit"),"\n" if($R::limit);
  print $q->hidden(-name=>'c_id4trans', -override=>'1', -value=>"$ctt->{content_id}"),"\n";
  print $q->hidden(-name=>'tpl_id4trans', -override=>'1', -value=>"$ctt->{template_id}"),"\n";
  print $q->hidden(-id=>'c_id', -name=>"c_id", -override=>'1'),"\n";
  print $q->hidden(-id=>'spart_ct_name', -name=>"spart_ct_name", -override=>'1'),"\n";

  $line_count2++; 
  print $q->Tr(); 
  print $q->th($but->singlesubmit("select_part","*")),"\n";
  foreach (@tpl_order){
   my ($key,$val) = split /=/,$_;
   $tc++ if($val);
   if($key eq "ct_name"){
     print $q->th($q->textfield(-class=>'etxt',-style=>'height:19px;width:140px;font-size:1.3em;text-align:right;',-id=>"json_select",-name=>"json_select",-value=>"", -override=>'1',-size=>"25",-maxlength=>50, -placeholder=>'Nummer'),""),"\n";
   }elsif($key =~ /int|txt|time\d+/){
     print $q->th("$val"),"\n";
   }
  }

  print $q->end_form,"\n";

  print $q->start_form(-name=>'transposform'),"\n";

  #Tablecontent (buttons and ct_name(primary key))
  #my $scol = "c_id";#changed to itime because of Storno resorts
  my $scol = "itime";
  my $sum_parts19=0;
  my $diff19 = 100 + 19;
  my $sum_umst19=0;
  my $i=0;
  my $accounting_start = "";
  my $accounting_end = "";

  #foreach my $id (sort { $cttpos->{$b}->{$scol} <=> $cttpos->{$a}->{$scol} } keys(%$cttpos)){
  foreach my $id (sort { $cttpos->{$b}->{$scol} cmp $cttpos->{$a}->{$scol} } keys(%$cttpos)){

   my $gesamt = 0;
   my $pricing = {};
   my $counting = {};
   my $rental_feed = {};

   if($cttpos->{$id}->{int35} && $cttpos->{$id}->{start_time} && $cttpos->{$id}->{end_time}){
     $cttpos->{$id}->{end_time} = $now_dt if($cttpos->{$id}->{int10} == 3);
     ($pricing,$counting) = $pri->counting_rental(\%varenv,$cttpos->{$id},"calc_price");
     $rental_feed = $pri->fetch_rentalfeed(\%varenv,$cttpos->{$id},$counting);
     $sum_parts19 += $pricing->{total_price};
     $gesamt = $pri->round($pricing->{total_price});
     $gesamt = sprintf('%.2f', $gesamt);
   }else{
     ($gesamt,my $rabatt) = $pri->price2calc($cttpos->{$id});
     $sum_parts19 += $gesamt;
     $gesamt = $pri->round($gesamt);
     $gesamt = sprintf('%.2f', $gesamt);
   }

   my $set_style="";
   my $occupied_style = "";
   my $time_style = "";
   #$occupied_style = "color:#ff1493" if($cttpos->{$id}->{txt10} =~ /occupied|requested|canceled/);
   $occupied_style = "color:#ff1493" if($cttpos->{$id}->{int10} == 2 ||$cttpos->{$id}->{int10} == 3 || $cttpos->{$id}->{int10} == 6);

   #max. Rechnungspositionen
   if(1==1){
    $i++;
    if($i==1){
    	$accounting_end = "$3.$2.$1" if($cttpos->{$id}->{itime} =~ /(\d+)\-(\d+)\-(\d+)/);
    	$accounting_end = "$3.$2.$1" if($cttpos->{$id}->{end_time} =~ /(\d+)\-(\d+)\-(\d+)/);
	$accounting_start = $accounting_end;
    }else{
    	$accounting_start = "$3.$2.$1" if($cttpos->{$id}->{itime} =~ /(\d+)\-(\d+)\-(\d+)/);
    }
    #print "$accounting_start - $accounting_end<br>";

    my @line_txt01 = split(/\n/,$cttpos->{$id}->{txt01});
    if($cttpos->{$id}->{int02} != 0){
      $line_count2++;
      #$line_count2 += scalar(@line_txt01);
    }

    #1. Spalte
    print $q->Tr(),"\n";
    print "<td class='tdtxt' nowrap>","\n";
    if(($c_idpos == $cttpos->{$id}->{c_id}) && ($R::trans2edit && $R::trans2edit =~ /transpos/)){
        print $q->hidden(-name=>'c_idpos', -value=>"$cttpos->{$id}->{c_id}", -override=>'1'),"\n";
        print $q->hidden(-name=>'cc_id', -value=>"$cttpos->{$id}->{cc_id}", -override=>'1'),"\n";
        print $q->hidden(-name=>'c_id4trans', -override=>'1', -value=>"$ctt->{content_id}"),"\n";
        print $q->hidden(-name=>'tpl_id4trans', -override=>'1', -value=>"$ctt->{template_id}"),"\n";
	print $but->singlesubmit2("ct_trans","save_pos","$ib{save_pos}","background-color:white;padding:20px;","")," ", 
	$but->singlesubmit2("ct_trans","delete_pos","$ib{delete_pos}","background-color:white;padding:20px;",""),"\n";
     }elsif(!$ctt->{close_time}){
        print $q->a({-class=>"editnav",-href=>"/DMS/Faktura?trans2edit=transpos\&c_idpos=$cttpos->{$id}->{c_id}\&c_id4trans=$ctt->{content_id}\&tpl_id4trans=$ctt->{template_id}\&owner=$users_dms->{u_id}",-title=>"Datensatz bearbeiten"}, $q->img({-src=>"$varenv{metahost}/glyphicons/glyphicons-151-edit.png"})),"\n";
    }
    my $calpath = "Mietjournal";
    print $q->a({-class=>"linknav3", -style=>"background-color:$varenv{calendar_active_color}",-href=>"/DMS/$calpath/?cttpos_id=$cttpos->{$id}->{c_id}",-title=>"Im $calpath öffnen"},"<br /><br />ID $cttpos->{$id}->{c_id}"),"\n";
    print "</td>\n";

    #Tablecontent (parameter)
    foreach (@tpl_order){
      my ($key,$val,$inputsize) = split /=/,$_;     
      $cttpos->{$id}->{$key} = $q->unescapeHTML($cttpos->{$id}->{$key});
      $cttpos->{$id}->{$key} = $lb->newline($cttpos->{$id}->{$key},"",$R::trans2edit) if($R::trans2edit);
      my $ct_pos = "$cttpos->{$id}->{ct_name}";
      my $txtstyle = "text-align:left;min-width:130px;";
      my $isize = "30";
      $isize = $inputsize if($inputsize);
      if($key =~ /int\d+/){
       $txtstyle = "text-align:right;min-width:50px;";
       $isize = "5";
      }

      #edit position
      if(($c_idpos == $cttpos->{$id}->{c_id}) && ($R::trans2edit && $R::trans2edit =~ /transpos/)){
	if($key =~ /ct_name/){
   	 print $q->td({-class=>'element',-style=>"$set_style text-align:right;"}, $q->textfield(-class=>'etxt',-style=>"text-align:right;min-width:120px;",-name=>"ct_name",-default=>"$ct_pos", -override=>'1',-size=>10,-readonly=>'1')),"\n";
       }elsif($key eq "int26"){
            my $co2saving = "";
            if($cttpos->{$id}->{int26}){
              $co2saving = "Einsparung</br>";
              my $co2diff = $pri->co2calc($cttpos->{$id});
	      #my $sprit_price = $pri->sprit2calc($cttpos->{$id});
              $co2saving .= "$co2diff kg CO&sup2;<br />";
	      #$co2saving .= "$sprit_price EUR<br />" if($sprit_price !~ /-/);
              $cttpos->{$id}->{int26} =~ s/\./,/;
              $co2saving .= "bei $cttpos->{$id}->{int26} KM";
            }
            print $q->td({-class=>'tdint'},"$co2saving"),"\n";

	}elsif($key =~ /int03/){
          if($cttpos->{$id}->{int35} && $cttpos->{$id}->{start_time} && $cttpos->{$id}->{end_time}){
            print $q->td({-class=>'tdint', -nowrap=>1},"$pricing->{real_clock} $pricing->{freed_time}"),"\n";
          }else{
	    print $q->td({-colspan=>'1',-class=>'element',-style=>"$txtstyle $set_style"},$q->textfield(-class=>'etxt',-style=>"$txtstyle",-name=>"col_$key",-default=>"$cttpos->{$id}->{$key}", -override=>'1',-size=>"$isize",-maxlength=>10)),"\n";
	  }
	}elsif($key =~ /int02/){
	  if($cttpos->{$id}->{int35} && $cttpos->{$id}->{start_time} && $cttpos->{$id}->{end_time}){
	    print "<td class='tdint' nowrap>\n";
	    foreach my $fid (sort keys(%{ $rental_feed->{rental_description}->{tarif_elements} })){
	      if(ref($rental_feed->{rental_description}->{tarif_elements}->{$fid}) eq "ARRAY"){
		print "$rental_feed->{rental_description}->{tarif_elements}->{$fid}[0]: $rental_feed->{rental_description}->{tarif_elements}->{$fid}[1]<br />\n";
	      }
	    }
	    print "</td>\n";
	  }else{
	    print $q->td({-class=>'element',-style=>"$txtstyle $set_style"},$q->textfield(-class=>'etxt',-style=>"$txtstyle",-name=>"col_$key",-default=>"$cttpos->{$id}->{$key}", -override=>'1',-size=>"$isize",-maxlength=>100)),"\n";
	  }
	}elsif($key =~ /int07/){
	  print $q->td({-class=>'element',-style=>"$txtstyle $set_style",-nowrap=>'1'},$q->textfield(-class=>'etxt',-style=>"$txtstyle",-name=>"col_$key",-default=>"$cttpos->{$id}->{$key}", -override=>'1',-size=>"$isize",-maxlength=>100),$but->selector("int08","40px","$cttpos->{$id}->{int08}",("0.00:%","1.00:€"))),"\n";
	}elsif($key =~ /int04/){
         print $q->td({-class=>'tdint',-nowrap=>"1"},"$gesamt €"),"\n";
 	}elsif($key =~ /txt01/){
 	 if($cttpos->{$id}->{int09} && $cttpos->{$id}->{$key} !~ /Manuell/){
	   $cttpos->{$id}->{$key} .= "\nManuell bearbeitet\n";
	 }
         
	 my ($s_yy,$s_mo,$s_dd,$s_hh,$s_mi) = $lb->split_date($cttpos->{$id}->{start_time});
         my ($e_yy,$e_mo,$e_dd,$e_hh,$e_mi) = $lb->split_date($cttpos->{$id}->{end_time});

	 print "<td class='element' style='$txtstyle $set_style;background-color: #ededed;$occupied_style;'>\n";
	 print $q->textarea(-class=>'autos',-style=>"border: 1px solid #ededed;background-color: #ededed;",-name=>"col_$key",-default=>"$cttpos->{$id}->{$key}", -override=>'1',-rows=>"1",-cols=>65, -autofocus=>1),"<br />\n";

	 #if Traiff Nr.
	 if($cttpos->{$id}->{int09}){
	    print $q->span({-style=>"$txtstyle $set_style"},
	    "Tarif Nr.: ", $q->textfield(-class=>'etxt',-name=>"int09",-default=>"$cttpos->{$id}->{int09}",-size=>"5",-maxlength=>5),
	    "Tarif Text", $q->textfield(-class=>'etxt',-name=>"txt04",-default=>"$cttpos->{$id}->{txt04}",-size=>"30",-maxlength=>50)),"<br />\n";

	   print $q->span({-style=>"$txtstyle $set_style"},
	   "Endstation: ", $q->textfield(-class=>'etxt',-name=>"int04",-default=>"$cttpos->{$id}->{int04}",-size=>"5",-maxlength=>40),
	   "GPS: ", $q->textfield(-class=>'etxt',-name=>"txt06",-default=>"$cttpos->{$id}->{txt06}",-size=>"30",-maxlength=>40)),"<br />\n";

	   print $q->span({-style=>"$txtstyle $set_style"},"Mietzeit: ", 
	   $q->textfield(-id=>'datepicker1',-class=>'etxt',-name=>"start_date",-default=>"$s_dd.$s_mo.$s_yy",-size=>"8",-maxlength=>10),
	   $q->textfield(-class=>'etxt',-name=>"s_hh",-default=>"$s_hh",-size=>"1",-maxlength=>2),":",
	   $q->textfield(-class=>'etxt',-name=>"s_mi",-default=>"$s_mi",-size=>"1",-maxlength=>2)," &rarr; ",
	   $q->textfield(-id=>'datepicker2',-class=>'etxt',-name=>"end_date",-default=>"$e_dd.$e_mo.$e_yy",-size=>"8",-maxlength=>10),
	   $q->textfield(-class=>'etxt',-name=>"e_hh",-default=>"$e_hh",-size=>"1",-maxlength=>2),":",
	   $q->textfield(-class=>'etxt',-name=>"e_mi",-default=>"$e_mi",-size=>"1",-maxlength=>2)),"\n";
	 }

	 print "</td>\n"; 
 	}elsif($key =~ /txt/){
	  print $q->td({-class=>'element',-style=>"$txtstyle $set_style"},$q->textfield(-class=>'etxt',-style=>"$txtstyle",-name=>"col_$key",-default=>"$cttpos->{$id}->{$key}", -override=>'1',-size=>"$isize",-maxlength=>100)),"\n";
	}elsif($key =~ /int/){
	  print $q->td({-class=>'tdint',-nowrap=>"1"},"&nbsp;"),"\n";
	}
      }
      #end edit position 
      #
      #start view position
      else{
        if($key =~ /ct_name/){
	  #print $q->td({-class=>'tdint',-style=>"min-width:60px;"},"$ct_pos");
	  my $stamm_style = "background-color:#98c13b;padding:2px;";
	  my $article = $cttpos->{$id}->{ct_name};
	  if($cttpos->{$id}->{int09}){
	    print $q->td({-class=>'tdint',-style=>"min-width:60px;padding-top:5px;"}, $q->a({-class=>"linknav3",-style=>"$stamm_style",-href=>"/DMS/Waren/?detail_search=1&s_barcode=$cttpos->{$id}->{barcode}",-title=>"Im Warenstamm"},"$article")),"\n";
	  }else{
	    print $q->td({-class=>'tdint'},"$article"),"\n";
	  }
       }elsif($key eq "int26"){
            my $co2saving = "";
            if($cttpos->{$id}->{int26}){
              $co2saving = "Einsparung</br>";
              my $co2diff = $pri->co2calc($cttpos->{$id});
	      #my $sprit_price = $pri->sprit2calc($cttpos->{$id});
              $co2saving .= "$co2diff kg CO&sup2;<br />";
	      #$co2saving .= "$sprit_price EUR<br />";
              $cttpos->{$id}->{int26} =~ s/\./,/;
              $co2saving .= "bei $cttpos->{$id}->{int26} KM";
            }
            print $q->td({-class=>'tdint'},"$co2saving"),"\n";

        }
	#yes, int03=Menge on parts, int35=unit_price1 on rental
	elsif($key =~ /int03/){
	  if($cttpos->{$id}->{int35} && $cttpos->{$id}->{start_time} && $cttpos->{$id}->{end_time}){
	    print $q->td({-class=>'tdint',-nowrap=>1},"$pricing->{real_clock} $pricing->{freed_time}"),"\n";
  	  }else{
	    $cttpos->{$id}->{$key} =~ s/\./,/;
	    print $q->td({-class=>'tdint'},"$cttpos->{$id}->{$key}"),"\n";
	  }
	}elsif($key =~ /int02/){
	  if($cttpos->{$id}->{int35} && $cttpos->{$id}->{start_time} && $cttpos->{$id}->{end_time}){
	    print "<td class='tdint'>\n";
	    foreach my $fid (sort keys(%{ $rental_feed->{rental_description}->{tarif_elements} })){
	      if(ref($rental_feed->{rental_description}->{tarif_elements}->{$fid}) eq "ARRAY"){
		print "$rental_feed->{rental_description}->{tarif_elements}->{$fid}[0]: $rental_feed->{rental_description}->{tarif_elements}->{$fid}[1]<br />\n";
	      }
	    }
	    print "</td>\n";
	  }else{
	    $cttpos->{$id}->{$key} =~ s/\./,/;
	    print $q->td({-class=>'tdint'},"$cttpos->{$id}->{$key} €"),"\n";
	  }
	}elsif($key =~ /int04/){
	  $gesamt =~ s/\./,/;
	  print $q->td({-class=>'tdint',-nowrap=>1},"$gesamt €"),"\n";
        }elsif($key =~ /int07/){
  	  my $proz="";
	  $proz = "%" if($cttpos->{$id}->{$key} && $cttpos->{$id}->{$key} != 0);
	  $proz = "€" if($cttpos->{$id}->{$key} && $cttpos->{$id}->{$key} != 0 && $cttpos->{$id}->{int08} && $cttpos->{$id}->{int08} == 1);
	  $cttpos->{$id}->{$key} =~ s/\./,/;
	  print $q->td({-class=>'tdint',-nowrap=>"1"},"$cttpos->{$id}->{$key} $proz"),"\n";
	}elsif($key =~ /txt01/){
	 $cttpos->{$id}->{$key} = $q->unescapeHTML("$cttpos->{$id}->{$key}");
         $cttpos->{$id}->{$key} = $lb->newline($cttpos->{$id}->{$key},"","");
          print "<td class='tdtxt', style='$occupied_style;'>\n";
	  if($cttpos->{$id}->{barcode} && $cttpos->{$id}->{int09}){#bike with tariff-nr
                my $u_name = $cttpos->{$id}->{owner};
                my $u_name_end = $cttpos->{$id}->{owner_end};
                foreach my $ctu_id (keys (%$ct_users)){
                  if($channel_map->{$u_name}){
                        $u_name = $channel_map->{$u_name};
                  }elsif($cttpos->{$id}->{owner} eq $ct_users->{$ctu_id}->{c_id}){
                        $u_name = $ct_users->{$ctu_id}->{txt01};
                  }
                  if($channel_map->{$u_name_end}){
                        $u_name_end = $channel_map->{$u_name_end};
                  }elsif($cttpos->{$id}->{owner_end} eq $ct_users->{$ctu_id}->{c_id}){
                        $u_name_end = $ct_users->{$ctu_id}->{txt01};
                  }
                 }
      	    if($cttpos->{$id}->{itime} =~ /(\d{4})-(\d{2})-(\d{2})\s(\d{2}):(\d{2})/){
              print $q->span("$dbt->{copri_conf}->{bike_state}->{$cttpos->{$id}->{int10}} &rarr; $dbt->{copri_conf}->{lock_state}->{$cttpos->{$id}->{int20}} &rarr; $u_name / $u_name_end"),"\n";
	      my $pos_raw = "";
	      if($users_dms->{u_id} && $users_dms->{u_id} =~ /1842/){
               my $pos_details = "";
	       foreach my $did (sort keys (%{$pricing->{rentalog}})){
	        $pos_details .= $did . " = " . $pricing->{rentalog}->{$did} . "</br>" if($pricing->{rentalog}->{$did});
	       }
	       #$pos_details = Dumper($pricing->{rentalog});
	       $pos_raw = $q->div({-class=>"popup",-onclick=>"toggle_box('$id')"},"$cttpos->{$id}->{c_id}", $q->span({-class=>"popuptext",-id=>"$id"},"$pos_details"));
	       print "(raw $pos_raw)<br />\n";
    	      }
	    }
	    print "<br />\n";
	  }
	  if($cttpos->{$id}->{txt01} || $cttpos->{$id}->{int09}){
	    #$line_count2++;
	    $cttpos->{$id}->{txt01} =~ s/fixed/\<span style='color:red'\>fixed\<\/span\>/;
	    $cttpos->{$id}->{txt01} =~ s/defect/\<span style='color:red'\>defect\<\/span\>/;
	    my $bike="";
	    my $tariff = "";
	    $bike = "$cttpos->{$id}->{$key}" if($cttpos->{$id}->{$key});
	    $tariff = ", Tarif: $cttpos->{$id}->{int09} $cttpos->{$id}->{txt04}" if($cttpos->{$id}->{txt04});
	    print $q->span("$bike $tariff<br />"),"\n";
	  }
	  if($cttpos->{$id}->{int06} || $cttpos->{$id}->{int04}){
	    print $q->span("Start/End Station: $cttpos->{$id}->{int06} / $cttpos->{$id}->{int04}, GPS: $cttpos->{$id}->{txt06}"),"\n";
	  }
	  if($cttpos->{$id}->{start_time} && $cttpos->{$id}->{end_time}){
    	    my $start_time = $lb->time4de($cttpos->{$id}->{start_time},"1");
    	    my $end_time = $lb->time4de($cttpos->{$id}->{end_time},"1");
	    print "<br />\n";
	    print $q->span({-style=>"$time_style"}, "Mietzeit: $start_time &rarr; $end_time"),"\n";
	  }
	  print "</td>\n";
	}elsif($key =~ /txt/){
          print $q->td({-class=>'tdtxt'},"$cttpos->{$id}->{$key}"),"\n";
	}elsif($key =~ /int/){
          print $q->td({-class=>'tdint',-nowrap=>"1"},"&nbsp;"),"\n";
	}
      }#end view position

     }
    }
   }#foreach end

   if($sum_parts19 && $sum_parts19 != 0){
     $sum_umst19 = $sum_parts19 / $diff19 * 19;
     $sum_umst19 = $pri->round($sum_umst19);
   }
   my $sum_netto19 = $sum_parts19 - $sum_umst19;
   $sum_netto19 = sprintf('%.2f', $sum_netto19);
   $sum_netto19 =~ s/\./,/;

   my $sum_paid = $sum_parts19;
   $sum_paid = $pri->round($sum_paid);
   my $sum_preauth = $sum_paid || 0;
   $sum_paid = sprintf('%.2f', $sum_paid);
   $sum_paid =~ s/\./,/;

   $sum_parts19 = sprintf('%.2f', $sum_parts19);
   $sum_umst19 = sprintf('%.2f', $sum_umst19);
   $sum_umst19 =~ s/\./,/;

   my $payment_text = "";
    foreach(@tplf_order){
      my ($key,$des,$size) = split /=/,$_;
      if($key =~ /txt5\d/){
        $ctf->{$key} = $q->unescapeHTML("$ctf->{$key}");
        $ctf->{$key} = $lb->newline($ctf->{$key},"","");
        $ctt->{state} =~ s/\(payone.*//;
        if($des =~ /$ctt->{state}/){
          if($sum_parts19 < 0){
            $payment_text = "$ctf->{txt58}";
          }else{
            $payment_text = "$ctf->{$key}";
          }
        }
      }
    }

   my $cs = $tc - 3;
   print "<tr>\n";
   print "<td colspan='$cs'><div  style='font-size:0.81em;padding:0.3em 0em;border:0px;'>$payment_text</div></td>\n";

   print "<td style='font-size:1em;' colspan='3'>\n";
   print $q->start_table({-class=>'list',-style=>'border-top:1px;border-style:solid;border-color:black;', -border=>'0', -width=>'100%',-align=>'center', -cellpadding=>'3', -cellspacing=>'0'}),"\n";
   print $q->Tr("\n");
   print $q->td("&nbsp;");

   if($node_meta->{node_name} !~ /steuerfrei/){
     print $q->Tr("\n"); $line_count2++;
     print $q->td({-class=>'tdint'},"Nettobetrag:");
     print $q->td({-class=>'tdint',-nowrap=>"1"},"$sum_netto19 €");

     if($sum_netto19 != 0){
      print $q->Tr("\n");$line_count2++;
      #print $q->td({-class=>'tdint',-nowrap=>"1"},"$umst1619% UmSt auf $sum_netto19 €:");
      print $q->td({-class=>'tdint',-nowrap=>"1"},"19% UmSt auf $sum_netto19 €:");
      print $q->td({-class=>'tdint',-nowrap=>"1"},"$sum_umst19 €");
     }
   }
   my $summe = "Summe";
   print $q->Tr("\n");$line_count2++;
   print $q->td({-class=>'tdsum'},"<b>$summe:</b>");
   print $q->td({-class=>'tdint',-nowrap=>"1"},"<b>$sum_paid €<b/>");
   print $q->hidden(-name=>'sum_paid', -override=>'1',-value=>"$sum_paid");

   print $q->end_table;
   print "</td><td>&nbsp;</td>";
   print "</tr>";
   print $q->end_table;

   print $q->hidden(-name=>'owner', -override=>'1', -value=>"$users_dms->{u_id}"),"\n";
   print $q->hidden(-name=>'offset', -override=>'1', -value=>"$R::offset"),"\n" if($R::offset);
   print $q->hidden(-name=>'c_id4trans', -override=>'1', -value=>"$ctt->{content_id}"),"\n";
   print $q->hidden(-name=>'tpl_id4trans', -override=>'1', -value=>"$ctt->{template_id}"),"\n";
   print $q->hidden(-name=>'offset', -override=>'1', -value=>"$R::offset"),"\n" if($R::offset);
   print $q->hidden(-name=>'limit', -override=>'1', -value=>"$R::limit"),"\n" if($R::limit);
 
   $set_main_id=$ctt->{main_id} if($ctt->{main_id} && $ctt->{main_id} > "300000");
   print $q->hidden(-name=>'set_main_id', -value=>"$set_main_id", -override=>'1'),"\n";


   if($users_dms->{int03} == 2){
    #only if user is also a primary DMS user with invoice rw
    print $q->hidden(-name=>'printer_id', -value=>"PDF", -override=>'1');
    my $dbh_primary = $dbt->dbconnect_extern("sharee_primary");
    my $users_dms_primary = { u_id => 0 };
    $users_dms_primary = $dbt->select_users($dbh_primary,$users_dms->{u_id},"and int03=2");

    if($users_dms_primary->{u_id} && $users_dms_primary->{int03} == 2){
     my @_paymentstate = split(/\|/,$dbt->{shareedms_conf}->{payment_state});
     push @_paymentstate, "";
     my $kind_of_payment = "";
     if($ctadr->{int03} == 1 && ($ctadr->{ct_name} =~ /PO-\d+/ || $ctadr->{ct_name} =~ /TM-\d+/)){
      	$kind_of_payment = "$_paymentstate[0]";
     }else{
	undef $_paymentstate[0];
     }
     if($ctadr->{int03} == 2  && length($ctadr->{ct_name}) >= 19){
      	$kind_of_payment = "$_paymentstate[1]";
     }else{
        undef $_paymentstate[1];
     }
     $kind_of_payment = "$ctt->{state}" if($ctt->{state} && $ctt->{int01});

     if($ctt->{state} && $ctt->{int01}){
     	$ctt->{int01} =~ s/\./,/;
     	my $style = "color:red;" if($ctt->{int01} ne $sum_paid);
     	my $opos = "";
     	$opos = "OPOS" if($ctt->{int14} && $ctt->{int14} > 0);
    	print $q->div({-style=>"padding:0.5em;font-size:0.81em;width:98%;text-align:right;"},"<span style='color:red;'>$opos</span> Buchung $vibuchen_mtime \"$ctt->{state}\" $ctt->{int01} €"),"\n";
     }

     if(!$ctt->{close_time}){
       my $send_invoice_checkbox = 1;
       $send_invoice_checkbox = 0 if($ctt->{txt30});
       print $q->div({-class=>'element6',-style=>'float:right;'},
       "buchen incl. drucken ",$but->checkbox("print_pdf","ct_trans","1","PDF drucken",""),
       " eMail Versand ",$but->checkbox("1","send_invoice","$send_invoice_checkbox","eMail $ctt->{txt00}",""),
       $but->singlesubmit1("set_state","buchen"),
       $but->selector("state","250px",$kind_of_payment,@_paymentstate)),"\n";
     }
    }
    #further payone capture are only allowed if OPOS && sequence && TXID is set
    if($ctt->{int14} && $ctt->{int18} && $ctt->{txt16}){
     	print $q->div({-class=>'element6',-style=>'clear:both;'},"Weitere Payone Einzüge über die vorhandene TXID $ctt->{txt16} (vorautorisiert Summe beachten) durch manuelle inkrement der Sequencenr mit +1", $q->textfield(-class=>'etxt',-name=>"payone_sequence",-default=>"", -override=>'1',-size=>"1",-maxlength=>1)),"\n";
    }
    #Payone reset if OPOS and TXID
    if($ctt->{int14} && $ctt->{txt16}){
     	print $q->div({-class=>'element6',-style=>'clear:both;'},"Payone reset löscht den Vorgang mit der TXID $ctt->{txt16} und versucht einen erneuten Einzug", $but->checkbox("1","payone_reset","0","Payone reset","")),"\n";
    }

   }
  print $q->end_form;

  my $praefix = "$ctt->{txt00}-$varenv{praefix}";
  print $q->div({-style=>"clear:both;height:0.1px;"},""),"\n";
  if($ctt->{txt30}){
    print $q->div({-style=>"padding:0.5em;font-size:0.81em;width:98%;text-align:right;"}, "$ctt->{txt30}"),"\n";
  }elsif( -f "$varenv{pdf}/$praefix-$ctt->{ct_name}.pdf" ){
     print $q->start_form(),"\n";
     print $q->hidden(-name=>'offset', -override=>'1', -value=>"$R::offset"),"\n" if($R::offset);
     print $q->hidden(-name=>'limit', -override=>'1', -value=>"$R::limit"),"\n" if($R::limit);
     print $q->div({-style=>"padding:0.5em;font-size:0.81em;width:98%;text-align:right;"}, "eMail wurde nicht versandt! $ctt->{txt00} eMail ", $but->singlesubmit1("ct_trans","send_invoice_again","send_invoice_again")),"\n" if($users_dms->{u_id} == $dbt->{copri_conf}->{superu_id});
     print $q->end_form;
  }

  if( -f "$varenv{basedir}/pdfinvoice/$praefix-$ctt->{ct_name}.pdf"){
    print $q->div({-style=>'padding:0.5em;font-size:0.81em;width:98%;text-align:right;'}, "Download: ", $q->a({-href=>"$varenv{wwwhost}/FileOut?file=$praefix-$ctt->{ct_name}.pdf&sessionid=$coo", -target=>"_blank" , -type=>'application/octet-stream', -style=>'text-decoration:underline;'}), "$praefix-$ctt->{ct_name}.pdf"),"\n";
  }


  if($ctt->{state} && $ctt->{state} =~ /payone/ && $ctt->{txt28} && $ctt->{txt28} =~ /error/i){
    print $q->div({-style=>"clear:both;padding:0.5em;font-size:0.81em;width:98%;text-align:right;color:red;"}, "Payone error: $ctt->{txt28}"),"\n";
  }elsif($ctt->{txt28} =~ /settleaccount=yes/i){
    print $q->div({-style=>"clear:both;padding:0.5em;font-size:0.81em;width:98%;text-align:right;color:green;"}, "Payone SEPA-Lastschrifteinzug war erfolgreich"),"\n";
  }

  $db->updater("contenttrans","c_id","$ctt->{content_id}","txt20","$accounting_start - $accounting_end","","","","","no_time") if(!$int05 && $accounting_start && $accounting_end);
  $db->updater("contenttrans","c_id","$ctt->{content_id}","int15","$sum_preauth","","","","","no_time") if($sum_preauth || $sum_preauth == 0);

  return "$line_count2";
}
1;
