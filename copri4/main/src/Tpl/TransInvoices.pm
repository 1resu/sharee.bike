package TransInvoices;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
#
use strict;
use warnings;
use POSIX;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI ':standard';
use DateTime;
use DateTime::Format::Pg;
use Date::Calc::Object qw(:ALL);
use Scalar::Util qw(looks_like_number);
use Data::Dumper;
use Lib::Config;
use Mod::Buttons;
use Mod::Libenz;
use Mod::Libenzdb;
use Mod::DBtank;
use Mod::APIfunc;
use Mod::Pricing;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
  my $self = shift;
  my $node_meta = shift;
  my $users_dms = shift;
  my $set_main_id = shift;
  my $ctt = shift;
  my $return = shift || "";

  my $q = new CGI;
  my $cf = new Config;
  my $lb = new Libenz;
  my $db = new Libenzdb;
  my $dbt = new DBtank;
  my $apif = new APIfunc;
  my $but = new Buttons;
  my $pri = new Pricing;
  my %varenv = $cf->envonline();
  my %ib = $but->ibuttons();
  my $today = strftime "%d.%m.%Y",localtime;
  my $now_dt = strftime "%Y-%m-%d %H:%M:%S", localtime;
  my $coo = $q->cookie(-name=>'domcookie');

  my $dbh = "";
  my $line_count2 = 0; 
  my $tc=0; 
  my $vibuchen_mtime = $lb->time4de($ctt->{mtime},1);
  my $txt20 = $R::txt20 || $ctt->{txt20} || "";#Leistungsdatum
  my $int05 = $R::int05 || $ctt->{int05} || "";#manuell
  my $rows = 0;
  my @tpl_order = ();
  my $cttpos = { c_id => 0 };

  my $ctf = { c_id => 0 };
  my $pref_cu = {
    table => "contentuser",
    fetch => "one",
    c_id  => $dbt->{shareedms_conf}->{parent_id},
  };
  $ctf = $dbt->fetch_tablerecord($dbh,$pref_cu);

  my $ctf_operator = { c_id => 0 };
  my $pref_cuop = {
    table => "contentuser",
    fetch => "one",
    c_id  => 2,
  };
  $ctf_operator = $dbt->fetch_tablerecord($dbh,$pref_cuop);

  #take original operator accounting c_id to collect related invoices
  #operator invoice
  if($ctt->{template_id} != 208){#not equal Abrechnung
    my $pref = {
        table           => "contenttrans",
        fetch           => "one",
        barcode         => $ctt->{barcode},
	txt00		=> "Abrechnung",
    };
    my $ctt_accounting = $dbt->fetch_record($dbh,$pref);
    ($cttpos,$rows) = $dbt->collect_contenttrans($dbh,$ctt_accounting->{content_id});
    #int9x are not in db
    @tpl_order = ("c_id=ID","int01=Summe Erlös","state=Zahlungsart","int93=TeilRad Gebühren","int98=19% UmSt","int100=Summe");
  }
  #operator accounting
  else{
    ($cttpos,$rows) = $dbt->collect_contenttrans($dbh,$ctt->{content_id});
    #int9x are not in db
    if($varenv{dbname} eq "sharee_sx"){
      @tpl_order = ("c_id=ID","int01=Summe Erlös","state=Zahlungsart","int94=payone Disagio","int02=Summe Gutschrift");
    }else{
      @tpl_order = ("c_id=ID","int01=Summe Erlös","state=Zahlungsart","int94=payone Disagio","int95=payone Transaktion","int96=payone Zahlungsmeldung","int97=payone Kreditkarte Zuordnung","int02=Summe Gutschrift");
    }
  }
  my $tplf = $dbt->get_tpl($dbh,201);#Kunden-Faktura, ex Firma
  my @tplf_order = split /,/,$tplf->{tpl_order};

  print $q->start_table({-class=>'list', -border=>'0', -width=>'100%',-align=>'left', -cellpadding=>'3', -cellspacing=>'0'}),"\n";

  print $q->hidden(-name=>'offset', -override=>'1', -value=>"$R::offset"),"\n" if($R::offset);
  print $q->hidden(-name=>'limit', -override=>'1', -value=>"$R::limit"),"\n" if($R::limit);
  print $q->hidden(-name=>'c_id4trans', -override=>'1', -value=>"$ctt->{content_id}"),"\n";
  print $q->hidden(-name=>'tpl_id4trans', -override=>'1', -value=>"$ctt->{template_id}"),"\n";

  $line_count2++; 
  print $q->Tr(),"\n"; 
  #print $q->th("Pos."),"\n";
  foreach (@tpl_order){
   my ($key,$val) = split /=/,$_;
   $tc++ if($val);
   $val .= " ($key)" if($users_dms->{u_id} eq $varenv{superu_id});
   print $q->th("$val"),"\n";
  }


  print $q->start_form(-name=>'transposform'),"\n";

  my $sum_operatorcredit=0;
  my $sum_payonecapture=0;
  my $sum_parts19=0;
  my $sum_all=0;
  my $diff19 = 100 + 19;
  my $sum_umst19=0;
  my $i=0;
  my $accounting_start = "";
  my $accounting_end = "";

  foreach my $id (sort { $cttpos->{$b}->{c_id} <=> $cttpos->{$a}->{c_id} } keys(%$cttpos)){

   my $oac = $pri->operator_accounting2calc(\%varenv,$cttpos->{$id},$ctf_operator);
   $sum_operatorcredit += $oac->{int02};#Abrechnung Gutschrift
   $sum_parts19 += $oac->{int100};#Operator Rechnung (TeilRad Gebühren + Disagio incl. 19%)
   $sum_all += $oac->{int99};#capture brutto
   $oac->{int01} = sprintf('%.2f', $oac->{int01});#capture netto

   if(1==1){
    $i++;
    $line_count2++;
    #FIXME
    if($i==1){
    	$accounting_end = "$3.$2.$1" if($cttpos->{$id}->{pay_time} =~ /(\d+)\-(\d+)\-(\d+)/);
	$accounting_start = $accounting_end;
    }else{
    	$accounting_start = "$3.$2.$1" if($cttpos->{$id}->{pay_time} =~ /(\d+)\-(\d+)\-(\d+)/);
    }
    #print "$cttpos->{$id}->{pay_time} xxxx $accounting_start - $accounting_end<br>";


    print $q->Tr(),"\n";
    #print $q->td({-class=>'tdint'},"$i"),"\n";

    foreach (@tpl_order){
      my ($key,$val) = split /=/,$_;     
      $cttpos->{$id}->{$key} = $q->unescapeHTML($cttpos->{$id}->{$key});
      my $txtstyle = "text-align:left;min-width:100px;";
      if($key =~ /int\d+/){
       $txtstyle = "text-align:right;min-width:50px;";
      }

      if(1==1){
        if($key =~ /ct_name/){
	  #print $q->td({-class=>'tdtxt'},"\# $cttpos->{$id}->{$key} $cttpos->{$id}->{txt01}"),"\n";
	  print $q->td({-class=>'tdtxt'},"\# $cttpos->{$id}->{$key}"),"\n";
        }
        elsif($key =~ /c_id/){
	  print $q->td({-class=>'tdint'},"$cttpos->{$id}->{$key}"),"\n";
        }
	elsif($key =~ /int/){
	  $oac->{$key} =~ s/\./,/;
	  print $q->td({-class=>'tdint',-nowrap=>1},"$oac->{$key} €"),"\n";
	}
	elsif($key =~ /state/){
	  print $q->td({-class=>'tdint',-nowrap=>1},"$cttpos->{$id}->{$key}"),"\n";
	}
	elsif($key =~ /txt/){
          print $q->td({-class=>'tdtxt'},"$cttpos->{$id}->{$key}"),"\n";
	}
      }

     }
    }
   }#foreach end

   if($sum_parts19 && $sum_parts19 != 0){
     $sum_umst19 = $sum_parts19 / $diff19 * 19;
     $sum_umst19 = $pri->round($sum_umst19);
   }
   my $sum_netto19 = $sum_parts19 - $sum_umst19;
   $sum_netto19 = sprintf('%.2f', $sum_netto19);
   $sum_netto19 =~ s/\./,/;

   my $sum_paid = $sum_parts19;
   $sum_paid = $pri->round($sum_paid);
   $sum_paid = sprintf('%.2f', $sum_paid);
   $sum_paid =~ s/\./,/;

   $sum_parts19 = sprintf('%.2f', $sum_parts19);
   $sum_umst19 = sprintf('%.2f', $sum_umst19);
   $sum_umst19 =~ s/\./,/;

   $sum_operatorcredit = $pri->round($sum_operatorcredit);
   $sum_operatorcredit = sprintf('%.2f', $sum_operatorcredit);
   $sum_operatorcredit =~ s/\./,/;

   $sum_all = sprintf('%.2f', $sum_all);
   $sum_all =~ s/\./,/;


   my $payment_text = "";
    foreach(@tplf_order){
      my ($key,$des,$size) = split /=/,$_;
      if($key =~ /txt5\d/){
        $ctf->{$key} = $q->unescapeHTML("$ctf->{$key}");
        $ctf->{$key} = $lb->newline($ctf->{$key},"","");
        $ctt->{state} =~ s/\(payone.*//;
        if($des =~ /$ctt->{state}/){
          if($sum_parts19 < 0){
            $payment_text = "$ctf->{txt58}";
          }else{
            $payment_text = "$ctf->{$key}";
          }
        }
      }
    }

   my $cs = $tc - 3;
   print "<tr>\n";
   print "<td colspan='$cs'><div  style='font-size:0.81em;padding:0.3em 0em;border:0px;'>$payment_text</div></td>\n";

   ###print sum
   print "<td style='font-size:1em;' colspan='3'>\n";
   print $q->start_table({-class=>'list',-style=>'border-top:0px;border-style:solid;border-color:black;', -border=>'0', -width=>'100%',-align=>'center', -cellpadding=>'3', -cellspacing=>'0'}),"\n";
   print $q->Tr("\n");
   print $q->td("&nbsp;");

   #Summe Betreiber Rechnung
   if($ctt->{template_id} != 208){#not equal Abrechnung
     print $q->Tr("\n"); $line_count2++;
     print $q->td({-class=>'tdint',-colspan=>2},"Nettobetrag");
     print $q->td({-class=>'tdint',-nowrap=>"1"},"$sum_netto19 €");

     if($sum_netto19 != 0){
      print $q->Tr("\n");$line_count2++;
      print $q->td({-class=>'tdint',-colspan=>2,-nowrap=>"1"},"19% UmSt auf $sum_netto19 €");
      print $q->td({-class=>'tdint',-nowrap=>"1"},"$sum_umst19 €");
     }

     print $q->Tr("\n");$line_count2++;
     print $q->td({-class=>'tdint',-colspan=>1,-style=>'color:silver;'},"(Einzüge brutto $sum_all)");
     print $q->td({-class=>'tdsum',-colspan=>1},"Summe $ctt->{state}");
     print $q->td({-class=>'tdint',-nowrap=>"1"},"$sum_paid €");
   }
   #Summe Betreiber Abrechnung (Gutschrift)
   else{
     print $q->Tr("\n");$line_count2++;
     print $q->td({-class=>'tdint',-colspan=>1,-style=>'color:silver;'},"(Einzüge brutto $sum_all)");
     print $q->td({-class=>'tdsum',-colspan=>1},"Summe $ctt->{state}");
     print $q->td({-class=>'tdint',-nowrap=>"1"},"$sum_operatorcredit €");
   }

   print $q->end_table;
   print "</td><td>&nbsp;</td>";
   ###print sum end

   print "</tr>";
   print $q->end_table;

   print $q->hidden(-name=>'owner', -override=>'1', -value=>"$users_dms->{u_id}");
   print $q->hidden(-name=>'c_id4trans', -override=>'1', -value=>"$ctt->{content_id}");
   print $q->hidden(-name=>'tpl_id4trans', -override=>'1', -value=>"$ctt->{template_id}");
   print $q->hidden(-name=>'offset', -override=>'1', -value=>"$R::offset") if($R::offset);
   print $q->hidden(-name=>'limit', -override=>'1', -value=>"$R::limit") if($R::limit);
   print $q->hidden(-name=>'relids', -override=>'1', -value=>"$R::relids") if($R::relids);
 
   $set_main_id=$ctt->{main_id} if($ctt->{main_id} && $ctt->{main_id} > "300000");
   print $q->hidden(-name=>'set_main_id', -value=>"$set_main_id", -override=>'1');


  if($users_dms->{int03} == 2){
    #only if user is also a primary DMS user with invoice rw
    print $q->hidden(-name=>'printer_id', -value=>"PDF", -override=>'1');
    my $dbh_primary = $dbt->dbconnect_extern("sharee_primary");
    my $users_dms_primary = { u_id => 0 };
    $users_dms_primary = $dbt->select_users($dbh_primary,$users_dms->{u_id},"and int03=2");

    if($users_dms_primary->{u_id} && $users_dms_primary->{int03} == 2){
     my @_paymentstate = ("");
     my $kind_of_payment = "";
     if($ctt->{template_id} != 208){#not equal Abrechnung
       print $q->hidden(-name=>'sum_paid', -override=>'1',-value=>"$sum_paid");
       push @_paymentstate, "Disagio";
       if($ctt->{state} && $ctt->{int01}){
        $kind_of_payment = "$ctt->{state}";
        $ctt->{int01} =~ s/\./,/;
     	my $style = "color:red;" if($ctt->{int01} ne $sum_paid);
     	my $opos = "";
     	$opos = "OPOS" if($ctt->{int14} && $ctt->{int14} > 0);
     	print $q->div({-style=>"padding:0.5em;font-size:0.81em;width:98%;text-align:right;"},"<span style='color:red;'>$opos</span> Buchung $vibuchen_mtime \"$ctt->{state}\" $ctt->{int01} €"),"\n";      
       }
     }
     else{
       print $q->hidden(-name=>'sum_operatorcredit', -override=>'1',-value=>"$sum_operatorcredit");
       push @_paymentstate, "Gutschrift";
       if($ctt->{state} && $ctt->{int02}){
        $kind_of_payment = "$ctt->{state}";
        $ctt->{int02} =~ s/\./,/;
     	my $style = "color:red;" if($ctt->{int02} ne $sum_operatorcredit);
     	my $opos = "";
     	$opos = "OPOS" if($ctt->{int14} && $ctt->{int14} > 0);
     	print $q->div({-style=>"padding:0.5em;font-size:0.81em;width:98%;text-align:right;"},"<span style='color:red;'>$opos</span> Buchung $vibuchen_mtime \"$ctt->{state}\" $ctt->{int02} €"),"\n";      
       }
     }

     if(!$ctt->{close_time}){
      my $send_invoice_checkbox = 0;
      #$send_invoice_checkbox = 0 if($ctt->{txt30});
      print $q->div({-class=>'element6',-style=>'float:right;'},
      "buchen incl. drucken ",$but->checkbox("print_pdf","ct_trans","1","PDF drucken",""),
      " eMail Versand ",$but->checkbox("1","send_invoice","$send_invoice_checkbox","eMail $ctt->{txt00}",""),
      $but->singlesubmit1("set_state","buchen"),
      $but->selector("state","250px",$kind_of_payment,@_paymentstate)),"\n";
     }
    }
  }

  print $q->end_form;

  my $praefix = "$ctt->{txt00}-$varenv{praefix}";
  print $q->div({-style=>"clear:both;height:0.1px;"},""),"\n";
  if($ctt->{txt30}){
    print $q->div({-style=>"padding:0.5em;font-size:0.81em;width:98%;text-align:right;"}, "$ctt->{txt30}"),"\n";
  }elsif( -f "$varenv{pdf}/$praefix-$ctt->{ct_name}.pdf" ){
     print $q->start_form(),"\n";
     print $q->hidden(-name=>'offset', -override=>'1', -value=>"$R::offset") if($R::offset);
     print $q->hidden(-name=>'limit', -override=>'1', -value=>"$R::limit") if($R::limit);
     print $q->hidden(-name=>'relids', -override=>'1', -value=>"$R::relids") if($R::relids);
     print $q->div({-style=>"padding:0.5em;font-size:0.81em;width:98%;text-align:right;"}, "eMail wurde nicht versandt! $ctt->{txt00} eMail ", $but->singlesubmit1("ct_trans","send_invoice_again","send_invoice_again")),"\n" if($users_dms->{u_id} == $dbt->{copri_conf}->{superu_id});
     print $q->end_form;
  }

  if( -f "$varenv{basedir}/pdfinvoice/$praefix-$ctt->{ct_name}.pdf"){
    print $q->div({-style=>'padding:0.5em;font-size:0.81em;width:98%;text-align:right;'}, "Download: ", $q->a({-href=>"$varenv{wwwhost}/FileOut?file=$praefix-$ctt->{ct_name}.pdf&sessionid=$coo", -target=>"_blank" , -type=>'application/octet-stream', -style=>'text-decoration:underline;'}), "$praefix-$ctt->{ct_name}.pdf"),"\n";
  }

  $db->updater("contenttrans","c_id","$ctt->{content_id}","txt20","$accounting_start - $accounting_end","","","","","no_time") if(!$int05 && $accounting_start && $accounting_end);

  return "$line_count2";
}
1;
