package Liste3;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
#
#uncomment for perl -cw src/Tpl/Liste3.pm
#use lib qw(/var/www/copri4/main/src);
#
use strict;
use warnings;
use POSIX;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI ':standard';
use URI::Escape;
use Encode;
use Lib::Config;
use Mod::Buttons;
use Mod::Libenz;
use Mod::Libenzdb;
use Mod::DBtank;
use Date::Calc qw(:all);
use Scalar::Util qw(looks_like_number);
use Storable;
use Mod::APIfunc;
use Mod::Pricing;
use Mod::Shareework;
use Mod::APIsigclient;
use Mod::Basework;
use Data::Dumper;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
 my $node_meta = shift;
 my $users_dms = shift;
 my $u_group = shift;
 my $return = shift;

  my $q = new CGI;
  my @keywords = $q->param;
  my $cf = new Config;
  my $lb = new Libenz;
  my $db = new Libenzdb;
  my $but = new Buttons;
  my $dbt = new DBtank;
  my $apif = new APIfunc;
  my $pri = new Pricing;
  my $tk = new Shareework;
  my $si = new APIsigclient;
  my $bw = new Basework;

  my %varenv = $cf->envonline();
  my $script = $q->script_name();
  my $path_info = $q->path_info();
  my $path = $path_info;
  #with meta_host, 
  if("$varenv{metahost}"){
   $path = "$script" . "$path_info";
   $script="";
  }
  my $lang = "de";
  my $now_db = strftime("%d.%m.%Y %H:%M:%S",localtime(time));
  my $dbh = "";
  my $debug = 0;
  my $coo = $q->cookie(-name=>'domcookie') || "";
  my $opdir_dms = "$dbt->{copri_conf}->{basedir}/$dbt->{operator}->{$varenv{dbname}}->{dir_dms}" || "";

  $path =~ s/\/login|\/user|\/manager|\/admin|\/$//;
  my $user_agent = $q->user_agent();
  my $saved_query = $q->url(-query=>1);
  my %ib = $but->ibuttons();
  my $main_id = $node_meta->{main_id};
  my $tpl_id = $node_meta->{template_id};
  my $time = time;
  my $today = strftime("%d.%m.%Y",localtime(time));
  my $now_date = strftime("%Y-%m-%d",localtime(time));
  my @months = ("Januar","Februar","März","April","Mai","Juni","Juli","August","September","Oktober","November","Dezember");
  my $hh=0;
  my $mm=0;
  my $day = strftime "%d", localtime;
  my $mon = strftime "%m", localtime;
  my $year = strftime "%Y", localtime;
  ($year,$mon,$day,$hh,$mm) = $lb->split_date($users_dms->{cal_start}) if($users_dms->{cal_start});

  my $start_date_time = $R::start_date_time;
  $start_date_time = "01.$mon.$year" if(!$start_date_time);

  my $end_date_time = $R::end_date_time;
  my $days4month = Days_in_Month($year,$mon);
  $end_date_time = "$days4month.$mon.$year" if(!$end_date_time);

  my $c_date; my $start_chck=0;my $end_chck=0;my $message;
  if($start_date_time){
    ($start_date_time,$start_chck) = $lb->checkdate($start_date_time) if($start_date_time ne "%");
    $message .= ">>> Datum Eingabefehler: $start_date_time <<<" if($start_chck);
  }
  if($end_date_time){
    ($end_date_time,$end_chck) = $lb->checkdate($end_date_time) if($end_date_time ne "%");
    $message .= ">>> Datum Eingabefehler: $end_date_time <<<" if($end_chck);
  }

  my $search_startdt = $start_date_time;
  my $search_enddt = $end_date_time;
  $end_date_time .= " 23:59" if($end_date_time !~ / \d\:\d/);

  if("$start_date_time" eq "01.$mon.$year"){
        $search_startdt = "date \'$start_date_time\' - integer \'31\'"
  }else{
        $search_startdt = "\'$start_date_time\'"
  }
  if("$end_date_time" eq "$days4month.$mon.$year"){
        $search_enddt = "date \'$end_date_time\' + integer \'31\'"
  }else{
        $search_enddt = "\'$end_date_time\'"
  }

  my $cttpos = {};
  my $cttpos_trans = {};
  my $cttpos_theft = {};
  if(($node_meta->{ct_table} eq "content") && ($node_meta->{tpl_id} == 205) && (!$start_chck && !$end_chck)){
    my $cttpos1 = $db->collect_postime("contenttranspos","$search_startdt","$search_enddt","$start_date_time","$end_date_time");
    foreach my $key (keys %$cttpos1){
          $cttpos_trans->{"trans_" . $key} = $cttpos1->{$key};
    }
    my $cttpos2 = $db->collect_postime("contenttheftpos","$search_startdt","$search_enddt","$start_date_time","$end_date_time");
    foreach my $key (keys %$cttpos2){
          $cttpos_theft->{"theft_" . $key} = $cttpos2->{$key};
    }
  }

  $cttpos = { %$cttpos_trans, %$cttpos_theft };


  my $table = $node_meta->{ct_table} || "content";

  my $hashfile = "$varenv{logdir}/$users_dms->{u_id}-$table-searchhash";

  if($table eq "content"){
	$node_meta->{tpl_order} =~ s/txt17=[\w\s=]+,//;
	$node_meta->{tpl_order} =~ s/byte01=[\w\s=]+,//;
  }
  if($table eq "contentadr" && $varenv{dbname} ne "sharee_primary"){
	$node_meta->{tpl_order} =~ s/txt17=[\w\s=]+,//;
	$node_meta->{tpl_order} =~ s/txt19=[\w\s=]+,//;
  }
  $node_meta->{tpl_order} =~ s/c_id=raw=[\w\s=]+// if($users_dms->{u_id} != $dbt->{copri_conf}->{superu_id});

  my @tpl_order = split /,/,$node_meta->{tpl_order};

  my $scol = "c_id";  
  $scol = "txt01" if($path =~ /Waren/);  
  #table columne check for col_sort
  if($users_dms->{col_sort} eq "node_name" && ($table eq "contenttver")){
    $scol = "node_name";
  }elsif($users_dms->{col_sort} eq "date_time" && ($table =~ /contenttver|contenttrans/)){
    $scol = "end_time";
  }elsif($users_dms->{col_sort}){
   my $tinfo = $db->table_info($table);
   foreach (keys(%$tinfo)){
     $scol = "$users_dms->{col_sort}" if("$users_dms->{col_sort}" eq "$tinfo->{$_}->{attname}");
   }
   $db->users_up("col_sort","0",$users_dms->{owner}) if("$scol" ne "$users_dms->{col_sort}");
  }

  my $limit = $R::limit || $varenv{limit};
  my $offset = $R::offset || "0";

  #backward | forward
    if($R::go && $R::go eq "backward_list"){
        $offset -= $limit if($offset >= $limit);
    }elsif($R::go && $R::go eq "forward_list"){
        $offset += $limit;
    }

 my ($daymarker,$raster_mmpx,$day4month) = $lb->month_line($users_dms);
 my $rows = 0;
 
 my $ctf = $db->get_content1("contentuser","$dbt->{sharedms_conf}->{parent_id}");

 my ($edit,$new_key,$copy_key,$save_key,$delete_key,$c_id4csv);
 my $ct4rel = {};
 my $ct4rel_parts = {};
 my $k=0;
 my $message = "";

  #print $q->end_form,"\n";
  print "<div id='Content4list'>\n";
  
  my $v_journal = $R::v_journal || "";
  if($node_meta->{main_id} == $dbt->{shareedms_conf}->{invoicejournal}){
    $v_journal = $node_meta->{node_name};
  }

  my $date = ""; 
  my $start_chck = 0;
  my $end_chck = 0;
  my $last_year = "";
  if($R::s_start_mtime){
    ($date,$start_chck) = $lb->checkdate($R::s_start_mtime) if($R::s_start_mtime !~ "%"); 
    $message .= ">>> Datum Eingabefehler: $date <<<" if($start_chck);
    my ($c_dd,$c_mm,$c_yy) = split(/\./,$date);
    $last_year = $c_yy if("$c_yy" eq "2011");
  }
  if($R::s_end_mtime){
    ($date,$end_chck) = $lb->checkdate($R::s_end_mtime) if($R::s_end_mtime !~ "%"); 
    $message .= ">>> Datum Eingabefehler: $date <<<" if($end_chck);
    my ($c_dd,$c_mm,$c_yy) = split(/\./,$date);
  }

  my $searchref = {};
  my $tplids = "$tpl_id," || 0;
  my $opos = "";
  my $main_ids = "";

  #Faktura actions
  if($node_meta->{template_id} =~ /208|209|218/){ 
   $main_id = $dbt->{shareedms_conf}->{faktura};
   $tplids = "218";

   if($node_meta->{node_name} =~ /OPOS/){
    $R::detail_search="suchen";
    $searchref->{int14} = ">=1";
    $R::todo="Filter";
    $message .= ">>> Offene Payone Posten (Fehlgeschlagene Geldeinzüge) <<<";
    $offset=0;
    $limit=10000;
    $tplids = "209,218";
    my $tpl = $dbt->get_tpl($dbh,209);
    @tpl_order = split /,/,$tpl->{tpl_order};
   }

   elsif(!$R::detail_search && -f $hashfile && $node_meta->{node_name} =~ /letzte Suche/){
    $R::todo="Filter";
    $message .= ">>> es wurden die letzten Suchparameter oder Filter geladen <<<";
    $offset=0;
    $limit=10000;
    #$tplids = "208,209,218";#reload store by hashfile
    $v_journal = $node_meta->{node_name};
    my $tpl = $dbt->get_tpl($dbh,209);
    @tpl_order = split /,/,$tpl->{tpl_order};
   }

   elsif($node_meta->{node_name} eq "Tagesbericht"){
    $R::detail_search="suchen";
    $R::todo="Filter";
    $offset=0;
    $limit = "1000";
    $v_journal = $node_meta->{node_name};
    $opos="null";
    $tplids = 218;
   }
   
   elsif($node_meta->{node_name} eq "Verkaufsjournal" || $R::tpl_id4trans eq "209"){
    $main_id = $dbt->{shareedms_conf}->{invoicejournal};
    $v_journal = $node_meta->{node_name};
    $tplids = 209;
    $offset=0;
    $limit=10000;
   }
   elsif($node_meta->{node_name} eq "Rechnung"){
    $main_id = $dbt->{shareedms_conf}->{invoice};
    $tplids = 218;
   }
   elsif($node_meta->{node_name} eq "Storno"){
    $main_id = 300009;
    $tplids = 218;
   }
   elsif($node_meta->{node_name} eq "Abrechnung" || $R::tpl_id4trans eq "208"){
    $main_id = 300029;
    $tplids = 208;
   }

   #search at all
   if($R::detail_search eq "suchen"){
    $tplids = "208,209,218";
   }
   $searchref->{tplids} = "$tplids";
  }
  $main_ids = "$main_id,";
  $main_ids .= $db->collect_noderec($main_id,$lang,"nothing");
  $main_ids =~ s/,$//;

  $message=$R::message if($R::message);
 
  #CSV
  my $export = "";
  my $ck4ex = "@R::ck4ex" || "";
  $export = "check4export" if($R::ck4ex);
  if($export || $R::ck4ex){
   $db->users_up("time4csv","$time",$users_dms->{u_id});
   $users_dms = $db->select_users($users_dms->{u_id});
  }

  #path-line
  my $redirect = "";
  $redirect = "(redirected)" if($R::redirected);
  #top of bootstrap
  my $header_style = "";
  $header_style = "border:2px solid #9f1f0e;" if($message);

  if($node_meta->{tpl_id} == 205 || $node_meta->{tpl_id} == 209){
    print $q->div({-class=>"copri_header",-style=>"background-color:$node_meta->{bg_color};"},"$path", $q->span({-style=>"padding:5px 10px;background-color:#86cbd7;color:white;"},
  " $months[$mon -1] $year",
    $q->a({-class=>"linknav",-style=>"padding:0 0.5em;",-title=>"Monat zurück",-href=>"?cal_delta_start=0:-1:0"}," &larr; "),
    $q->a({-class=>"linknav",-style=>"padding:0 0.5em;",-title=>"Monat aktuell",-href=>"?cal_today=1"},"&bull;"),
    $q->a({-class=>"linknav",-style=>"padding:0 0.5em;",-title=>"Monat vorwärts",-href=>"?cal_delta_start=0:1:0"}," &rarr; "),
    "$message"
    )),"\n";
  }else{
    print $q->div({-class=>"copri_header",-style=>"background-color:$node_meta->{bg_color};"},"$path $redirect",$q->span({-style=>"$header_style"},"$message"));
  }

  print $q->start_form(-name=>'searchform');
  print $q->hidden(-name=>'offset', -value=>"$offset");
  print $q->hidden(-name=>'main_id', -value=>"$node_meta->{main_id}");
  print $q->hidden(-name=>'mode', -value=>"manager");
  print $q->hidden(-name=>'owner', -value=>"$users_dms->{u_id}");
  print $q->hidden(-name=>'template_id', -value=>"$node_meta->{template_id}");
 
    $tplids = "205,224,225,210,226,227,228,229" if($table eq "content");
    $tplids = 205 if($path =~ /Waren$/);#defaults to rental bikes


    my $s_ct_name = $q->escapeHTML($R::s_ct_name) || "";
    my $s_barcode = $q->escapeHTML($R::s_barcode) || "";

  my $relnod = "";
  $relnod = $db->collect_rel4nodes($main_ids,"","","rel_id");

  my $j_exist = $db->get_node4multi("300011",$lang);
 
  my $nodes = "";
  if($main_ids){
    $nodes = $db->collect_node2($main_ids);
  }else{
    $lb->failure("Fehler! Es fehlen wichtige Einstellungen. ....journal-ID!");
  }
  my $ct_ids = "";
  my $last_ab = {};

  if($R::detail_search && $R::detail_search eq "suchen"){
    $offset = 0;
    $limit = 10000;
  }

  my $channel_map = $dbt->channel_map();
  my $mapref = {};
  my $ct_users = $dbt->users_map($dbh,$mapref);#get serviceAPP and DMS users from contentadr

  my @_users = ("");
  foreach my $id (sort { $channel_map->{$a} cmp $channel_map->{$b} } keys (%$channel_map)){
     push (@_users, "$id:$channel_map->{$id}");
     if($R::s_owner && $channel_map->{$id} eq $R::s_owner){
	$searchref->{owner} = $id;
     }
  }
  if(!$searchref->{owner}){
   foreach my $ctu_id (sort { $ct_users->{$a}->{txt01} cmp $ct_users->{$b}->{txt01} } keys (%$ct_users)){
     push (@_users, "$ct_users->{$ctu_id}->{c_id}:$ct_users->{$ctu_id}->{txt01}");
     if($ct_users->{$ctu_id}->{ct_name} && ($R::s_owner && $ct_users->{$ctu_id}->{txt01} =~ /$R::s_owner/i) || ($ct_users->{$ctu_id}->{c_id} eq $searchref->{owner})){
  	$searchref->{owner} = $ct_users->{$ctu_id}->{c_id};
     }
   }
  }

  my $node = { template_id => 205,
                 parent_id => 200013,
                 fetch  => "all",
                 keyfield => "main_id",
                 };
  my $bike_nodes = $dbt->fetch_rel4tpl4nd($dbh,$node);
  my $tariff_all = $db->collect_ct4rel("content","300026",$lang,"","","","","210");

  my $dbh_primary = $dbt->dbconnect_extern("sharee_primary");
  my $users_dms_primary = { u_id => 0 };
  $users_dms_primary = $dbt->select_users($dbh_primary,$users_dms->{u_id},"and int02 >= 1");

  my $ctrel = {};
  #only if permission read
  if(($node_meta->{ct_table} eq "content" && $users_dms->{int01} >= 1) || ($node_meta->{ct_table} eq "contentadr" && $users_dms_primary->{int02} >= 1) || ($node_meta->{ct_table} eq "contenttrans" && $users_dms_primary->{int03} >= 1)){

 
  my $c_id4trans = $R::c_id4trans || "";
  #Take only c_id from users if 
  if($users_dms->{c_id4trans} && $R::ct_trans && (($R::ct_trans eq "add_transadr" && $R::c_idadr) || ($R::ct_trans eq "add_transpos" && $R::c_id) || $R::ct_trans eq "new_trans")){
    $c_id4trans = $users_dms->{c_id4trans};
  }

  #it will takes only one dataset
  if($node_meta->{ct_table} eq "contenttrans" && $c_id4trans && $c_id4trans =~ /^\d+$/){
    $ct4rel = $db->collect_cid($table,$lang,$tplids,$R::rel_id,$R::barcode,"c_id",$c_id4trans);
  }
  elsif(!$start_chck && !$end_chck){

	#collect search keys
 	foreach my $postkey (@keywords){
	  foreach(@tpl_order){
    	    my ($key,$val,$size) = split /=/,$_;
	    if($postkey =~ /s_$key|s_start_$key|s_end_$key/){
     	  	my $val = $q->param($postkey);
		$postkey =~ s/^s_//; 
		$searchref->{$postkey} = $val; 
	    }
	  }
	}

	#trying to save hashref
	if($R::detail_search && ref($searchref) eq "HASH"){
	  store $searchref, $hashfile;
	}elsif(!$R::detail_search && -f $hashfile && ($path =~ /letzte Suche/)){
	  $searchref = {};
 	  $searchref = retrieve($hashfile);
	  $tplids = $searchref->{tplids} if($searchref->{tplids});
	}
	#print Dumper($searchref);exit;
      $rows = $db->count_content($table,"$main_ids","$tplids");

      #Verkaufsjournal default view
      if($node_meta->{tpl_id} == 209 && $v_journal && !$R::detail_search){
	$searchref->{start_pay_time} = $start_date_time;
	$searchref->{end_pay_time} = $end_date_time;
      }
      if(1==2 && $node_meta->{tpl_id} == 209 && $v_journal && !$R::detail_search){
          my $pref_209 = {
            table => "contenttrans",
            fetch => "one",
	    order  => "int11",
	    state => "Tagesabschluss",
          };
          $last_ab = $dbt->fetch_tablerecord($dbh,$pref_209);
	  $searchref->{int11} = $last_ab->{int11} if($last_ab->{int11});

      }
      #print Dumper($last_ab->{int11});
      
      #reload sig bikes and stations states
      #$bw->log("Liste3 condition",$node_meta,"");
      if($varenv{syshost} eq "shareedms-sx"){
	my $ctadr = { c_id => $users_dms->{u_id} };
	if($node_meta->{tpl_id} == 205 || $node_meta->{node_name} eq "Waren"){
	  $q->param(-name=>'request',-value=>"bikes_available");
          (my $response->{bikes},my $return2copri->{bikes}) = $si->sig_available($q,\%varenv,$ctadr);
	  #$bw->log("return2copri",$return2copri->{bikes},"");
          $tk->sigbike_cupdate($return2copri->{bikes});
	}elsif($node_meta->{tpl_id} == 225){
          $q->param(-name=>'request',-value=>"stations_available");
          (my $response->{stations},my $return2copri->{stations}) = $si->sig_available($q,\%varenv,$ctadr);
	  #$bw->log("return2copri",$return2copri->{stations},"");
          $tk->sigstation_cupdate($return2copri->{stations});
	}
      }
      $ct4rel = $db->search_content3($searchref,$table,$dbt->{shareedms_conf}->{parent_id},$node_meta,$users_dms->{u_id},$lang,"$main_ids","$tplids","$ct_ids",$v_journal,$time,$R::s_kontext,$scol,$users_dms->{sort_updown},$offset,$limit,$export,$R::todo,$ck4ex,$opos);

      if($node_meta->{node_name} eq "Faktura" || $tplids =~ /218/){
	my $v_journalparts = "";
       	$v_journalparts = $v_journal . "_parts";
       	$ct4rel_parts = $db->search_content3($searchref,$table,$dbt->{shareedms_conf}->{parent_id},$node_meta,$users_dms->{u_id},$lang,"$main_ids","$tplids","$ct_ids",$v_journalparts,$time,$R::s_kontext,$scol,$users_dms->{sort_updown},$offset,$limit,$export,$R::todo,$ck4ex,$opos);
      }

   }#end $ct4rel collect
  }else{
    $return = "failure::Abbruch. Keine Zugriffsberechtigung";
  }#end permission read

   my $max_sum_pos = -10000;
   my $max_op = ">=";
   if($searchref->{int01} =~ /\d/){
     $max_sum_pos = $searchref->{int01};
     $max_sum_pos =~ s/\s//g;
     $max_sum_pos =~ s/,/./g;
     #$max_op = "==";
     if($max_sum_pos =~ /(\<\=|\>\=|\<|\>|\=\=)(\d+)/){
	$max_op = $1;
	$max_sum_pos  = $2;
     }
   }   
  print $q->div({-style=>'background-color:silver;height:10px;'},"&nbsp;"),"\n";

  my $hstyle = "border-right: solid thin gray;border-bottom: solid thin gray;";

  #accounting workflow
  if($node_meta->{node_name} eq "Verkaufsjournal"){
   print "<div style='padding-bottom:5px;background-color:silver;'>\n";
   if($R::accounting_select){
     print $but->singlesubmit1("detail_search","operator_accounting","","margin:0 0 0px 20px;"),"\n";
   }else{
      print $q->a({-class=>'elinkbutton',-style=>'margin:0 0 2px 20px;', -href=>'?accounting_select=1'},"1. Einzüge selektieren"),"\n";
   }
   print "$months[$mon -1] $year\n";
   print "</div>\n";
  }
  print $q->hidden(-name=>'mandant_main_id', -value=>"$dbt->{shareedms_conf}->{parent_id}"),"\n";
  print $q->hidden(-name=>'tpl_id4trans', -value=>"$node_meta->{tpl_id}"),"\n";

  print $q->start_table({ -style=>'clear:both;', -border=>'0', -width=>'100%',-align=>'center', -cellpadding=>'0', -cellspacing=>'0'}),"\n";

  #1. search line
  print $q->Tr();
  print $q->td({-style=>"background-color:silver;"},""),"\n";
  print $q->td({-style=>"background-color:silver;$hstyle"},$but->singlesubmit1("detail_search","search","","width:100%;")),"\n";

  $edit="rel_edit"; 
   $new_key="new_content";
   $save_key="save_content";
   $delete_key="delete_content";
   my $a_color = $node_meta->{bg_color} || "yellow";
   if($table eq "contentadr"){
     $edit="rel_edit"; 
     $new_key="new_adr";
     $save_key="save_adr";
     $delete_key="delete_adr";
   }
  #contenttrans | contenttver via ct_trans
   if($table eq "contenttrans"){
     $edit="ct_trans"; 
     $new_key="new_trans";
     $delete_key="delete_trans";
   }elsif($table eq "contenttver"){
     $edit="ct_trans"; 
     $new_key="new_tver";
     $delete_key="delete_tver";
   }
   #if($node_meta->{parent_id} >= "200000" && $node_meta->{template_id} =~ /202|218/){
   if($node_meta->{parent_id} >= 200000){
     print $q->td({-style=>"width:25px;background-color:$a_color;$hstyle"}, $but->singlesubmit2glyph("$edit","$new_key","$ib{$new_key}","background-color:$a_color;"));
     print $q->hidden(-name=>'open_set_main_id', -value=>"$node_meta->{main_id}") if($table =~ /contenttrans/);
   }else{
     print $q->td({-class=>'ebutton',-style=>"width:25px;background-color:$a_color;$hstyle"},"&nbsp;"),"\n";
   }

  #1. Search-fields
  my $h=0;
  my $s_val = "";
  foreach(@tpl_order){
    $h++;
    my ($key,$val,$size) = split /=/,$_;
    if($size =~ /area/){
     $size = "5em";
    }elsif($key =~ /int0|c_id|ct_name/){
     $size = "1em";# if(!$size); 
    }else{
     $size = "2em";
    }

    $s_val = $searchref->{$key};
    #print "$key=$searchref->{$key}|";
    if($key =~ /byte|node|txt|int|time\d+|ct_name|c_id|barcode|sort|public/){
      if($table eq "content" && $tpl_id == 225 && $key eq "int04"){
      	print $q->td({-class=>"search_line"},$q->textfield(-class=>'stxt',-name=>"s_$key",-default=>"$s_val",-size=>"$size",-maxlength=>40, -autofocus=>1),"\n");
      }elsif($table eq "content" && $key eq "barcode"){
      	print $q->td({-class=>"search_line"},$q->textfield(-class=>'stxt',-name=>"s_$key",-default=>"$s_val",-size=>"$size",-maxlength=>40, -autofocus=>1),"\n");
      }elsif($table =~ /contentadr|contenttrans/ && $key eq "txt08"){
      	print $q->td({-class=>"search_line"},$q->textfield(-class=>'stxt',-name=>"s_$key",-default=>"$s_val",-size=>"$size",-maxlength=>40, -autofocus=>1),"\n");
      }elsif($size =~ /select/ && $key =~ /int12/ && $tpl_id =~ /210/){#Tarif for Flot
	my @s_valxx = ("");
	foreach my $rid (sort { $bike_nodes->{$a}->{node_name} cmp $bike_nodes->{$b}->{node_name} } keys (%$bike_nodes)){
          push (@s_valxx, "$bike_nodes->{$rid}->{main_id}:$bike_nodes->{$rid}->{node_name} - $bike_nodes->{$rid}->{main_id}");
        }
        print $q->td({-class=>'search_line'},$but->selector_class("s_$key","","",$s_val,@s_valxx)),"\n";
      }elsif($size =~ /select/ && $key =~ /txt24/ && $tpl_id =~ /225/){#txt24=bike_group (for station filter)
	#my %station_filter_hash = ();
	my @s_valxx = ("");
	foreach my $rid (sort { $bike_nodes->{$a}->{node_name} cmp $bike_nodes->{$b}->{node_name} } keys (%$bike_nodes)){
	  push (@s_valxx, "$bike_nodes->{$rid}->{main_id}:$bike_nodes->{$rid}->{node_name} - $bike_nodes->{$rid}->{main_id}");
	  #$station_filter_hash{$bike_nodes->{$rid}->{type_id}} = 1;

        }
	#foreach my $type_id (keys (%station_filter_hash)){
	#  push (@s_valxx, "$type_id:$dbt->{copri_conf}->{type_id}->{$type_id} - $type_id");
	#}
        print $q->td({-class=>'search_line'},$but->selector_class("s_$key","","",$s_val,@s_valxx)),"\n";

      }elsif($size =~ /select/ && $key =~ /int21|int22/ && $tpl_id == 228){#Bonusnummern
	my @s_valxx = ("");
        foreach my $rid (sort { $tariff_all->{$a}->{barcode} <=> $tariff_all->{$b}->{barcode} } keys (%$tariff_all)){
          push (@s_valxx, "$tariff_all->{$rid}->{barcode}:$tariff_all->{$rid}->{barcode} - $tariff_all->{$rid}->{ct_name}");
        }
        print $q->td({-class=>'search_line'},$but->selector_class("s_$key","","",$s_val,@s_valxx)),"\n";
      }elsif($size =~ /select/ && $key !~ /txt23|txt24/){#txt23=color-code or txt24=Flotten ID select
	my @s_valxx = ("");
	my $s_hash = {};
	$s_hash = $dbt->{copri_conf}->{lock_state} if($tpl_id == 205 && $key eq "int20");
	$s_hash = $dbt->{copri_conf}->{bike_state} if($tpl_id == 205 && $key eq "int10");
	$s_hash = $dbt->{copri_conf}->{station_state} if($tpl_id == 225 && $key eq "int10");
	$s_hash = $dbt->{copri_conf}->{lock_system} if($tpl_id == 205 && $key eq "int11");
	$s_hash = $dbt->{copri_conf}->{sharing_type} if($tpl_id == 210 && $key eq "int18");
	#$s_hash = $dbt->{copri_conf}->{tariff_unit} if($tpl_id == 210 && $key eq "int01");
	$s_hash = { 1 => 1, 2 => 2, 3 => 3, 4 => 4 } if($tpl_id == 225 && $key eq "txt07");
	#while (my ($key, $value) = each %{ $s_hash }) {
        foreach my $s_key (sort keys (%{ $s_hash })) {
          push @s_valxx, "$s_key:$s_hash->{$s_key}";#[2:unlocked]
        }
        print $q->td({-class=>'search_line'},$but->selector_class("s_$key","","",$s_val,@s_valxx)),"\n";
      }else{
      	print $q->td({-class=>"search_line"},$q->textfield(-class=>'stxt',-name=>"s_$key",-default=>"$s_val",-size=>"$size",-maxlength=>40),"\n");
      }
    }elsif($key =~ /owner/){
        print $q->td({-class=>'search_line'},$but->selector("s_$key","120px","$s_val",@_users)),"\n";
    }elsif($key =~ /state/){
        my @_states = split(/\|/,$dbt->{shareedms_conf}->{payment_state});
	my @_orderstates = split(/\|/,$dbt->{shareedms_conf}->{order_state});
	push (@_states,@_orderstates);
        print $q->td({-class=>'search_line'},$but->selector("s_$key","120px","$s_val",@_states)),"\n";
    }
    my $s_mtime=""; 
    my $e_mtime="";
    if($key eq "mtime"){
      $s_mtime = $searchref->{start_mtime};
      $e_mtime = $searchref->{end_mtime};
    }
    if($key eq "atime"){
      $s_mtime = $searchref->{start_atime};
      $e_mtime = $searchref->{end_atime};
    }
    if($key eq "pay_time"){
      $s_mtime = $searchref->{start_pay_time};
      $e_mtime = $searchref->{end_pay_time};
    }
    if($key eq "date_time"){
      $s_mtime = $searchref->{start_date_time};
      $e_mtime = $searchref->{end_date_time};
    }

    print $q->td({-nowrap=>1,-class=>"search_line_date"},$q->textfield(-id=>'datepicker1',-class=>'etxt',-name=>"s_start_$key",-default=>"$s_mtime",-size=>"7",-maxlength=>10),"-",$q->textfield(-id=>'datepicker2',-class=>'etxt',-name=>"s_end_$key",-default=>"$e_mtime",-size=>"7",-maxlength=>10)),"\n" if($key =~ /time$/);
  }


  #2. Tableheader
  print $q->Tr();
  #permissions
  if($users_dms_primary->{int03} == 2 && $users_dms->{int03} == 2){
    print $q->td({-style=>"background-color:silver;"},$q->checkbox(-name=>'toggle_all', -checked=>'', -value=>'off', -label=>'', -title=>'Alle auswählen', -onclick=>'CheckAll();')),"\n";
  }else{
    print $q->td({-style=>"background-color:silver;"},"&nbsp;"),"\n";
  }
  my $i=0;
  $i += 2;
  if($R::ck4ex){
    print $q->th({-colspan=>2,-style=>'text-align:left;'},$q->a({-class=>'linknav3', href=>"$varenv{metahost}/pdf/$users_dms->{u_id}-$users_dms->{time4csv}.csv"},"CSV")," | ",$q->a({-class=>'linknav3', href=>"$varenv{metahost}/pdf/$users_dms->{u_id}-$users_dms->{time4csv}.xls"},"XLS")),"\n";
  }else{
    my $sort_up = "up";
    my $sort_down = "down";
    $sort_up = "<b>$sort_up</b>" if($users_dms->{sort_updown} eq "up");
    $sort_down = "<b>$sort_down</b>" if($users_dms->{sort_updown} eq "down");

    print $q->th($q->a({-class=>"sortnav",-href=>"?sort_updown=up\&offset=$offset\&limit=$limit",-title=>'Aufsteigend sortieren'},"$sort_up"),"|",$q->a({-class=>"sortnav",-href=>"?sort_updown=down\&offset=$offset\&limit=$limit",-title=>'Absteigend sortieren'},"$sort_down")),"\n";
    print $q->th(""),"\n";
  }
  foreach (@tpl_order){
   my ($key,$val,$size,$title) = split /=/,$_;
   my $sort_title=" $title";
   $val = "<b>$val</b>" if($key eq $users_dms->{col_sort});
   if($key =~ /byte|txt|time|node|int|time\d+|save|state|owner|c_id|ct_name|barcode|sort|public/){
     $i++;
     print $q->th($q->a({-class=>"sortnav",-href=>"?col_sort=$key\&offset=$offset\&limit=$limit",-title=>"$sort_title"},"$val")),"\n";
   }
  }

  my $tdcal = scalar @tpl_order;
  if($tpl_id == "205"){
   print $q->Tr();
   print $q->td({-style=>'background-color:silver;'},""),"\n";
   print $q->td({-class=>'tdtxt',-style=>'background-color:silver;',-colspan=>2},"$months[$mon -1] $year"),"\n";
   print $q->td({-style=>"font-size:0.71em;padding:0;border:0px solid green;",-colspan=>"$tdcal",-nowrap=>"1"},"$day4month"),"\n";
  }

  my $sum_saldo="0";my $sum_opos="0";my $sum_abb="0";my $sum_ueb="0";my $sum_success="0";my $sum_SEPApayone="0";my $sum_CCpayone="0";my $sum_ausfall="0";my $close_time="";my $payment_time=""; 
  my $nr=0;my $nx=0; 
  
  #BIG LOOP loop content table
  foreach my $id (sort { 
    if($users_dms->{sort_updown} eq "down"){
	if ($scol =~ /barcode|int/) { 
		$ct4rel->{$b}->{$scol} <=> $ct4rel->{$a}->{$scol} 
	}else{ 
		lc($ct4rel->{$b}->{$scol}) cmp lc($ct4rel->{$a}->{$scol}) 
	} 
    }else{
	if ($scol =~ /barcode|int/) { 
		$ct4rel->{$a}->{$scol} <=> $ct4rel->{$b}->{$scol} 
	}else{ 
		lc($ct4rel->{$a}->{$scol}) cmp lc($ct4rel->{$b}->{$scol}) 
	} 
    }
  } keys(%$ct4rel)){

   my $set_style = "";
   my $set_style4nr = "";
   my $sum_error = 0;
   my $ecol=0;
   my $lock_system = 2;#defaults to Ilockit
   $lock_system = $ct4rel->{$id}->{int11} if($ct4rel->{$id}->{int11});

   # check sum_pos and sum_buchen
   my $sum_pos = 0;
   my $pricing = {};
   my $counting = {};
   #pre-sum only if client invoice
   if($ct4rel->{$id}->{template_id} == 218 && $ct4rel->{$id}->{int10} != 2){
     foreach my $cpid (keys (%$ct4rel_parts)){
	if($ct4rel->{$id}->{c_id} == $ct4rel_parts->{$cpid}->{ct_id}){
   
	  if($ct4rel_parts->{$cpid}->{int35} && $ct4rel_parts->{$cpid}->{start_time} && $ct4rel_parts->{$cpid}->{end_time}){
    	    ($pricing,$counting) = $pri->counting_rental(\%varenv,$ct4rel_parts->{$cpid},"calc_price");
	    $sum_pos += $pricing->{total_price}
	  }elsif((looks_like_number($ct4rel_parts->{$cpid}->{int02}) && $ct4rel_parts->{$cpid}->{int02} != 0) && (looks_like_number($ct4rel_parts->{$cpid}->{int03}) && $ct4rel_parts->{$cpid}->{int03} != 0)){
	   my ($gesamt,$rabatt) = $pri->price2calc($ct4rel_parts->{$cpid});
	   $sum_pos += $gesamt;
	  }
	}
     }
     my $sum_chk = $ct4rel->{$id}->{int01};
     $sum_pos = $lb->round($sum_pos);
     $sum_pos = sprintf('%.2f',$sum_pos);
     my $sum_chkdiff = $sum_chk - $sum_pos;
     if($ct4rel->{$id}->{int01} && ($sum_pos ne $sum_chk) && ($sum_chkdiff > 0.02 || $sum_chkdiff < -0.02)){
     	$sum_error = "1";
     }
     $sum_error = "0" if($R::s_kontext && $R::s_kontext eq "Waren" || $R::node2edit || $R::ct_trans);
   }


   my $u_name = $ct4rel->{$id}->{owner};
     foreach my $ctu_id (keys (%$ct_users)){
       if($channel_map->{$u_name}){
	$u_name = $channel_map->{$u_name};
       }elsif($ct4rel->{$id}->{owner} eq $ct_users->{$ctu_id}->{c_id}){
	$u_name = $ct_users->{$ctu_id}->{txt01};
       }
     }
 
   #print $q->start_form(-name=>'listeform');

   if(1==1){    
   if(1==1){
    $nr++;
    $nx=$nr;
    $close_time = $ct4rel->{$id}->{close_time} if($ct4rel->{$id}->{close_time});
    $close_time = $lb->time4de($close_time,"1") if($close_time);
    $payment_time = "";
    if($ct4rel->{$id}->{payment_time}){
      $payment_time = $ct4rel->{$id}->{payment_time};
      $payment_time = $lb->time4de($payment_time,"1");
    }
    $sum_saldo += $ct4rel->{$id}->{int16} if($ct4rel->{$id}->{state});
    $sum_opos += $ct4rel->{$id}->{int01} if($ct4rel->{$id}->{state} && $ct4rel->{$id}->{state} ne "Zaglungsausfall" && $ct4rel->{$id}->{int14} >= 1);
    $sum_abb += $ct4rel->{$id}->{int01} if($ct4rel->{$id}->{state} && $ct4rel->{$id}->{state} =~ /Abbuchung/);
    $sum_ueb += $ct4rel->{$id}->{int01} if($ct4rel->{$id}->{state} && $ct4rel->{$id}->{state} =~ /Überweisung/);
    $sum_SEPApayone += $ct4rel->{$id}->{int01} if($ct4rel->{$id}->{state} && $ct4rel->{$id}->{state} =~ /SEPA.*payone|Zahlungseingang.*payone/);
    $sum_CCpayone += $ct4rel->{$id}->{int01} if($ct4rel->{$id}->{state} && $ct4rel->{$id}->{state} =~ /Kreditkarte.*payone/);
    $sum_ausfall += $ct4rel->{$id}->{int01} if($ct4rel->{$id}->{state} && $ct4rel->{$id}->{state} =~ /Zahlungsausfall/);
    $c_id4csv .= "$ct4rel->{$id}->{c_id},";
    $ct4rel->{$id}->{ct_name} = $q->unescapeHTML($ct4rel->{$id}->{ct_name}) || "";
    $ct4rel->{$id}->{c_id} = $q->unescapeHTML($ct4rel->{$id}->{c_id}) || "";
    $set_style = "background-color:#fcfdfb;";
    $set_style = "background-color:#f4f1ee;" if($nx %= 2);
    #$set_style = "background-color:#e4f1ee;" if($ct4rel->{$id}->{ct_name} eq $ct4rel->{$id}->{barcode});
    my $ccc_id = $R::c_id || $R::c_id4trans || $users_dms->{c_id4trans};
    $set_style = "background-color:$node_meta->{bg_color};" if(($R::rel_id && $R::rel_id eq $ct4rel->{$id}->{rel_id}) && !$R::empty_rel_id);
    $set_style = "background-color:$node_meta->{bg_color};" if(($ccc_id && $ccc_id eq $ct4rel->{$id}->{content_id}) && ("$table" =~ /contenttrans/));
    $set_style = "background-color:$node_meta->{bg_color};" if(($ccc_id && $ccc_id eq $ct4rel->{$id}->{content_id}) && ("$table" =~ /contenttver/));

    #red
    $set_style = "background-color:#d39c9c;" if($sum_error && $sum_error == 1 && $node_meta->{node_name} eq "Faktura");
    $set_style4nr = $set_style;
    my $m_id;
    my $nodeline2;
    my $nodeline_name2;
    my $nodelist;


    #Instanz Buttons-Logic
    print $q->Tr(),"\n";
    #checkboxes
    #permissions
    if($users_dms_primary->{int03} == 2 && $users_dms->{int03} == 2){
      my $checked = 0;
      $checked = 1 if($R::accounting_select && $node_meta->{template_id} == 209 && $ct4rel->{$id}->{int01} > 0 && $ct4rel->{$id}->{state} =~ /payone/i && !$ct4rel->{$id}->{int14});
      print $q->td({-style=>"background-color:silver;"}, $q->checkbox(-name=>"ck4ex", -checked=>"$checked", -value=>"$ct4rel->{$id}->{c_id}", -label=>'')),"\n";
    }else{
      print $q->td({-style=>"background-color:silver;"},"&nbsp;"),"\n";
    }
    #1.Spalte
    if("$ct4rel->{$id}->{main_id}" eq "$node_meta->{main_id}" || "$ct4rel->{$id}->{main_id}" eq "$node_meta->{parent_id}"){
     if($table =~ /content$|contentadr|contentnel/){        
	$ecol++;
	print "<td class='tb_inst' style='$set_style' nowrap>\n";

	#fee2pos
 	if($table eq "content" && ($ct4rel->{$id}->{template_id} =~ /229/) && $R::ca_id && $R::ct_id){
	  print $q->a({-class=>"editnav",-href=>"/$dbt->{shareedms_conf}->{parent_node}/Mietjournal?insert_contenttranspos=1\&cc_id=$ct4rel->{$id}->{c_id}\&ca_id=$R::ca_id\&ct_id=$R::ct_id\&owner=$users_dms->{u_id}",-title=>"Gebühr hinzufügen"}, $q->img({-src=>"$varenv{metahost}/glyphicons/glyphicons-512-copy.png", -style=>'height:1.3em;'}));
	}

	if($table eq "content" && ($ct4rel->{$id}->{template_id} =~ /205|225/)){
	    my $rnid = $ct4rel->{$id}->{rel_id};
      	    my $node_names = "failure $rnid | $relnod->{$rnid}->{content_id} == $ct4rel->{$id}->{c_id}";
	    if($relnod->{$rnid}->{content_id} && $relnod->{$rnid}->{content_id} == $ct4rel->{$id}->{c_id}){
		$node_names = $relnod->{$rnid}->{node_name};
	    }else{
	      	my $subnode = $dbt->get_subrelnode($dbh,$node_meta->{main_id});
		$node_names = $subnode->{node_name};
	    }
	    my $search_key = "s_barcode=$ct4rel->{$id}->{barcode}";#bike
	    $search_key = "s_int04=$ct4rel->{$id}->{int04}" if($ct4rel->{$id}->{template_id} == 225);#station 
	    print $q->a({-class=>"editnav",-href=>"$path/$node_names?detail_search=1\&$search_key",-title=>"zur Liste der Rad $ct4rel->{$id}->{barcode} Servicearbeiten"}, $q->img({-src=>"$varenv{metahost}/glyphicons/glyphicons-440-wrench.png", -style=>'height:1em;'}));
	}
	##
	print "</td>\n";
      }elsif($users_dms->{u_id} > 0 && "$table" =~ /contenttrans|contenttver/){        
	print "<td class='tb_inst' style='$set_style' nowrap>\n";
	print "</td>\n";
      }else{
	$ecol++;
	print $q->td({-style=>"$set_style"}),"\n";
      }
    }elsif($ct4rel->{$id}->{content_id} && !$v_journal){
      #2017-11-10 simple multi node_name directory view
      $ecol++;
      my $rnid = $ct4rel->{$id}->{rel_id};
      my $node_names = "failure $rnid | $relnod->{$rnid}->{content_id} == $ct4rel->{$id}->{c_id}";
      if($relnod->{$rnid}->{content_id} && $relnod->{$rnid}->{content_id} == $ct4rel->{$id}->{c_id}){
	$node_names = $relnod->{$rnid}->{node_name};
      }else{
	my $subnode = $dbt->get_subrelnode($dbh,$node_meta->{main_id});
	$node_names = $subnode->{node_name};
      }

      #node_names fails TODO
      if(1==1 && $table eq "content" && ($ct4rel->{$id}->{template_id} =~ /205|225/)){
	    my $search_key = "s_barcode=$ct4rel->{$id}->{barcode}";#bike 
	    $search_key = "s_int04=$ct4rel->{$id}->{int04}" if($ct4rel->{$id}->{template_id} == 225);#station
      	    print $q->td({-class=>'tdtxt',-colspan=>1,-style=>"$set_style", -nowrap=>1}, $q->a({-class=>"editnav",-href=>"$path/$node_names?detail_search=1\&$search_key",-title=>"zur Liste der Rad $ct4rel->{$id}->{barcode} Servicearbeiten $ct4rel->{$id}->{main_id}"},"$node_names")), "\n";
      }else{
      	    print $q->td({-class=>'tdtxt',-colspan=>1,-style=>"$set_style", -nowrap=>1},"$node_names"),"\n";
      }
    }else{
      $ecol++;
      print $q->td({-class=>'element',-colspan=>1,-style=>"$set_style"},""),"\n";
    }
    $ecol++;

    #2.Spalte
    my $ny="";
    if($table eq "contentadr" && ($ct4rel->{$id}->{int12} || !$ct4rel->{$id}->{int04} || !$ct4rel->{$id}->{int13})){
      print $q->td({-class=>'tdicon',-style=>"$set_style color:red;"},"&bull;","\n") if($ecol <= "2");
    }elsif($table eq "contenttrans" && (($ct4rel->{$id}->{txt28} && $ct4rel->{$id}->{txt28} =~ /error/i) || ($ct4rel->{$id}->{int14} && $ct4rel->{$id}->{int14} >= 1))){
      print $q->td({-class=>'tdicon',-style=>"$set_style color:red;"},"&bull;","\n") if($ecol <= "2");
    }elsif($table eq "content" && $ct4rel->{$id}->{template_id} == 227){
     my $dir_thumb = "$varenv{data}/$node_meta->{main_id}-thumb/$ct4rel->{$id}->{c_id}"; 
     my @pics = $lb->read_dirfiles($dir_thumb,"\.JPG|\.PNG","file","");
     my $dir_main = "$varenv{data}/$node_meta->{main_id}/$ct4rel->{$id}->{c_id}"; 
     my @docs = $lb->read_dirfiles($dir_main,"\.HTML","file","");

     print "<td class='tdtxt' style='$set_style' nowrap>\n";

     if( -d "$dir_thumb" && scalar(@pics) > 0){
      foreach(@pics){
       print $q->span($q->img({-style=>'max-width:75px;',-src=>"$varenv{metahost}/data/$node_meta->{main_id}-thumb/$ct4rel->{$id}->{c_id}/$_"},"$_")),"\n";
       print $q->br(),"\n" if(scalar(@pics) > 1);
      }
     }
     print "</td>\n";

    }else{
      $ny = $nr if($path =~ /letzte Suche/);
      print $q->td({-class=>'tdint',-style=>"$set_style color:grey;"},"$ny"),"\n" if($ecol <= "2");
    }

    #Tablecontent (parameter)
    $k=0;
    foreach (@tpl_order){
      $k++;
      my ($key,$val,$size,$colorize) = split /=/,$_;     
      my $txtstyle = "text-align:left;";
      if($key =~ /barcode|c_id|ct_name|int|time\d+|state|sort|public/){
        $txtstyle = "text-align:right;max-width:8em;";
      }
      $ct4rel->{$id}->{$key} = $lb->time4de($ct4rel->{$id}->{$key},"1") if($key =~ /time$/);
      $ct4rel->{$id}->{$key} = $q->unescapeHTML($ct4rel->{$id}->{$key});# if($key !~ /byte/);
      my $br4text = $R::node2edit || "";
      $ct4rel->{$id}->{$key} = $lb->newline($ct4rel->{$id}->{$key},"","$br4text");
      $set_style4nr = $set_style;
      $set_style4nr="background-color:#e3dbc9;" if(($key eq "barcode") || ($key eq "int04"));
      $ct4rel->{$id}->{$key} =~ s/\./,/ if($key =~ /int/);

	if($key eq "c_id" && $val eq "raw"){

            my $pos_hash = $ct4rel->{$id};
            my $pos_details = "";
            foreach my $did (sort keys (%{$pos_hash})){
          	my $teaser = "";
          	if($pos_hash->{$did}){
                  $teaser = substr($pos_hash->{$did},0,30);
          	}
                $pos_details .= $did . " = " . $teaser . "</br>";
            }

            my $pos_id = $q->div({-class=>"popup",-onclick=>"toggle_box('$id')"},"$ct4rel->{$id}->{c_id}", $q->span({-class=>"popuptext",-id=>"$id"},"$pos_details"));
	    print $q->td({-class=>'tdtxt',-style=>"font-weight:normal;$set_style4nr"},"$pos_id"),"\n";	
	}
	elsif($key =~ /ct_name|c_id/ && $ct4rel->{$id}->{$key}){
	  if($table =~ /contenttrans|contenttver/){
	    print $q->td({-class=>'tdint',-style=>"font-weight:normal;$set_style4nr"},$q->a({-class=>"linknav3",-href=>"?ct_trans=open\&mode=manager\&c_id4trans=$ct4rel->{$id}->{c_id}\&tpl_id4trans=$ct4rel->{$id}->{template_id}\&kind_of_trans=$node_meta->{node_name}\&owner=$users_dms->{u_id}\&offset=$offset\&limit=$limit",-title=>"Terminal öffnen"},"$ct4rel->{$id}->{$key}"));
	  }elsif($table =~ /content$|contentadr|contentnel/){
	      print $q->td({-class=>"tdint",-style=>"font-weight:normal;$set_style4nr"},$q->a({-class=>"linknav3",-href=>"?node2edit=editpart\&mode=manager\&rel_id=$ct4rel->{$id}->{rel_id}\&offset=$offset\&limit=$limit",-title=>"Terminal öffnen"},"$ct4rel->{$id}->{$key}")),"\n";
	  }elsif(($node_meta->{tpl_order} !~ /barcode/) || ("$ct4rel->{$id}->{$key}" ne "$ct4rel->{$id}->{barcode}")){
            print $q->td({-class=>'tdint',-style=>"font-weight:bold;$set_style4nr"},"$ct4rel->{$id}->{$key}"),"\n"; 
	  }else{
            print $q->td({-class=>'tdint',-style=>"font-weight:bold;$set_style4nr"},""),"\n"; 
	  }
	}elsif($key =~ /txt01/ && $table =~ /contenttrans|contenttver/){
	    print $q->td({-class=>'tdtxt',-style=>"font-weight:normal;$set_style4nr"},$q->a({-class=>"linknav3",-href=>"?ct_trans=open\&mode=manager\&c_id4trans=$ct4rel->{$id}->{c_id}\&tpl_id4trans=$ct4rel->{$id}->{template_id}\&kind_of_trans=$node_meta->{node_name}\&owner=$users_dms->{u_id}\&offset=$offset\&limit=$limit",-title=>"Terminal öffnen"},"$ct4rel->{$id}->{$key}")),"\n";
	
	}elsif($key =~ /txt01/ && $table =~ /content$|contentadr/){# && $lock_system != 3){
	  my $txt01 = "---";
	  $txt01 = "$ct4rel->{$id}->{$key}" if($ct4rel->{$id}->{$key});
	  print $q->td({-class=>'tdtxt',-style=>"font-weight:normal;$set_style4nr"},$q->a({-class=>"linknav3",-href=>"?node2edit=editpart\&mode=manager\&rel_id=$ct4rel->{$id}->{rel_id}\&offset=$offset\&limit=$limit",-title=>"Terminal öffnen"},"$txt01")),"\n";
	}elsif($key =~ /barcode/ && $table =~ /content$|contentadr/ && $ct4rel->{$id}->{template_id} != 225){# && $lock_system != 3){
	  print $q->td({-class=>'tdint',-style=>"font-weight:normal;$set_style4nr"},$q->a({-class=>"linknav3",-href=>"?node2edit=editpart\&mode=manager\&rel_id=$ct4rel->{$id}->{rel_id}\&offset=$offset\&limit=$limit",-title=>"Terminal öffnen"},"$ct4rel->{$id}->{$key}")),"\n";
	}elsif($key =~ /int04/ && $table eq "content" && $ct4rel->{$id}->{template_id} == 225){# && $lock_system != 3){
	  print $q->td({-class=>'tdint',-style=>"font-weight:normal;$set_style4nr"},$q->a({-class=>"linknav3",-href=>"?node2edit=editpart\&mode=manager\&rel_id=$ct4rel->{$id}->{rel_id}\&offset=$offset\&limit=$limit",-title=>"Terminal öffnen"},"$ct4rel->{$id}->{$key}")),"\n";
	}elsif($key =~ /int04/ && $table eq "content" && $ct4rel->{$id}->{template_id} == 205){# && $lock_system != 3){
	  print $q->td({-class=>'tdint',-style=>"font-weight:normal;"},$q->a({-class=>"linknav3",-style=>"",-href=>"?detail_search=1&s_int04=$ct4rel->{$id}->{$key}",-title=>"Rad Warenstamm nach Station filtern"},"$ct4rel->{$id}->{$key}"));
        }elsif($key =~ /date_time/){
	  $ct4rel->{$id}->{start_time} = $lb->time4de($ct4rel->{$id}->{start_time},"1") if($ct4rel->{$id}->{start_time});
	  $ct4rel->{$id}->{end_time} = $lb->time4de($ct4rel->{$id}->{end_time},"1") if($ct4rel->{$id}->{end_time});
          print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style", -nowrap=>1},"$ct4rel->{$id}->{start_time} - $ct4rel->{$id}->{end_time}"),"\n";
        }elsif($key =~ /int01/ && ($node_meta->{parent_id} == $dbt->{shareedms_conf}->{faktura} || $node_meta->{main_id} == $dbt->{shareedms_conf}->{faktura})){
          if($ct4rel->{$id}->{state} eq "Kassenbestand"){
            print $q->td({-class=>'tdint',-style=>"$txtstyle $set_style"},""),"\n";
	  }else{
	    my $betrag = $ct4rel->{$id}->{$key} || "&sum; $sum_pos";# pre counting
	    $betrag = $ct4rel->{$id}->{$key} if($v_journal);
	    $betrag =~ s/\./,/;
            print $q->td({-class=>'tdint',-style=>"$txtstyle $set_style"},"$betrag"),"\n";
	  }
        }elsif($key =~ /int02/ && $ct4rel->{$id}->{state} eq "Tagesabschluss"){
          print $q->td({-class=>'tdint',-style=>"$txtstyle $set_style"},""),"\n";
	}elsif($key =~ /int03/ && "$table" eq "contenttrans"){ 
	  print $q->td({-class=>'tdint',-style=>"$txtstyle $set_style"},""),"\n";
        }elsif($key =~ /mtime/ && $ct4rel->{$id}->{close_time}){
	  my $close = "<br />$close_time TaAb.";
	  my $payment = "";
	  $payment = "<br />$payment_time ZaEi." if($payment_time);
          print $q->td({-class=>'tdint',-style=>"$txtstyle $set_style"},"$ct4rel->{$id}->{$key} $close $payment"),"\n";
        }elsif($key =~ /owner/){
          print $q->td({-class=>'tdint',-style=>"$txtstyle $set_style"},"$u_name"),"\n";
        }elsif($key =~ /state/){
	  my $order_state = "";
	  $order_state = "$ct4rel->{$id}->{txt22}" if($ct4rel->{$id}->{txt22});
	  $order_state = "$ct4rel->{$id}->{txt22},<br />" if($ct4rel->{$id}->{txt22} && $ct4rel->{$id}->{$key});
	  my $capture_state = "<span>$ct4rel->{$id}->{$key}</span>";
	  $capture_state = "<span style='color:red;'>$ct4rel->{$id}->{$key}</span>" if($ct4rel->{$id}->{$key} =~ /payone/ && $ct4rel->{$id}->{int14} >= 1);
          print $q->td({-class=>'tdint',-style=>"$txtstyle $set_style"},"$order_state $capture_state"),"\n";
        }elsif($ct4rel->{$id}->{template_id} =~ /205|225/ && $key eq "int10"){#bike_state
	  my $colorize = "";
	  $colorize = "color:#c63e3e;" if($ct4rel->{$id}->{$key} == 2 || $ct4rel->{$id}->{$key} == 3);
	  $colorize = "color:#bd2424;" if($ct4rel->{$id}->{$key} == 4 || $ct4rel->{$id}->{$key} == 5 || $ct4rel->{$id}->{$key} == 6);
 	  print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style $colorize"},"$dbt->{copri_conf}->{bike_state}->{$ct4rel->{$id}->{$key}}"),"\n";
        }elsif($ct4rel->{$id}->{template_id} == 205 && $key eq "int20"){#lock_state
	  my $colorize = "";
	  $colorize = "color:#c63e3e;" if($ct4rel->{$id}->{$key} == 2 || $ct4rel->{$id}->{$key} == 3);
 	  print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style $colorize"},"$dbt->{copri_conf}->{lock_state}->{$ct4rel->{$id}->{$key}}"),"\n";
        }elsif($ct4rel->{$id}->{template_id} == 205 && $key eq "int11"){#lock_system
 	  print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style"},"$dbt->{copri_conf}->{lock_system}->{$ct4rel->{$id}->{$key}}"),"\n";
        }elsif($ct4rel->{$id}->{template_id} == 210 && $key eq "int18"){#sharing_type
 	  print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style"},"$dbt->{copri_conf}->{sharing_type}->{$ct4rel->{$id}->{$key}}"),"\n";
        }elsif($ct4rel->{$id}->{template_id} == 210 && $key eq "int12"){
          my $flotten = "";
          foreach my $rid (sort { $bike_nodes->{$a}->{node_name} cmp $bike_nodes->{$b}->{node_name} } keys (%$bike_nodes)){
             $flotten .= "$bike_nodes->{$rid}->{node_name} - $bike_nodes->{$rid}->{main_id}<br />" if($ct4rel->{$id}->{$key} =~ /$bike_nodes->{$rid}->{main_id}/);
          }
          print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style"},"$flotten"),"\n";
        }elsif($ct4rel->{$id}->{template_id} == 225 && $key eq "txt24"){
          my $flotten = "";
          foreach my $rid (sort { $bike_nodes->{$a}->{node_name} cmp $bike_nodes->{$b}->{node_name} } keys (%$bike_nodes)){
             $flotten .= "$bike_nodes->{$rid}->{node_name} - $bike_nodes->{$rid}->{main_id}<br />" if($ct4rel->{$id}->{$key} =~ /$bike_nodes->{$rid}->{main_id}/);
          }
          print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style"},"$flotten"),"\n";
        }elsif($ct4rel->{$id}->{template_id} == 228 && $key =~ /int21|int22/){
          my $bonustarif = "";
          foreach my $rid (sort { $tariff_all->{$a}->{barcode} <=> $tariff_all->{$b}->{barcode} } keys (%$tariff_all)){
            $bonustarif .= "$tariff_all->{$rid}->{barcode} - $tariff_all->{$rid}->{ct_name}<br />" if($ct4rel->{$id}->{$key} == $tariff_all->{$rid}->{barcode});
          }

          print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style"},"$bonustarif"),"\n";
 
	#for all other withot matching integer type condition
        }elsif($key =~ /int|barcode|save|time$|sort|public/){
          print $q->td({-class=>'tdint',-style=>"$txtstyle $set_style"},"$ct4rel->{$id}->{$key}"),"\n";

        }elsif($key =~ /txt01/ && $v_journal){
	  my $txt01 = "$ct4rel->{$id}->{$key}";
	  $txt01 = "<b>$txt01</b>" if($ct4rel->{$id}->{state} eq "Kassenbestand");
          print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style"},"$txt01"),"\n";
        }elsif($key =~ /txt03/ && $ct4rel->{$id}->{template_id} eq "227"){
          print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style"},$q->a({-style=>"color:black;",-href=>"$varenv{metahost}/site/$ct4rel->{$id}->{$key}", -target=>'_blank',-title=>'download',-type=>'application/octet-stream'}, $q->img({-src=>"$varenv{metahost}/glyphicons/file-any.png", -style=>'height:1.3em;'}),"$ct4rel->{$id}->{$key}")),"\n";
        }elsif($key =~ /txt00/ && $v_journal){
          print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style"},"$ct4rel->{$id}->{$key}"),"\n";
        }elsif($key =~ /txt08/ && $table eq "contentadr"){
          print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style"},"$ct4rel->{$id}->{$key}"),"\n";
	 #color code
        }elsif($ct4rel->{$id}->{template_id} == 205 && $key eq "txt23"){
	  my @service_code = split(/\s/,$ct4rel->{$id}->{$key});
	  my $color_code = "";
	  foreach(@service_code){
	    $color_code .= "<span class='coloricon' style='background-color:$_;'></span>";
	  }
          print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style"},"$color_code"),"\n";
	 #teaser shorts longtext
        }elsif(($table eq "contentadr" && $key =~ /txt29/) || ($table eq "content" && $key =~ /txt04/) || ($table eq "contenttrans" && $key =~ /txt23/)){
	  my $teaser = "";
	  if($ct4rel->{$id}->{$key}){
	  	$teaser = substr($ct4rel->{$id}->{$key},0,50) . " ..."; 
	  }
          print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style; min-width:200px;"},"$teaser"),"\n";

	 #default to anything else
        }elsif($key =~ /ct_name|node|txt|uri/){
	  $txtstyle = "text-align:left;";
          print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style"},"$ct4rel->{$id}->{$key}"),"\n";
        }elsif($key =~ /byte/){
	  #$ct4rel->{$id}->{$key} =~ s/\\x//;
	  my $K_int = $ct4rel->{$id}->{$key};
	  #$K_int =~ s/(.)/sprintf( "%x", ord($1))/eg;
	  my $K_int = unpack "H*", $ct4rel->{$id}->{$key};
	  $txtstyle = "text-align:left;";
          print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style"},"$K_int"),"\n";
        }elsif($key =~ /time\d+/){
	  $txtstyle = "text-align:right;";
	  $ct4rel->{$id}->{$key} =~ s/:00$//;
          print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style"},"$ct4rel->{$id}->{$key}"),"\n";
 
        }
     }
    }

    #sub-table-row for rent Calendar
    my $cal_count = 0;
    foreach my $ctid (sort { $cttpos->{$b}->{end_time} cmp $cttpos->{$a}->{end_time} } keys (%$cttpos)){
     if($ct4rel->{$id}->{c_id} == $cttpos->{$ctid}->{cc_id}){	
	$cal_count++;
	if($cal_count <= 1){
    	 my $scale_color = "#86cbd7;";
	 my $calement = "calement_86cbd7";
	 if($ct4rel->{$id}->{int13} == $cttpos->{$ctid}->{int13} && $cttpos->{$ctid}->{int10} == 7){
	   $scale_color = "#f0536e;";
	   $calement = "calement_f0536e";
 	 }
	
      	my ($year_st,$mon_st,$day_st,$hh_st,$mm_st) = $lb->split_date($cttpos->{$ctid}->{start_time}) if($cttpos->{$ctid}->{start_time});
      	my ($year_en,$mon_en,$day_en,$hh_en,$mm_en) = $lb->split_date($cttpos->{$ctid}->{end_time}) if($cttpos->{$ctid}->{end_time});

      	if($year_st && $mon_st && $day_st && $hh_st && $mm_st && $year_en && $mon_en && $day_en && $hh_en && $mm_en){
          #generate px for rent scale

          my $start_nr = $year_st . $mon_st . $day_st . $hh_st . $mm_st;
          my $end_nr = $year_en . $mon_en . $day_en . $hh_en . $mm_en;
          my $day_stpx = 0;
          my $rent_day_px = 0;
          my $time_style;
 	  if($start_nr <= $end_nr){
          	($day_stpx,$rent_day_px) = $lb->rent_scale($users_dms,$year_st,$mon_st,$day_st,$hh_st,$mm_st,$year_en,$mon_en,$day_en,$hh_en,$mm_en);
          }else{
  	        $time_style="color:red;";
          }
	  print $q->Tr();
	  print $q->td({-style=>'background-color:silver;'},""),"\n";
	  print $q->td({-class=>"$calement",-colspan=>2,-style=>"$set_style;"},""),"\n";
	  print "<td class='$calement' colspan='$tdcal' style='$set_style;'>\n";
	  #print $q->div({-style=>"position:absolute;margin-left:$daymarker;border-right: solid thin #86cb00;height:1.7em;"},"&nbsp;"),"\n";# if("$mon" eq "$mon_today");
	  my $calpath = "Mietjournal";
	  if($ct4rel->{$id}->{int13} == $cttpos->{$ctid}->{int13} && $cttpos->{$ctid}->{int10} =~ /7|8/){
	    $calpath = "Alarmjournal";
  	    $time_style="color:red;";
  	  }
	  print $q->div({-style=>"position:static;margin-left:$day_stpx;width:$rent_day_px;background-color:$scale_color"},$q->a({-class=>"linknav3",-style=>"$time_style",-href=>"$varenv{wwwhost}/DMS/$calpath/?cttpos_id=$cttpos->{$ctid}->{c_id}",-title=>"Im $calpath öffnen"},"$hh_en:$mm_en")),"\n";
	  print "</td>\n";
	  print $q->Tr();
	  print $q->td({-style=>'padding-top:1px;'},""),"\n";
       }
      }
     }
    }
  }
 }#journal offen ende

 
 if(($nr > 0) && ($v_journal || $R::v_abschluss) && !$R::rel_id){
   $k="9" if(!$k);
   my $m = $k;
   $close_time = $R::s_mtime if($R::s_mtime);
   print $q->Tr();
   print $q->td({-style=>"background-color:silver;"},""),"\n";
   print $q->td({-class=>'tdtxt',-style=>'text-align:center;background-color:silver;',-colspan=>"$m"},"Gesamt Umsätze");
   print $q->td({-class=>'tdsum',-colspan=>"1",-style=>'background-color:silver;'},"");
   print $q->td({-class=>'tdsum',-colspan=>"1",-style=>'background-color:silver;'},"");


   if(1==1){
    if($sum_saldo != 0){
     $sum_saldo = sprintf('%.2f',$sum_saldo);
     print $q->Tr();
     print $q->td({-style=>"background-color:silver;"},""),"\n";
     print $q->td({-class=>'tdint',-colspan=>"$m"},"Saldo Summe");
     print $q->td({-class=>'tdint',-colspan=>"1",-nowrap=>"1"},"");
     print $q->td({-class=>'tdint',-colspan=>"1",-nowrap=>"1"},"$sum_saldo €");
    }
    if($sum_opos != 0){
     $sum_opos *= -1;
     $sum_opos = sprintf('%.2f',$sum_opos);
     $sum_success += $sum_opos;
     print $q->Tr();
     print $q->td({-style=>"background-color:silver;"},""),"\n";
     print $q->td({-class=>'tdint',-colspan=>"$m"},"OPOS Summe");
     print $q->td({-class=>'tdint',-colspan=>"1",-nowrap=>"1"},"$sum_opos €");
     print $q->td({-class=>'tdint',-colspan=>"1",-nowrap=>"1"},"");
    }
    if($sum_ueb != 0){
     $sum_ueb = sprintf('%.2f',$sum_ueb);
     $sum_success += $sum_ueb;
     print $q->Tr();
     print $q->td({-style=>"background-color:silver;"},""),"\n";
     print $q->td({-class=>'tdint',-colspan=>"$m"},"Überweisung Summe");
     print $q->td({-class=>'tdint',-colspan=>"1",-nowrap=>"1"},"$sum_ueb €");
     print $q->td({-class=>'tdint',-colspan=>"1",-nowrap=>"1"},"");
    }
    if($sum_SEPApayone != 0){
     $sum_SEPApayone = sprintf('%.2f',$sum_SEPApayone);
     $sum_success += $sum_SEPApayone;
     print $q->Tr();
     print $q->td({-style=>"background-color:silver;"},""),"\n";
     print $q->td({-class=>'tdint',-colspan=>"$m"},"SEPA-Lastschrift (payone) Summe");
     print $q->td({-class=>'tdint',-colspan=>"1",-nowrap=>"1"},"$sum_SEPApayone €");
     print $q->td({-class=>'tdint',-colspan=>"1",-nowrap=>"1"},"");
    }
    if($sum_CCpayone != 0){
     $sum_CCpayone = sprintf('%.2f',$sum_CCpayone);
     $sum_success += $sum_CCpayone;
     print $q->Tr();
     print $q->td({-style=>"background-color:silver;"},""),"\n";
     print $q->td({-class=>'tdint',-colspan=>"$m"},"Kreditkarte (payone) Summe");
     print $q->td({-class=>'tdint',-colspan=>"1",-nowrap=>"1"},"$sum_CCpayone €");
     print $q->td({-class=>'tdint',-colspan=>"1",-nowrap=>"1"},"");
    }
    if($sum_ausfall != 0){
     $sum_ausfall *= -1;
     $sum_ausfall = sprintf('%.2f',$sum_ausfall);
     $sum_success += $sum_ausfall;
     print $q->Tr();
     print $q->td({-style=>"background-color:silver;"},""),"\n";
     print $q->td({-class=>'tdint',-colspan=>"$m"},"Zahlungsausfall Summe");
     print $q->td({-class=>'tdint',-colspan=>"1",-nowrap=>"1"},"$sum_ausfall €");
     print $q->td({-class=>'tdint',-colspan=>"1",-nowrap=>"1"},"");
    }
    if($sum_success != 0){
     $sum_success = sprintf('%.2f',$sum_success);
     print $q->Tr();
     print $q->td({-style=>"background-color:silver;"},""),"\n";
     print $q->td({-class=>'tdsum',-colspan=>"$m"},"Erfolgreiche Zahlungen");
     print $q->td({-class=>'tdsum',-colspan=>"1",-nowrap=>"1"},"$sum_success €");
     print $q->td({-class=>'tdsum',-colspan=>"1",-nowrap=>"1"},"");
    }

     if($users_dms->{u_id}){
      if($v_journal =~ /bericht/ && $j_exist->{main_id}){
      	print $q->Tr();
      	print $q->td({-style=>"background-color:silver;"},""),"\n";
      	print $q->td({-style=>"background-color:silver;",-class=>'tdsum',-colspan=>"$m"},"Tagesabschluss"),"\n";
      	print $q->td({-style=>"background-color:silver;",-class=>'tdsum',-colspan=>"1",-nowrap=>"1"},$but->checkbox("1","close_trans","","Tagesbericht abschließen und in das Verkaufsjournal senden"),$but->singlesubmit("v_abschluss","Speichern")),"\n";
        print $q->td({-class=>'tdsum',-colspan=>"1",-nowrap=>"1"},"");
      }
     }

   }
 }
 
 if($varenv{dbname} eq "sharee_kn" && $node_meta->{tpl_id} == 228){
   if( -d "$opdir_dms/ftp/SWK_codes"){
    my @pdfs = $lb->read_dirfiles("$opdir_dms/ftp/SWK_codes","got_last\.csv","file","");
    foreach(@pdfs){
       print $q->Tr(),"\n";
       print $q->td({-class=>"tdtxt",-colspan=>10},"SWK Bonunsnummern download ",$q->a({-class=>'linknav3',-href=>"$varenv{wwwhost}/FileOut?file=$_&sessionid=$coo", -target=>'_default', -title=>'Download',-type=>'application/octet-stream'}, $q->img({-src=>"$varenv{metahost}/glyphicons/file-any.png", -style=>'width:25px;'}), "\"$_\"")),"\n";
    }
   }
 }

 print $q->end_table;
 
 print $q->hidden(-name=>'tpl_id',-override=>'1', -value=>"$tpl_id");
 print $q->end_form;

 my $offset_nr = $offset + $nr;
 my $counter = $rows;
 print $q->div({-style=>'float:left;padding:6px 0 0 10px;'},"Zeile: $offset - $offset_nr / $counter");

 #backward | forward
 print "<div style='padding:6px 0 0 10px;'>\n";
 print $q->a({-class=>"linknav1",-href=>"?go=backward_list;offset=$offset;limit=$limit",-title=>'backward'},"&larr; ") if($offset >= $limit);
 print $q->a({-class=>"linknav1",-href=>"?go=forward_list;offset=$offset;limit=$limit",-title=>'forward'}," &rarr;") if($counter >= $limit-10); #if($rows > $limit && $nr > 0);
 print "</div>\n";

 print $q->div({-style=>''}, "&nbsp;"),"\n";
 print $q->div({-style=>'padding:6px 0 6px 20px;text-decoration:underline;'}, "Symbol Legende"),"\n";
 if($table eq "contentadr"){
   print $q->div({-style=>'padding:0 20px;font-style:italic;'}, $q->span({-style=>'padding:0.1em 0.8em;',-style=>'color:red;'},"&bull;"), "Der Verleih ist nicht freigeschaltet"),"\n";
 }elsif($table eq "contenttrans"){
   print $q->div({-style=>'padding:0 20px;font-style:italic;'}, $q->span({-style=>'padding:0.1em 0.8em;',-style=>'color:red;'},"&bull;"), "payone error"),"\n";
 }elsif($table eq "content" && $node_meta->{tpl_id} =~ /205|225/){
   print $q->div({-style=>'padding:0 20px;font-style:italic;'}, $q->span({-style=>'padding:0.1em 0.8em;'},$q->img({-src=>"$varenv{metahost}/glyphicons/glyphicons-440-wrench.png", -style=>'height:1.3em;'})), "Service-Wartung"),"\n";
 }
 print $q->div({-style=>'padding:0.5em;'}, "&nbsp;"),"\n";

 print "</div>";
 
 #if($varenv{wwwhost} !~ /regiox/ && $node_meta->{main_id} > 300000 && $rows =~ /\d/){
 #  	$db->updater("nodes","main_id",$node_meta->{main_id},"int10",$rows);
 #}

 #letzte Suche
 #if(!$v_journal && $R::detail_search && ref($searchref) eq "HASH" && $nr =~ /\d/){
 #	$db->updater("nodes","main_id","300023","int10",$nr);
 #}
 #print Dumper($node_meta);
 return $return;
}
1;
