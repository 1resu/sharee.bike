package Karte;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
use strict;
use warnings;
use CGI ':standard';
use Lib::Config;
use Mod::DBtank;
use Mod::APIfunc;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
 my $node_meta = shift;
 my $users_dms = shift || "";
 my $mode = shift || "";
 my $varenv = shift;
 my $users_sharee = shift || { c_id => 0 };
 my $return = shift || "";

 my $q = new CGI;
 my $dbt = new DBtank;
 my $apif = new APIfunc;

 my $api_file = "/var/www/copri4/shareeconf/apikeys.cfg";
 my $aconf = Config::General->new($api_file);
 my %apikeyconf = $aconf->getall;

 my $api_return = { authcookie => '' };
 my $coo = $q->cookie(-name=>'domcookie') || $R::sessionid;
 ($api_return,$users_sharee) = $apif->auth_verify($q,$coo,"");

 my $initMap = $dbt->{website}->{$varenv->{syshost}}->{initMap};
 my $map_zoom = $dbt->{website}->{$varenv->{syshost}}->{map_zoom} || "10";
 my $project = "all";
 my $uri_request = $dbt->{primary}->{sharee_primary}->{primaryDMS};

  if($dbt->{website}->{$varenv->{syshost}}->{merchant_id}){
   $api_return->{authcookie} = $dbt->{website}->{$varenv->{syshost}}->{merchant_id} if(!$api_return->{authcookie});
   $project = $dbt->{website}->{$varenv->{syshost}}->{project} if($dbt->{website}->{$varenv->{syshost}}->{project});
   $uri_request = $dbt->{website}->{$varenv->{syshost}}->{operatorWeb};
  }elsif($dbt->{operator}->{$varenv->{dbname}}->{merchant_id}){
   $api_return->{authcookie} = $dbt->{operator}->{$varenv->{dbname}}->{merchant_id} if(!$api_return->{authcookie});
   $initMap = $dbt->{operator}->{$varenv->{dbname}}->{initMap} if($dbt->{operator}->{$varenv->{dbname}}->{initMap});
   $map_zoom = $dbt->{operator}->{$varenv->{dbname}}->{map_zoom} if($dbt->{operator}->{$varenv->{dbname}}->{map_zoom});
   $project = $dbt->{operator}->{$varenv->{dbname}}->{project} if($dbt->{operator}->{$varenv->{dbname}}->{project});
   $uri_request = $dbt->{operator}->{$varenv->{dbname}}->{operatorDMS};
  }elsif($dbt->{primary}->{$varenv->{dbname}}->{merchant_id}){
   $api_return->{authcookie} = $dbt->{primary}->{$varenv->{dbname}}->{merchant_id} if(!$api_return->{authcookie});
   $initMap = $dbt->{primary}->{$varenv->{dbname}}->{initMap} if($dbt->{primary}->{$varenv->{dbname}}->{initMap});
   $uri_request = $dbt->{primary}->{$varenv->{dbname}}->{primaryDMS};
  }

  my $icon_green = "Open_Green.png";
  my $icon_red = "Open_Red.png";
  my $icon_blue = "Open_Blue.png";
  my $mapheight = "600px;";
  if($project eq "Bayern"){
   $icon_green = "LastenradBay_Standortpfeil_gruen_03a.png";
   $icon_red = "LastenradBay_Standortpfeil_rot_03a.png";
   $icon_blue = "LastenradBay_Standortpfeil_blau_03a.png";
   $mapheight = "800px;";
  }

 $users_sharee->{c_id} = "0" if(!$users_sharee->{c_id});
 $initMap =~ s/\s//g;
 my ($lat,$lng) = split(/,/,$initMap);
 my $kmlfile = "";
 my $filesize = 0;
 my $preserve_view = "false";

 require "Mod/KMLout.pm";
 $kmlfile = Mod::KMLout::kmlGenerator($api_return,$varenv,$users_sharee);
 my $filesize = -s "$varenv->{metahost}/xml/$kmlfile";
 #
 #used by empty station card like without public stations
 #setting to true to keeps lat,lng and zoom
 $preserve_view = "true" if($filesize < 200);

 print $q->div({-class=>"content2_contact"},"Hier können Lastenfahrräder gemietet werden:"),"\n"; 
 print "<div style='background-color:#cccccc;width:100%;margin-top:1em;'>\n";

 print "<div id='map' style='height:$mapheight'></div>\n";
  
print<<EOF 
 <script>
  
   function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        scrollwheel: false,
	//draggable: !("ontouchend" in document),
        center: {lat: $lat, lng: $lng},
        zoom: $map_zoom
    });

    var ctaLayer = new google.maps.KmlLayer({
      position: map.getCenter(),
      preserveViewport: $preserve_view,
      //url: '$uri_request/KMLout?authcookie=$api_return->{authcookie}&dynamo=' + (new Date()).getTime(),
      url: '$varenv->{metahost}/xml/$kmlfile',
      map: map
    });
    console.log(ctaLayer);

   };


    </script>
    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=$apikeyconf{googlemap}->{google_key}&callback=initMap">
    </script>
EOF
;

print "</div>\n";

#print $q->div({-class=>"content2_legende", -style=>'font-weight:bold;'}, "&bull; Ist ein Lastenrad an einer Mietradstation verfügbar?"),"\n";
print $q->div({-class=>"content2_legende"}, $q->img({-style=>'height:30px;', -src=>"$varenv->{metahost}/img/$icon_green"}), "Lastenrad verfügbar"),"\n";
print $q->div({-class=>"content2_legende"}, $q->img({-style=>'height:30px;', -src=>"$varenv->{metahost}/img/$icon_red"}), "Lastenrad nicht verfügbar"),"\n";
#print $q->div({-class=>"content2_legende"}, "&nbsp;"),"\n";

 print "$initMap|$varenv->{dbname}|$api_return->{authcookie}|$users_sharee->{c_id}|$kmlfile" if($users_dms->{u_id} == $dbt->{copri_conf}->{superu_id});

}
1;

