package Libenzdb;
#
#Deprecated module, please use DBtank.pm
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
#uncomment for perl -cw src/Mod/Libenzdb.pm
#use lib qw(/var/www/copri4/main/src);
#
use strict;
use warnings;
use POSIX;
use DBI;
use CGI;
use CGI ':standard';
use Text::CSV_XS;
use Spreadsheet::WriteExcel;
use Lib::Config;
use Scalar::Util qw(looks_like_number);
use Digest::MD5 qw(md5 md5_hex);
use Date::Calc qw(check_date);
use Mod::DBtank;
use Data::Dumper;


my $cf = new Config;
my $dbt = new DBtank;
my $q = new CGI;
$q->import_names('R');

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

my $today = strftime("%d.%m.%Y",localtime(time));
my $date_time = strftime("%d.%m.%Y %H:%M",localtime(time));
my %varenv = $cf->envonline();

my $dbh = $dbt->dbconnect();

sub dbdisconnect {
 my $self = shift;
 $dbh->disconnect;
 return;
}


#get table column info
sub table_info(){
  my $self = shift;
  my ($table,$dbinit) = @_;
  $dbh = $dbt->dbconnect() if($dbinit);#because of ajax
  my $sth = $dbh->prepare("SELECT attnum,attname FROM pg_class c join pg_attribute a on c.oid = a.attrelid WHERE c.relname = '$table' AND a.attnum >= 0;");
  my $rc = $sth->execute();
  my $column = $sth->fetchall_hashref("attnum");
  $sth->finish;
  $dbh->disconnect if($dbinit);
  return $column;
}

# new node
sub insert_node(){
  my $self = shift;
  my ($parent_id,$main_id,$node_name,$lang,$sort,$template_id,$c_id,$owner) = @_;

 my $node_path = $node_name; 
 #für sub-menü hack
 $node_path =~ s/\!/\//; 
 $node_name =~ s/\!.*//; 
 #split name path 
 ($node_name,$node_path) = split(/\?/,$node_name) if($node_name =~ /\?/); 

  $template_id = "0" if(!$template_id);
  $c_id = "0" if(!$c_id);
  $sort = "0" if(!$sort);
  $owner = "0" if(!$owner);
  my ($sth2,$rc);
   $sth2 = $dbh->prepare("INSERT INTO nodes (main_id,parent_id,lang,node_name,node_path,n_sort,owner) VALUES('$main_id','$parent_id','$lang',trim('$node_name'),trim('$node_path'),'$sort','$owner')");
   $rc = $sth2->execute();
   $sth2 = $dbh->prepare("INSERT INTO relation (main_id,template_id,content_id,lang,change) VALUES('$main_id','$template_id','$c_id','$lang','now()') RETURNING rel_id");
    $rc = $sth2->execute();

  my $last_id;
  $sth2->bind_columns(\$last_id);
  my $rel_id = $sth2->fetchrow_array();

  return $rel_id;
}
##########################


# get next free node
sub get_freenode(){
  my $self = shift;
  my $prefix_id = shift;
  my $s_id = "$prefix_id";
  my $e_id = "$prefix_id";
  $s_id .= "00001";
  $e_id .= "99999";
  my $main_id;
  #print "$s_id - $e_id";
  for (my $x_id=$s_id; $x_id < $e_id; $x_id++){
   my $sth1 = $dbh->prepare("SELECT main_id from nodes where main_id='$x_id'");
   my $rc1 = $sth1->execute();
   $main_id = $sth1->fetchrow_array();
   if(!$main_id){
    #print "$x_id";
    return $x_id;
   }
  }
}
#

# get next free template id
sub get_freetpl(){
  my $self = shift;
  my ($s_id,$e_id) = @_;
  my $tpl_id;
  for (my $x_id=$s_id; $x_id < $e_id; $x_id++){
   my $sth1 = $dbh->prepare("SELECT tpl_id from template where tpl_id='$x_id'");
   my $rc1 = $sth1->execute();
   $tpl_id = $sth1->fetchrow_array();
   if(!$tpl_id){
    return $x_id;
   }
  }
}
#


# Delete node and relation! (without content)
sub delete_node(){
 my $self = shift;
 my ($main_id,$lang) = @_;
 my $sth = $dbh->prepare("DELETE FROM relation WHERE main_id='$main_id' and lang='$lang' and content_id='0'");
 my $rc = $sth->execute();
 $sth = $dbh->prepare("DELETE FROM nodes WHERE main_id='$main_id' and lang='$lang'");
 $rc = $sth->execute();
 #$dbh->commit or die $dbh->errstr;
 $sth->finish();
 return $rc;
}

#all nodes
sub collect_node4all(){
 my $self = shift;
 my ($main_id,$lang,$u_id,$min_main_id,$max_main_id) = @_;
 my $where = "where 1=1";
 #$where .= " and node_public='1'" if(!$u_id && $varenv{mlogic} eq "web"); 
 $where .= " and parent_id = '$main_id'" if($main_id);
 $where .= " and main_id >= '$min_main_id'" if($min_main_id);
 $where .= " and main_id <= '$max_main_id'" if($max_main_id);
 my $sth = $dbh->prepare("SELECT * FROM nodes $where");
 my $rc = $sth->execute();
 my $nodes = $sth->fetchall_hashref("n_id");
 $sth->finish;
 return $nodes;
}

# Nodes einer Ebene
sub collect_node(){
 my $self = shift;
 my ($main_id,$lang,$u_id,$dbinit) = @_;
 $dbh = $dbt->dbconnect() if($dbinit); 
 my $where = "";
 #$where = " and node_public='1'" if(!$u_id && $varenv{mlogic} eq "web"); 
 my $sth = $dbh->prepare("SELECT * FROM nodes WHERE parent_id='$main_id' and lang='$lang' $where");
 my $rc = $sth->execute();
 my $nodes = $sth->fetchall_hashref("main_id");
 $sth->finish;
 $dbh->disconnect if($dbinit);
 return $nodes;
}

# Nodes with relation
sub collect_noderel(){
 my $self = shift;
 my ($main_id,$lang,$u_id,$dbinit) = @_;
 $dbh = $dbt->dbconnect() if($dbinit); 
 my $where="";
 #$where = " and nd.node_public='1'" if(!$u_id && $varenv{mlogic} eq "web"); 
 my $sth = $dbh->prepare("SELECT * FROM nodes nd, relation rel WHERE nd.main_id=rel.main_id and nd.parent_id='$main_id' and nd.lang='$lang' and rel.content_id=0 $where");
 my $rc = $sth->execute();
 my $nodes = $sth->fetchall_hashref("main_id");
 $sth->finish;
 $dbh->disconnect if($dbinit);
 return $nodes;
}


# Nodes recursive
sub collect_noderec(){
 my $self = shift;
 my ($main_id,$lang,$excluded,$u_id,$hashref) = @_;
 $main_id =~ s/,$//;
 my $main_ids = "$main_id,";
 my $main_ids_ref;
 my $main_collect = &collect_node("",$main_id,$lang,$u_id,"","");
 if(ref($main_collect) eq "HASH"){
   #$main_ids_ref = { $main_collect };
   foreach my $id (keys (%$main_collect)){
    if($main_collect->{$id}->{node_name} !~ /$excluded/){
      $main_ids .= "$main_collect->{$id}->{main_id},";
      #print "$main_id:$main_collect->{$id}->{main_id}<br>";
      my $main_collect2 = &collect_node("",$main_collect->{$id}->{main_id},$lang,$u_id,"") if($main_collect->{$id}->{main_id});
      if(ref($main_collect2) eq "HASH"){
       #$main_ids_ref = { $main_collect, $main_collect2 };
       foreach my $id (keys (%$main_collect2)){
        $main_ids .= "$main_collect2->{$id}->{main_id},";
        my $main_collect3 = &collect_node("",$main_collect2->{$id}->{main_id},$lang,$u_id,"") if($main_collect2->{$id}->{main_id});
        if(ref($main_collect3) eq "HASH"){
         #$main_ids_ref = { $main_collect, $main_collect2, $main_collect3 };
	 foreach my $id (keys (%$main_collect3)){
          $main_ids .= "$main_collect3->{$id}->{main_id},";
	 }
	}
       }
      }
    }
   }
  }
  $main_ids =~ s/,$//;
 if($hashref){#disabled and never uesd
   return $main_ids_ref;
 }else{
   return $main_ids;
 }
}

# Nodes 
sub collect_node2(){
 my $self = shift;
 my $main_ids = shift;
 $main_ids =~ s/,$//;
 my $sth = $dbh->prepare("SELECT * FROM nodes WHERE main_id IN ($main_ids)");
 my $rc = $sth->execute();
 my $nodes = $sth->fetchall_hashref("main_id");
 $sth->finish;
 return $nodes;
}

# Nodes with variable
sub collect_node3(){
 my $self = shift;
 my ($column1,$op1,$content1,$column2,$op2,$content2,$main_ids) = @_;
 $main_ids =~ s/,$//;
 my $where = "$column1 $op1 '$content1'";
 $where .= " and $column2 $op2 '$content2'" if($content2);
 $where .= " and main_id IN ($main_ids)";
 my $sth = $dbh->prepare("SELECT * FROM nodes WHERE $where");
 my $rc = $sth->execute();
 my $nodes = $sth->fetchall_hashref("main_id");
 $sth->finish;
 return $nodes;
}

# Nodes Anhand der main_id
sub get_node4all(){
  my $self = shift;
  my ($main_id) = @_;
  my $sth = $dbh->prepare("SELECT * FROM nodes WHERE main_id='$main_id'");
  my $rc = $sth->execute();
  my $nodes = $sth->fetchall_hashref("lang");
  $sth->finish;
  return $nodes;
}

# Node Anhand der main_id für multilang
sub get_node4multi(){
  my $self = shift;
  my ($main_id,$lang,$owner,$parent_id,$dbinit) = @_;
  $dbh = $dbt->dbconnect() if($dbinit);
  $owner=0 if(!$owner);
  my $sel="";
  $sel .= " and owner='$owner'" if($owner > 0);
  $sel .= " and parent_id='$parent_id'" if($parent_id && $parent_id =~ /^\d+$/ && $parent_id > 0);
  $sel .= " order by parent_id $parent_id" if($parent_id && $parent_id =~ /ASC|DESC/);#workaround Verleihräder
  my $sth = $dbh->prepare("SELECT * FROM nodes WHERE main_id='$main_id' and lang='$lang' $sel");
  my $rc = $sth->execute();
  my $node = $sth->fetchrow_hashref();
  $dbh->disconnect if($dbinit);
  return $node;
}


# get child node
sub get_subnode(){
  my $self = shift;
  my ($main_id,$lang,$n_sort) = @_;
  my $where;
  $where = "and n_sort = '$n_sort'" if($n_sort);
  my $sth = $dbh->prepare("SELECT * FROM nodes WHERE parent_id='$main_id' and lang='$lang' $where");
  my $rc = $sth->execute();
  my $node = $sth->fetchrow_hashref();
  return $node;
}


# Node Anhand des node_name und parent_id
sub get_nodeset(){
  my $self = shift;
  my ($parent_id,$node_name,$lang,$dbinit) = @_;
  $dbh = $dbt->dbconnect() if($dbinit);
  $node_name = $q->escapeHTML($node_name);
  my $sel_node_name;
  $sel_node_name = "and (nodes.node_path='$node_name' OR nodes.node_name='$node_name')" if($node_name);
  my $sth = $dbh->prepare("SELECT * FROM nodes, relation WHERE nodes.main_id=relation.main_id $sel_node_name  and nodes.lang='$lang' and nodes.parent_id='$parent_id'");
  my $rc = $sth->execute();
  my $node = $sth->fetchrow_hashref();
  $dbh->disconnect if($dbinit);
  return $node;
}

# Node Anhand des node_name and parent_id
sub get_node(){
  my $self = shift;
  my ($node_name,$lang,$operator,$parent_id) = @_;
  #print "($node_name,$lang,$operator,$parent_id)";
  $node_name = $q->escapeHTML($node_name);
  my $where = "lang='$lang'";
  $where .= " and parent_id $operator '$parent_id'" if($parent_id);
  my $sth = $dbh->prepare("SELECT * FROM nodes WHERE (node_path='$node_name' OR node_name='$node_name') and $where");
  #print "SELECT * FROM nodes WHERE (node_path='$node_name' OR node_name='$node_name') and $where";
  my $rc = $sth->execute();
  my $node = $sth->fetchrow_hashref();
  return $node;
}

# Node main_id and template_id and node_name
sub get_node4rel(){
  my $self = shift;
  my ($main_id,$template_id,$node_name,$content_id) = @_;
  my $where = "no.main_id=rel.main_id";
  $where .= " and rel.template_id=$template_id" if($template_id);
  $where .= " and no.main_id=$main_id" if($main_id);
  $where .= " and no.node_name ilike '$node_name'" if($node_name);
  $where .= " and no.node_public = '1'" if($varenv{wwwhost} =~ /tinkdms/);
  $where .= " and rel.content_id = '0'" if($content_id eq "null");
  my $sth = $dbh->prepare("SELECT * FROM nodes no, relation rel WHERE $where ");
  my $rc = $sth->execute();
  my $node = $sth->fetchrow_hashref();
  return $node;
}


# Node Anhand des node_name und parent-node_name
sub get_node2(){
  my $self = shift;
  my ($parentsel,$node_name,$lang) = @_;
  $node_name = $q->escapeHTML($node_name);
  my $sth = $dbh->prepare("SELECT * FROM nodes WHERE (node_path='$node_name' OR node_name='$node_name') and lang='$lang' and parent_id IN (SELECT main_id from nodes where (node_path='$parentsel' OR node_name='$parentsel') and lang='$lang')");
  my $rc = $sth->execute();
  my $node = $sth->fetchrow_hashref();
  return $node;
}
# Node Anhand des node_name und parent_id
sub get_node3(){
  my $self = shift;
  my ($parent_id,$node_name,$lang) = @_;
  my $sth = $dbh->prepare("SELECT * FROM nodes WHERE node_name='$node_name' and lang='$lang' and parent_id='$parent_id'");
  my $rc = $sth->execute();
  my $node = $sth->fetchrow_hashref();
  return $node;
}

# all templates
sub collect_tpl(){
 my $self = shift;
 my ($tpl_ids,$key) = @_;
 my $where = "where tpl_id != 98 and tpl_id != 97";#without meta-Config and EditNode
 $where .= " and tpl_id IN ($tpl_ids)" if($tpl_ids);
 $where .= " and tpl_order like '%$key%'" if($key);
 my $sth = $dbh->prepare("SELECT * FROM template $where");
 my $rc = $sth->execute();
 my $tpl_all = $sth->fetchall_hashref("tpl_id");
 $sth->finish;
 return $tpl_all;
}

# get template row
sub get_tpl(){
 my $self = shift;
 my ($template_id) = @_;
 $template_id= 0 if(!$template_id);
 my $sth = $dbh->prepare("SELECT * FROM template WHERE tpl_id='$template_id'");
 my $rc = $sth->execute();
 my $tpl = $sth->fetchrow_hashref();
 return $tpl;
}

# get template row
sub get_tpl2(){
 my $self = shift;
 my ($tpl_name) = @_;
 my $sth = $dbh->prepare("SELECT * FROM template WHERE tpl_name='$tpl_name'");
 my $rc = $sth->execute();
 my $tpl = $sth->fetchrow_hashref();
 return $tpl;
}



# get all relation from main_id
sub get_rel4all(){
 my $self = shift;
 my ($main_id) = @_;
 my $sth = $dbh->prepare("SELECT * FROM relation WHERE main_id='$main_id'");
 my $rc = $sth->execute();
 my $rel = $sth->fetchall_hashref("lang");
 $sth->finish;
 return $rel;
}


# get relation row
sub get_relation(){
 my $self = shift;
 my ($main_id,$lang,$rel_id) = @_;
 my $where = "1=1";
 $where .= " and main_id='$main_id' and lang='$lang'" if($main_id);
 $where .= " and rel_id='$rel_id'" if($rel_id);
 my $sth = $dbh->prepare("SELECT * FROM relation WHERE $where");
 my $rc = $sth->execute();
 my $rel = $sth->fetchrow_hashref();
 return $rel;
}

#NO isnt't consistent, because of after delete, we need a empty relation 
#get empty relation row
sub get_rel4empty(){
 my $self = shift;
 my ($main_id,$lang,$content_id,$template_id) = @_;
 my $where = "main_id='$main_id' and lang='$lang'";
 $where .= " and (content_id = '0' OR content_id is null)";
 $where .= " and template_id='$template_id'" if($template_id > 0);
 my $sth = $dbh->prepare("SELECT * FROM relation WHERE $where");
 my $rc = $sth->execute();
 my $rel = $sth->fetchrow_hashref();
 return $rel;
}



# get relation with template
sub get_rel4tpl(){
 my $self = shift;
 my ($main_id,$lang,$content_id,$template_id,$ascdesc,$rel_id,$realct) = @_;
 $content_id=0 if(!$content_id);
 my $where = "WHERE relation.template_id=template.tpl_id and lang='$lang'";
 $where .= " and relation.main_id='$main_id'" if($main_id > 0);
 $where .= " and relation.content_id='$content_id'" if($content_id > 0);
 $where .= " and relation.template_id='$template_id'" if($template_id > 0);
 $where .= " and relation.rel_id='$rel_id'" if($rel_id > 0);
 $where .= " and content_id $realct" if($realct);# MandantConf >0
 $where .= " order by content_id $ascdesc" if($ascdesc);
 my $sth = $dbh->prepare("SELECT * FROM relation,template $where");
 my $rc = $sth->execute();
 my $rel = $sth->fetchrow_hashref();
 return $rel;
}

# get relation, template, nodes
sub get_rel4tpl4nd(){
 my $self = shift;
 my ($main_id,$lang,$content_id,$template_id,$ascdesc,$rel_id) = @_;
 $content_id=0 if(!$content_id);
 my $where = "WHERE nodes.main_id=relation.main_id and relation.template_id=template.tpl_id";
 $where .= " and relation.main_id='$main_id'" if($main_id > 0);
 $where .= " and relation.content_id='$content_id'" if($content_id > 0);
 $where .= " and relation.template_id='$template_id'" if($template_id > 0);
 $where .= " and relation.rel_id='$rel_id'" if($rel_id > 0);
 $where .= " order by content_id $ascdesc" if($ascdesc);
 my $sth = $dbh->prepare("SELECT * FROM relation,template,nodes $where");
 my $rc = $sth->execute();
 my $rel = $sth->fetchrow_hashref();
 return $rel;
}



# get content based by owner
sub get_content4owner(){
 my $self = shift;
 my ($table,$owner) = @_;
 my $where = "WHERE owner='$owner'";
 my $sth = $dbh->prepare("SELECT * FROM $table $where");
 my $rc = $sth->execute();
 my $rel = $sth->fetchrow_hashref();
 return $rel;
}

# delete relation (without node)
sub delete_relation(){
 my $self = shift;
 my ($main_id,$lang,$rel_id) = @_;
 my $sth = $dbh->prepare("DELETE FROM relation WHERE main_id='$main_id' and lang='$lang' and rel_id='$rel_id'");
 my $rc = $sth->execute();
 $sth->finish();
 return $rc;
}

# delete template (without node)
sub delete_template(){
 my $self = shift;
 my ($tpl_id) = @_;
 my $sth = $dbh->prepare("DELETE FROM template WHERE tpl_id='$tpl_id'");
 my $rc = $sth->execute();
 $sth->finish();
 return $rc;
}

# insert relation
sub insert_relation(){
  my $self = shift;
  my ($table,$main_id,$lang,$c_id,$tpl_id,$offer_id) = @_;
  my $foreign_key="";
  my $foreign_id = $c_id;#alle bis auf offer
  $foreign_key = "cc_id" if("$table" eq "content");
  $foreign_key = "cp_id" if("$table" eq "contentpers");
  $foreign_key = "cu_id" if("$table" eq "contentuser");
  $foreign_key = "co_id" if("$table" eq "offer");
  $foreign_id = 0 if(!$c_id);
  $foreign_id = $offer_id if("$table" eq "offer");
  $c_id = 0 if(!$c_id);
  $offer_id = 0 if(!$offer_id);
  $tpl_id = 0 if(!$tpl_id);
  my $sth = $dbh->prepare("INSERT INTO relation ($foreign_key,main_id,content_id,offer_id,template_id,lang,change) VALUES('$foreign_id','$main_id','$c_id','$offer_id','$tpl_id','$lang','now()') RETURNING rel_id");
  my $rows = $sth->execute();

  my $last_id;
  $sth->bind_columns(\$last_id);
  my $rel_id = $sth->fetchrow_array();

  return $rel_id;
}

# insert relationlist for multi relation in listing mode
sub insert_relationlist(){
  my $self = shift;
  my ($table,$main_id,$lang,$c_id,$tpl_id,$foreign_key,$mastermain_id,$dbinit) = @_;
  $dbh = $dbt->dbconnect() if($dbinit);#because of ajax
  $c_id = 0 if(!$c_id);
  $tpl_id = 0 if(!$tpl_id);
  $mastermain_id = 0 if(!$mastermain_id);
  my $sth = $dbh->prepare("INSERT INTO relation (main_id,content_id,$foreign_key,template_id,lang,change,mastermain_id) VALUES('$main_id','$c_id','$c_id','$tpl_id','$lang','now()',$mastermain_id) RETURNING rel_id");
  my $rows = $sth->execute();

  my $last_id;
  $sth->bind_columns(\$last_id);
  my $rel_id = $sth->fetchrow_array();

  $dbh->disconnect if($dbinit);
  return $rel_id;
}

# obsolet 
# update relation
sub update_relation(){
 my $self = shift;
 my ($main_id,$lang,$new_main_id,$template_id,$c_id,$rel_id,$foreign_key) = @_;
 my $foreign_id = $c_id;
 $foreign_id = 0 if(!$c_id);
 $c_id = 0 if(!$c_id);
 my $rel_set = "change='now()'";
 $rel_set .= ",main_id='$new_main_id'" if($new_main_id);
 $rel_set .= ",template_id='$template_id'" if($template_id =~ /\d/);
 $rel_set .= ",content_id='$c_id'" if($c_id =~ /\d/);
 $rel_set .= ",$foreign_key='$foreign_id'" if($foreign_key && $foreign_id);
 $rel_set .= ",template_id='0'" if(!$template_id);
 $rel_set .= ",content_id='0'" if(!$c_id);

 my $where = "where main_id='$main_id' and lang='$lang' and rel_id='$rel_id'";
 my $sth = $dbh->prepare("UPDATE relation SET $rel_set $where");
 my $rows = $sth->execute();
 return $rows;  
}

sub update_relation2(){
 my $self = shift;
 my ($main_id,$lang,$new_main_id,$new_template_id,$rel_id,$dbinit) = @_;
 $dbh = $dbt->dbconnect() if($dbinit);#because of ajax
 my $rel_set = "change='now()'";
 $rel_set .= ",main_id='$new_main_id'" if($new_main_id);
 $rel_set .= ",template_id='$new_template_id'" if($new_template_id);
 my $where = "where lang='$lang' and rel_id='$rel_id'";
 $where .= " and main_id='$main_id'" if($main_id);
 my $sth = $dbh->prepare("UPDATE relation SET $rel_set $where");
 my $rows = $sth->execute();
 $dbh->disconnect if($dbinit);
 return $rows;  
}

#change all relations of main_id (maybe changeing template ore node)
sub update_relation3(){
 my $self = shift;
 my ($main_id,$lang,$new_main_id,$template_id) = @_;
 my $rel_set = "change='now()'";
 $rel_set .= ",main_id='$new_main_id'" if($new_main_id);
 $rel_set .= ",template_id='$template_id'" if($template_id =~ /\d/);
 my $where = "where lang='$lang' and main_id='$main_id'";
 my $sth = $dbh->prepare("UPDATE relation SET $rel_set $where");
 my $rows = $sth->execute();
 return $rows;  
}


sub update_users4trans(){
 my $self = shift;
 my ($c_id4trans,$tpl_id4trans,$kind_of_trans,$u_id) = @_;
 my $sth = $dbh->prepare("UPDATE users SET
 			c_id4trans='$c_id4trans',
 			tpl_id4trans='$tpl_id4trans',
 			ctpos_activ='0',
 			kind_of_trans='Faktura'
			where u_id=$u_id");
 my $rows = $sth->execute();
 return $rows;
}

sub cleanup_users(){
 my $self = shift;
 my $u_id = shift;
 my $sth = $dbh->prepare("UPDATE users SET
 			c_id4trans='0',
 			tpl_id4trans='0',
 			ctpos_activ='0',
                       c_id4package='0',
                       kind_of_trans='',
                       parts_uri='',
                       address_uri='',
                       project_uri='',
                       transaction_uri='',
                       nel_uri=''
			where u_id=$u_id");
 my $rows = $sth->execute();
 return $rows;
}

# final users_update
sub users_up(){
 my $self = shift;
 my ($column,$value,$u_id) = @_;
 #if($value){
   my $sth = $dbh->prepare("UPDATE users SET $column='$value' where u_id='$u_id'");
   my $rows = $sth->execute();
   return $rows;
 #}
}

# all content
sub collect_content(){
 my $self = shift;
 my $sth = $dbh->prepare("SELECT c_id,ct_name FROM content");
 my $rc = $sth->execute();
 my $ct_all = $sth->fetchall_hashref("c_id");
 $sth->finish;
 return $ct_all;
}

# all content or tt_news_cat
sub collect_content2(){
 my $self = shift;
 my ($table,$column,$content,$id_key) = @_;
 $id_key="c_id" if(!$id_key);
 my $sel="";
 $sel = "where $column='$content'" if($content);
 my $sth = $dbh->prepare("SELECT * FROM $table $sel");
 my $rc = $sth->execute();
 my $ct_all = $sth->fetchall_hashref("$id_key");
 $sth->finish;
 return $ct_all;
}

# all content, mainly used by get_freenr
sub collect_content3(){
 my $self = shift;
 my ($table,$column,$content,$key) = @_;
 $key = "barcode" if(!$key);
 my $where;
 $where = "where $column = '$content'" if($content);
 my $sth = $dbh->prepare("SELECT * FROM $table $where");
 my $rc = $sth->execute();
 my $ct_all = $sth->fetchall_hashref("$key");
 $sth->finish;
 return $ct_all;
}


#collect relation nodes
sub collect_rel4nodes(){
 my $self = shift;
 my ($main_ids,$content_id,$template_id,$key) = @_;
 my $where;
 $where .= " and parent_id IN ($main_ids)" if($main_ids);
 if($content_id =~ /\d\s\d/){
   $content_id =~ s/\s/,/g;
   $where .= " and rel.content_id IN ($content_id)";
 }elsif($content_id){
   $where .= " and rel.content_id='$content_id'";
 }
 $where .= " and rel.template_id='$template_id'" if($template_id);
 my $sth = $dbh->prepare("SELECT * FROM relation rel, nodes nd where rel.main_id=nd.main_id $where");
 my $rc = $sth->execute();
 $key = "main_id" if(!$key);
 my $rel4nd = $sth->fetchall_hashref("$key");
 $sth->finish;
 return $rel4nd;
}

#collect relation templates
sub collect_rel4tpl(){
 my $self = shift;
 my ($main_id) = @_;
 my $where;
 $where .= " and rel.main_id='$main_id'" if($main_id);
 my $sth = $dbh->prepare("SELECT * FROM relation rel, template tpl where rel.template_id=tpl.tpl_id $where");
 my $rc = $sth->execute();
 my $rel4tpl = $sth->fetchall_hashref("main_id");
 $sth->finish;
 return $rel4tpl;
}

# all content4relation
sub collect_ct4rel(){
 my $self = shift;
 my ($table,$main_ids,$lang,$scol,$offset,$limit,$sort4edit,$tplids,$relids,$sort_updown,$content_ids,$id) = @_;
 $main_ids =~ s/,$//;
 $tplids =~ s/,$//;
 $relids =~ s/,$//;
 $content_ids =~ s/,$//;
 my $updown = "ASC";
 $updown = "DESC" if($sort_updown eq "down");

 my $where;
 if($relids =~ /\d/){
   $where .= " and rel.rel_id IN ($relids)";
 }elsif($table eq "contenttrans"){
   $where .= " and ct.close_time is null";
 }
 #$where .= " and rel.main_id='$main_id'" if($main_id);
 $where .= " and rel.main_id IN ($main_ids)" if($main_ids);
 $where .= " and rel.template_id IN ($tplids)" if($tplids =~ /\d/);
 $where .= " and rel.content_id IN ($content_ids)" if($content_ids =~ /\d/);
 $where .= " and sort like '$sort4edit%'" if($sort4edit);
 $where .= " ORDER BY $scol $updown" if($scol);
 $where .= " LIMIT $limit" if($limit);
 $where .= " OFFSET $offset" if($offset);
 my $sth = $dbh->prepare("SELECT * FROM $table ct, relation rel where ct.c_id=rel.content_id and rel.lang='$lang' $where");
 my $rc = $sth->execute();
 $id = "c_id" if(!$id);
 my $ct4rel = $sth->fetchall_hashref("$id");
 $sth->finish;
 return $ct4rel;
}

# all content4relation for main_ids
sub collect_ct4rel2(){
 my $self = shift;
 my ($table,$main_ids,$lang,$scol,$offset,$limit,$owner,$id,$u_id,$trader) = @_;
 $main_ids =~ s/,$//;
 my $where;
 $where = " and ct.close_time is null" if($table eq "contenttrans");
 $where .= " and ct.owner = '$owner'" if($owner);
 $where = " and ct.trader = '$trader'" if($trader);
 #$where .= " and ct.content_public = '1'" if($u_id !~ /\d|always_public/); 
 $where .= " and ct.content_public = '1'" if($u_id == 0 || $u_id !~ /\d/); 
 if($scol && $limit){
   $where .= " ORDER BY $scol LIMIT $limit OFFSET $offset";
 }elsif($scol){
   $where .= " ORDER BY $scol";
 }
 my $sth = $dbh->prepare("SELECT * FROM $table ct, relation rel where ct.c_id=rel.content_id and rel.main_id IN ($main_ids) $where");
 my $rc = $sth->execute();
 $id="c_id" if(!$id);
 #$id="rel_id" if(!$id && $varenv{mlogic} eq "web"); #impliziert mehrfach-einträge, siehe Kacheln
 #$id="main_id" if($varenv{mlogic} eq "web"); 
 my $ct4rel = $sth->fetchall_hashref("$id");
 $sth->finish;
 return $ct4rel;
}


# all content4relation with optional c_ids and operator
sub collect_ct4rel3(){
 my $self = shift;
 my ($table,$column1,$content1,$op2,$column2,$content2,$tpl_ids,$c_ids,$id) = @_;
 $tpl_ids =~ s/,$//;
 $c_ids =~ s/,$//;
 my $where="where ct.c_id=rel.content_id";
 if($content1 =~ /\d+,\d+/){
   $where .= " and rel.$column1 IN ($content1)";
 }elsif($content1){
   $where .= " and rel.$column1='$content1'";
 }
 if("$content2" || "$content2" eq "0"){
   $where .= " and ct.$column2 $op2 $content2" if($column2 =~ /int/);
   $where .= " and ct.$column2 $op2 '$content2'" if($column2 !~ /int/);
 }
 $where .= " and rel.template_id IN ($tpl_ids)" if($tpl_ids);
 $where .= " and ct.c_id IN ($c_ids)" if($c_ids);
 my $sth = $dbh->prepare("SELECT * FROM $table ct, relation rel $where");
 my $rc = $sth->execute();
 $id="c_id" if(!$id);
 my $ct_all = $sth->fetchall_hashref($id);
 $sth->finish;
 return $ct_all;
}

# all content + relation + nodes
sub collect_ct4rel4nd(){
 my $self = shift;
 my ($table,$main_ids,$lang,$scol,$offset,$limit,$colname,$colval,$tpl_ids,$rel_ids,$id,$parent_id) = @_;
 $tpl_ids =~ s/,$//;
 $main_ids =~ s/,$//;
 $rel_ids =~ s/,$//;
 my $where;
 if($colname && $colname =~ /int/ && $colval eq "null"){
   $where .= " and $colname is $colval";
 }elsif($colname && $colval){
   $where .= " and $colname = '$colval'";
 }
 $where .= " and rel.template_id IN ($tpl_ids)" if($tpl_ids);
 $where .= " and rel.main_id IN ($main_ids)" if($main_ids);
 $where .= " and nd.parent_id = '$parent_id'" if($parent_id);
 $where .= " and rel.rel_id IN ($rel_ids)" if($rel_ids);
 $where .= " and nd.node_public='1'" if($varenv{wwwhost} =~ /k9/); 

 my $scol_table = "ct";
 $scol_table = "nd" if($scol eq "n_sort");
 if($scol && $limit){
   $where .= " ORDER BY $scol_table.$scol LIMIT $limit OFFSET $offset";
 }elsif($scol){
   $where .= " ORDER BY $scol_table.$scol";
 }
 my $sth = $dbh->prepare("SELECT * FROM $table ct, relation rel, nodes nd where ct.c_id=rel.content_id and nd.main_id=rel.main_id $where");
 my $rc = $sth->execute();
 my $ct4rel = $sth->fetchall_hashref("$id");
 $sth->finish;
 return $ct4rel;
}


#collect content with contenttranspos by c.c_id...
sub collect_postime(){
 my $self = shift;
 my ($table,$start_date_time,$end_date_time,$start_date2,$end_date2) = @_;
 $start_date_time =~ s/,/./g;
 $end_date_time =~ s/,/./g;
 my $where = "where cp.cc_id=c.c_id";
 $where .= " and cp.start_time >= $start_date_time";
 $where .= " and cp.end_time <= $end_date_time";
 $where .= " and (cp.start_time >= '$start_date2' OR cp.end_time >= '$start_date2')" if($start_date2 && $end_date2);
 my $sth = $dbh->prepare("SELECT cp.* from content c, $table cp $where order by cp.end_time");
 my $rc = $sth->execute();
 my $ct = $sth->fetchall_hashref("cc_id");
 $sth->finish;
 return $ct;
}


#all position of contenttrans
sub collect_contentpos(){
 my $self = shift;
 my ($table,$c_id) = @_;
 my $tb = "contenttrans";
 my $tbpos  = "contenttranspos";
 if($table eq "contenttver"){
  $tb  = "contenttver";
  $tbpos  = "contenttverpos";
 }
 my $where = "where ctt.c_id=pos.ct_id and ctt.c_id='$c_id'";

 if($c_id){
  my $sth = $dbh->prepare("SELECT pos.* FROM $tbpos pos, $tb ctt $where");
  my $rc = $sth->execute();
  my $cpos = $sth->fetchall_hashref("c_id");
  my $rows = $sth->rows;
  $sth->finish;
  return ($cpos,$rows);
 }else{
  my $cpos = { c_id => 0 };
  my $rows = 0;
  return ($cpos,$rows);
 }
}

# get content 4 time row where c_id
sub get_time4ct(){
 my $self = shift;
 my ($table,$ctpos_id,$cvpos_id,$owner) = @_;
  my $pos_key = "ctpos_id";
  my $pos_id = $ctpos_id;
  $pos_id = "0" if(!$ctpos_id);
  if($cvpos_id){
    $pos_key = "cvpos_id";
    $pos_id = $cvpos_id;
  }

 $owner=0 if(!$owner);
 my $ownersel="";
 $ownersel = "and owner='$owner'" if($owner > 0);
 my $sth = $dbh->prepare("SELECT * FROM $table ct,timetable WHERE ct.c_id='$pos_id' and ct.c_id=timetable.$pos_key $ownersel");
 my $rc = $sth->execute();
 my $ct = $sth->fetchrow_hashref();
 return $ct;
}

# get content row where c_id
sub get_content(){
 my $self = shift;
 my ($c_id,$owner) = @_;
 $c_id=0 if(!$c_id);
 $owner=0 if(!$owner);
 my $ownersel="";
 $ownersel = "and owner='$owner'" if($owner > 0);
 my $sth = $dbh->prepare("SELECT * FROM content WHERE c_id='$c_id' $ownersel");
 my $rc = $sth->execute();
 my $ct = $sth->fetchrow_hashref();
 return $ct;
}


sub get_contentrow(){
 my $self = shift;
 my ($table,$c_id,$column,$content,$column2,$content2,$column3,$content3) = @_;
 my $where = "1 = 1";
 if($c_id){
   $where .= " and c_id='$c_id'";
 }elsif($column && $content){
   $where .= " and $column='$content'";
 }else{
   exit 1;
 }
 if($column2 && $content2){
   $where .= " and $column2='$content2'";
 }
 if($column3 && $content3){
   $where .= " and $content3"; # length(ct_name) >= 11 matches PO-14058223 and 9550010000059998099
   $where .= " and (int03=1 OR int03=2)" if($column3 eq "ct_name");
 }

 my $sth = $dbh->prepare("SELECT * FROM $table WHERE $where");
 my $rc = $sth->execute();
 my $ct = $sth->fetchrow_hashref();
 return $ct;
}

#get not closed contenttrans row
sub get_contenttrans(){
 my $self = shift;
 my ($userID,$main_id,$main_id_ch) = @_;
 my $sth = $dbh->prepare("SELECT * FROM contenttrans ct, relation rel WHERE ct.c_id=rel.content_id and rel.main_id IN ($main_id,$main_id_ch) and ct.txt08 ilike '$userID' and (ct.state is null OR ct.state = '') and ct.close_time is null");
 my $rc = $sth->execute();
 my $ct = $sth->fetchrow_hashref();
 return $ct;
}

# insert contenttrans
# only used in Prelogic and should be deleted.
sub insert_contenttrans(){
 my $self = shift;
  my ($ctadr,$main_id,$tpl_id,$owner) = @_;
  $owner="199" if(!$owner);
  my $sth = $dbh->prepare("INSERT INTO contenttrans (ct_name,txt00,int10,int03,txt02,txt01,txt03,txt06,txt07,txt08,txt09,txt15,txt17,txt18,txt19,owner,itime) VALUES('----','Rechnung','$ctadr->{c_id}','$ctadr->{int03}','$ctadr->{txt02}','$ctadr->{txt01}','$ctadr->{txt03}','$ctadr->{txt06}','$ctadr->{txt07}','$ctadr->{txt08}','$ctadr->{txt09}','$ctadr->{txt15}','$ctadr->{txt17}','$ctadr->{txt18}','$ctadr->{txt19}','$owner','now()') RETURNING c_id");

  my $rows = $sth->execute();
  my $last_id;
  $sth->bind_columns(\$last_id);
  my $c_id = $sth->fetchrow_array();

  my $sth3 = $dbh->prepare("INSERT INTO relation (ca_id,main_id,content_id,template_id,change) VALUES('$ctadr->{c_id}','$main_id','$c_id','$tpl_id','now()')");
  $sth3->execute();
  return $c_id;
}

# get content row where c_id
sub get_content1(){
 my $self = shift;
 my ($table,$c_id,$owner,$mtime) = @_;
 $c_id=0 if(!$c_id);
 $owner=0 if(!$owner);
 #my $ownersel;
 #$ownersel = "and owner='$owner'" if($owner > 0);
 my $sel = "1 = 1";
 $sel .= " and c_id='$c_id'";
 $sel .= " and owner='$owner'" if($owner > 0);
 $sel .= " and mtime <= '$mtime'" if($mtime);
 #my $sth = $dbh->prepare("SELECT * FROM $table WHERE c_id='$c_id' $ownersel");
 my $sth = $dbh->prepare("SELECT * FROM $table WHERE $sel");
 my $rc = $sth->execute();
 my $ct = $sth->fetchrow_hashref();
 return $ct;
}

# get content row 
sub get_content2(){
 my $self = shift;
 my ($table,$ct_name,$owner,$mtime,$column,$content,$op) = @_;
 $owner=0 if(!$owner);
 my $sel = "1 = 1";
 $sel .= " and ct_name='$ct_name'" if($ct_name);
 $sel .= " and owner='$owner'" if($owner > 0);
 $sel .= " and mtime >= '$mtime'" if($mtime);
 $op="=" if(!$op);
 $sel .= " and $column $op '$content'" if($content && $content ne "null");
 $sel .= " and $column $op $content" if($content && $content eq "null");
 my $sth = $dbh->prepare("SELECT * FROM $table WHERE $sel order by c_id DESC");
 my $rc = $sth->execute();
 my $ct = $sth->fetchrow_hashref();
 return $ct;
}

sub get_content4new(){
 my $self = shift;
 my ($table,$ct_name,$barcode,$txt11) = @_;
 my $sel = "ct_name = '$ct_name'";
 $sel .= " OR barcode='$barcode'" if($barcode =~ /^\d+$/);
 $sel .= " OR txt11='$txt11'" if($txt11);
 my $sth = $dbh->prepare("SELECT * FROM $table WHERE $sel order by c_id DESC");
 my $rc = $sth->execute();
 my $ct = $sth->fetchrow_hashref();
 return $ct;
}


# get content row where ct_name and ct_id
sub get_content5(){
 my $self = shift;
 my ($table,$ct_name,$c_id,$ct_id,$operator) = @_;
 my $sel = "1 = 1";
 my $op="=";
 $op="!=" if($operator eq "not");
 $sel .= " and ct_name = '$ct_name'" if($ct_name);
 $sel .= " and c_id $op '$c_id'" if($c_id);
 $sel .= " and ct_id = '$ct_id'" if($ct_id);
 my $sth = $dbh->prepare("SELECT * FROM $table WHERE $sel order by c_id DESC");
 my $rc = $sth->execute();
 my $ct = $sth->fetchrow_hashref();
 return $ct;
}


# get content row where column content
sub get_content6(){
 my $self = shift;
 my ($table,$column1,$content1,$column2,$content2,$column3,$content3,$column4,$content4,$owner) = @_;
 my $where = "$column1 = '$content1'";
 $where = "$column1 is $content1" if($content1 eq "null");
 $where .= " and $column2 = '$content2'" if($content2);
 $where .= " and $column3 = '$content3'" if($content3);
 $where .= " and $column4 <= $content4" if($content4);
 $where .= " and owner = '$owner'" if($owner);
 my $sth;
 if($table eq "timetable"){
   $sth = $dbh->prepare("SELECT * FROM $table WHERE $where");
 }else{
   $sth = $dbh->prepare("SELECT * FROM $table WHERE $where order by c_id DESC");
 }
 my $rc = $sth->execute();
 my $ct = $sth->fetchrow_hashref();
 return $ct;
}


# get last content row via c_id and operator
sub get_content7(){
 my $self = shift;
 my ($table,$column,$content,$column2,$op2,$content2) = @_;
 my $where = "1 = 1";
 $where .= " and $column = '$content'" if($content && $content > 0);
 $where .= " and $column2 $op2 '$content2'" if("$content2" || "$content2" eq "0");
 my $sth = $dbh->prepare("SELECT * FROM $table where $where order by mtime DESC");
 my $rc = $sth->execute();
 my $ct = $sth->fetchrow_hashref();
 return $ct;
}

# get last content row for sorted insert
sub get_content2sort(){
 my $self = shift;
 my ($table,$sort,$sortop,$col1,$val1,$col2,$val2) = @_;
 $sortop = "=" if(!$sortop);
 my $where = "1 = 1";
 $where .= " and sort $sortop '$sort'" if($sort > 0);
 $where .= " and $col1 = '$val1'" if($val1);
 $where .= " and $col2 != '$val2'" if($val2);
 my $sth = $dbh->prepare("SELECT * FROM $table where $where order by sort DESC");
 my $rc = $sth->execute();
 my $ct = $sth->fetchrow_hashref();
 return $ct;
}

# get last content,relation! row for sorted insert
sub get_content3sort(){
 my $self = shift;
 my ($table,$main_id) = @_;
 my $where = "$table.c_id=relation.content_id";
 $where .= " and relation.main_id= '$main_id'" if($main_id);
 my $sth = $dbh->prepare("SELECT * FROM $table,relation where $where order by $table.sort DESC");
 my $rc = $sth->execute();
 my $ct = $sth->fetchrow_hashref();
 return $ct;
}

# get last content row via c_id like for sorted insert
sub get_like2sort(){
 my $self = shift;
 my ($table,$column,$content,$column2,$op2,$content2,$column3,$op3,$content3) = @_;
 my $where = "1 = 1";
 $where .= " and $column ilike '$content%'" if($content && $content > 0);
 $where .= " and $column2 $op2 '$content2'" if("$content2" || "$content2" eq "0");
 $where .= " and $column3 $op3 '$content3'" if("$content3");
 #my $sth = $dbh->prepare("SELECT * FROM $table where $where order by $column DESC");
 my $sth = $dbh->prepare("SELECT * FROM $table where $where order by mtime DESC");
 my $rc = $sth->execute();
 my $ct = $sth->fetchrow_hashref();
 return $ct;
}

# get last barcode row
sub get_barcode(){
 my $self = shift;
 my ($table,$bc4table) = @_;
 my $min_content = "0";
 $min_content = "1000" if($table eq "contentadr");
 my $sth = $dbh->prepare("SELECT * FROM $table where $bc4table >= '$min_content' order by $bc4table DESC");
 my $rc = $sth->execute();
 my $ct = $sth->fetchrow_hashref();
 return $ct;
}


# get content row where cookie
sub get_content3(){
 my $self = shift;
 my ($table,$ct_name,$owner,$coo) = @_;
 $owner=0 if(!$owner);
 my $sel = "1 = 1";
 $sel .= " and ct_name='$ct_name'" if($ct_name);
 $sel .= " and owner='$owner'" if($owner > 0);
 $sel .= " and coo='$coo'" if($coo);
 my $sth = $dbh->prepare("SELECT * FROM $table WHERE $sel");
 my $rc = $sth->execute();
 my $ct = $sth->fetchrow_hashref();
 return $ct;
}

sub get_ctnode(){
 my $self = shift;
 my ($table,$tpl_id,$nd_col,$nd_con,$ct_col,$ct_op,$ct_con,$rel_col,$rel_con) = @_;
 $ct_op = "=" if(!$ct_op);
 my $where = "nd.main_id=rel.main_id and rel.content_id=ct.c_id";
 $where .= " and rel.template_id='$tpl_id'" if($tpl_id);
 $where .= " and nd.$nd_col='$nd_con'" if($nd_col && $nd_con);
 $where .= " and ct.$ct_col $ct_op '$ct_con'" if($ct_col && $ct_con);
 $where .= " and rel.$rel_col = '$rel_con'" if($rel_col && $rel_con);
 my $sth = $dbh->prepare("SELECT * FROM nodes nd, relation rel, $table ct WHERE $where");
 my $rc = $sth->execute();
 my $ct = $sth->fetchrow_hashref();
 return $ct;
}

# get content row over relation where nodes n_id
sub get_ct4node(){
 my $self = shift;
 my ($tpl_id,$nd_column,$nd_content,$ct_column,$ct_content) = @_;

 my $where = "nd.main_id=rel.main_id and rel.content_id=ct.c_id";
 $where .= " and rel.template_id='$tpl_id'" if($tpl_id);
 $where .= " and nd.$nd_column='$nd_content'" if($nd_column && $nd_content);
 $where .= " and ct.$ct_column >= '$ct_content'" if($ct_column && $ct_content);
 my $sth = $dbh->prepare("SELECT * FROM nodes nd, relation rel, content ct WHERE $where");
 my $rc = $sth->execute();
 my $ct = $sth->fetchrow_hashref();
 return $ct;
}

# get content row over relation
sub get_ctrel(){
 my $self = shift;
 my ($table,$main_id,$lang,$rel_id,$c_id,$tpl_id,$sort,$dbinit) = @_;
 $dbh = $dbt->dbconnect() if($dbinit);#because of ajax
 my $sel = "rel.lang='$lang' and rel.content_id=ct.c_id";
 $sel .= " and rel.main_id='$main_id'" if ($main_id && $main_id > 0);
 $sel .= " and rel.rel_id='$rel_id'" if ($rel_id && $rel_id > 0);
 $sel .= " and rel.rel_id='0'" if ($rel_id && $rel_id eq "null");
 $sel .= " and rel.content_id='$c_id'" if ($c_id && $c_id > 0);
 $sel .= " and rel.template_id='$tpl_id'" if ($tpl_id && $tpl_id > 0);
 $sel .= " and ct.sort='$sort'" if ($sort);
 my $sth = $dbh->prepare("SELECT * FROM relation rel, $table ct WHERE $sel");
 my $rc = $sth->execute();
 my $ct = $sth->fetchrow_hashref();
 $dbh->disconnect if($dbinit);
 return $ct;
}


# get content row over relation
sub get_ctrel2(){
 my $self = shift;
 my ($table,$ct_name,$main_id,$lang,$rel_id,$c_id,$tpl_id,$zcolumn,$zcontent,$zcoperator,$orderby,$adesc) = @_;
 my $sel = "rel.lang='$lang' and rel.content_id=ct.c_id";
 if("$zcolumn" eq "barcode" && $zcontent =~ /^\d+$/ && $zcontent < 9223372036854775807){
   $sel .= " and (ct_name='$ct_name' OR $zcolumn='$zcontent')";
 }elsif("$zcolumn" eq "start_time"){
   if($zcontent && $varenv{dataflow} =~ /wiki/ && $zcoperator && $zcoperator eq "="){
     $sel .= " and start_time='$zcontent'";
   }elsif($zcontent){
     $sel .= " and start_time>='$zcontent'";
   }
 }else{
   $sel .= " and ct_name='$ct_name'" if($ct_name);
 }
 $sel .= " and rel.main_id='$main_id'" if ($main_id > 0);
 $sel .= " and rel.rel_id='$rel_id'" if ($rel_id > 0);
 $sel .= " and rel.content_id='$c_id'" if ($c_id > 0);
 #$sel .= " and (ct.txt12 ilike '$pre_lager%' OR ct.txt12 like '_')" if($pre_lager);

 if($tpl_id =~ /\d,\d/){
   $sel .= " and rel.template_id IN ($tpl_id)";
 }elsif($tpl_id > 0){
   $sel .= " and rel.template_id='$tpl_id'";
 }

 #It must be available (Gutschein  oder RFID)
 #  $sel .= " and ct.int03 > 0";
 my $order = "";
 $order = "order by $orderby $adesc" if($orderby);
 my $sth = $dbh->prepare("SELECT * FROM relation rel, $table ct WHERE $sel $order");
 my $rc = $sth->execute();
 my $ct = $sth->fetchrow_hashref();
 return $ct;
}

# with like and tplids
sub get_ctrel3(){
 my $self = shift;
 my ($table,$column,$content,$column2,$op2,$content2,$tplids,$sum) = @_;
 my $rval="*";
 $rval = "sum(ct.int03) AS int03" if($sum eq "sum_kaution");
 my $where;
 $where .= " and $column ilike '$content%'" if($content && $content > 0);
 $where .= " and $column2 $op2 '$content2'" if("$content2" || "$content2" eq "0");
 my $sth = $dbh->prepare("SELECT $rval FROM relation rel, $table ct WHERE rel.content_id=ct.c_id and rel.template_id IN ($tplids) and rel.lang='de' $where");
 my $rc = $sth->execute();
 my $ct = $sth->fetchrow_hashref();
 return $ct;
}

# get content row over relation and start_time
sub get_ctrel4(){
 my $self = shift;
 my ($table,$ct_name,$main_id,$lang,$rel_id,$c_id,$tpl_id,$column,$op,$content) = @_;
 $op = "=" if(!$op);
 my $sel = "rel.lang='$lang' and rel.content_id=ct.c_id";
 $sel .= " and $column $op '$content'" if($column && $content);
 $sel .= " and ct_name='$ct_name'" if($ct_name);
 $sel .= " and rel.main_id='$main_id'" if ($main_id > 0);
 $sel .= " and rel.rel_id='$rel_id'" if ($rel_id > 0);
 $sel .= " and rel.content_id='$c_id'" if ($c_id > 0);
 $sel .= " and rel.template_id='$tpl_id'" if($tpl_id > 0);
 my $sth = $dbh->prepare("SELECT * FROM relation rel, $table ct WHERE $sel");
 my $rc = $sth->execute();
 my $ct = $sth->fetchrow_hashref();
 return $ct;
}

# search nodes
sub search_nodes(){
 my $self = shift;
 my ($search_pattern,$lang) = @_;
 $search_pattern =~ s/\s/\%/g;
 my $sth = $dbh->prepare("SELECT * from nodes nd where nd.lang='$lang' and nd.node_name ilike '%$search_pattern%'");
 my $rc = $sth->execute();
 my $search = $sth->fetchall_hashref("main_id");
 $sth->finish;
 return $search;
}

#search json contentuser for automcomplete
sub get_jsoncuser(){
 my $self = shift;
 my ($table,$lang,$mandant_id,$key,$value) = @_;
 my $dbh = $dbt->dbconnect();#because of ajax external handle request

 my $sel = "c_id = '$mandant_id'";
 my $sth = $dbh->prepare("SELECT ct.$key FROM $table ct WHERE $sel");
 my $rc = $sth->execute();
 my $rows = $sth->rows;

 $dbh->disconnect; 
 return $sth;
}


#search json address for automcomplete
sub search_jsonadr(){
 my $self = shift;
 my ($table,$lang,$search,$mandant_id,$retype) = @_;
 my $dbh = $dbt->dbconnect();#because of ajax external handle request
 my $template_id=200;#$table eq content
 $template_id=202 if($table eq "contentadr");

 my $mjkey;
 $mjkey = $1 if($retype =~ /combobox_(txt\d+)/);
 my $sth;
 if($retype =~ /combobox_txt/ && $search =~ /^\d+$/ && $mjkey =~ /txt\d+/){
  my $sel = "c_id = '$search'";
  $sth = $dbh->prepare("SELECT ct.c_id AS id ,ct.$mjkey AS name FROM $table ct, relation rel WHERE ct.c_id=rel.content_id and rel.template_id=$template_id and $sel");
 }else{
  my $sel = "txt01 ilike '%$search%'";
  $sth = $dbh->prepare("SELECT ct.txt01 || ', ' || ct.c_id AS value, ct.txt01 AS vorname_name, ct.c_id FROM $table ct, relation rel WHERE ct.c_id=rel.content_id and rel.template_id=$template_id and $sel");
 }
 my $rc = $sth->execute();
 my $rows = $sth->rows;

 $dbh->disconnect;
 return $sth;
}


#search json for automcomplete
sub search_json(){
 my $self = shift;
 my ($table,$lang,$ct_name,$mandant_id) = @_;
 my $dbh = $dbt->dbconnect();#because of ajax external handle request
 #my $sel = "rel.lang='$lang' and rel.content_id=ct.c_id";
 my $sel = "1=1";
 my $sel2;

 if($ct_name =~ /^\d+$/ && $table eq "content"){
   $sel .= " and (ct_name ilike '$ct_name%' OR CAST(barcode AS text) like '$ct_name%')";
 }else{
   $sel .= " and ct_name ilike '$ct_name%'";
 }
 my $sth = "";

 #If select only mandant specific warenstamm
 if($dbt->{shareedms_conf}->{waren}){
   my $main_ids = &collect_noderec("",$dbt->{shareedms_conf}->{waren},$lang,"nothing");
   $sth = $dbh->prepare("SELECT  ct.ct_name || ' , ' || ct.barcode || ' , ' || ct.txt12 AS value, ct.ct_name AS spart_ct_name, ct.c_id FROM $table ct, relation rel WHERE ct.c_id=rel.content_id and rel.main_id IN ($main_ids) and $sel $sel2");
 }else{
   $sth = $dbh->prepare("SELECT  ct.ct_name || ' , ' || ct.barcode || ' , ' || ct.txt12 AS value, ct.ct_name AS spart_ct_name, ct.c_id FROM $table ct WHERE $sel $sel2");
 }
 my $rc = $sth->execute();
 my $rows = $sth->rows;

 $dbh->disconnect; 
 return $sth;
}

sub search_content3(){
 my $self = shift;
 my ($searchref,$table,$mandant_id,$node_meta,$owner,$lang,$main_ids,$tplids,$ct_ids,$v_journal,$time,$kontext,$scol,$sort_updown,$offset,$limit,$export,$todo,$ck4ex,$opos) = @_;

 $main_ids =~ s/,$//;
 $tplids =~ s/,$//;
 $ct_ids =~ s/,$//;

   my $stamp_time = strftime("%d.%m.%Y %H:%M",localtime(time));
   my $debug;
   $debug=1;
   open(FILE,">>$varenv{logdir}/Liste3.log") if($debug);
   print FILE "*** $stamp_time Libenzdb.search_content3 ***\n" if($debug);
   print FILE Dumper($searchref) if($debug);
   print FILE "$table,$mandant_id,$node_meta->{node_name},$owner,$lang,$main_ids,$tplids,$ct_ids,$v_journal,$time,$kontext,$scol,$sort_updown,$offset,$limit,$export,$todo,$ck4ex,$opos\n" if($debug);
   close(FILE) if($debug);


 my $table_pos = $searchref->{table_pos} || "";
 my $txt_where;
 my $cptxt_where;
 my $cpgroup_where;

 my $valref = {};
 my $opref = {};
 foreach my $key (keys(%$searchref)){
  if($searchref->{$key} || $searchref->{$key} eq "0"){
   $valref->{$key} = $searchref->{$key};#because of escapeHTML manipulates special chars
   $opref->{$key} = "=";
   if($key =~ /table_pos/){
	$table_pos = $valref->{$key};
   }
   if($key =~ /c_id|int\d+|barcode|time|sort|owner/){
     $searchref->{$key} =~ s/,/./g;
     if($searchref->{$key} =~ /(\<\=|\>\=|\<|\>|\=)/){
	$opref->{$key} = $1;
     }
     if($searchref->{$key} =~ /([-0-9\.]+)/){
	$valref->{$key} = $1;
     }
     if($searchref->{$key} =~ /null/){
	$opref->{$key} = "is";
     }
     chomp($valref->{$key});
   }
   $valref->{$key} = $q->escapeHTML($valref->{$key});
   #print $key . ":" . $opref->{$key} . ":" . $valref->{$key} . "\n";

   if($table_pos =~ /contentpos|contentadrpos|users/){
     #my $tinfo = &table_info("","contenttranspos");
     if($key =~ /ct_name/){
      $cptxt_where .= " and cp.$key ilike '$valref->{$key}'";
     }elsif($key =~ /txt/){
      $cptxt_where .= " and cp.$key ilike '%$valref->{$key}%'";
     }elsif($key =~ /barcode|int|owner/ && (looks_like_number($valref->{$key}) || $valref->{$key} eq "null")){
      $cptxt_where .= " and cp.$key $opref->{$key} $valref->{$key}";
     }
     $txt_where .= " and ct.txt11 ilike '%$valref->{$key}%'" if($key eq "txt11");#Rahmennr. hack
   }else{
     if($key eq "ct_name" && $valref->{$key} =~ /^(\d+)(0\d)$/){#wg. strichcode ohne "-"
       $txt_where .= " and (ct.$key ilike '$valref->{$key}' OR ct.barcode $opref->{$key} $valref->{$key})";
     }elsif($key eq "ct_name" && $varenv{wwwhost} =~ /regiox|woge/){
       $txt_where .= " and ct.$key ilike '%$valref->{$key}%'"; 
     }elsif($key eq "ct_name"){
       $txt_where .= " and ct.$key ilike '$valref->{$key}'"; 
     }elsif($key =~ /int01/ && $table eq "contenttrans" && looks_like_number($valref->{$key})){
	#$cpgroup_where .= " group by cp.ct_id HAVING sum(cp.int02*cp.int03) $opref->{$key} $valref->{$key}";#special calc pos sum
	#TODO, set DB int03=Menge default to 1 and testing
       $cpgroup_where .= " group by cp.ct_id HAVING sum(cp.int02) $opref->{$key} $valref->{$key}";#special calc pos sum
     }elsif($key =~ /_id|barcode|int\d+|sort|owner/ && (looks_like_number($valref->{$key}) || $valref->{$key} =~ /null|0/)){
       $txt_where .= " and ct.$key $opref->{$key} $valref->{$key}";
     }elsif($key =~ /txt|uri/){
       $txt_where .= " and ct.$key ilike '%$valref->{$key}%'";
     }elsif($key eq "state"){
       #order_state hack
       $txt_where .= " and (ct.state ilike '%$valref->{$key}%' OR ct.txt22 ilike '%$valref->{$key}%')"; 
     }elsif($key =~ /byte/){
       $txt_where .= " and ct.$key = '\\x$valref->{$key}'";
     }
   }

   if($key =~ /end_itime|end_mtime|end_date_time|pay_time/){
      $valref->{$key} .= " 23:59" if($valref->{$key} !~ /\d:\d/);
   }


   if(!$v_journal){
     if($table_pos =~ /contentpos|contentadrpos|users/ && $key =~ /mtime/ && $valref->{$key} =~ /\d+\.\d+\.\d+/){
	$cptxt_where .= " and cp.mtime >= '$valref->{$key}'" if($key =~ /start/);
	$cptxt_where .= " and cp.mtime < '$valref->{$key}'" if($key =~ /end/);
     }
     elsif($table_pos =~ /contentpos|contentadrpos|users/ && $key eq "template_id_pos" && $valref->{$key}){
	$cptxt_where .= " and cp.template_id = $valref->{$key}";
     }
     elsif($key =~ /mtime/ && $valref->{$key} =~ /\d+\.\d+\.\d+/){
	$txt_where .= " and ct.mtime >= '$valref->{$key}'" if($key =~ /start/);
	$txt_where .= " and ct.mtime < '$valref->{$key}'" if($key =~ /end/);
     }
     elsif($key =~ /bctime/ && $valref->{$key} =~ /\d+\.\d+\.\d+/){
	$txt_where .= " and ct.bctime >= '$valref->{$key}'" if($key =~ /start/);
	$txt_where .= " and ct.bctime < '$valref->{$key}'" if($key =~ /end/);
     }

   }elsif($v_journal){
     if($key =~ /itime/ && $valref->{$key} =~ /\d+\.\d+\.\d+/){
	$txt_where .= " and ct.itime >= '$valref->{$key}'" if($key =~ /start/);
	$txt_where .= " and ct.itime < '$valref->{$key}'" if($key =~ /end/);
     }
     if($key =~ /start_mtime/ && $valref->{$key} =~ /^\d+$/){
       $txt_where .= " and ct.int11 = '$valref->{$key}'";#int11 = c_id-abschluss key
     }elsif($key =~ /start_mtime/ && $valref->{$key} =~ /date.*\d+\.\d+\.\d+/){
       $txt_where .= " and ct.mtime >= $valref->{$key}";
     }elsif($key =~ /start_mtime/ && $valref->{$key} =~ /\d+\.\d+\.\d+/){
       $txt_where .= " and ct.mtime >= '$valref->{$key}'";
     }
     if($key =~ /end_mtime/ && $valref->{$key} =~ /\d+\.\d+\.\d+/){
       $txt_where .= " and ct.mtime < '$valref->{$key}'";
     }

     if($key =~ /start_pay_time/ && $valref->{$key} =~ /\d+\.\d+\.\d+/){
       $txt_where .= " and ct.pay_time >= '$valref->{$key}'";
     }
     if($key =~ /end_pay_time/ && $valref->{$key} =~ /\d+\.\d+\.\d+/){
       $txt_where .= " and ct.pay_time < '$valref->{$key}'";
     }

   }

  }
 }#end foreach($searchref)

 $txt_where .= " and ct.int03 < ct.int08" if($todo =~ /Mindermenge/);
 if($v_journal =~ /Tagesbericht/){
   $txt_where .= " and (ct.state ~ '[a-z]') and ct.int01 is not null and ct.close_time is null";
   $txt_where .= " and ct.int14 is $opos" if($opos eq "null");
 }
 #end valref ---------------------------------------------------------------------------

 $ck4ex =~ s/\s/,/g;
 $txt_where = " and ct.c_id IN ($ck4ex)" if($export =~ /check4export/);
 #$txt_where = " and rel.rel_id IN ($ck4ex)" if($export =~ /check4ex_rel/);
 #
 #SUM contenttranspos bsp
 #SELECT cp.ct_id, sum(cp.int02*cp.int03) from contenttranspos cp WHERE 1=1  and cp.end_time <= '31.10.2017 23:59' and cp.start_time >= '01.10.2017' group by cp.ct_id,cp.int02,cp.int03 HAVING sum(cp.int02*cp.int03) > 10;

 #print "$v_journal|$table_pos\n";
 #print "txt_where: $txt_where<br>\n";
 #print "cptxt_where: $cptxt_where<br>\n";

 my $sth = "";
 my $updown = "ASC";
 $updown = "DESC" if($sort_updown eq "down");
 if($v_journal =~ /_parts/){#collects ct_ids
   my $cp_scol = "ct_name";
   $cp_scol = $1 if($scol =~ /(ct_name|barcode|mtime|txt0[1-9]|int0[1-9])/);
   $sth = $dbh->prepare("SELECT cp.* FROM contenttranspos cp WHERE cp.ct_id IN (SELECT ct.c_id FROM relation rel, contenttrans ct WHERE rel.content_id=ct.c_id and rel.main_id IN ($main_ids) and rel.template_id IN ($tplids) and rel.lang='$lang' $txt_where) $cptxt_where ORDER BY cp.$cp_scol $updown");
 }elsif($valref->{long_rent} || $valref->{start_date_time} =~ /^\d+\.\d+\.\d+/ || $valref->{end_date_time} =~ /^\d+\.\d+\.\d+/ || $cpgroup_where){
   $sth = $dbh->prepare("SELECT * FROM relation rel, $table ct WHERE rel.content_id=ct.c_id and rel.main_id IN ($main_ids) and rel.template_id IN ($tplids) and rel.lang='$lang' $txt_where and c_id IN (SELECT cp.ct_id from contenttranspos cp WHERE 1=1 $cptxt_where $cpgroup_where) ORDER BY $scol $updown LIMIT $limit OFFSET $offset");
 }elsif($v_journal && $ct_ids){#and executes ct_ids
   $ct_ids = 0 if(!$ct_ids);
   $sth = $dbh->prepare("SELECT * FROM relation rel, $table ct WHERE rel.content_id=ct.c_id and rel.main_id IN ($main_ids) and rel.template_id IN ($tplids) and rel.content_id IN ($ct_ids) and rel.lang='$lang' $txt_where ORDER BY $scol $updown LIMIT $limit OFFSET $offset");
 
 }elsif($table_pos && $table_pos eq "contentpos"){
   $sth = $dbh->prepare("SELECT cp.* FROM contentpos cp WHERE cp.cc_id IN (SELECT ct.c_id FROM relation rel, content ct WHERE rel.content_id=ct.c_id and rel.main_id IN ($main_ids) and rel.template_id IN ($tplids) and rel.lang='$lang' $txt_where) $cptxt_where ORDER BY cp.$scol $updown LIMIT $limit OFFSET $offset");
 }elsif($table_pos && $table_pos eq "contentadrpos"){
  if($valref->{template_id_pos} eq 602){#because of user_minianswer have to be anonym
   $sth = $dbh->prepare("SELECT cp.* FROM contentadrpos cp WHERE 1=1 $cptxt_where ORDER BY cp.$scol $updown LIMIT $limit OFFSET $offset");
  }else{
   $sth = $dbh->prepare("SELECT ct.txt01,ct.txt08,cp.c_id,cp.mtime,cp.barcode,cp.int01,cp.txt02 FROM contentadr ct, contentadrpos cp WHERE ct.c_id=cp.ca_id $txt_where $cptxt_where ORDER BY cp.$scol $updown LIMIT $limit OFFSET $offset");
  }
 }elsif($table_pos && $table_pos eq "users"){
   $sth = $dbh->prepare("SELECT cp.* FROM contentadr ct, users cp WHERE ct.c_id=cp.u_id $txt_where $cptxt_where ORDER BY cp.$scol $updown LIMIT $limit OFFSET $offset");

 }else{
   $sth = $dbh->prepare("SELECT * FROM relation rel, $table ct WHERE rel.content_id=ct.c_id and rel.main_id IN ($main_ids) and rel.template_id IN ($tplids) and rel.lang='$lang' $txt_where ORDER BY $scol $updown LIMIT $limit OFFSET $offset");
 }

 my $rc = $sth->execute();
 my $s_id = "c_id";
 $s_id = "u_id" if($table_pos && $table_pos eq "users");#because to view multi relation
 my $search = $sth->fetchall_hashref("$s_id");
 $sth->finish;

 #save_query
 #my $save_query = $q->escapeHTML("$txt_where");
 
 #CSV/FiBu Export###
 if($export){
  
  my $ctf = &get_content1("","contentuser","$mandant_id");
  my @tpl_order = split /,/,$node_meta->{tpl_order};
  
  #wg. Buchungsgruppen bzw. Ursprung-nodes (int12) -->  Konten im FiBu Export
  my $colmain_ids = &collect_noderec("",$node_meta->{main_id},$lang,"999999");
  my $nodes = &Libenzdb::collect_node2("","$colmain_ids");
  my $ctt;my $ctadr;my $aw_ids;
  my $what;
  $what = "$export-" if($export =~ /CSV|FIBU/);
  open(CSV, "> $varenv{pdf}/$what$owner-$time.csv") or die "Can't open $varenv{pdf}/$owner-$time.csv\n";

    #CSV Table Header
    my $col_header="Ordner";
    foreach(@tpl_order){
     my ($key,$val) = split /=/,$_;
     if($key eq "txt01"){
       $col_header .= ";Name-01;Name-02;Name-03";
     }else{
       $col_header .= ";$val";
     }
    }
    print CSV "$col_header\n";

   foreach my $id (sort {
    if($sort_updown eq "down"){
        if ($scol =~ /barcode|int/) {
                $search->{$b}->{$scol} <=> $search->{$a}->{$scol}
        }else{
                lc($search->{$b}->{$scol}) cmp lc($search->{$a}->{$scol})
        }
    }else{
        if ($scol =~ /barcode|int/) {
                $search->{$a}->{$scol} <=> $search->{$b}->{$scol}
        }else{
                lc($search->{$a}->{$scol}) cmp lc($search->{$b}->{$scol})
        }
    }
   } keys(%$search)){

    my $s_main_id = "$search->{$id}->{main_id}";
    $s_main_id = "$search->{$id}->{int12}" if(($v_journal && ($v_journal =~ /_parts/)) || ($node_meta->{node_name} eq "Faktura"));
    
    #lxfibu vars
    my ($l_soll,$l_umsatz,$l_ustnr,$l_haben,$l_gvc,$l_beldat,$l_beleg,$l_opbeleg,$l_skonto,$l_kostnr,$l_text,$l_text2);
    #SOLL alias state/Zahlungsart
    foreach my $tid (keys (%$ctt)){
      if($search->{$id}->{ct_id} == $ctt->{$tid}->{c_id}){ 
	my ($date,$time);
        $ctt->{$tid}->{mtime} =~ s/:\d{2}\..*$//;
        ($date,$time) = split(/ /,$ctt->{$tid}->{mtime});
        $l_beldat = "$date";
	$l_beleg = "$ctt->{$tid}->{txt00}" . "-" . "$ctt->{$tid}->{ct_name}";
	$l_opbeleg = "$ctt->{$tid}->{ct_name}" if(("$ctt->{$tid}->{txt00}" eq "Rechnung") && ($ctt->{$tid}->{state}  =~ /Abbuchung|Überweisung|PayPal/));
    	$ctf->{txt43} = $1 if($ctf->{txt43} =~ /(\d+)/);
    	$ctf->{txt44} = $1 if($ctf->{txt44} =~ /(\d+)/);
    	$ctf->{txt45} = $1 if($ctf->{txt45} =~ /(\d+)/);
    	$ctf->{txt46} = $1 if($ctf->{txt46} =~ /(\d+)/);
    	$ctf->{txt47} = $1 if($ctf->{txt47} =~ /(\d+)/);
    	$ctf->{txt48} = $1 if($ctf->{txt48} =~ /(\d+)/);
    	$ctf->{txt49} = $1 if($ctf->{txt49} =~ /(\d+)/);
    	$ctf->{txt50} = $1 if($ctf->{txt50} =~ /(\d+)/);
	$l_soll = $ctf->{txt43} if($ctt->{$tid}->{state} eq "Bar");
	$l_soll = $ctf->{txt44} if($ctt->{$tid}->{state} eq "EC-Karte");
	$l_soll = $ctf->{txt45} if($ctt->{$tid}->{state} eq "Kreditkarte");
	$l_soll = $ctf->{txt46} if($ctt->{$tid}->{state} eq "Abbuchung");
	$l_soll = $ctf->{txt47} if($ctt->{$tid}->{state} eq "Überweisung");
	$l_soll = $ctf->{txt48} if($ctt->{$tid}->{state} eq "Geldkarte");
	$l_soll = $ctf->{txt50} if($ctt->{$tid}->{state} eq "PayPal");
	$ctf->{txt31} =~ s/,/|/;
	$l_soll = $ctf->{txt49} if(($ctt->{$tid}->{state} eq "Bar") && ($ctf->{txt31} && $search->{$id}->{int12} =~ /$ctf->{txt31}/));#Bar Kaution Gegenkonto

        #2018-01-28, disabled because of not used anymore
	#Debitor/Kreditor
	#if(("$export" eq "FiBu" && $ctt->{$tid}->{txt14} =~ /\d/) && ($ctt->{$tid}->{state} =~ /Abbuchung|Überweisung|PayPal/)){
	 #$ctadr = &get_content2("","contentadr","$ctt->{$tid}->{txt14}");
         #$l_soll = $ctadr->{int01} if($ctadr->{int01});
        #}
      }
    }

    my $line="";
    #Nodes alias Buchungsgruppen parameter
    foreach my $nid (keys (%$nodes)){
     if($s_main_id == $nodes->{$nid}->{main_id}){
    	$line = "$nodes->{$nid}->{node_name}";
    	$l_text = "$nodes->{$nid}->{node_name}";

	$ctf->{txt13} = $1 if($ctf->{txt13} =~ /(\d+)/);#VK Umst
	$ctf->{txt14} = $1 if($ctf->{txt14} =~ /(\d+)/);#EK Umst
	$ctf->{txt15} = $1 if($ctf->{txt15} =~ /(\d+)/);#Aufwandskonto
	$ctf->{txt16} = $1 if($ctf->{txt16} =~ /(\d+)/);#Erlöskonto
	$ctf->{txt40} = $1 if($ctf->{txt40} =~ /(\d+)/);#Kostenstelle
	#FIXME, Kaution und Wertmarken main_id
	my $ctf_txt40 = $ctf->{txt40};
	#$ctf_txt40 = "" if($nodes->{$nid}->{main_id} =~ /300087|400056|400054|400053|400063|300070|400048|400049|400047/);
	$l_kostnr = "$nodes->{$nid}->{int05}";#Kostenstelle
	$l_kostnr = "$ctf_txt40" if($nodes->{$nid}->{int05} !~ /\d/);

	#template definition in src/Tpl/EditMeta.pm
	my $ustnr;
	if($node_meta->{node_name} eq "Faktura"){
	  $ustnr = $nodes->{$nid}->{int03};#Verkauf = Verleih Umst
	  $ustnr = $ctf->{txt13} if($nodes->{$nid}->{int03} !~ /\d/); 
    	  $l_haben = $nodes->{$nid}->{int07};#Verleih Erlöskonto
	  $l_haben = $ctf->{txt16} if($nodes->{$nid}->{int07} !~ /\d/);
   	  $l_kostnr = $nodes->{$nid}->{int08};#Verleih Kostenstelle
	  $l_kostnr = "$ctf_txt40" if($nodes->{$nid}->{int08} !~ /\d/);
          #sollte nicht mur an 999 geknüpft sein, sondern Warengruppen ...
	  #$l_soll = "$nodes->{$nid}->{int02}" if($search->{$id}->{ct_name} eq "999");
	}

    	$l_ustnr = "0" if($ustnr == "0" || !$ustnr);
    	$l_ustnr = "1" if($ustnr == "19");
    	$l_ustnr = "2" if($ustnr == "7");

     }
    }

    #AW Kostenstelle
    $aw_ids =~ s/\|$//;
    if("$search->{$id}->{ct_name}" eq "$ctf->{txt51}"){
    	$ctf->{txt41} = $1 if($ctf->{txt41} =~ /(\d+)/);
    	$l_kostnr = "$ctf->{txt41}";
    #AM Kostenstelle
    }elsif($aw_ids && $search->{$id}->{ct_id} =~ /$aw_ids/){
    	$ctf->{txt42} = $1 if($ctf->{txt42} =~ /(\d+)/);
    	$l_kostnr = "$ctf->{txt42}";
    }

    foreach(@tpl_order){
     my ($key,$val) = split /=/,$_;
     $search->{$id}->{$key} = $q->unescapeHTML("$search->{$id}->{$key}");
     $search->{$id}->{$key} =~ s/:\d{2}\..*$// if($key =~ /time/);
     if($search->{$id}->{$key} && $key =~ /int/){
	my $cash = sprintf('%.2f',$search->{$id}->{$key});
        $line .= ";$cash";
 	if("$export" eq "FiBu"){
	  my $einzel = $search->{$id}->{int02};
    	  my $menge = $search->{$id}->{int03};
	  my $rabatt_val = $search->{$id}->{int07};
    	  $cash = $einzel * $menge;
    	  if($rabatt_val != 0){
               my $rabatt_eur = $rabatt_val;
               $rabatt_eur = $einzel * $menge * $rabatt_val/100 if($search->{$id}->{int08} != 1);
      		$cash = $einzel * $menge - $rabatt_eur;
    	  }
         $l_umsatz = &Libenz::round("","$cash");
	}
     }else{
	if($key eq "txt01"){
	  my ($n01,$n02,$n03) = split(/\n/,$search->{$id}->{$key});
	  $n02 =~ s/^\s//;#jbw fix
	  $line .= ";$n01;$n02;$n03";
	}elsif($key eq "date_time"){
	  $search->{$id}->{start_time} =~ s/\s.*// if($search->{$id}->{start_time});
	  $search->{$id}->{end_time} =~ s/\s.*// if($search->{$id}->{end_time});
	  $line .= ";$search->{$id}->{start_time} - $search->{$id}->{end_time}";
 	}else{
     	  $line .= ";$search->{$id}->{$key}";
	}
 	$l_text2 = "$search->{$id}->{$key}" if("$key" eq "ct_name");
 	#$l_text2 = "" if("$key" eq "txt01");
     }
    }
    if("$export" eq "FiBu"){
       print CSV "$l_soll;$l_umsatz;$l_ustnr;$l_haben;$l_gvc;$l_beldat;$l_beleg;$l_opbeleg;$l_skonto;$l_kostnr;$l_text;$l_text2\n" if($l_umsatz != 0);
    }else{
	$line =~ s/\n//g;
	$line =~ s/\r//g;
    	print CSV "$line\n";
    }
   }
   close CSV;

   &csv2xls("",$owner,$time) if($export =~ /check4|FiBu/);

 }#end CSV/FiBu Export####
#
 return $search;
}

# CSV to Excel. Needs first CSV to write XLS.
sub csv2xls(){
 my $self = shift;
 my ($owner,$time) = @_;
 open (CSVFILE, "<:encoding(utf8)","$varenv{pdf}/$owner-$time.csv");# or die "$varenv{pdf}/$owner-$time.csv: $!";
 my $workbook = Spreadsheet::WriteExcel->new("$varenv{pdf}/$owner-$time.xls");
 die "Problems creating new Excel file: $!" unless defined $workbook;
 my $worksheet  = $workbook->add_worksheet();
 #$worksheet->set_column(2, 3, 25);
 my $csv = Text::CSV_XS->new({
                'quote_char'  => '',
                 'escape_char' => '',
                 'sep_char'    => ';',
                 'binary'      => 1
                 });

 my $row = 0;
 while (<CSVFILE>) {
  if ($csv->parse($_)) {
    my @Fld = $csv->fields;
    my $col = 0;
    foreach my $token (@Fld) {
      $token =~ s/[\=\'\"]//g;
      $worksheet->write($row, $col, $token);
      $col++;
     }
    $row++;
   }
   else {
     my $err = $csv->error_input;
     print "Text::CSV_XS parse() failed on argument: ", $err, "\n";
   }
 }
 return;
}


#with c_ids over tpl_ids. ct4tpl
sub collect_cid(){
 my $self = shift;
 my ($table,$lang,$tplids,$rel_id,$barcode,$column2,$content2) = @_;
  $tplids =~ s/,$//;

 my $where = "where ct.c_id=rel.content_id and rel.lang='$lang' and rel.template_id IN ($tplids)";
 if($barcode && $barcode =~ /^\d+$/){
  $where .= " and ct.barcode='$barcode'";
 }elsif($rel_id){
  $where .= " and rel.rel_id='$rel_id'";
 }
 $where .= " and ct.$column2='$content2'" if($content2);
 my $sth = $dbh->prepare("SELECT * FROM $table ct, relation rel $where");
 #my $sth = $dbh->prepare("SELECT ct.barcode || '.' || ct.sort AS barcode_sort, * FROM $table ct, relation rel $where");
 my $rc = $sth->execute();
 my $ct = $sth->fetchall_hashref("c_id");
 $sth->finish;
 return $ct;
}



#counting
sub count_content(){
 my $self = shift;
 my ($table,$main_ids,$tplids) = @_;
 $main_ids =~ s/,$//;
 $tplids =~ s/,$//;
 my $where = "WHERE rel.content_id=ct.c_id and rel.main_id IN ($main_ids)";
 $where .= " and rel.template_id IN ($tplids)" if($tplids =~ /\d/);
 my $sth = $dbh->prepare("SELECT DISTINCT(ct.c_id) FROM relation rel, $table ct $where");
 my $rc = $sth->execute();
 my $rows = $sth->rows;
 return $rows;
}



# copy content
sub copy_content(){
  my $self = shift;
  my ($table,$key_id,$x_id,$columns,$dbinit) = @_;
  $dbh = $dbt->dbconnect() if($dbinit);#because of ajax 
  $columns = "ct_name,barcode,txt01,txt02,txt03,txt04,txt05,txt06,txt07,txt08,txt09,int10,txt10,txt11,txt12,int01,int02,int03,int04,int05,int06,int07,int08" if(!$columns);
  my $sth = $dbh->prepare("INSERT INTO $table ($columns) SELECT $columns from $table where $key_id='$x_id' RETURNING $key_id");
  my $rows = $sth->execute();

  my $last_id;
  $sth->bind_columns(\$last_id);
  my $y_id = $sth->fetchrow_array();

  $dbh->disconnect if($dbinit);
  return $y_id;
}

# insert content (init)
sub insert_content(){
  my $self = shift;
  my ($table,$ct_name,$owner,$sort) = @_;
  $owner="0" if(!$owner);
  $sort="0" if(!$sort);
  my $sth = $dbh->prepare("INSERT INTO $table (ct_name,owner,sort,itime) VALUES(trim('$ct_name'),'$owner','$sort','now()') RETURNING c_id");
  my $rows = $sth->execute();

  my $last_id;
  $sth->bind_columns(\$last_id);
  my $c_id = $sth->fetchrow_array();

  return $c_id;
}


#c_id must be serial primary-key
sub insert_content2(){
  my $self = shift;
  my ($table,$ct_name,$owner,$state) = @_;
  $owner="0" if(!$owner);
  my $sql = "INSERT INTO $table (ct_name,owner) VALUES(trim('$ct_name'),'$owner') RETURNING c_id";
  $sql = "INSERT INTO $table (ct_name,owner,state) VALUES(trim('$ct_name'),'$owner','$state') RETURNING c_id" if($state);
  my $sth = $dbh->prepare($sql);
  my $rows = $sth->execute();

  my $last_id;
  $sth->bind_columns(\$last_id);
  my $c_id = $sth->fetchrow_array();

  return $c_id;
}

#ADD Parts
#$cc_id = 		c_id from content (Waren)
#$foreign_key =		c_id from contenttrans (Verkauf ...), maybe (c_id4trans)
#$from_main_id =	mail_id from content relation (Waren)
sub insert_contenttranspos(){
  my $self = shift;
  my ($table,$ct_name,$owner,$barcode,$cc_id,$foreign_key,$from_main_id,$txt01,$node_name,$unit,$int02,$int03,$umst,$int07,$mandant_id,$int08,$packaged,$txt06,$txt07,$email,$ca_id) = @_;
  $owner="0" if(!$owner);
  $barcode="0" if(!$barcode);
  $int02="0" if(!$int02);#VK Einzel
  $int03="0" if(!$int03);#Menge
  $umst="null" if(!$umst);#UmSt
  $int07="0" if(!$int07);#Rabatt
  $int08="0" if(!$int08);#Rabatteinheit
  $packaged="" if(!$packaged);#packaged
  $txt06="" if(!$txt06);#cooordinates
  $txt07="" if(!$txt07);#voltage
  $foreign_key="0" if(!$foreign_key);
  $from_main_id="0" if(!$from_main_id);
  $ca_id="0" if(!$ca_id);
  my $sth = $dbh->prepare("INSERT INTO $table (ct_name,owner,barcode,cc_id,ct_id,itime,txt01,txt00,txt03,int02,int03,int05,int07,int12,txt12,int08,txt05,txt06,txt07,txt08,ca_id) VALUES(trim('$ct_name'),'$owner','$barcode','$cc_id','$foreign_key','now()','$txt01','$node_name','$unit','$int02','$int03',$umst,'$int07','$from_main_id','$mandant_id','$int08','$packaged','$txt06','$txt07','$email','$ca_id') RETURNING c_id");
  my $rows = $sth->execute();

  my $last_id;
  $sth->bind_columns(\$last_id);
  my $c_id = $sth->fetchrow_array();

  my $set = "SET c_idpos=c_id";
  my $sth12 = $dbh->prepare("UPDATE $table $set WHERE c_id='$c_id'");
  my $rc12 = $sth12->execute();
  return $c_id;
}

# update trivial
sub updater(){
 my $self = shift;
 my ($table,$w_col,$w_val,$column,$content,$owner,$w_col2,$w_op2,$w_val2,$set_time) = @_;
 my $ct_set = "mtime='now()'";
 if($set_time && $set_time eq "no_time"){
  $ct_set = "";
 }elsif($table !~ /content/){
  $ct_set = "change='now()'";
 }

 if("$content" eq "null" || (!$content && $content !~ /^0$/)){
  $ct_set .= ",$column=null";
 }elsif(($column ne "c_id") && ($content || $content == 0)){
  $ct_set .= ",$column='$content'";
 }
 if($table eq "contenttranspos"){#to keep initial channel-ID
  $ct_set .= ",owner_end='$owner'" if($owner);
 }else{
  $ct_set .= ",owner='$owner'" if($owner);
 }
 $ct_set =~ s/^,/ /;
 my $where = "$w_col='$w_val'";

 if("$w_col2" && "$w_op2" && "$w_val2"){
 	if($w_op2 =~ /IN/i){
 		$where .= " and $w_col2 $w_op2 ($w_val2)";
	}else{
 		$where .= " and $w_col2 $w_op2 '$w_val2'";
	}
 } 

 my $rows = 0;
 if($w_col && $w_val){
   my $sth = $dbh->prepare("UPDATE $table SET $ct_set where $where");
   $rows = $sth->execute();
 }
 return $rows;
}

# update trivial in short
sub updater2(){
 my $self = shift;
 my ($table,$w_col,$w_val,$column,$content,$owner,$set_time) = @_;
 my $ct_set = "mtime='now()'";
 if($set_time && $set_time eq "no_time"){
  $ct_set = "";
 }elsif($table !~ /content/){
  $ct_set = "change='now()'";
 }

 if("$content" eq "null" || (!$content && $content !~ /^0$/)){
  $ct_set .= ",$column=null";
 }elsif(($column ne "c_id") && ($content || $content == 0)){
  $ct_set .= ",$column='$content'";
 }
 $ct_set .= ",owner='$owner'" if($owner);
 $ct_set =~ s/^,/ /;
 my $where = "$w_col='$w_val'";

 my $sth = $dbh->prepare("UPDATE $table SET $ct_set where $where");
 my $rows = $sth->execute();
 return $rows;
}
# update for barcode
sub update_barcode(){
 my $self = shift;
 my ($table,$c_id,$ct_name,$barcode,$txt01) = @_;
 my $ct_set = "mtime='now()'";
 $ct_set = "change='now()'" if($table !~ /content/);
 if("$barcode" eq "null" || (!$barcode && $barcode != 0)){
  $ct_set .= ",barcode=null";
 }elsif($barcode || $barcode == 0){
  $ct_set .= ",barcode='$barcode'";
 }

 $ct_set .= ", txt01='$txt01'" if($txt01);
 my $where = "c_id='$c_id'";
 my $sth = $dbh->prepare("UPDATE $table SET $ct_set where $where");
 my $rows = $sth->execute();
 $sth->finish;
 return $rows;
}

#for trivial ajax update
sub update_ajaxes(){
 my $self = shift;
 my ($table,$w_key,$op,$w_val,$column,$value,$owner) = @_;
 my $dbh = $dbt->dbconnect();#because of ajax external handle request
 $op = "=" if(!$op);
 my $where = "where 1=1";
 $where .= " and $w_key $op '$w_val'" if($w_key);
 $where .= " and owner='$owner'" if($owner);
 my $sth = $dbh->prepare("UPDATE $table SET $column='$value' $where");
 my $rows = $sth->execute();
 $dbh->disconnect;
 return $rows;
}

# update content like copy
sub update_content4change(){
 my $self = shift;
 my ($table,$c_id,$ct_name,$content,$column,$owner) = @_;
 my $ct_set = "mtime='now()'";
 #my $ct_set;
 $ct_set .= ",ct_name='$ct_name'" if($ct_name);
 if($content && $content eq "no"){
  $ct_set .= ",$column=''";
 }elsif($content && $content eq "null"){
  $ct_set .= ",$column=$content";
 }elsif($content || ($content && $content eq "0")){
  $ct_set .= ",$column='$content'" if($column ne "ct_name");
 }
 if($column eq "mtime"){#überschreibt obiges
  if($content =~ /\d/){#TODO real time
   $ct_set = "mtime='$content'";
  }else{
   $ct_set = "";
  }
 }
 if($table eq "contenttranspos"){#to keep initial channel-ID
  $ct_set .= ",owner_end='$owner'" if($owner);
 }else{
  $ct_set .= ",owner='$owner'" if($owner);
 }
 if($ct_set){
  my $sth = $dbh->prepare("UPDATE $table SET $ct_set where c_id='$c_id'");
  my $rows = $sth->execute();
  return $rows;
 }
}


# update content without mtime-change
sub update_content4change2(){
 my $self = shift;
 my ($table,$c_id,$content,$column,$owner) = @_;
 my $ct_set;
 if("$content" eq "no"){
  $ct_set = "$column=''";
 }elsif($content || $content eq "0"){ 
  $ct_set = "$column='$content'";
 }
 if($table eq "contenttranspos"){#to keep initial channel-ID
  $ct_set .= ",owner_end='$owner'" if($owner);
 }else{
  $ct_set .= ",owner='$owner'" if($owner);
 }
 if($ct_set){
  my $sth = $dbh->prepare("UPDATE $table SET $ct_set where c_id='$c_id'");
  my $rows = $sth->execute();
  return $rows;
 }  
}

sub update_kasse(){
 my $self = shift;
 my ($table,$c_id,$sum_kasse,$sum_start) = @_;
 #set kassenbestand
 my $sth = $dbh->prepare("UPDATE $table SET int01='$sum_kasse',int02='$sum_start' where c_id='$c_id'");
 my $rows = $sth->execute();
 return $rows;  
}

sub update_tagesabschluss(){
 my $self = shift;
 my ($table,$c_idkasse,$journal_main_id,$tpl_journal,$tpl_vk,$mandant_main_id,$main_ids,$s_owner_id,$opos) = @_;
 my $where;
 $where = " and ct.owner='$s_owner_id'" if($s_owner_id);
 $where = " and ct.int14 is $opos" if($opos eq "null");
 #close transactions #int11 zusätzlich für Kassenabschluss referenz c_id
 my $sth = $dbh->prepare("UPDATE $table SET close_time='now()',int11=$c_idkasse where c_id IN (SELECT ct.c_id from $table ct, relation rel where ct.c_id=rel.content_id and (ct.state ~ '[a-z]') and ct.int01 is not null and rel.main_id IN ($main_ids) and ct.close_time is null $where)");
 my $rows = $sth->execute();
 #move/set to journal
 $sth = $dbh->prepare("UPDATE relation SET main_id='$journal_main_id', template_id='$tpl_journal' where template_id='$tpl_vk' and content_id IN (SELECT ct.c_id from $table ct, relation rel where ct.c_id=rel.content_id and (ct.state ~ '[a-z]') and ct.int01 is not null and rel.main_id IN ($main_ids) $where)");
 #$sth = $dbh->prepare("UPDATE $table SET close_time='now()',int11='$c_idkasse' where (state ~ '[a-z]') and int01 is not null and int12 IN ($main_ids) and close_time is null $where");
 $rows = $sth->execute();
 return $rows;  
}

#X collect last journal abschluss to get workflow transactions
sub collect_Xlast(){
 my $self = shift;
 my ($c_idkasse,$main_id,$tpl_id,$mandant_main_id) = @_;
 my $sth = $dbh->prepare("SELECT ct.c_id,ct.ct_name FROM contenttrans ct, relation rel where ct.c_id=rel.content_id and rel.main_id = $main_id and rel.template_id = $tpl_id and rel.lang='de' and ct.int11 = $c_idkasse and ct.ct_name like '%-%'"); # Muster: RechnungNr-XBelegNr OR QuittungNr-XWorkflowNr
 my $rc = $sth->execute();
 my $ct4rel = $sth->fetchall_hashref("c_id");
 $sth->finish;
 return $ct4rel;
}

sub update_Xabschluss(){
 my $self = shift;
 my ($table,$c_idkasse,$journal_main_id,$tpl_journal,$tpl_vk,$mandant_main_id,$main_ids,$s_owner_id,$ct_name) = @_;
 my $where;
 $where = " and ct.owner='$s_owner_id'" if($s_owner_id);
 my $sth = $dbh->prepare("UPDATE $table SET close_time='now()',int11='$c_idkasse' where c_id IN (SELECT ct.c_id from $table ct, relation rel where ct.c_id=rel.content_id and (ct.ct_name = '$ct_name' OR ct.ct_name like '$ct_name-%' OR ct.ct_name like '%-$ct_name') and ct.int01 is null and rel.main_id IN ($main_ids) and ct.close_time is null $where)");
 my $rows = $sth->execute();
 #move/set to journal
 $sth = $dbh->prepare("UPDATE relation SET main_id='$journal_main_id', template_id='$tpl_journal' where template_id='$tpl_vk' and content_id IN (SELECT ct.c_id from $table ct, relation rel where ct.c_id=rel.content_id and (ct.ct_name = '$ct_name' OR ct.ct_name like '$ct_name-%' OR ct.ct_name like '%-$ct_name') and ct.int01 is null and rel.main_id IN ($main_ids) $where)");
 $rows = $sth->execute();
 return $rows;  
}


#compute content
sub update_content4comp(){
 my $self = shift;
 my ($table,$ct_name,$c_id,$operator,$menge,$kind_of_trans,$lager) = @_;
 my $where;

 if($c_id){
   $where = "c_id='$c_id'";
 }elsif($ct_name =~ /(\d+)/ && ($table eq "content")){
   $ct_name = $1;
   $where = "(ct_name='$ct_name' OR barcode='$ct_name')";
 }elsif($ct_name){
   $where = "where (ct_name='$ct_name')";
 }
 my $val="0";
 $val=$menge if($menge);
 $val = $val * -1 if($table eq "content" && $kind_of_trans eq "Einkauf");
 my $ct_set = "int03=(int03 $operator $val)";
 my $sth = $dbh->prepare("UPDATE $table SET $ct_set where $where") if($c_id || $ct_name);
 my $rows = $sth->execute();
 return $rows;
}

# Delete content
sub delete_content(){
 my $self = shift;
 my ($table,$c_id,$owner,$sort) = @_;
   my $where = "c_id='$c_id'";
   $where .= " and owner='$owner'" if($owner);
   $where .= " and sort='$sort'" if($sort);
   my $sth = $dbh->prepare("DELETE FROM $table WHERE $where");
   my $rc = $sth->execute();
   return $rc;
}

# Delete time
sub delete_time(){
 my $self = shift;
 my ($table,$t_id) = @_;
 my $sth = $dbh->prepare("DELETE FROM $table WHERE t_id='$t_id'");
 my $rc = $sth->execute();
 return $rc;
}

#delete users
sub delete_users(){
  my $self = shift;
  my ($u_id,$owner) = @_;
  my $sth = $dbh->prepare("DELETE from users where u_id = '$u_id'");
  my $rows = $sth->execute();
  if($owner){
   $sth = $dbh->prepare("DELETE from contentuser where owner = '$owner'");
   $rows = $sth->execute();
  }
  #$sth = $dbh->prepare("DELETE from contentpers where owner = '$owner'");
  #$rows = $sth->execute();
  return $rows;
}

# logout alias cook_out
sub cook_out(){
  my $self = shift;
  my $coo = shift;
  my $sth = $dbh->prepare("UPDATE users SET cookie='', change='now()' where cookie='$coo'");
  my $rc = $sth->execute();
  return $rc;
}


#just after checked if users.u_id=contentadr.c_id
sub select_users(){
  my $self = shift;
  my $u_id = shift || 0;
  my $sth = $dbh->prepare("SELECT * FROM users WHERE u_id=$u_id");
  my $rc = $sth->execute();
  my $auth = $sth->fetchrow_hashref();
  return $auth;
}


sub get_owner(){
  my $self = shift;
  my ($owner,$dbinit) = @_;
  $dbh = &dbconnect() if($dbinit);#because of ajax
  my $sth = $dbh->prepare("SELECT * FROM users WHERE owner='$owner'");
  my $rc = $sth->execute();
  my $ner = $sth->fetchrow_hashref();
  $dbh->disconnect if($dbinit);
  return $ner;
}


# collect all users
sub collect_users(){
 my $self = shift;
 my $sth = $dbh->prepare("SELECT * FROM users");
 my $rc = $sth->execute();
 my $users = $sth->fetchall_hashref("owner");
 $sth->finish;
 return $users;
}

1;
