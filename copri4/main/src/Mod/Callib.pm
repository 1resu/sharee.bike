package Callib;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
use strict;
use warnings;
use POSIX;
use DBI;
use CGI;    # only for debugging

#Deb libcalendar-simple-perl
use Calendar::Simple;
use Date::Calc qw(:all);
use Scalar::Util qw(looks_like_number);
use Mod::Buttons;
use Lib::Config;
use Mod::Basework;

my $cf  = new Config;
my $but = new Buttons;
my $bw = new Basework;

my $q   = new CGI;
$q->import_names('R');

sub new {
    my $class = shift;
    my $self  = {};
    bless( $self, $class );
    return $self;
}

my %varenv   = $cf->envonline();
my $now_time = strftime "%Y-%m-%d %H:%M", localtime;
my $day  = strftime "%d", localtime;
my $mon  = strftime "%m", localtime;
my $year = strftime "%Y", localtime;

#start- end- date time
sub datetime_defaults(){
    my $self = shift;
    my ($in_date,$in_time,$lang) = @_;
    my $now_date_time = strftime "%Y-%m-%d %H:%M", localtime;
    my $now_date      = strftime "%Y-%m-%d",       localtime;
    my $now_time      = strftime "%H:%M",          localtime;

    if($in_date =~ /(\d{4})-(\d+)-(\d+)/){
	$now_date = "$1-$2-$3";
    }elsif($in_date =~ /(\d+)\.(\d+)\.(\d+)/){
        $now_date = "$3-$2-$1";
    }

    my ( $year, $month, $day ) = split( /-/, $now_date );

    my ($nyear,$nmonth,$nday) = Add_Delta_YMD($year,$month,$day, 0,0,1);
    $nday = "0" . $nday if ( $nday < 10 );
    $nmonth = "0" . $nmonth if ( $nmonth < 10 );
    my $start_datetime = "$year-$month-$day";
    my $end_datetime = "$nyear-$nmonth-$nday";
    $start_datetime = "$day.$month.$year" if(lc($lang) eq "de");
    $end_datetime = "$nday.$nmonth.$nyear" if(lc($lang) eq "de");
    $start_datetime .= " $now_time" if(!$in_time);
    $end_datetime .= " $now_time" if(!$in_time);
    return ($start_datetime,$end_datetime);
}

#month map
sub monthmap(){
  my @_months = ("Januar","Februar","März","April","Mai","Juni","Juli","August","September","Oktober","November","Dezember");
  return @_months;
}

#day map
sub daymap(){
  my @_days = ("So","Mo","Di","Mi","Do","Fr","Sa");
  return @_days;
}

#hour map
sub hourmap(){
  my @_hours = (
        "00:00", "01:00", "02:00", "03:00", "04:00", "05:00",
        "06:00", "07:00", "08:00", "09:00", "10:00", "11:00",
        "12:00", "13:00", "14:00", "15:00", "16:00", "17:00",
        "18:00", "19:00", "20:00", "21:00", "22:00", "23:00"
  );
  return @_hours;
}

#english input date_time check
sub checkdate_time() {
    my $self = shift;
    my $date_time = shift;
    $date_time =~ s/:\d{2}\..*$//;
    my $date = $date_time;
    my $time = "00:00:00";
    ($date, $time) = split(/ /, $date_time) if($date_time =~ /\d+\s\d+/); 
    my ( $y, $m, $d ) = split( /-/, $date );
    my ( $hour, $min, $sec ) = split( /\:/, $time );

    my $check_time = 1;
    $check_time = 0 if(looks_like_number($hour) && $hour > 24);
    $check_time = 0 if(looks_like_number($min) && $min > 60);
    $check_time = 0 if(looks_like_number($sec) && $sec > 60);
    #print "$y, $m, $d && $check_time";
    if ( check_date( $y, $m, $d ) && $check_time) {
	return $date_time;
    }else{
	return 0;
    }
}

# input date check
sub checkdate() {
    my $self = shift;
    my ( $date, $time ) = @_;
    my $d_chck = 1;
    $date =~ s/,/-/g;
    $date =~ s/\./-/g;
    my ( $y, $m, $d ) = split( /-/, $date );
    if ( check_date( $y, $m, $d ) ) {
        return "$y-$m-$d";
    }
    else {
        return "$year-$mon-$day";
    }
}


#split date
sub split_date(){
 my $self = shift;
 my ($time_db) = @_;
 $time_db =~ s/:\d{2}\..*$// if($time_db);
 my ($date,$time) = split(/ /,$time_db);
 my ($yy,$mo,$dd) = split(/-/,$date) if($date =~ /-/);
 my ($hh,$mi) = split(/\:/,$time);
 return ($yy,$mo,$dd,$hh,$mi);
}

#time and date format for DE
sub time4de(){
 my $self = shift;
 my ($time_db,$hhmi,$decode) = @_;
 $time_db =~ s/:\d{2}\..*$// if($time_db);
 my ($date,$time) = split(/ /,$time_db);
 my ($yy,$mo,$dd) = split(/-/,$date);
 my ($hh,$mi) = split(/\:/,$time);
 my $date_de = "&nbsp;";
 $date_de = "$dd.$mo.$yy";
 $date_de = "$dd.$mo.$yy $hh:$mi" if($hhmi);

 #Deutsch     (German)       ==>    3
 $date_de = Date_to_Text_Long($yy,$mo,$dd,3) if($decode eq "Date_to_Text_Long");
 $date_de =~ s/M.*rz/März/;
 return $date_de;
}


#Prepares contenttranspos start_time, end_time and count/Menge
sub contenttranspos_dating() {
	my $self = shift;
	my ($pos_id,$pos_start_time,$pos_end_time,$today4db,$hours) = @_;
	my $menge = 0;

	$bw->log("sub contenttranspos_dating call from Callib:",\@_,"");

         my $start_datetime = $today4db;
          my $end_datetime = $today4db;
          $start_datetime = "$1-$2-$3 $4:$5" if($pos_start_time =~ /(\d{4})-(\d{2})-(\d{2})\s(\d{2}):(\d{2})/);
          $start_datetime = "$3-$2-$1 $4:$5" if($pos_start_time =~ /(\d{2})\.(\d{2})\.(\d{4})\s(\d{2}):(\d{2})/);
          $end_datetime = "$1-$2-$3 $4:$5" if($pos_end_time =~ /(\d{4})-(\d{2})-(\d{2})\s(\d{2}):(\d{2})/);
          $end_datetime = "$3-$2-$1 $4:$5" if($pos_end_time =~ /(\d{2})\.(\d{2})\.(\d{4})\s(\d{2}):(\d{2})/);

          if($start_datetime && $end_datetime){
            my ($s_yy,$s_mo,$s_dd,$s_hh,$s_mi) = &split_date("",$start_datetime);
            my $s_time = Mktime($s_yy,$s_mo,$s_dd,$s_hh,$s_mi,0);

            my ($e_yy,$e_mo,$e_dd,$e_hh,$e_mi) = &split_date("",$end_datetime);

	    #Add 1hour and rebuild end_datetime
	    my $sec=0;
	    ($e_yy,$e_mo,$e_dd,$e_hh,$e_mi,$sec) = Add_Delta_DHMS($e_yy,$e_mo,$e_dd,$e_hh,$e_mi,0, 0,$hours,0,0) if($hours =~ /^\d+$/);
            my $e_time = Mktime($e_yy,$e_mo,$e_dd,$e_hh,$e_mi,0);
	    $end_datetime = "$e_yy-$e_mo-$e_dd $e_hh:$e_mi";
	    
	    #$menge not used via Transposition and at last Prelogic.pm, 
	    #we believe setting by manually insert of int03=$menge
	    if(1==1){
	    	#Count Menge in hours
            	my $diff_time = $e_time - $s_time;
            	$menge = $diff_time / 3600;#to get hours
	    }
          }
   
   my @return_array = ($pos_id,$start_datetime,$end_datetime,$menge);
   $bw->log("sub contenttranspos_dating return from Callib:",\@return_array,"");

   return ($start_datetime,$end_datetime,$menge);
}

1;
