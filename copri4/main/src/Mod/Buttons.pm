package Buttons;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
#
use strict;
use warnings;
use Lib::Config;
use Data::Dumper;

sub new {
 my $class = shift;
  my $self = {};
  bless($self,$class);
  return $self;
}

my $cf = new Config;
my %varenv = $cf->envonline();
my $icon = "/icon";

#my standard internationalize submit buttons
sub ibuttons(){
 my $self = shift;
 my %ib = (
	'set_gps' => 'Position',
	'operator_accounting' => '2. Abrechnung generieren',
	'service_done' => 'Wartungsprotokoll für ausgewähltes Rad einfügen/bearbeiten',
	'post_email' => 'submit',
	'barcode' => 'Barcode Label drucken',
	'buchen' => 'buchen',
	'send_invoice_again' => 'senden',
	'new_contenttver' => 'Neues Arbeitsprofil',
	'new_content' => 'NEUER Datensatz',
	'new_content_2' => 'Add New Optical Instrument',
	'new_adr' => 'Kunde anlegen',
	'new_trans' => 'NEUE Transaktion',
	'new_tver' => 'NEU',
	'new_nel' => 'NEUE Nachricht',
	'new_transdate' => 'NEUER Kalendereintrag',
	'new_time' => 'NEUER Termin',
	'new_dmsusers' => 'NEUER DMS Account',
	'new_relation' => 'NEUES Menue anlegen',
	'new_relation4sub' => 'NEUES Sub-Menue anlegen',
	'save_content' => 'Speichern',
	'save_ctuser' => 'Save',
	'save_nel' => 'Nachricht speichern',
	'context_copy_content' => 'Kopie im Kontext der internen Barcode Nummer',
	'copy_content' => 'Kopie fuer neuen Datensatz',
	'move_relation' => 'Menue verschieben',
	'move_content' => 'Datensatz in anderen Ordner verschieben',
	'save_adr' => 'Kunden speichern',
	'save_text' => 'Text speichern',
	'save_dmsusers' => 'DMS Account speichern',
	'save_relation' => 'Menue speichern',
	'remove_chk4rel' => 'löschen',
	'delete_content' => 'löschen',
	'delete_adr' => 'Kunden löschen',
	'delete_nel' => 'Nachricht löschen',
	'newsletter_mailman' => 'Nachricht senden',
	'delete_dmsusers' => 'DMS Account löschen',
	'delete_trans' => 'löschen',
	'delete_relation' => 'Menue löschen',
	'save_pos' => 'Datensatz speichern',
	'delete_pos' => 'Datensatz löschen',
	'save_name' => 'Kunden speichern',
	'search' => 'suchen',
	'search_product' => 'Search',
	'search_calendar' => 'anzeigen',
	'search_adr' => 'Kunden suchen',
	'search_extrakt' => 'extrakt',
	'change_login' => 'ID',
	'print_pdf' => ' PDF ',
	'set_relation' => 'Formular',
	'set_workflow' => 'Workflow',
	'set_workflow2invoice' => 'Rechnung generieren',
	'set_workflow2storno' => 'Storno generieren',
	'set_accounting2invoice' => 'Rechnung generieren',
	'relate_content' => 'Content-Menu Relation',
	'relate_dialog' => 'Relation herstellen',
	'relate_dialog4menu' => 'Relation herstellen',
	'save_media' => 'Foto speichern',
        'delete_media' => 'Foto löschen',
        'save_database' => 'Datenbank Sicherung',
	'XLSout_contentadr' => 'XLS Export',
	'more' => 'more',
        'Login' => 'Login',
        'login' => 'login'
	);
 return %ib;
}

my %ib = ibuttons();

#buttons for jvbasel
sub ibuttons_arch(){
 my $self = shift;
 my ($counter) = @_;
 my %ib = (
	'reload_search' => 'Reload',
	'new_content_1' => 'NEU',
	'new_content_2' => 'Objekt hinzufuegen',
	'new_relation4sub' => 'Ordner hinzufuegen',
	'new_relation4sub_collection' => 'Sammlung hinzufuegen',
	'new_attrtpl_int' => 'Checkbox/Integer Datenfeld erzeugen',
	'new_attrtpl_txt' => 'Text Datenfeld erzeugen',
	'new_adr' => 'NEUE Adresse',
	'move_dnd' => 'Objekt verschieben',
	'relate_dnd' => 'Objekt verlinken',
	'copy_dnd' => 'Objekt kopieren',
	'remove_chk4rel' => 'Objekt löschen',
	'remove_chk4rel_en' => 'Delete Content',
	'save_content' => 'Objekt speichern',
	'relate_dialog4menu' => 'Relation herstellen',
	'save_content_en' => 'Save Content',
	'save_attrtpl' => 'Eigenschaft speichern',
	'remove_chk4attr' => 'Eigenschaft löschen',
	'delete_content' => 'löschen',
	'XLSout' => 'XLS Export',
	'PDFout' => 'PDF Export',
	'object' => 'Ansicht',
	'list_view' => 'Ansicht',
	'object_view' => 'Ansicht',
	'search' => 'suchen',
	'more' => 'more',
        'Login' => 'Login',
        'login' => 'login'
        );
 return %ib;
}
	

# Language Icons
sub langicon_pic(){
 my $self = shift;
 my ($script,$lang_name,$lang,$view) = @_;
 if($script =~ /(\/src\/index\.pl)/){
   $script = $1
 }else{
   $script = "/src";
 }
 my $icon = "$icon/$lang.gif";
 my $lang_href = "$script/$lang/$view";
 my $langicon = "<a title='$lang_name' href='$lang_href'><img src='$icon' border='0' width='18' height='12' alt='$lang_name' /></a>\n";
 return $langicon;
}

sub langicon_txt(){
 my $self = shift;
 my ($script,$lang_name,$main_id,$lang,$view) = @_;
 if($script =~ /(\/src\/index\.pl)/){
   $script = $1
 }else{
   $script = "/src";
 }
 my $lang_href = "$script/$lang/$view";
 my $langicon = "<a class='headnav' style='margin:1em 0 0 1em' title='$main_id' href='$lang_href'>$lang_name</a>\n";
 return $langicon;
}

# Head Main Menue Button
sub head_button(){
 my $self = shift;
 my ($node_path,$node_name,$style) = @_;
 my $button = "<div style='font-size:0.80em;float:left;$style'><a style='margin:0 0.5em;' class='linknav' href='$node_path'>$node_name</a></div>\n";
 return $button;
}

# Top Main Menue Button
sub top_button(){
 my $self = shift;
 my ($node_path,$node_name,$main_id,$style) = @_;
 my $txt_color;
 $txt_color = "color: black;" if($style =~ /white/);
 my $button = "<li><a style='$style $txt_color' title='' href='$node_path'>$node_name</a></li>\n";
 return $button;
}
# a link for Top2
sub a_button(){
 my $self = shift;
 my ($node_path,$node_name,$main_id,$mstyle) = @_;
 my $txt_color;
 #$txt_color = "color: black;" if($mstyle =~ /white/);
 my $button = "<a id='id_$main_id' style='$mstyle $txt_color' title='$main_id' href='$node_path'>$node_name</a>|\n";
 #my $button = "<a id='id_$main_id' style='$style $txt_color' title='$main_id' href='javascript:mainbrowser(\"$node_path\")'>$node_name</a>|";
 return $button;
}

#with mousevent
sub event_button(){
 my $self = shift;
 my ($node_path,$node_name,$main_id,$class,$style,$owner,$url,$icon,$icon_style) = @_;
 my $debug = $main_id if($owner && $owner eq $varenv{superu_id});
 my $image = "<img src='$icon' style='$icon_style' />" if($icon) || "";
 my $button = "<li><a onmousedown='WhichButton(event,\"$url\");' class='$class' style='$style' title='$debug' href='$node_path'>$image $node_name</a></li>";
 return $button;
}

sub event_button2(){
 my $self = shift;
 my ($node_path,$node,$class,$style,$owner,$url,$icon,$icon_style) = @_;
 $node->{change} =~ s/\.\d.*//;
 my $title = "($node->{change})";
 $title .= " | main_id: $node->{main_id}" if($owner && $owner eq $varenv{superu_id});
 my $image = "<img src='$icon' style='$icon_style' />" if($icon) || "";
 my $button = "<li><a onmousedown='WhichButton(event,\"$url\");' class='$class' style='$style' title='$title' href='$node_path'>$image $node->{node_name}</a></li>";
 return $button;
}

#just a without li
sub lo_button(){
 my $self = shift;
 my ($node_path,$node_name,$main_id,$class,$style,$owner,$script,$mtop,$mleft,$icon,$icon_style) = @_;
 my $debug = $main_id if($owner && $owner eq $varenv{superu_id});
 my $image = "<img src='$icon' style='$icon_style' />" if($icon) || "";
 my $button;
 if($script){
   $button = "<a class='$class' style='$style' title='$debug' onclick='$script(\"$node_path\",\"$mtop\",\"$mleft\")'>$image $node_name</a>";
 }else{
   $button = "<a class='$class' style='$style' title='$debug' href='$node_path'>$image $node_name</a>";
 }
 return $button;
}

#with mousevent over/out
sub lo_button2(){
 my $self = shift;
 my ($node_path,$node_name,$node_name_int,$main_id,$class,$style,$owner) = @_;
 my $debug = $main_id if($owner && $owner eq $varenv{superu_id});
 my $button = "<a onmouseover=\"this.innerHTML = '$node_name_int'\" onmouseout=\"this.innerHTML = '$node_name'\" class='$class' style='$style' title='$debug' href='$node_path'>$node_name</a>";
 return $button;
}
#mit li
sub lia_button(){
 my $self = shift;
 my ($node_path,$node_name,$main_id,$class,$style,$owner) = @_;
 my $debug = $main_id if($owner && $owner eq $varenv{superu_id});
 my $target = "_self";
 $target = "_blank" if($node_path =~ /http/);
 my $button = "<li><a class='$class' style='$style' title='$debug' href='$node_path' target='$target'>$node_name</a></li>\n";
 return $button;
}



# select tag
sub selector_color() {
    my $self        = shift;
    my $colname = shift;
    my $style = shift;
    my $value  = shift;
    my @selval = @_;

    my @selopt = ();
    foreach my $id (@selval) {
        my $ostyle;
        $ostyle = "background-color:$id" if($style =~ /background-color/);
        if ($id eq $value) {
            push @selopt, "<option style='$ostyle' selected value='$id'>$value</option>\n";
        }
        else {
            push @selopt, "<option style='$ostyle' value='$id'>$id</option>\n";
        }
    }
    my $selret;
    if($style =~ /background-color/){
      $selret = "<select style='$style;' onchange='this.style.backgroundColor = this.options[this.selectedIndex].style.backgroundColor;' name='$colname'>@selopt</select>\n";
    }
    return $selret;
}


# select tag
sub selector(){
 my $self = shift;
 my $tbl_columne = shift;
 my $width = shift || "";
 my $sel = shift || "";
 my @selval = @_;

 my @selopt = ();
 foreach my $opt (@selval){
  my $id = $opt;
  my $value = $opt;
  ($id,$value) = split /:/,$opt if($opt =~ /\w+\:.*/);
    if($sel eq $id){
     push @selopt, "<option selected value=\"$id\">$value</option>\n";
    }else{
     push @selopt, "<option value=\"$id\">$value</option>\n";
    }
 }
 my $selret = "<select class='eselect' style='width:$width' size=1 name='$tbl_columne' id='$tbl_columne'>@selopt</select>\n";
 return $selret;
}

# select tag with class
sub selector_class(){
 my $self = shift;
 my ($column,$class,$style,$sel,@selval) = @_;

 my @selopt = ();
 foreach my $opt (@selval){
  my $des_style = "";
  my $id = $opt;
  my $value = $opt;
  ($id,$value) = split /:/,$opt if($opt =~ /\:/);
  $des_style = "color:grey;" if(!$id);
    if($sel && $sel eq $id){
     push @selopt, "<option style='$des_style' selected value='$id'>$value</option>\n";
    }else{
     push @selopt, "<option style='$des_style' value='$id'>$value</option>\n";
    }
 }
 my $selret = "<select class='$class' style='background-color:white;$style' name='$column'>@selopt</select>\n";
 return $selret;
}

# select tag by id and class onChange
sub selector_byidclass(){
 my $self = shift;
 my ($column,$byid,$class,$style,$sel,@selval) = @_;

 my @selopt = ();
 foreach my $opt (@selval){
  my $id = $opt;
  my $value = $opt;
  ($id,$value) = split /:/,$opt if($opt =~ /\w+\:.*/);
    if($sel && $sel eq $id){
     push @selopt, "<option selected value=\"$id\">$value</option>\n";
    }else{
     push @selopt, "<option value=\"$id\">$value</option>\n";
    }
 }
 #my $selret = "<select id='$byid' class='$class' style='$style' name='$column' onChange='day_mod_save(this)'>@selopt</select>\n";
 my $selret = "<select id='$byid' class='$class' style='$style' name='$column'>@selopt</select>\n";
 return $selret;
}

# select  onChange
sub userselector_onchange(){
 my $self = shift;
 my $key = shift;
 my $selected = shift;
 my $users_all = shift;
 my $admin_name = shift || "";
 my $class = shift || "";
 my $style = shift || "";

 my @selopt = ();
 foreach my $c_id (sort { $users_all->{$a}->{txt08} cmp $users_all->{$b}->{txt08} } keys (%$users_all)){
    #print $c_id . "|" . $users_all->{$c_id}->{txt08} . "\n";
    $users_all->{$c_id}->{txt08} .= " (Ferien)" if($users_all->{$c_id}->{txt08} eq $admin_name);
    if($selected eq $c_id){
     push @selopt, "<option selected value='$c_id'>$users_all->{$c_id}->{txt08}</option>\n";
    }else{
     push @selopt, "<option value='$c_id'>$users_all->{$c_id}->{txt08}</option>\n";
    }
 }
 my $selret = "<select class='$class' style='$style' name='$key' onChange='select_option(this)'>\n@selopt</select>\n";
 return $selret;
}

# select2 tag
sub selector2(){
 my $self = shift;
 my $tbl_columne = shift;
 my $width = shift;
 my $height = shift;
 my $sel = shift;
 my @selval = @_;

 my @selopt = ();
 foreach my $opt (@selval){
  my $id = $opt;
  my $value = $opt;
  ($id,$value) = split /:/,$opt if($opt =~ /\d\:.*/);
    #print "$sel eq $id || ($sel && $sel =~ /$id|$opt/<br>";
    if($sel && $id && (("$sel" eq "$id") || ("$sel" =~ /$id|$opt/))){
     push @selopt, "<option selected value=\"$id\">$value</option>\n";
    }else{
     push @selopt, "<option value=\"$id\">$value</option>\n";
    }
 }
 my $selret = "<select multiple class=\"eselect_multiple\" style=\"width:$width\" size=$height name=\"$tbl_columne\" >@selopt</select>\n";
 return $selret;
}

# select3 tag with id:value
sub selector3(){
 my $self = shift;
 my $tbl_columne = shift;
 my $width = shift;
 my $height = shift;
 my $sel = shift;
 my @selval = @_;

 my @selopt = ();
 foreach my $opt (@selval){
  my $id = $opt;
  my $value = $opt;
  ($id,$value) = split /:/,$opt if($opt =~ /\d\:.*/);
    #print "$sel =~ /\w+/ && $sel =~ /$id|$value/)<br>";
    if($sel && "$sel" =~ /$id|$value/){
     push @selopt, "<option selected value=\"$id:$value,\">$value</option>\n";
    }else{
     push @selopt, "<option value=\"$id:$value,\">$value</option>\n";
    }
 }
 my $multiple = "";
 $multiple = "multiple" if($height > 1);
 my $selret = "<select $multiple class=\"eselect_multiple\" style=\"width:$width\" size=$height name=\"$tbl_columne\" >\n@selopt</select>\n";
 return $selret;
}


#with onchange event
sub selector_onchange(){
 my $self = shift;
 my $key = shift;
 my $width = shift;
 my $path = shift;
 my $jscript = shift;
 my $sel = shift;
 my $todo = shift;
 my @selval = @_;

 my @selopt = ();
 foreach my $opt (@selval){
    my $id = $opt;
    my $value = $opt;
    ($id,$value) = split /:/,$opt if($opt =~ /\d\:.*/);
    my $rid = "$id";
    $rid = "$path?change_login=$value" if($todo eq "change_login");
    $rid = "$value?$path\&$key=$id" if($key =~ /set_main_id/);
    if("$sel" eq "$id"){
     push @selopt, "<option selected value=\"$rid\">$value</option>\n";
    }else{
     push @selopt, "<option value=\"$rid\">$value</option>\n";
    }
 }
 my $selret = "<a><select class=\"eselect\" style=\"width:$width\" size=1 name=\"$key\" onChange=\"$jscript go2select(this)\">@selopt</select></a>\n";
 return $selret;
}

sub checkbox_style() {
        my $self = shift;
        my ($sid_key,$dialog)  = @_;
        my $ck_style = "<style type='text/css'><!--
        .sq_$dialog$sid_key {margin:auto;position: relative;width: 16px;height: 16px;}
        .sq_$dialog$sid_key label {cursor: pointer;position: absolute;width: 16px;height: 16px;background-color:grey;}
        .sq_$dialog$sid_key :checked + label {content: '';position: absolute;width: 16px;height: 16px;background-color: #333333;}
        //--></style>\n";
        return $ck_style;
}

#checkbox
sub checkbox(){
 my $self = shift;
 my $val = shift;
 my $check_name = shift;
 my $checked = shift;
 my $title = shift || "";
 my $required = shift || "";
 my $autofocus = shift || "";

 $checked = "checked" if($checked);
 $required = "required" if($required);
 my $checkb = "<input type='checkbox' style='border: 1px solid silver;' name='$check_name' value='$val' title='$title' $checked $required $autofocus>"; 
 return $checkb;
}

#reverse checkbox
sub rev_checkbox(){
 my $self = shift;
 my ($val,$check_name,$checked,$title) = @_;
 if(!$checked){
   $checked = "checked"; 
 }
 my $checkb = "<input type='checkbox' style='border: 1px solid;' name='$check_name' value='$val' title='$title' $checked>"; 
 return $checkb;
}

#radio (seems not working!!)
sub radiobox(){
 my $self = shift;
 my ($val,$check_name,$checked) = @_;
 $checked = "checked" if($checked);
 my $checkb = "<input type='radio' class='ebutton' name='$check_name' value='$val' $checked>"; 
 return $checkb;
}

#radio
#$but->radiobox2("$key","$ctrel->{$key}","$a","$b","$c")
sub radiobox2(){
 my $self = shift;
 my $key = shift;
 my $val = shift;
 my $a_name = shift || "";
 my $b_name = shift || "";
 my $c_name = shift || "";
 my $a_checked = "";
 my $b_checked = "";
 my $c_checked = "";
 $a_checked = "checked" if(!$val);
 $b_checked = "checked" if($val==1);
 $c_checked = "checked" if($val==2);
 my $checkb = "";
 $checkb .= " $a_name <input type='radio' name='$key' value='0' $a_checked>" if($a_name);
 $checkb .= " $b_name <input type='radio' name='$key' value='1' $b_checked>" if($b_name);
 $checkb .= " $c_name <input type='radio' name='$key' value='2' $c_checked>" if($c_name);
 return $checkb;
}

#radio reverse
sub radiobox2reverse(){
 my $self = shift;
 my ($key,$val,$a_name,$b_name) = @_;
 my $a_checked;
 my $b_checked;
 $a_checked = "checked" if($val);
 $b_checked = "checked" if(!$val);
 my $checkb = "$a_name <input type='radio' name='$key' value='1' $a_checked> $b_name <input type='radio' name='$key' value='0' $b_checked>"; 
 return $checkb;
}

#Returns one radiobox ... you need min two to get a radioboxe
sub radiobox_vertical(){
 my $self = shift;
 my ($key,$val,$checked,$boxname) = @_;
 $checked = "checked" if($checked);
 my $radiobox = "<label><input type='radio' name='$key' value='$val' $checked>$boxname</label>"; 
 return $radiobox;
}

# Submit Buttons
sub singlesubmit(){
 my $self = shift;
 my ($function,$b_name,$jscript) = @_;
 my $button;
 if($jscript){
     $button = "<input type='submit' onClick='javascript:$jscript' class='ebutton' name='$function' value='$b_name'>";
 }else{
     $button = "<input type='submit' class='ebutton' name='$function' value='$b_name'>"; 
 }
 return $button;
}

# Submit Buttons with db based button
sub singlesubmit10(){
 my $self = shift;
 my ($function,$b_val,$b_name) = @_;
 my $button = "<button type='submit' class='ebutton' name='$function' value='$b_val'>$b_name</button>"; 
 return $button;
}

sub singlesubmit1(){
 my $self = shift;
 my $function = shift;
 my $button_key = shift;
 my $class = shift || "ebutton";
 my $style = shift || "";
 my $ib_view = $button_key;
 while (my($key,$val) = each(%ib)) {
   $ib_view = $val if($key eq $button_key);
 }
 my $button = "<button type='submit' class='$class' style='$style' name='$function' value='$button_key' title=''>$ib_view</button>"; 
 return $button;
}

# with counter field
sub singlesubmit6(){
 my $self = shift;
 my ($function,$b_name,$counter) = @_;
 %ib = &ibuttons("","$counter");
 my ($key,$val,$ibv);
 while (($key,$val) = each(%ib)) {
   #print "$val if($b_name eq $key) <br>";
   $ibv = $val if($b_name eq $key);
 }
 my $button = "<input type='submit' class='ebutton' name='$function' value='$ibv'>"; 
 return $button;
}

sub singlesubmit3(){
 my $self = shift;
 my ($function,$b_name,$b_img,$style,$title) = @_;
 my ($key,$val,$ib_key,$ib_value);
 #my %ib = ibuttons_arch();
 my %ib = ibuttons();
 while (($key,$val) = each(%ib)) {
   if($b_name eq $key){
     $ib_key = $key;
     $ib_value = $val;
   }
 }
 my $button = "<button class='ebutton' style='height:17px;'></button>";
 if("$b_name" =~ /delete/){
   $button = "<button type='submit' onClick=\"return confirm('Wirklich löschen?')\" class='ebutton' style='$style' name='$function' value='$ib_key'>$ib_value</button>";
 }elsif($b_img){
   $button = "<button type='submit' style='border: 1px solid silver;' name='$function' value='$ib_key' title='$title' ><img src='$b_img' style='$style' /></button>";
 }elsif($b_name){ 
   $button = "<button type='submit' class='ebutton' style='$style' name='$function' value='$ib_key'>$ib_value</button>"; 
 }
 return $button;
}

# without grafic
sub singlesubmit7(){
 my $self = shift;
 my ($function,$b_name,$title,$set_style,$jscript,$ebutton) = @_;
 $ebutton = "ebutton" if(!$ebutton);
 my ($b_img,$a_key);
 $b_img = "Barcode" if("$b_name" =~ /barcode/);
 $b_img = "Copy" if("$b_name" =~ /copy/);
 $b_img = "Relate" if("$b_name" =~ /relate/);
 $b_img = "Move" if("$b_name" =~ /move_/);
 $b_img = "Delete" if("$b_name" =~ /delete|remove/);
 $b_img = "Delete" if("$b_name" =~ /delete_media/);
 $b_img = "Save" if("$b_name" =~ /save|service_done|_contenttverpos/);
 $a_key = "y" if("$b_name" =~ /save/);
 $b_img = "Text Save" if("$b_name" =~ /save_text/);
 $b_img = "Delete All" if("$b_name" =~ /delete_all/);
 $b_img = "Search" if("$b_name" =~ /search/);
 $b_img = "New" if("$b_name" =~ /^new/);
 $b_img = "Open" if("$b_name" eq "open");
 $b_img = "Close" if("$b_name" eq "close");
 $b_img = "Print" if("$b_name" eq "print_sheet");
 $b_img = "Change Login" if("$b_name" eq "change_login");
 $b_img = "Senden" if("$b_name" =~ /send_newsletter/);
 $b_img = "XLS Export" if("$b_name" =~ /XLSout/);
 $b_img = "Save & Close" if("$b_name" =~ /_and_close/);
 $b_img = "Save & Close & Print " if("$b_name" =~ /_print_and_close/);
 $b_img = "Print" if("$b_name" =~ /_print_only/);
 $b_img = "Kunde" if("$b_name" =~ /client/);
 $b_img = "Artikel" if("$b_name" =~ /part/);
 $b_img = "speichern & schließen" if("$b_name" =~ /save_tver_close/);
 
 my $button = "<span  style='width:25px;height=15px;'>&nbsp;</span>";
 if($b_name){
   if("$b_name" =~ /delete/){
     $button = "<button type='submit' onClick=\"return confirm('Wirklich löschen?')\" class='$ebutton' style='$set_style' name='$function' value='$b_name' title='$title'>$b_img</button>"; 
   }elsif($jscript){
     $button = "<button type='submit' onClick='javascript:$jscript' class='$ebutton' style='$set_style' name='$function' value='$b_name' title='$title'>$b_img</button>"; 
   }else{
     $button = "<button type='submit' class='$ebutton' style='$set_style' name='$function' value='$b_name' title='$title' accesskey='$a_key'>$b_img</button>"; 
   }
 }
 return $button;
}

#without submit
sub singlesubmit17(){
 my $self = shift;
 my ($function,$b_name,$title,$set_style,$jscript,$ebutton) = @_;
 $ebutton = "ebutton" if(!$ebutton);
 my ($b_img,$a_key);
 $b_img = "Barcode" if("$b_name" =~ /barcode/);
 $b_img = "Copy" if("$b_name" =~ /copy/);
 $b_img = "Relate" if("$b_name" =~ /relate/);
 $b_img = "Move" if("$b_name" =~ /move_/);
 $b_img = "Delete" if("$b_name" =~ /delete|remove/);
 $b_img = "Delete" if("$b_name" =~ /delete_media/);
 $b_img = "Save" if("$b_name" =~ /save/);
 $a_key = "y" if("$b_name" =~ /save/);
 $b_img = "Text Save" if("$b_name" =~ /save_text/);
 $b_img = "Delete All" if("$b_name" =~ /delete_all/);
 $b_img = "Search" if("$b_name" =~ /search/);
 $b_img = "New" if("$b_name" =~ /^new/);
 $b_img = "Open" if("$b_name" eq "open");
 $b_img = "Close" if("$b_name" eq "close");
 $b_img = "Print" if("$b_name" eq "print_sheet");
 $b_img = "Change Login" if("$b_name" eq "change_login");
 $b_img = "Senden" if("$b_name" =~ /send_newsletter/);
 $b_img = "XLS Export" if("$b_name" =~ /XLSout/);
 $b_img = "Save & Close" if("$b_name" =~ /_and_close/);
 $b_img = "Save & Close & Print " if("$b_name" =~ /_print_and_close/);
 $b_img = "Print" if("$b_name" =~ /_print_only/);
 $b_img = "Kunde" if("$b_name" =~ /client/);
 $b_img = "Artikel" if("$b_name" =~ /part/);
 
 my $button = "<span  style='width:25px;height=15px;'>&nbsp;</span>";
 if($b_name){
     $button = "<div onClick='javascript:$jscript' class='$ebutton' style='$set_style' name='$function' value='$b_name' title='$title'>$b_img</div>"; 
 }
 return $button;
}

# Grafic Submit Buttons
sub singlesubmit2glyph(){
 my $self = shift;
 my ($function,$b_name,$title,$set_style,$jscript) = @_;
 my $b_img; my $a_key;
 $b_img = "submit.png" if("$b_name" =~ /post_email/);
 $b_img = "actions/view-barcode.png" if("$b_name" =~ /barcode/);
 $b_img = "actions/edit-copy.png" if("$b_name" =~ /copy|relate/);
 $b_img = "actions/edit-paste.png" if("$b_name" =~ /move_/);
 $b_img = "glyphicons/glyphicons-151-edit.png" if("$b_name" =~ /delete|remove/);
 $b_img = "actions/edit-delete.png" if("$b_name" =~ /delete_media/);
 $b_img = "actions/document-save.png" if("$b_name" =~ /save/);
 $a_key = "y" if("$b_name" =~ /save/);
 $b_img = "edittrash.png" if("$b_name" =~ /delete_all/);
 $b_img = "search.png" if("$b_name" =~ /search/);
 $b_img = "glyphicons/glyphicons-151-edit.png" if("$b_name" =~ /_done|_contenttverpos/);
 $b_img = "glyphicons/glyphicons-703-file-plus.png" if("$b_name" =~ /new/);
 $b_img = "glyphicons/glyphicons-151-edit.png" if("$b_name" =~ /open|client/);
 $b_img = "fileclose.png" if("$b_name" eq "close");
 $b_img = "actions/view-media-artist.png" if("$b_name" eq "change_login");
 my $button = "<span  style='width:25px;height=15px;'>&nbsp;</span>";
 if($b_name){
   if("$b_name" =~ /delete/ && "$b_name" !~ /delete_verposdate/){
    if($jscript){
     $button = "<button type='submit' onClick=\"javascript:$jscript; return confirm('Wirklich löschen?')\" style='width:25px;height=15px; border: 0px solid;$set_style' name='$function' value='$b_name' title='$title'><img src='$varenv{metahost}/$b_img' style='height:18px;' /></button>"; 
    }else{
     $button = "<button type='submit' onClick=\"return confirm('Wirklich löschen?')\" style='width:25px;height=15px; border: 0px solid;$set_style' name='$function' value='$b_name' title='$title'><img src='$varenv{metahost}/$b_img' style='height:18px;' /></button>"; 
    }
   }else{
     $button = "<button type='submit' style='border: 0px solid;$set_style' name='$function' value='$b_name' title='$title' accesskey='$a_key'><img src='$varenv{metahost}/$b_img' style='height:18px;' /></button>"; 
   }
 }
 return $button;
}

# Grafic Submit Buttons
sub singlesubmit2(){
 my $self = shift;
 my ($function,$b_name,$title,$set_style,$jscript) = @_;
 my $b_img; my $a_key;
 $b_img = "glyphicons-446-floppy-remove.png" if("$b_name" =~ /delete|remove/);
 $b_img = "glyphicons-445-floppy-saved.png" if("$b_name" =~ /save/);
 $a_key = "y" if("$b_name" =~ /save/);
 my $button = "<span  style='width:25px;height=15px;'>&nbsp;</span>";
 if($b_name){
   if("$b_name" =~ /delete/ && "$b_name" !~ /delete_verposdate/){
    if($jscript){
     $button = "<button type='submit' onClick=\"javascript:$jscript; return confirm('Wirklich löschen?')\" style='width:25px;height=15px; border: 0px solid;$set_style' name='$function' value='$b_name' title='$title'><img src='$varenv{metahost}/glyphicons/$b_img' style='height:18px;' /></button>"; 
    }else{
     $button = "<button type='submit' onClick=\"return confirm('Wirklich löschen?')\" style='width:25px;height=15px; border: 0px solid;$set_style' name='$function' value='$b_name' title='$title'><img src='$varenv{metahost}/glyphicons/$b_img' style='height:18px;' /></button>"; 
    }
   }else{
     $button = "<button type='submit' style='width:25px;height=15px; border: 0px solid;$set_style' name='$function' value='$b_name' title='$title' accesskey='$a_key'><img src='$varenv{metahost}/glyphicons/$b_img' style='height:18px;' /></button>"; 
   }
 }
 return $button;
}

#with onclick
sub singlesubmit4(){
 my $self = shift;
 my ($function,$b_name,$title,$jscript,$url) = @_;
 my $b_img;
 $b_img = "search.png";# if("$title" =~ /suchen/);
 my $button = "<input type='image' src='/img/$b_img' class='ebutton2' name='$function' value='$b_name' onClick='javascript:$jscript()' title='$title'>"; 
 return $button;
}

#with onclick
sub singlesubmit5(){
 my $self = shift;
 my ($function,$b_name,$jscript,$url) = @_;
 my ($key,$val,$ibv);
 while (($key,$val) = each(%ib)) {
   $ibv = $val if($b_name eq $key);
 }
 my $button = "<input type='submit' class='ebutton' name='$function' value='$ibv' onClick='javascript:$jscript(this.form,\"$url\")'>"; 
 return $button;
}

#with onclick script
sub singlesubmit9(){
 my $self = shift;
 my ($function,$b_name,$main_id,$class,$style,$owner) = @_;
 my $debug = $main_id if($owner && $owner eq $varenv{superu_id});
 my $button = "<input type='button' class='$class' style='$style' name='$function' value='$b_name' title='$debug' onclick='javascript:history.back()'>\n";
 return $button;
}

sub singlesubmit8(){
 my $self = shift;
 my ($function,$b_name,$jscript,$url) = @_;
 my $button = "<button type='submit' onClick='javascript:$jscript' class='ebutton' name='$function' value='$b_name'>$b_name</button>";
 return $button;
}
1;
