package APIjsonclient;
# 
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
#Client for shareejson api
#
use strict;
use warnings;
use POSIX;
use CGI; # only for debugging
use JSON;
use LWP::UserAgent;
use URI::Encode;
use Config::General;
use Mod::Basework;
use Mod::DBtank;
use Data::Dumper;


sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}


my $q = new CGI;
my $netloc = $q->url(-base=>1);
my $ua = LWP::UserAgent->new;
my $uri_encode = URI::Encode->new( { encode_reserved => 1 } );
$ua->agent("APIclient $netloc");
my $bw = new Basework;
my $dbt = new DBtank;

my $json = JSON->new->allow_nonref;

sub loop_sharees {
 my $self = shift;
 my $q = shift || "";
 my $authraw = shift;
 my $return_merchant = shift || "";
 my @keywords = $q->param;
 #my $project = $return_merchant->{project_id} || "all";

 #only request keys which initated by sharee primary requests to operator 
 my $rest_hash = {};
 my $rest = "";
 foreach (@keywords){
  if($_ =~ /request|authcookie|system|bike|station/g){
   my $val = $q->param($_);
   my $encoded_val = $uri_encode->encode($val);
   $rest .= "$_=$encoded_val&";
   $rest_hash->{$_} = "$encoded_val";
  }
 }
 $rest =~ s/\&$//;
 
 my $response_in = {};

 my $globalconf_file = "/var/www/copri4/shareeconf/global.cfg";
 my $conf = Config::General->new($globalconf_file);
 my %globalconf = $conf->getall;

 my @uri_operator_array = ();
 my @user_group = ();
 my @user_tour = ();
 my $fetch_hash = {};
 while (my ($key, $value) = each %{ $globalconf{operator} }) {
   my $ret_json = "";

   #shareetool, to loop only operators who is the shareetool by usersconf enabled 
   my $users_serviceapp = { u_id => 0 };
   if($return_merchant->{aowner} && $return_merchant->{aowner} == 187 && $value->{database}->{dbname} && $authraw->{txt17} =~ /$value->{database}->{dbname}/){
     my $dbh_operator = $dbt->dbconnect_extern($value->{database}->{dbname});
     $users_serviceapp = $dbt->select_users($dbh_operator,$authraw->{c_id},"and int09=1");
     $bw->log("shareetool select_users $return_merchant->{aowner} on $value->{database}->{dbname} $authraw->{c_id} $authraw->{txt17}",$users_serviceapp->{u_id},"");
   }
   #every sharee client must have a merchant_id which associate a project
   if($value->{operatorApp} && ($users_serviceapp->{u_id} || ($return_merchant->{project_id} eq $value->{project}))){

     #$bw->log("--> LOOP-start ($return_merchant->{merchant_id}) jsonclient loop_sharees $key by operatorApp: $value->{operatorApp}, netloc: $netloc if($return_merchant->{project_id} eq $value->{project} || $users_serviceapp->{u_id} > 0)\n","","");
     $ret_json = $self->fetch_operator_json("$value->{operatorApp}/APIjsonserver",$rest);
     #print Dumper($ret_json);

     if($ret_json){
      push(@uri_operator_array, $value->{operatorApp});
      eval {
       my $response_in = "";
       $response_in = decode_json($ret_json);
       #print Dumper($response_in);
   
       #collect OP user_group
       if($response_in->{shareejson}->{user_group}){
	 push (@user_group, @{$response_in->{shareejson}->{user_group}});
       }
       #collect OP user_tour
       if($response_in->{shareejson}->{user_tour}){
	 push (@user_tour, @{$response_in->{shareejson}->{user_tour}});
       }

       if($q->param('request') && $q->param('request') =~ /stations_all|stations_available/){
       	foreach my $result (keys (%{ $response_in->{shareejson}->{stations} })) {
	 $fetch_hash->{$result} = $response_in->{shareejson}->{stations}->{$result};
       	}
       }
       if($q->param('request') && $q->param('request') =~ /bikes_all|bikes_available/){
       	foreach my $result (keys (%{ $response_in->{shareejson}->{bikes} })) {
	 $fetch_hash->{$result} = $response_in->{shareejson}->{bikes}->{$result};
       	}
       }
       if($q->param('request') && $q->param('request') =~ /user_bikes_occupied/){
       	foreach my $result (keys (%{ $response_in->{shareejson}->{bikes_occupied} })) {
	 $fetch_hash->{$result} = $response_in->{shareejson}->{bikes_occupied}->{$result};
       	}
       }
      };
      if ($@){
	$bw->log("Failure, eval json from jsonclient","","");
  	warn $@;
      }
     }else{
	$bw->log("NO json from Operator:",$value->{operatorApp},"");
     }
     #$bw->log("--> LOOP-end jsonclient loop_sharees user_group:\n",\@user_group,"");
   }

 }
 #print "ALL:" . Dumper($fetch_hash);
 #
 return ($fetch_hash,\@uri_operator_array,\@user_group,\@user_tour);
}

sub fetch_operator_json {
 my $self = shift;
 my $operator_server = shift || "";
 my $rest = shift || "";
 my $operator_request = "$operator_server?$rest";

 #$bw->log("fetch_operator_json >> ","$operator_request","");

 my $req = HTTP::Request->new(GET => "$operator_request");
 $req->content_type('application/x-www-form-urlencoded');
 $req->content($rest);

 my $res = $ua->request($req);
 if ($res->is_success) {
  #print $res->content;
  #return $res->content;
  my $encoded = Encode::encode('utf-8', Encode::decode('iso-8859-1',$res->content));
  return $encoded;
 }else {
  return "";
 }
}

1;
