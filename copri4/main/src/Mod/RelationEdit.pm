package RelationEdit;
#
##
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
use strict;
use warnings;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI ':standard';
use Lib::Config;
use Mod::Buttons;
use Mod::Libenz;
use Mod::Libenzdb;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
  my $node_meta = shift;
  my $users_dms = shift;
  my $u_group = shift;
  my $return = shift;

  my $q = new CGI;
  my @keywords = $q->param;
  my $cf = new Config;
  my $lb = new Libenz;
  my $db = new Libenzdb;
  my $but = new Buttons;
  my %ib = $but->ibuttons();
  my $script = $q->script_name();
  my %varenv = $cf->envonline();
  my $path_info = $q->path_info();
  my $path = $path_info;
  #with meta_host, 
  if("$varenv{metahost}"){
   $path = "$script" . "$path_info";
   $script="";
  }
  my $lang = "de";
  my $ctn;
  if($R::rel_id){
	$ctn = $db->collect_rel4nodes("",$node_meta->{content_id},$node_meta->{template_id});
  }else{
    return "failure::Fehler bei der Auswahluebergabe";
  }
  my $bg_color = "gray";
  my $table = "content";
  my $relate_key;
  $relate_key = "move_content" if($R::rel_edit =~ /move_/);
  $relate_key = "relate_content" if($R::rel_edit =~ /relate_/);
  $relate_key = "copy_content" if($R::rel_edit =~ /copy_/);
  if($node_meta->{ct_table} eq "contentadr"){
    $table = "contentadr";
    $relate_key = "move_contentadr" if($R::rel_edit =~ /move_/);
    $relate_key = "relate_contentadr" if($R::rel_edit =~ /relate_/);
  }
  if($node_meta->{ct_table} eq "contenttrans"){
    $table = "contenttrans";
    $relate_key = "move_conttrans" if($R::rel_edit =~ /move_/);
    $relate_key = "relate_contenttrans" if($R::rel_edit =~ /relate_/);
  }
  if($node_meta->{ct_table} eq "contenttver"){
    $table = "contenttver";
    $relate_key = "move_contenttver" if($R::rel_edit =~ /move_/);
    $relate_key = "relate_contenttver" if($R::rel_edit =~ /relate_/);
  }
  if($node_meta->{ct_table} eq "contentnel"){
    $table = "contentnel";
    $relate_key = "move_contentnel" if($R::rel_edit =~ /move_/);
    $relate_key = "relate_contentnel" if($R::rel_edit =~ /relate_/);
  }


  #print "$table,$node_meta->{main_id},$lang,$R::rel_id, xxxxxxxxxxxxxx";
  my $ctrel = $db->get_ctrel($table,"",$lang,$R::rel_id);
  my $node = $db->get_node4multi($node_meta->{main_id},$lang);
  $ctrel->{mtime} = $lb->time4de($ctrel->{mtime},"1") if($ctrel->{mtime});
  $ctrel->{rel_id} = 0 if(!$ctrel->{rel_id});
  my $ct_users;
  $ct_users = $db->collect_users("users") if($u_group eq "manager");#users map
  
  #we mean roots for sub-NodePath selection
  my @viewsel = split /\//,$1 if($path =~ /^\/(.*)/);
  my $depth = scalar(@viewsel);
  my $view_root = $viewsel[0] || "root";

  my $selector = "this_is_no_selection";
  $selector = $1 if($path =~ /\/(Waren|Kunden|Veranstaltung|Nachrichten)/);

  #collect recursive nodes
  my $selsize="200px";
  #nodes with relation would be better
  my $nodes = $db->collect_node4all("","","","100000");
  my @_menu_rel;
  foreach my $id (sort { lc($nodes->{$a}->{node_name}) cmp lc($nodes->{$b}->{node_name}) }  keys (%$nodes)){
     my @viewsel;
     my $depth=0;
     my $j=0;
     my ($m_id,$uri) = $lb->make_uri5($nodes->{$id}->{main_id},$nodes);
     if(($uri =~ /\w\/\w/) && ($uri !~ /000 root/)){
      @viewsel = split(/\//,$uri) if($uri =~ /^\/(.*)/);
      $depth = scalar(@viewsel);
      if($varenv{orga} eq "dms"){
	#sharee-copri workaround to get only bike menue 
       if($selector && ($path =~ /\/$selector/) && ($uri =~ /$selector/) && ($node_meta->{template_id} == 205)){#Waren bikes
	if($depth >= 2){
	 my $rel = $db->get_relation($nodes->{$id}->{main_id},$lang,"");
      	 push (@_menu_rel, "$m_id:$uri") if($rel->{template_id} == 205);
	}
       }
      }
     }
  }
  my $mcount = scalar(@_menu_rel);
  #

  #collect content
  my $content = $db->collect_content();
  my @_content;
  foreach my $id (sort { lc($content->{$a}->{ct_name}) cmp lc($content->{$b}->{ct_name}) }  keys (%$content)){
      push (@_content, "$id:$content->{$id}->{ct_name}") if($content->{$id}->{ct_name});
  }
  #

  print "<div id='Container_cms'>";

  my $u_name;
  foreach my $ctu_id (keys (%$ct_users)){
       if($ctrel->{owner} eq $ct_users->{$ctu_id}->{owner}){
        $u_name = $ct_users->{$ctu_id}->{u_name};
       }
  }
  $db->users_up("rel_id4edit",$ctrel->{rel_id},$users_dms->{u_id});
  print $q->hidden(-name=>'main_id', -value=>"$ctrel->{main_id}");  
  print $q->hidden(-name=>'rel_id', -value=>"$ctrel->{rel_id}") if($ctrel->{rel_id});  
  print $q->hidden(-name=>'template_id', -value=>"$ctrel->{template_id}");  

  print $q->start_table({-class=>'list', -border=>'0', -width=>'100%', -align=>'left', -cellpadding=>'3', -cellspacing=>'3'});
  
  #Buttons
  print $q->Tr();
  print "<td style='margin:1px 0;padding:2px;background-color:$bg_color' colspan='2' nowrap>\n";
  print $but->singlesubmit7("rel_edit","$relate_key","$ib{$relate_key}");
  print "</td>\n";

  print $q->Tr();
  print $q->td({-colspan=>'2'},"&nbsp;");

  if("$R::relate_dialog" eq "menu" || $R::rel_edit =~ /dialog4menu/){
    print $q->hidden(-name=>'set_content_id', -value=>"$ctrel->{content_id}");  
    print $q->Tr();
    print $q->td({-class=>'tdescr2'}, "Content"),"\n";
    print $q->td({-class=>'tdval2'},$q->b("$ctrel->{ct_name} ($ctrel->{txt01})"),"\n");
    print $q->Tr();
    print $q->td({-class=>'tdescr2'}, "Node"),"\n";
    if($mcount > 0 || $varenv{orga} eq "dms"){
      if($varenv{wwwhost} =~ /woge|fsd/){#with multiple select
        my $height = scalar(@_menu_rel);
	my $remain_id;
	foreach my $in (keys(%$ctn)){
		$remain_id .= "$ctn->{$in}->{main_id},";
	}
	$remain_id =~ s/,$//;
        print $q->td({-class=>'tdval2'},$but->selector2("set_main_id","600px","$height",$remain_id,@_menu_rel)),"\n";
      }else{
      	print $q->td({-class=>'tdval2'},$but->selector("set_main_id","250px",$node_meta->{main_id},@_menu_rel)),"\n";
      }
    }else{
	print $q->td({-class=>'tdval2'},"Es ist kein leeres Menue fuer eine Verknuepfung vorhanden."),"\n";
    }
    print $q->hidden(-name=>'mastermain_id', -value=>"$node_meta->{mastermain_id}") if($node_meta->{mastermain_id});  
  }elsif("$R::relate_dialog" eq "content" || $R::rel_edit =~ /dialog4content/){
    print $q->hidden(-name=>'set_main_id', -value=>"$node_meta->{main_id}");  
    print $q->Tr();
    print $q->td({-class=>'tdescr2'}, "Node"),"\n";
    print $q->td({-class=>'tdval2'},$q->b($node->{node_name}),"\n");
    print $q->Tr();
    print $q->td({-class=>'tdescr2'}, "Content"),"\n";
    print $q->td({-class=>'tdval2'},$but->selector("set_content_id","250px",$ctrel->{c_id},@_content),"\n");
  }
  print $q->Tr();
  print $q->td({-colspan=>'2'},"&nbsp;");
  #print $q->Tr();
  #print "<td class='element3' colspan='2'>";
  #print $q->a({-class=>"linknav2",-href=>'javascript:history.back()'}, "zurück");
  #print "</td>\n";

  print $q->end_table;
  print "</div>\n"; 

}
1;
