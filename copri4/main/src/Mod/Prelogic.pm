package Prelogic;
#
#Deprecated module, please use Prelib.pm
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
#should be refactored
#
use strict;
use warnings;
use POSIX;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI ':standard';
use Date::Calc qw(:all);
use Scalar::Util qw(looks_like_number);
use DateTime;
use DateTime::Format::Pg;

use Lib::Config;
use Mod::Buttons;
use Mod::Libenz;
use Mod::Libenzdb;
use Mod::DBtank;
use Mod::Callib;
use Mod::Payment;
use Mod::Pricing;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub preinit(){
 my $self = shift;
 my $users_dms = shift;

 my $q = new CGI;
 $q->import_names('R');
 my @keywords = $q->param;
 my $cf = new Config;
 my $lb = new Libenz;
 my $db = new Libenzdb;
 my $dbt = new DBtank;
 my $cb = new Callib;
 my $but = new Buttons;
 my $payone = new Payment;
 my $pri = new Pricing;

 my %ib = $but->ibuttons();
 my %varenv = $cf->envonline();
 my $script = $q->script_name();
 my $path_info = $q->path_info();
  my $path = $path_info;
  #with meta_host,
  if("$varenv{metahost}"){
   $path = "$script" . "$path_info";
   $script="";
  }
 my $lang = "de";
 my $c_id = $R::c_id || "0";#c_id aus content
 my $time = time();
 my $now_date = strftime "%Y-%m-%d", localtime;
 my $today = strftime("%d.%m.%Y",localtime(time));
 my $today4db = strftime("%Y-%m-%d %H:%M",localtime(time));
 my $now_dt = strftime "%Y-%m-%d %H:%M:%S", localtime;
 my $day = strftime "%d", localtime;
 my $mon = strftime "%m", localtime;
 my $year = strftime "%Y", localtime;
 my $i_rows=0;
 my $u_rows=0;
 my $d_rows=0;
 my $dbh = "";

 if(!$users_dms->{u_id}){
   return "failure::Fehler<br /> Die Anwender Authentifikation wurde unterbrochen.";
 }

 #Mindestmengenanzeige #ist in Liste.pm implementiert
 #if(!$R::detail_search && $path =~ /root$/ && "$R::redirected" ne "1"){
 # my $repath = $lb->shortcut2("$path","$users_dms->{u_id}");
 #print redirect("$varenv{wwwhost}$script$repath?redirected=1&detail_search=1&s_int03=<=0");
  #exit 0;
 #}els
 #topmenu shortcut handling
 #if(!$R::no_redirect && !$R::detail_search && $path =~ /^\/([\w-\s]+)\/([\w-\s]+)$|root$/ && "$R::redirected" ne "1"){
 # my $repath = $lb->shortcut2("$path","$users_dms->{u_id}");
 # if("$repath" ne "$path"){
 # 	print redirect("$varenv{wwwhost}$script$repath?redirected=1\&return=0-0-0|$i_rows-$u_rows-$d_rows");
 # 	exit 0;
 # }
 #}



 #international buttons
 my ($key,$val,$ib_key);
 while (($key,$val) = each(%ib)) {
   $ib_key = $key if($R::ct_trans eq $val);
 }
 $ib_key = $R::ct_trans if(!$ib_key);
 my ($ct_last,$c_idnew);

 my $table = "contenttrans";
 my $ctt = $db->get_content1($table,$users_dms->{c_id4trans});
 my $buchen_mtime = $lb->time4de($ctt->{mtime});

 #actual-path an get right mandant for verkauf
 $R::kind_of_trans = $R::kind_of_trans || $users_dms->{kind_of_trans};
 my $mandant_main_id = 100002;
 my $parent_trans = $db->get_node3("$mandant_main_id","Faktura","$lang") if($mandant_main_id);
 my $ctf = $db->get_content1("contentuser","$mandant_main_id");

 if(!$parent_trans->{main_id}){
   return "failure::Fehler<br /> keine eindeutige Modulzuweisung vorhanden.";
 }
 my $rel = $db->get_rel4tpl4nd($parent_trans->{main_id},$lang,$users_dms->{c_id4trans});
 $rel = $db->get_rel4tpl4nd("",$lang,$users_dms->{c_id4trans},$rel->{template_id}) if($users_dms->{c_id4trans} > "0");

 my $ct_name = $q->escapeHTML("$R::ct_name");
 my $barcode=0;

 #Kassen-Abschluss Logik
 my $s_owner_id=0;
 if(($ctf->{txt06} eq "Benutzer") && ($R::kind_of_trans =~ /Verkauf|Verleih|Faktur/)){
    $s_owner_id = $users_dms->{u_id};
 }

 if($R::cash_sort){
   $db->users_up("cash_sort",$R::cash_sort,$users_dms->{u_id});
 }
 if($R::ctpos_close){
   $db->users_up("ctpos_activ","0",$users_dms->{u_id});
 }elsif($R::ctpos_activ){
   $db->users_up("ctpos_activ",$R::ctpos_activ,$users_dms->{u_id});
 }

 if($R::s_start_date_time && $varenv{dataflow} =~ /wiki/){
   $db->users_up("cal_start","$R::s_start_date_time",$users_dms->{u_id});
 }
 if($R::s_end_date_time && $varenv{dataflow} =~ /wiki/){
   $db->users_up("cal_end","$R::s_end_date_time",$users_dms->{u_id});
 }
 if($R::view_list && $varenv{dataflow} =~ /wiki/){
   $db->users_up("view_list","$R::view_list",$users_dms->{u_id});
 }



 #OPEN/CLOSE
 if(($R::ct_trans eq "open") && ("$R::kind_of_trans" !~ /Kalender|Mietjournal/)){
   $db->update_users4trans($R::c_id4trans,$R::tpl_id4trans,$R::kind_of_trans,$users_dms->{u_id});
   $users_dms = $db->select_users($users_dms->{u_id});
 }elsif(($R::ct_trans eq "close") && ("$R::kind_of_trans" !~ /Kalender|Mietjournal/)){
     $db->update_users4trans("0","0","",$users_dms->{u_id});
     $users_dms = $db->select_users($users_dms->{u_id});
 }


 #NEW contenttrans
 if($ib_key =~ /^new_/){
  $ct_name = "----" if(!$ct_name);
  my $foreign_key;
  my $sort;
  my $main_id = $R::main_id;
  if($ib_key =~ /new_trans/){
     $table = "contenttrans";
     $foreign_key="ct_id";
     $main_id = $parent_trans->{main_id} if(!$R::main_id);
  }
  my $node = $db->get_node4multi($main_id,$lang);

  if($table && $main_id){
     my $rel = $db->get_rel4tpl($main_id,$lang);
     $c_idnew = $db->insert_content2($table,$ct_name,$users_dms->{u_id},"");
     $i_rows += 1 if($c_idnew > 0);
     my $rel_id = $db->insert_relationlist($table,$rel->{main_id},$lang,$c_idnew,$rel->{tpl_id},$foreign_key);
     $rel->{content_id} = $c_idnew; #notwendig bei prozeduralem $R::select_part
     $db->update_content4change($table,$c_idnew,"",$parent_trans->{parent_id},"int09");#mandant_main_id
     $db->update_content4change($table,$c_idnew,"",$node->{main_id},"int12");#zusätzl. Formtyp
     $db->update_content4change($table,$c_idnew,"",$node->{node_name},"txt00");#node_name
     $db->update_content4change($table,$c_idnew,"",$today4db,"start_time") if($R::kind_of_trans =~ /Faktur|Verleih/);
     $db->update_content4change($table,$c_idnew,"",$today4db,"end_time") if($R::kind_of_trans =~ /Faktur|Verleih/);
     $db->update_users4trans($c_idnew,$R::tpl_id4trans,$R::kind_of_trans,$users_dms->{u_id}) if("$R::kind_of_trans" !~ /Kalender|Mietjournal/);
     if($ib_key eq "new_trans"){
       $db->update_content4change($table,$c_idnew,"","txt63","txt21");#Text-1
       $db->update_content4change($table,$c_idnew,"","Barkunde","txt01") if($varenv{wwwhost} =~ /lx-rad/);
       $db->update_content4change($table,$c_idnew,"","txt61,txt63","txt21") if($R::kind_of_trans =~ /Faktur|Verleih/);#Text-3
     }
  }else{
    return "failure::Fehler, Datensatz kann nicht angelegt werden weil folgende Informationen fehlen: ($main_id)";
  }
 }


 ###ADD PARTS/DATE TO Transactions
 #FIXME, refactorit
 if((($ib_key =~ /add_transpos|add_transdate/) || ($R::select_part && ($R::spart_ct_name || $R::json_select))) && (!$ctt->{close_time})){
  $table = "contenttranspos";

  my $zcolumn = "barcode";
  my $zcontent = "$R::spart_ct_name" || "$R::json_select";

  #get part for adding and get from_main_id
  my $ct_name = "$R::spart_ct_name" || "$R::json_select" || "";#turn-around!§$
  my $c_id = "$R::c_id" || "";
  my $ct = { c_id => 0 };
  $ct = $db->get_ctrel2("content","$ct_name","","$lang","","$c_id","224,229","$zcolumn","$zcontent","");

  $ct_name = $ct->{ct_name} if($ct->{ct_name});

  if($ct->{main_id}){
   $users_dms = $db->select_users($users_dms->{u_id}) if($ib_key =~ /new_/);#nur bei new_ mit save_
   if(!$users_dms->{c_id4trans} && $ib_key !~ /new_transdate/){
    return "failure::Zu welchem Vorgang soll die Aktion verknüpft werden? Bitte erst den entsprechenden Vorgang öffnen.";
   }
  }

  if($ct->{barcode} && $ct->{barcode} > 0 && $rel->{content_id}){
    my $partnr = "$ct_name / $ct->{barcode}";
    $partnr = "$ct_name" if("$ct_name" eq "$ct->{barcode}" || !$ct->{barcode});
    my $bezeichnung = "$ct->{txt01}";
    $ct->{int02} = $ct->{int02} * -1 if($ct->{template_id} == 224 && $ct->{int02} > 0);#Gutscheine

    #Add Parts from content to contenttranspos#################
    my $pos_id = 0;

    my $ctt = $db->get_content1("contenttrans",$users_dms->{c_id4trans});
    my $ctadr = $db->get_content1("contentadr",$ctt->{int10});
    $pos_id = $dbt->insert_pos($dbh,$ctt->{c_id},$ct,"",$ctadr,"","",$partnr,"0",$users_dms->{u_id},"");
       
    $i_rows += 1 if($pos_id > 0);
    $dbt->update_content4comp($dbh,$ct->{c_id},"-","1");
    return "pos_id=$pos_id";#new return-code to get opened Part-Position in Transposition
  }elsif($ct_name && !$rel->{content_id}){
    return "failure::Für die Terminzuordnung bitte zuerst einen Verkauf Vorgang öffnen oder neu erzeugen.<br /> ::$varenv{wwwhost}/Verkauf::Zum Verkauf";
  }else{
   return "failure::Die Artikelnummer konnte nicht gefunden werden.<br /> ::$varenv{wwwhost}/Waren::Waren verwalten";
  }
 }
 #####

 #UPDATE CONTENT
 #print "@keywords ";
 if($ib_key =~ /save/){
  $c_id = $R::c_id if($ib_key eq "save");#only in journal edit
  $db->users_up("c_id4edit","0","$users_dms->{u_id}");#delete edit marker
  my $table = "contenttrans";
  #Maybe obsolete, because of ajax autosave --> for now back to old
  #print "$ib_key|$R::ct_trans2c_idadr|$R::c_idadr";
  if($ib_key eq "save_adr" && $R::ct_trans2c_idadr eq "save_contentadr" && $R::c_idadr =~ /^\d+$/){
    $table = "contentadr";
    $c_id = "$R::c_idadr";
  }elsif($ib_key eq "save_adr" && $R::ct_trans2c_idadr eq "save_contentadr"){
  #   #like new_adr in Premain.pm
     $table = "contentadr";
     my $foreign_key="ca_id";
     my $p_main_id = $ctf->{txt32};
     my $barcode_last = $db->get_barcode("users","$table");#hier gilt: $table=column in users
     my $freenr = $lb->get_freenr($table,$barcode_last->{$table});
     ($ct_name,$barcode) = $lb->barcodeable($table,$freenr);#new nr routine
     if($barcode){
        $db->users_up("$table","$barcode",$users_dms->{u_id});
     }else{
        return "failure::Fehler, neue Nummer kann nicht generiert werden";
     }
     if($p_main_id && $users_dms->{kind_of_trans}){
     #if($address->{main_id} && $users_dms->{kind_of_trans}){
       $rel = $db->get_rel4tpl($p_main_id,$lang);
       $c_idnew = $db->insert_content2($table,$ct_name,$users_dms->{u_id},"");
       $i_rows += 1 if($c_idnew > 0);
       $db->update_barcode($table,$c_idnew,$ct_name,$barcode);
       my $rel_id = $db->insert_relationlist($table,$rel->{main_id},$lang,$c_idnew,$rel->{tpl_id},$foreign_key);
       $c_id = $c_idnew;	#for contenadr update
       $R::c_idadr = $c_idnew;	#for copy contentadr to contenttrans
     }
  #Freitext oder nur in contentrans speichern
  }elsif($ib_key eq "save_adr" || $ib_key =~ /save_text/){
    $table = "contenttrans";
    $c_id = "$R::c_id4trans";
  }elsif($ib_key eq "save_pos"){
    $table = "contenttranspos";
    $c_id = $R::c_idpos || $R::pos_id;
  }

  #Miet- bzw. BaseVA- Zeitraum
  if($ib_key =~ /save_pos/ && $R::c_idpos && $R::start_date =~ /\d{1,2}\.\d{1,2}\.\d{4}/ && $R::end_date =~ /\d{1,2}\.\d{1,2}\.\d{4}/){
   my $pos_id = $R::c_idpos;
   my $start_date = "$3-$2-$1" if($R::start_date =~ /(\d{1,2})\.(\d{1,2})\.(\d{4})/);
   my $end_date = "$3-$2-$1" if($R::end_date =~ /(\d{1,2})\.(\d{1,2})\.(\d{4})/);
   my $s_hh = $q->escapeHTML("$R::s_hh") || "0";
   my $s_mi = $q->escapeHTML("$R::s_mi") || "0";
   my $e_hh = $q->escapeHTML("$R::e_hh") || "0";
   my $e_mi = $q->escapeHTML("$R::e_mi") || "0";
   $s_hh = "24" if($s_hh > "24");
   $e_hh = "24" if($e_hh > "24");
   $s_mi = "59" if($s_mi > "59");
   $e_mi = "59" if($e_mi > "59");
   $s_hh = sprintf('%.2d',$s_hh);
   $e_hh = sprintf('%.2d',$e_hh);
   $s_mi = sprintf('%.2d',$s_mi);
   $e_mi = sprintf('%.2d',$e_mi);
   my $start_time="";
   my $end_time="";
   $start_time = "$start_date $s_hh:$s_mi:00" if("$start_date $s_hh:$s_mi" =~ /\d{4}-\d{1,2}-\d{1,2}\s\d{1,2}:\d{1,2}/);
   $end_time = "$end_date $e_hh:$e_mi:00" if("$end_date $e_hh:$e_mi" =~ /\d{4}-\d{1,2}-\d{1,2}\s\d{1,2}:\d{1,2}/);

   if($start_time && $end_time){
    my $dtnow = DateTime->now( time_zone => "Europe/Berlin" );
    my $dt0 = DateTime::Format::Pg->parse_datetime($start_time);
    my $dt1 = DateTime::Format::Pg->parse_datetime($end_time);

    if($dt0 < $dtnow && $dt1 < $dtnow){

       my $pref = {
        table           => "contenttrans",
        table_pos       => "contenttranspos",
        fetch           => "one",
        template_id     => "218",#Mietjournal tpl_id
        c_id         => $pos_id,
        "ct.close_time" => "is::null",
       };
   
       my $record_pos = {};
       my $pricing = {};
       my $counting = {};
       $record_pos =  $dbt->collect_post($dbh,$pref);

       my $update_pos = {
                table   =>      "contenttranspos",
		start_time => "$start_time",
		end_time => "$end_time",
		owner_end => $users_dms->{u_id},
                mtime   =>      "now()",
       }; 
       my $rows = $dbt->update_record($dbh,$update_pos,$record_pos);
       #again to get setted date-times
       $record_pos =  $dbt->collect_post($dbh,$pref);

       ($pricing,$counting) = $pri->counting_rental(\%varenv,$record_pos,"calc_price");             

       #int03 only used for tarif counting backwards compatibility
       #$update_pos->{int03} = "$pricing->{computed_hours}" if(looks_like_number($pricing->{computed_hours}));
       $update_pos->{int38} = "$counting->{int38}" if(looks_like_number($counting->{int38}));
       $update_pos->{int39} = "$counting->{int39}" if(looks_like_number($counting->{int39}));
       $update_pos->{int40} = "$counting->{int40}" if(looks_like_number($counting->{int40}));
       $update_pos->{int41} = "$counting->{int41}" if(looks_like_number($counting->{int41}));

	$update_pos->{int10} = 1;
	$update_pos->{int04} = $R::int04 if(looks_like_number($R::int04));

	if($R::cc_id =~/^\d+$/){
    	  my $ctpref = {
            table  => "content",
            fetch  => "one",
            c_id   => $R::cc_id,
          };
    	  my $ctbike = $dbt->fetch_tablerecord($dbh,$ctpref);
	
	  if($ctbike->{int10} && ($ctbike->{int10} == 2 || $ctbike->{int10} == 3)){
            $db->updater("content","c_id",$R::cc_id,"int10","1",$users_dms->{u_id});
       	    if($R::int04 =~/^\d+$/){
	      $db->updater("content","c_id",$R::cc_id,"int04","$R::int04",$users_dms->{u_id});
	    }
	  }
	}

        $rows = $dbt->update_record($dbh,$update_pos,$record_pos);
        $db->updater("contenttrans","c_id",$ctt->{c_id},"start_time","$start_time",$users_dms->{u_id});
        $db->updater("contenttrans","c_id",$ctt->{c_id},"end_time","$end_time",$users_dms->{u_id});
     }
    }
  }

  #hidden select keys to delete first, marked with off_ (Project.pm)
  foreach(@keywords){
     if($_ =~ /off_(txt\d+)/){
	$db->updater("$table","c_id",$c_id,"$1","");
     }
  }

  my $ct_exist2;
  $ct_exist2 = $db->get_content2($table,$ct_name) if($ct_name);
  $ct_exist2 = $db->get_content1($table,$c_id) if($c_id);
  if($ct_exist2->{ct_name} && ($ct_exist2->{c_id} ne $c_id)){
     return "failure::$ct_exist2->{ct_name} - $ct_exist2->{c_id} - $c_id<br /> content name exists";
  }elsif($c_id){
    $users_dms = $db->select_users($users_dms->{u_id});#to get new cal_start
    my $j=0;
    foreach(@keywords){
     $j++;
     my $val = $q->param($_);
     my $valxx = $q->escapeHTML("$val");
      my @val = $q->param($_);
      $valxx = $q->escapeHTML("@val");
     $_ =~ s/col_//;
     $ct_exist2->{$_} =~ s/^\s+//; $ct_exist2->{$_} =~ s/\s+$//;
     $valxx =~ s/^\s+//; $valxx =~ s/\s+$//;
     $valxx .= ".00" if($valxx =~ /^\d+$/ && $_ =~ /int\d+/);
     #print "|$_:$ct_exist2->{$_} -- $valxx|<br>";
     if($_ =~ /sort4pos_\d+/ && $valxx =~ /^\d+$/){
	$db->updater("contenttverpos","c_id",$1,"sort","$valxx") if($_ =~ /sort4pos_(\d+)/);
     }elsif(("$ct_exist2->{$_}" ne "$valxx") || $R::ckid_main || $_ =~ /txt10|txt11/ || $R::edit_main){
      if($_ eq "ct_name" && $valxx && $table){
       $u_rows +=$db->updater("$table","c_id",$c_id,"$_","$valxx");
      }
      if($_ =~ /txt21|txt6/ && $table){
       $db->update_content4change2($table,$c_id,"$R::txt61,$R::txt62,$R::txt63,$R::txt64,$R::txt65,$R::txt66,$R::txt67,$R::txt68,$R::txt69","txt21")
      }elsif($_ =~ /txt(\d+)$/ && $table){
       $valxx = "null" if(!$valxx);#for empty
       my $txtxx = $valxx;
       $u_rows +=$db->updater("$table","c_id",$c_id,"$_","$valxx");
      }

      if($_ =~ /mtime/ && $table){
       my $timexx = $valxx;
       $u_rows += $db->update_content4change($table,$c_id,$ct_name,$timexx,$_,$users_dms->{u_id}) if($timexx =~ /\d{1,2}\.\d{1,2}\.\d{2,4}\s\d{1,2}:\d{1,2}/ || $timexx =~ /\d{1,2}\.\d{1,2}\.\d{2,4}$/);
      }
      if($_ =~ /state/ && $table){
       my $state = $valxx;
       $u_rows += $db->update_content4change($table,$c_id,$ct_name,$state,$_,$users_dms->{u_id});
      }
      if($_ =~ /(xml_export)/ && $table){#boolean
       my $key = $1;
       my $value = $valxx || "f";
       $u_rows += $db->update_content4change($table,$c_id,$ct_name,$value,$key,$users_dms->{u_id});
      }
      if($_ =~ /int(\d+)/ && $table){
       $valxx =~ s/,/./;
       $valxx =~ s/\.00//;
       $valxx = "null" if(!$valxx);#for empty
       my $intxx = $valxx;
       #Ausgaben trigger
       if($ib_key eq "save_pos" && $_ eq "int02" && $intxx =~ /-\d/){
	  $u_rows += $db->update_content4change($table,$c_id,$ct_name,$intxx,"int02",$users_dms->{u_id});
       }elsif($ib_key eq "save_pos" && $_ eq "int03"){
        #Warenbestands trigger
	my $op="+";
	$op = $1 if($intxx =~ s/(-|\+)//);
	if($op =~ /-/){
   	  return "failure::Bitte einen fiktiven Artikel unter \"Ausgaben\" verwenden.";
	}
	my $add_menge="0";
	#differenz, es wird nur der zuwachs verwendet
	$ct_exist2->{$_} = 0 if(!$ct_exist2->{$_});
	$add_menge = - $ct_exist2->{$_} + $intxx if($op eq "+");
    	my $rows = $db->update_content4comp("contenttranspos",$R::ct_name,$R::c_idpos,"$op","$add_menge","$R::kind_of_trans");
	if($rows){
         $u_rows += $db->update_content4comp("content",$R::ct_name,"$R::cc_id","-","$add_menge","$R::kind_of_trans","$ct_exist2->{txt12}");
	}
       }else{
         $u_rows +=$db->updater("$table","c_id",$c_id,"$_","$valxx");
       }
      }
     }
    }
   }
  }


 #delete ask
 if("$ib_key" eq "remove_chk4rel" && $R::main_id && $R::c_id && $R::template_id && $R::rel_id){
   my $rel4nd = $db->collect_rel4nodes("",$R::c_id,$R::template_id);
   my $rel4tpl = $db->get_rel4tpl($R::main_id,$lang,"","","",$R::rel_id);
   my $node_names; my $i=0;
   my $delete_key = "";
   $delete_key = "delete_trans" if($rel4tpl->{ct_table} eq "contenttrans");
   $delete_key = "delete_tver" if($rel4tpl->{ct_table} eq "contenttver");
   foreach my $rid (sort { lc($rel4nd->{$a}->{node_name}) cmp lc($rel4nd->{$b}->{node_name}) }  keys (%$rel4nd)){
        $i++;
        $node_names .= "&bull; $rel4nd->{$rid}->{node_name}</br>" if($rel4nd->{$rid}->{node_name});
   }
   if($i == 1){
        return "failure::Datensatz wirklich löschen. ::?ct_trans=$delete_key\&exit_box2=1\&xml_export=$R::xml_export\&c_id=$R::c_id\&rel_id=$R::rel_id ::löschen"
   }else{
        return "failure::Es gibt hier zwei Möglichkeiten. Nur die Relation löschen oder den Content komplett löschen. ::?ct_trans=delete_rel4ct\&exit_box2=1\&main_id=$R::main_id\&rel_id=$R::rel_id ::Relation löschen ::?rel_edit=$delete_key\&exit_box2=1\&xml_export=$R::xml_export\&c_id=$R::c_id\&rel_id=$R::rel_id ::Content löschen"
   }
 }

 #delete Only relation ... without content
 if("$ib_key" eq "delete_rel4ct" && $R::main_id && $R::rel_id){
   $d_rows += $db->delete_relation($R::main_id,$lang,$R::rel_id);
   $db->cleanup_users($users_dms->{u_id}) if($users_dms->{u_id});
 }

 #DELETE abschluss
 if($ib_key eq "delete" || $ib_key =~ /delete_tver/){
  $table = "contenttrans";
  $table = "contenttver" if($ib_key =~ /delete_tver/);
  $d_rows += $db->delete_content("$table","$R::c_id");
  $db->update_users4trans("0","0","",$users_dms->{u_id});
 }

 #DELETE
 if($ib_key =~ /delete_trans/){
  my $c_id4del = $R::c_id4trans || $R::c_id;
  $table = "contenttrans" if($ib_key =~ /delete_trans/);
  my $ctt = $db->get_content1("$table","$c_id4del");

    my ($cttpos,$rows) = $db->collect_contentpos("$table","$c_id4del");
    foreach my $id (sort { lc($cttpos->{$a}->{sort}) cmp lc($cttpos->{$b}->{sort}) } keys(%$cttpos)){
      $db->update_content4comp("content",$cttpos->{$id}->{ct_name},"$cttpos->{$id}->{cc_id}","+","$cttpos->{$id}->{int03}","$users_dms->{kind_of_trans}","$cttpos->{$id}->{txt12}");
    }

  $d_rows += $db->delete_content("$table","$c_id4del");
  $db->update_users4trans("0","0","",$users_dms->{u_id});
  print $q->div({-class=>'elementwhite'},"2... redirecting to ... or CLICK ", $q->a({href=>"$varenv{wwwhost}$script$path"},"$varenv{wwwhost}$script$path"));
  print redirect("$varenv{wwwhost}$script$path?redirected=1\&return=0-0-0|$i_rows-$u_rows-$d_rows");
  exit 0;
 }

 my $pos_id = $R::c_idpos || $R::pos_id;
 #DELETE verpos
 if($ib_key eq "remove_verpos" && $pos_id){
  $table = "contenttverpos";
  $d_rows += $db->delete_content("$table","$pos_id");
 }

 #DELETE pos
 if($ib_key =~ /delete_pos|delete_verpos/ && $pos_id){
    $table = "contenttranspos";
    $table = "contenttverpos" if($ib_key =~ /delete_verpos/);
    my $cttpos = $db->get_content1($table,$pos_id);

    my $ctpos_sort;
    if($pos_id == $users_dms->{ctpos_activ}){
      my $ctt_subpos = $db->collect_content2($table,"ctpos_id","$pos_id");
      foreach my $id (keys(%$ctt_subpos)){
	$d_rows += $db->delete_content("$table","$ctt_subpos->{$id}->{c_id}");
      }
    }
    $d_rows += $db->delete_content("$table","$pos_id");
    $db->users_up("ctpos_activ","0",$users_dms->{u_id}) if("$pos_id" eq "$users_dms->{ctpos_activ}");
    $ct_name = $1 if($R::ct_name =~ /^(\d+)/);
    $u_rows += $db->update_content4comp("content",$ct_name,"$cttpos->{cc_id}","+","$cttpos->{int03}","$R::kind_of_trans","$cttpos->{txt12}");
 }
 ###

  #Auftragsstatus
  if($R::order_state && $R::c_id4trans){
       my $table = "contenttrans";
       $u_rows += $db->updater($table,"c_id",$R::c_id4trans,"txt22","$R::txt22",$users_dms->{u_id});
  }

  ###SET Relation (like move doc-type)
  #executed by Terminal submit-buttons
  #2. counter for WaWi and ReNr
  if((!$R::close_time && $R::set_main_id) && ($R::set_relation || $R::set_state || $R::open_set_main_id > 300000 || $R::ct_trans =~ /print_pdf|print/i)){
    if($R::open_set_main_id){
	$R::set_main_id = $R::open_set_main_id;
	$users_dms = $db->select_users($users_dms->{u_id});
    }
    my $c_id = $users_dms->{c_id4trans} || "";#Document id
    return "failure::Abbruch, die Aktion konnte keinem Dokument zugeordnet werden. Arbeiten Sie mit mehreren Browser-Tabs? Bitte multiple COPRI Tabs schließen und anschließend das Verkauf-Terminal neu öffnen." if(!$c_id);

    my $table = "contenttrans"; 
    my $ctt = { c_id => 0 };
    my $rel = $db->get_rel4tpl("",$lang,$users_dms->{c_id4trans},$users_dms->{tpl_id4trans});
    $ctt = $db->get_content1("contenttrans",$rel->{content_id});
    return "failure::Bitte erst einen Formular-Typ wählen" if($R::set_main_id <= "300000");
    $u_rows += $db->update_relation2("",$lang,$R::set_main_id,"",$rel->{rel_id}) if(!$R::close_time && $rel->{rel_id});

    my $node_faktura = $dbt->get_node($dbh,$dbt->{shareedms_conf}->{faktura});
    my $node = $dbt->get_node($dbh,$R::set_main_id);

    my $update_ctt = {
      table   => "contenttrans",
      mtime   => "now()",
      owner   => $users_dms->{u_id},
    };

    if(($node_faktura->{invoice_nr} > 0) && ("$ctt->{ct_name}" !~ /\d/ || "$R::set_main_id" != "$rel->{main_id}")){

        my $nextNr = $node_faktura->{invoice_nr};
        $update_ctt->{ct_name} = "$nextNr";
        $update_ctt->{barcode} = "$nextNr";
        my $update_node = {
          table   => "nodes",
          main_id => "$dbt->{shareedms_conf}->{faktura}",
          change => "no_time",
        };
        my $invoice_nr = $node_faktura->{invoice_nr} + 1;
        $dbt->update_one($dbh,$update_node,"invoice_nr='$invoice_nr'");

      	$update_ctt->{int12} = $node->{main_id};
      	$update_ctt->{txt00} = $node->{node_name};
      	$u_rows += $dbt->update_record($dbh,$update_ctt,$ctt);
   }

   #SET state alias Zahlung buchen
   if($R::set_state && !$R::close_time){
    my $state = $R::state || "";
    my $sum_paid = "null";
    if($R::set_state eq "buchen" && $R::sum_paid){
        $sum_paid = $R::sum_paid;
        $sum_paid =~ s/,/\./;
    	$update_ctt->{int01} = $sum_paid;
    }

    my $sum_operatorcredit = "null";
    if($R::set_state eq "buchen" && $R::sum_operatorcredit){
        $sum_operatorcredit = $R::sum_operatorcredit;
        $sum_operatorcredit =~ s/,/\./;
    	$update_ctt->{int02} = $sum_operatorcredit;
    }

    $update_ctt->{state} = "$state";
    $update_ctt->{int14} = 2 if($state =~ /payone/);#set OPOS
    $u_rows += $dbt->update_record($dbh,$update_ctt,$ctt);

    if($state =~ /payone/){
      my $ctadr = $db->get_content1("contentadr",$ctt->{int10});

      $ctt->{payone_reset} = 0;
      if($R::payone_reset){
        #after delete preauth after 0€ capture sets new TXID and increment reference
      	$ctt->{payone_reset} = $R::payone_reset;
	$ctt->{int01} = 0;
	$ctt->{sequence} = $ctt->{int18} || 1;
	$ctt->{sequence}++;
	if($ctadr->{int03} == 1 && $ctt->{txt16} && $R::state =~ /SEPA/){#SEPA 0
	  my $payoneret = $payone->captureSEPA_main(\%varenv,$ctadr,$ctt,$users_dms->{u_id});
	}
        if($ctadr->{int03} == 2 && $ctt->{txt16} && $R::state =~ /Kreditkarte/){#CC 0
         my $payoneret = $payone->captureCC_main(\%varenv,$ctadr,$ctt,$users_dms->{u_id});
        }
        $ctt = $db->get_content1("contenttrans",$ctt->{c_id});
      }

      if($ctadr->{int03} == 1 && $ctadr->{ct_name} =~ /\w{2}-\d+/){
	my $payoneret = $payone->preauthorizationSEPA_main(\%varenv,$ctadr,$ctt,$users_dms->{u_id});
	sleep 2;
      }elsif($ctadr->{int03} == 2 && length($ctadr->{ct_name}) >= 19){
	my $payoneret = $payone->preauthorizationCC_main(\%varenv,$ctadr,$ctt,$users_dms->{u_id});
	sleep 2;
      }

      $ctt = $db->get_content1("contenttrans",$ctt->{c_id});
      $ctt->{sequence} = $R::payone_sequence || 1;
      
      #only if int14 = OPOS (set by preauthorization)
      #if($ctt->{int14})#TODO doupleclick 
      if($state !~ /Zahlungseingang/){
       #SEPA capture
       if($ctadr->{int03} == 1 && $ctt->{txt16} && $R::state =~ /SEPA/){#SEPA
	my $payoneret = $payone->captureSEPA_main(\%varenv,$ctadr,$ctt,$users_dms->{u_id});
       }
       #CC capture
       elsif($ctadr->{int03} == 2 && $ctt->{txt16} && $R::state =~ /Kreditkarte/){#CC
	my $payoneret = $payone->captureCC_main(\%varenv,$ctadr,$ctt,$users_dms->{u_id});
       }
       else{
	my $return_text = "payone capture fails, errorcode ($ctadr->{int03} && TXID:$ctt->{txt16} && $R::state).";
	$update_ctt->{txt23} = "$now_dt $return_text\n" . $ctt->{txt23};
        $dbt->update_record($dbh,$update_ctt,$ctt);
        return "failure::$return_text";

	}
	#}else{
	#return "failure::Payone Geldeinzug nicht ausgeführt. Hat der Einzug bereits stattgefunden?";
      }
    }else{
    	$db->updater($table,"c_id",$ctt->{c_id},"int14","null","","","","","");
    }
   }

  }
  ###end SET Relation or state (V Terminal submit's)

  my $c_id4print = $users_dms->{c_id4trans};
  $c_id4print= $R::c_id if($R::printer_id =~ /Adresse|Kunde/);
  if($c_id4print){
   my $print_return = "";
   my $exit_code = 1;
   my $table = "contenttrans";
   my $ctt = $db->get_content1($table,$c_id4print);
   my $praefix = "$ctt->{txt00}-$varenv{praefix}";#like Rechnung-sharee_operator


   ###PRINT
   if($R::ct_trans =~ /print_pdf|print/i){
    my $main_id = $R::set_main_id || $R::main_id;
    my $node = $db->get_node4multi($main_id,$lang);
    my $psize="A4";
    if("$ctt->{txt00}" =~ /Quittung/){
       $psize="A5";
       #-B 0 -T 0 -L 0 -R 0 # testing paper-margin=0
    }
    my $wc_line=0;
    $wc_line= $ctt->{int04};#Adresse.Tabelle
    my ($sysname, $nodename, $release, $version, $machine) = uname();
    my $topdf = "$varenv{basedir}/src/wkhtmltopdf-amd64";

    if("$R::printer_id" =~ /PDF/){
	#without system() because we have to wait until PDF is ready
	$print_return = `$topdf --page-size $psize "$varenv{wwwhost}$script/Printpreview?printer_id=$R::printer_id\&mandant_main_id=$mandant_main_id\&main_id=$node->{main_id}\&ct_name2print=$ctt->{ct_name}\&c_id4trans=$c_id4print\&u_id=$users_dms->{u_id}\&wc=$wc_line" $varenv{pdf}/$praefix-$ctt->{ct_name}.pdf 2>&1`;
	$exit_code = $?;

	if(1==1){#debugging
	  my $filesize = -s "$varenv{pdf}/$praefix-$ctt->{ct_name}.pdf";
	  open(EMA, ">> $varenv{logdir}/copri-print.log");
	  print EMA "$today4db\n$topdf --page-size $psize $varenv{wwwhost}$script/Printpreview?printer_id=$R::printer_id\&mandant_main_id=$mandant_main_id\&main_id=$node->{main_id}\&ct_name2print=$ctt->{ct_name}\&c_id4trans=$c_id4print\&u_id=$users_dms->{u_id}\&wc=$wc_line $varenv{pdf}/$praefix-$ctt->{ct_name}.pdf\nreturn: $print_return\nfilesize: $filesize\nexit_code: $exit_code\nset_state: $R::set_state\n";
	  close EMA;
	  #exit 0;
	}

	if($R::set_state ne "buchen"){#no redirect if buchen incl. print_pdf
	  if( -f "$varenv{basedir}/pdf/$praefix-$ctt->{ct_name}.pdf"){
		#print redirect("$varenv{wwwhost}/pdf/$praefix-$ctt->{ct_name}.pdf");
  		#exit 0;
		print "<script type=\"text/javascript\">window.open('$varenv{wwwhost}$script/pdf/$praefix-$ctt->{ct_name}.pdf');</script>";
          }else{
		return "failure::PDF konnte nicht generiert werden, bitte Info an: info\@gnu-systems.de\n   $varenv{wwwhost}/pdf/$praefix-$ctt->{ct_name}.pdf";
       	  }
      	}

    }
   }

   if(-f "$varenv{basedir}/pdf/$praefix-$ctt->{ct_name}.pdf" && (($R::set_state eq "buchen" && $R::send_invoice && $ctt->{int01} && $ctt->{int01} != 0) || ($ib_key eq "send_invoice_again"))){
    #sleep 3;#PDF invoice sending disabled because of partly empty invoices
    #
    #my $key_pdf;
    #$key_pdf = `/usr/bin/pdftotext $varenv{basedir}/pdf/$praefix-$ctt->{ct_name}.pdf - | grep "Seite:"`;
    #system(`$varenv{basedir}/src/Mod/newsletter_tink.pl "$varenv{basedir}" "$varenv{wwwhost}" "send_invoice" "$ctt->{int10}" "$ctt->{ct_name}"`);# if($key_pdf =~ /Seite:/);
    system("$varenv{basedir}/src/scripts/mailTransport.pl '$varenv{syshost}' 'send_invoice' '$ctt->{c_id}' '$praefix-$ctt->{ct_name}.pdf'");

   }
   #
  }#end c_id4print

  #SET Tagesabschluss
  if($R::v_abschluss){
   my $journal_tpl="209";
   my $journal_id="300011";

   #test node
   my $n_exist = $db->get_node4multi($journal_id,$lang);
   my $t_exist = $db->get_tpl($journal_tpl);
   if(!$n_exist->{n_id} || !$t_exist->{tpl_id}){
	return "failure::Die Journal Konfiguration ist fehlerhaft.";
   }

   #collect sub-nodes
   #TODO
   my $main_ids = "300008,300009,300011";#Rechnung,Storno,Verkaufsjournal
   #my $main_ids = "$parent_trans->{main_id},";
   #$main_ids .= $db->collect_noderec($parent_trans->{main_id},$lang,"nothing") if($parent_trans->{main_id});
   #$main_ids =~ s/,$//;

   my $tpl_vk = "218";
   my $table = "contenttrans";
   my $sum_start = $R::sum_start || "0";
   my $sum_kasse = $R::sum_kasse || "0";
   $sum_start = $lb->checkint($sum_start);
   $sum_kasse = $lb->checkint($sum_kasse);
   my $c_id4kasse = "";
   my $c_id4abschluss = $db->get_content6("$table","close_time","null","state","Tagesabschluss","int09","$parent_trans->{parent_id}","","","$s_owner_id");
   if($journal_id){
     if($c_id4abschluss->{c_id} && $c_id4abschluss->{c_id} > 0){
       $c_id4kasse = $c_id4abschluss->{c_id};
     }else{
       $ct_name = "--- auto ct_name = c_id ---";
       $c_id4kasse = $db->insert_content2($table,$ct_name,$users_dms->{u_id},"Tagesabschluss");
	$i_rows += 1 if($c_id4kasse > 0);
       $db->update_content4change("contenttrans",$c_id4kasse,"$c_id4kasse");
       $db->update_content4change("contenttrans",$c_id4kasse,"",$parent_trans->{parent_id},"int09");#mandant_main_id
       $db->update_content4change("contenttrans",$c_id4kasse,"",$c_id4kasse,"int11");#last_ab
       $db->update_content4change("contenttrans",$c_id4kasse,"",$journal_id,"int12");#main_id

       $db->update_content4change("contenttrans",$c_id4kasse,"",$n_exist->{node_name},"txt00");#node_name
       my $rel_id = $db->insert_relationlist($table,$journal_id,$lang,$c_id4kasse,$tpl_vk,"ct_id");
     }
     $u_rows += $db->update_kasse($table,$c_id4kasse,$sum_kasse,$sum_start);

     if($journal_id && $journal_tpl && $R::close_trans){
	my $opos="null";
        $u_rows += $db->update_tagesabschluss($table,$c_id4kasse,$journal_id,$journal_tpl,$tpl_vk,$parent_trans->{parent_id},"$main_ids","$s_owner_id","$opos");

       print $q->div({-class=>'elementwhite'},"1... redirecting to ... or CLICK ", $q->a({href=>"$varenv{wwwhost}/DMS/Faktura/Verkaufsjournal"},"$varenv{wwwhost}/DMS/Faktura/Verkaufsjournal"));
       print redirect("$varenv{wwwhost}/DMS/Faktura/Verkaufsjournal?redirected=1\&return=0-0-0|$i_rows-$u_rows-$d_rows");
       exit 0;
     }
   }
   $db->cleanup_users($users_dms->{u_id});
  }
  ###
  return "$i_rows-$u_rows-$d_rows";
}
1;
