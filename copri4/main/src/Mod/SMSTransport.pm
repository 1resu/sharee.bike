package SMSTransport;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
#SMS sender
#
#perl -cw do
#use lib "/var/www/copri-bike/shareeapp-primary/src";

use strict;
use warnings;
use POSIX;
use CGI; # only for debugging
use LWP::UserAgent;
use URI::Encode;
use JSON;

use Scalar::Util qw(looks_like_number);
use Mod::DBtank;
use Mod::Basework;
use Data::Dumper;

my $q = new CGI;
my $dbt = new DBtank;
my $bw = new Basework;


sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

my $now_dt = strftime "%Y-%m-%d %H:%M:%S", localtime;

my $ua = LWP::UserAgent->new;
$ua->agent("sharee smsclient");

my $uri_encode = URI::Encode->new( { encode_reserved => 1 } );
my $json = JSON->new->allow_nonref;
my $response_in = {};
my $dbh = "";
my $owner = 181;

#used for sending smsAck codes to user
sub sms_ack_digest {
 my $self = shift;
 my $ctadr = shift;

 #Ack digest
 my $ack_digest = $q->unescapeHTML($ctadr->{txt34}) || "";
 my $email_ack_digest = $1 if($ack_digest =~ /^(.{5})/);
 my $sms_ack_digest = $1 if($ack_digest =~ /(.{5})$/);

 my $sms_from = "Mietradcode";
 my $sms_to = $ctadr->{txt07};# || "+491799xxxx72";
 my $sms_message = "";

 #goes to test if devel or joke bsp nr
 if($dbt->{copri_conf}->{stage} eq "test" || $ctadr->{txt07} =~ /17012345678/){
    $ctadr->{txt07} = "$dbt->{copri_conf}->{sms_to}";
    $sms_message = "* offline Test *";
 }
 if($ctadr->{txt07} =~ /^0/ || $ctadr->{txt07} !~ /\+[1-9]/){
   my $sms_tosub = $ctadr->{txt07};
   $sms_tosub =~ s/^\+00/\+/;
   $sms_tosub =~ s/^00/\+/;
   $sms_tosub =~ s/^0/\+49/;
   $sms_to = $sms_tosub;
 }
 $sms_message .= "Ihr Mietradsystem SMS-Bestätigungscode lautet: $sms_ack_digest";
 my $message = Encode::encode('iso-8859-1', Encode::decode('utf-8',"$sms_message"));

 open(FILE,">>$dbt->{copri_conf}->{logdir}/sms_gtx.log");
 print FILE "\n*** $now_dt 'sharee smsclient' \n";

   my $request = {
           from => $sms_from,
           to => $sms_to,
           text => $message,
   };

   print FILE "---> request:\n" . Dumper($request);

   my $ret_json = $self->get_sms_gtx($request);

   eval {
     $response_in = decode_json($ret_json);
     print FILE "<--- response_in:\n" . Dumper($response_in);
   };
   if ($@){
     print FILE "<--- failure raw response_in:\n" . Dumper($ret_json) . "\n";
     warn $@;
   }

 close(FILE);

 return $ret_json;
}

#used for sending message to user
sub sms_message {
 my $self = shift;
 my $todo = shift;
 my $hotline = shift;
 my $sms_to = shift;
 my $bike = shift;

 my $sms_from = "Mietradinfo";
 my $sms_message = "";

 #goes to test if devel or joke bsp nr
 if($dbt->{copri_conf}->{stage} eq "test" || $sms_to =~ /17012345678/){
    $sms_to = "$dbt->{copri_conf}->{sms_to}";
    $sms_message = "* offline Test *";
 }
 if($sms_to =~ /^0/ || $sms_to !~ /\+[1-9]/){
   my $sms_tosub = $sms_to;
   $sms_tosub =~ s/^\+00/\+/;
   $sms_tosub =~ s/^00/\+/;
   $sms_tosub =~ s/^0/\+49/;
   $sms_to = $sms_tosub;
 }

 #todo locking_progress
 if($todo eq "locking_progress"){
   $sms_message .= "Ihre Miete von $bike wurde nicht beendet! Ihre kostenpflichtige Miete läuft weiter! Stellen Sie sicher, dass das Schloss geschlossen ist. $hotline";
 }

 #todo if > 24h rental
 if($todo eq "24h_occupied"){
   $sms_message .= "Zur Info! Sie haben $bike seit 24 Stunden kostenpflichtig gemietet!. $hotline";
 }
 if($todo eq "48h_occupied"){
   $sms_message .= "Zur Info! Sie haben $bike seit 48 Stunden kostenpflichtig gemietet!. $hotline";
 }
 if($todo eq "72h_occupied"){
   $sms_message .= "Zur Info! Sie haben $bike seit 72 Stunden kostenpflichtig gemietet!. $hotline";
 }

 #todo fraud_rental
 if($todo eq "fraud_rental"){
   $sms_message .= "Miete von einem Nutzer der als Betrugsfall hinterlegt ist! Mietrad $bike ";
 }


 my $message = Encode::encode('iso-8859-1', Encode::decode('utf-8',"$sms_message"));

 open(FILE,">>$dbt->{copri_conf}->{logdir}/sms_gtx.log");
 print FILE "\n*** $now_dt 'sharee smsclient' \n";

   my $request = {
           from => $sms_from,
           to => $sms_to,
           text => $message,
   };

   print FILE "---> request:\n" . Dumper($request);

   #my $ret_json = "";#$self->get_sms_gtx($request);#for testing without send
   my $ret_json = $self->get_sms_gtx($request);

   eval {
     $response_in = decode_json($ret_json);
     print FILE "<--- response_in:\n" . Dumper($response_in);
   };
   if ($@){
     print FILE "<--- failure raw response_in:\n" . Dumper($ret_json) . "\n";
     warn $@;
   }

 close(FILE);

 return $ret_json;
}

#sms gtx http request
sub get_sms_gtx {
 my $self = shift;
 my $request = shift || "";

 my $api_file = "/var/www/copri4/shareeconf/apikeys.cfg";
 my $aconf = Config::General->new($api_file);
 my %apikeyconf = $aconf->getall;
 #print $apikeyconf{smsgtx}->{gtx_key};

 my $endpoint = "https://rest.gtx-messaging.net/smsc/sendsms/$apikeyconf{smsgtx}->{gtx_key}/json";
 my $rest;
 foreach (keys (%$request)){
   my $encoded_val = $uri_encode->encode($request->{$_});
   $rest .= "$_=$encoded_val&";
 }
 $rest =~ s/\&$//;

 my $gtx_request = "$endpoint?$rest";

 print FILE "===> GET2gtx >> " . $gtx_request . "\n";

 my $req = HTTP::Request->new(GET => "$gtx_request");
 $req->content_type('application/x-www-form-urlencoded');

 my $res = $ua->request($req);
 if ($res->is_success) {
  #print $res->content;
  return $res->content;
  print $res->status_line, "\n";
 }else {
  print $res->status_line, "\n";
 }
}
#

1;
