#!/usr/bin/perl -w
#
## SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
#TODO, migrate to MailTransport.pm
#
#su www-data -c "./src/Mod/newsletter_tink.pl '/var/www/copri4/shareeapp-operator' 'https://shareeapp-operator.copri-bike.de' 'emailack' '1842'"

#on command
#sudo su www-data -c "./src/Mod/newsletter_tink.pl '/var/www/copri3/konrad' 'https://konrad.tink-konstanz.de' 'send_password' 'ragu@gnu-systems.de' '59e5c7bce6'"

#su www-data -c "./src/Mod/newsletter_tink.pl '/var/www/copri3/tinkdms' 'https://tinkdms.copri.eu' 'send_invoice' '1842' '2866'"


use strict;
use warnings;
use utf8;
use Encode;
use CGI ':standard';
use DBI;
use POSIX;
use Email::MIME;
use IO::All;
use Email::MIME::CreateHTML;
use Email::Sender::Simple qw(sendmail);
#use Email::Sender::Transport::SMTPS;
use Net::SMTP;
use Try::Tiny;
use Config::General;
use URI::Encode;
my $uri_encode = URI::Encode->new( { encode_reserved => 1 } );
use Data::Dumper;

use Sys::Hostname;
my $hostname = hostname;

my $q = new CGI;
my $basedir = $ARGV[0];
my $wwwhost = $ARGV[1];
my $todo = $ARGV[2];
my $c_id="";
my $ct_name="";
my $emailpw="";
my $coopw="";
if($todo =~ /emailack|send_invoice|send_capture_fail/){
  $c_id = $ARGV[3] || "";#contentadr.c_id
  $ct_name = $ARGV[4] || "";#contenttrans.c_id
}elsif($todo eq "send_password"){
  $emailpw = $ARGV[3] || "";
  $emailpw =~ s/\s//g;
  $coopw = $ARGV[4] || "";
}

my $today = strftime("%d.%m.%Y %H:%M:%S",localtime(time));

my $globalconf_file = "/var/www/copri4/shareeconf/global.cfg";
my $gconf = Config::General->new($globalconf_file);
my %globalconf = $gconf->getall;

my $mailx_file = "/var/www/copri4/shareeconf/mailx.cfg";
my $mconf = Config::General->new($mailx_file);
my %mailxconf = $mconf->getall;

require "$basedir/src/Lib/Config.pm";
my %varenv = &Config::envonline("","$wwwhost");
#print Dumper(%varenv);

open(EMA, ">> $varenv{logdir}/newsletter_tink.log");
   print EMA "\n$today, start mailing\n";
   print EMA "'$today' '$basedir' '$wwwhost' '$todo' 'c_id:$c_id' 'invoice ct_name:$ct_name' 'emailpw:$emailpw' 'coopw:$coopw'\n";
close EMA;


my $dbh = &dbconnect();
sub dbconnect {
 my $options ="";
 my $dbh = DBI->connect("dbi:Pg:dbname=$varenv{dbname};host=$varenv{dbhost};port=5432;options='$options'", "$varenv{dbuser}", "$varenv{dbpassw}",{ RaiseError => 1, AutoCommit => 1 }) or die "Can't connect to $varenv{dbname}: $DBI::errstr";
  return $dbh;
}

sub get_contentadr(){
 my ($c_id,$emailpw) = @_;
 my $where="where ct.c_id=rel.content_id";
 $where .= " and ct.c_id=$c_id" if($c_id =~ /^\d+$/);
 $where .= " and ct.txt08 ilike '$emailpw'" if($emailpw =~ /\w+/);
 $where .= " and rel.template_id = 202";#tpl_id Adressen
 my $sth = $dbh->prepare("SELECT * FROM contentadr ct, relation rel $where");
 my $rc = $sth->execute();
 my $ct = $sth->fetchrow_hashref();
 return $ct;
}

sub get_content(){
 my ($table,$c_id,$tpl_id) = @_;
 my $where="where ct.c_id=rel.content_id";
 $where .= " and ct.c_id=$c_id" if($c_id =~ /^\d+$/);
 $where .= " and rel.template_id = $tpl_id" if($tpl_id =~ /^\d+$/);
 my $sth = $dbh->prepare("SELECT * FROM $table ct, relation rel $where");
 my $rc = $sth->execute();
 my $ct = $sth->fetchrow_hashref();
 return $ct;
}

#update trivial, matchs anything
sub updater(){
 my ($table,$w_col,$w_val,$column,$content,$owner,$w_col2,$w_op2,$w_val2,$set_time) = @_;
 my $ct_set = "mtime='now()'";
 if($set_time eq "no_time"){
   $ct_set = "";
 }elsif($table !~ /content/){
   $ct_set = "change='now()'";
 }

 if("$content" eq "null" || (!$content && $content !~ /^0$/)){
    $ct_set .= ",$column=null";
 }elsif($content || $content == 0){
    $ct_set .= ",$column='$content'";
 }
 $ct_set .= ",owner='$owner'" if($owner);
 $ct_set =~ s/^,/ /;
 my $where = "$w_col='$w_val'";
 $where .= " and $w_col2 $w_op2 ($w_val2)" if("$w_col2" && "$w_op2" && "$w_val2");
 my $sth = $dbh->prepare("UPDATE $table SET $ct_set where $where");
 my $rows = $sth->execute();
 return $rows;
}


my $ctadr = &get_contentadr($c_id,$emailpw) if($c_id =~ /^\d+$/ || $emailpw =~ /\w+\@\w+/);

 my $smtp = Net::SMTP->new($mailxconf{mailx_default}->{mail_gateway},
                        Port => 465,
                       Hello => 'sharee.bike',
                       Timeout => 30,
                       Debug   => 0,
                       SSL     => 1,
                      );


 #$varenv{sasl_password} = Encode::encode('iso-8859-1', Encode::decode('utf-8', $varenv{sasl_password}));
 $smtp->auth($mailxconf{mailx_default}->{sasl_username},$mailxconf{mailx_default}->{sasl_password});
 $smtp->mail($mailxconf{mailx_default}->{mail_from});

my $smtp_return = "";
if($todo eq "emailack"){
  $smtp_return = &esender_success($smtp,$ctadr,$wwwhost);
}elsif($todo eq "send_password" && $ctadr->{txt08} =~ /\w+\@\w+/){
  $smtp_return = &esender_password($smtp,$ctadr,$coopw,$wwwhost);
}elsif($todo eq "send_invoice"){
  my $send_invoice;
  $smtp_return = &esender_invoice($smtp,$todo,$ctadr,$ct_name,$wwwhost);
  # if sendmail true then null. For us we need text
  #if(!$smtp_return && $ct_name =~ /\d+/){
  if($ct_name =~ /\d+/){
    $today =~ s/:\d+$//;
    $send_invoice = "$today, Rechnungsinfo per eMail versandt (debug: $smtp_return)";
    &updater("contenttrans","ct_name","$ct_name","txt30","$send_invoice","","","","","no_time");
  }
}elsif($todo eq "send_capture_fail"){
  $smtp_return = &esender_invoice($smtp,$todo,$ctadr,$ct_name,$wwwhost);
}
open(EMA, ">> $varenv{logdir}/newsletter_tink.log");
   print EMA "smtp_return: $smtp_return\n";
close EMA;



#Registration acknowledgement
sub esender_success {
 	my $smtp = shift;
 	my $ctadr = shift;
 	my $wwwhost = shift;
	my %varenv = &Config::envonline("","$wwwhost");

  	my $ct = &get_content("contentuser",$globalconf{shareedms_conf}->{parent_node},"201");


	my $email = $q->unescapeHTML($ctadr->{txt08});
	my $anrede = $q->unescapeHTML($ctadr->{txt02});
	my $name = $q->unescapeHTML($ctadr->{txt01});
        $name = Encode::encode('iso-8859-1', Encode::decode('utf-8', $name));

	my $strasse = $q->unescapeHTML($ctadr->{txt03});
        $strasse = Encode::encode('iso-8859-1', Encode::decode('utf-8', $strasse));

	my $ort = $q->unescapeHTML($ctadr->{txt06});
	my $telefon = $q->unescapeHTML($ctadr->{txt07});

	#Ack digest
	my $ack_digest = $q->unescapeHTML($ctadr->{txt34}) || "";
	my $email_ack_digest = $1 if($ack_digest =~ /^(.{5})/);
	my $sms_ack_digest = $1 if($ack_digest =~ /(.{5})$/);

	#disabled because of payone payment
	#my $iban = $q->unescapeHTML($ctadr->{txt22});
	#$iban =~ s/.{3}$/XXX/;
	#IBAN: $iban (letzte drei Ziffern maskiert)
	

#email html body---------------------------------------------------
my $body = "";
my $encoded_email = $uri_encode->encode($ctadr->{txt08});
$body = <<EOF
Vielen Dank für Ihre Registrierung im Mietradsystem. 

Damit Sie Ihr Nutzerkonto in der App freischalten können, verwenden Sie folgenden e-Mail Bestätigungscode bei Ihrer Registrierung.
Der e-Mail Bestätigungscode lautet: <b>$email_ack_digest</b>

Viel Spaß beim Radeln. 
<div>
$ct->{txt09}
$ct->{txt04}
$ct->{txt05}

$ct->{txt08}
$ct->{txt11}

Bei Fragen zum e-Mail Bestätigungscode können sie uns unter hotline\@sharee.bike erreichen.
</div>


EOF
;

 $body  =~ s/\n/<br \/>/g;
 my $html = "<html><head><title>$varenv{title}</title></head><body style='text-align:left;border:0px solid silver;padding:15px;margin:2%;width:90%;'>\n";
 $html .= "<div>$body</div>";
 $html .= "</body></html>";


#-----------------------------------------------------------------

     
     my $subject = "Mietrad Anmeldebestätigung";
     if($hostname ne "ginger"){
	$email = $mailxconf{mailx_default}->{mail_to};
        $subject .= "* offline Test *";
     }

 if ($smtp->to($email)) {
  $smtp->data();
  $smtp->datasend("To: $email\n");
  $smtp->datasend("Subject: $subject\nMIME-Version: 1.0\nContent-Type: text/html; charset=UTF-8 \n\n");
  $smtp->datasend($html);
  $smtp->dataend();
 } else {
  print "Error: ", $smtp->message();
 }

 return "1. okay";
}

#Password forgotten
sub esender_password {
 	my ($smtp,$ctadr,$coopw,$wwwhost) = @_;

	my %varenv = &Config::envonline("","$wwwhost");

        my $ct = {};
	if($varenv{syshost} =~ /konrad|tink/i){
          $ct = &get_content("content","3435","1");
          $ct->{txt01} = $q->unescapeHTML($ct->{txt01});
          $ct->{txt01} =~ s/\r\n/<br \/>/g;
          $ct->{txt01} = Encode::encode('iso-8859-1', Encode::decode('utf-8', $ct->{txt01}));
	}else{
	  $ct = &get_content("contentuser",$globalconf{shareedms_conf}->{parent_node},"201");
        }


	my $email = $q->unescapeHTML($ctadr->{txt08});
	my $anrede = $q->unescapeHTML($ctadr->{txt02});
	my $name = $q->unescapeHTML($ctadr->{txt01});
        $name = Encode::encode('iso-8859-1', Encode::decode('utf-8', $name));


#email html body---------------------------------------------------
my $body;
if($varenv{syshost} =~ /konrad|tink/i){
$body = <<EOF
Hallo $name,

Sie haben ein neues Passwort beantragt. Das neue Passwort lautet: <b>$coopw</b> 

Nach Ihrem Login <a href='http://www.konrad-konstanz.de'>www.konrad-konstanz.de</a> können Sie das Passwort in Ihrem Profil unter Kundendaten auch gerne wieder ändern. 

Viel Spaß beim Radeln. 

EOF
;

}else{#sharee

$body = <<EOF
Hallo $name,

Sie haben ein neues Passwort beantragt. Das neue Passwort lautet: <b>$coopw</b> 

Nach Ihrem Login können Sie das Passwort in Ihrem NutzerInnen Konto unter 1.Kundendaten auch gerne wieder ändern. 

<div>
Freundliche Grüße, 
--
$ct->{txt09}
$ct->{txt04}
$ct->{txt05}

$ct->{txt08}
$ct->{txt11}
</div>

sharee.bike ist ein Angebot der TeilRad GmbH

EOF
;
}

 $body  =~ s/\n/<br \/>/g;
 my $html = "<html><head><title>$varenv{title}</title></head><body style='text-align:left;border:0px solid silver;padding:15px;margin:2%;width:90%;'>\n";
 $html .= "<div>$body</div>";
 #if($mandant eq "TINK"){
 #  $html .= "<div><img src=\"https://www2.tink-konstanz.de/img/TINK_Signatur.jpg\" \/></div>";
 #}
 if($varenv{syshost} =~ /konrad|tink/i){
    $html .= "<div>$ct->{txt01}</div>\n";
 }
 $html .= "</body></html>";


#-----------------------------------------------------------------

     
     my $subject = "$varenv{title} Passwort vergessen";
     if($hostname ne "ginger"){
	$email = $mailxconf{mailx_default}->{mail_to};
        $subject .= "* offline Test *";
     }

 if ($smtp->to($email)) {
  $smtp->data();
  $smtp->datasend("To: $email\n");
  $smtp->datasend("Subject: $subject\nMIME-Version: 1.0\nContent-Type: text/html; charset=UTF-8 \n\n");
  $smtp->datasend($html);
  $smtp->dataend();
 } else {
  print "Error: ", $smtp->message();
 }

 return "2. okay";
}

#Send Invoice and capture_fails
sub esender_invoice {
 	my ($smtp,$todo,$ctadr,$ct_name,$wwwhost) = @_;

	my %varenv = &Config::envonline("","$wwwhost");
	my $ct = {};
	  $ct = &get_content("contentuser",$globalconf{shareedms_conf}->{parent_node},"201");

     	#generate html for email-body
	my $email = $q->unescapeHTML($ctadr->{txt08});
	my $anrede = $q->unescapeHTML($ctadr->{txt02});
	my $name = $q->unescapeHTML($ctadr->{txt01});
        $name = Encode::encode('iso-8859-1', Encode::decode('utf-8', $name));


#email html body---------------------------------------------------
my $subject;
my $body;
my $signature;

if($varenv{syshost} =~ /konrad|tink/i){
$signature = <<EOF
Mit freundlichen Grüßen,
--
fahrradspezialitaeten.com
Heinrich-von-Stephan-Str. 12
79100 Freiburg


<small>Für Hinweise auf herumstehende Räder (gerne mit Radnummer, genauer Position und Fotos) aber auch Personen, die unsere Räder unrechtmäßig fahren oder beschädigen sind wir sehr dankbar. Sie erreichen uns über E-Mail (konrad\@fahrradspezialitaeten.com) und telefonisch über unsere Hotline: +49 761 45370099 (7 Tage/ 24 Stunden erreichbar).
Wenn Sie beobachten, wie jemand eines unserer Räder beschädigt, rufen Sie bitte unmittelbar die Polizei (Tel.:07531 995 2222) hinzu!

Vielen Dank für Ihre Unterstützung!</small>

EOF
;
$signature  =~ s/\n/<br \/>/g;
}

if($todo eq "send_invoice"){

#$subject = "TINK Rechnung";
$subject = "Fahrradmietsystem Rechnung";

if($varenv{syshost} =~ /konrad|tink/i){

$body = <<EOF
Guten Tag $name,

Ihre aktuelle Rechnung mit der Nummer $ct_name liegt ab sofort unter <a href='https://www.stadtwerke-konstanz.de/mobilitaet/rad-mietsystem?konrad_goto=Anmelden'>https://www.stadtwerke-konstanz.de/mobilitaet/rad-mietsystem</a> für Sie bereit.
Nach dem Anmelden ist in Ihrem Nutzer Profil unter Verleihdaten die Rechnung als PDF hinterlegt.

EOF
;
$body  =~ s/\n/<br \/>/g;

}else{#sharee

$body = <<EOF
Guten Tag $name,

Ihre aktuelle Rechnung mit der Nummer $ct_name liegt ab sofort in Ihrem sharee.bike App Konto für Sie bereit.
Nach dem Anmelden ist in Ihrem Nutzer Konto unter Verleihdaten die Rechnung als PDF hinterlegt.

<div>
Freundliche Grüße,
--
$ct->{txt09}
$ct->{txt04}
$ct->{txt05}

$ct->{txt08}
$ct->{txt11}
</div>

sharee.bike ist ein Angebot der TeilRad GmbH

EOF
;
$body  =~ s/\n/<br \/>/g;
}

}elsif($todo eq "send_capture_fail"){


$subject = "Fahrradmietsystem Account";

if($varenv{syshost} =~ /konrad|tink/i){

$body = <<EOF
Guten Tag $name,

wir mussten Ihren Fahrradmietsystem Account sperren weil der letzte Mietbetrag nicht eingezogen werden konnte.
Sie können Ihren Account selbst wieder freischalten, indem Sie die Zahlungsdaten erneuern. Dies kann aus Sicherheitsgründen von Seiten Ihrer Bank nötig sein oder es gab einen Fehler in Ihren Eingaben.

Nach der Anmeldung im Fahrradmietsystem Konstanz ( <a href='https://www.stadtwerke-konstanz.de/mobilitaet/rad-mietsystem?konrad_goto=Anmelden'>https://www.stadtwerke-konstanz.de/mobilitaet/rad-mietsystem</a> ) haben Sie die Möglichkeit Ihre Daten zu überprüfen und ggf. zu erneuern.
Kontaktieren Sie uns bitte falls Ihr Account für den Verleih nicht automatisch freigeschaltet wurde.

Für weitere Fragen wenden Sie sich bitte an unsere Buchhaltung unter: buchhaltung\@fahrradspezialitaeten.com oder telefonisch 0761/5158912 (Mo, Mi, Fr 9-12 Uhr)

EOF
;
$body  =~ s/\n/<br \/>/g;

}else{#sharee

$body = <<EOF
Guten Tag $name,

wir mussten Ihren Fahrradmietsystem Account sperren weil der letzte Mietbetrag nicht eingezogen werden konnte.
Sie können Ihren Account selbst wieder freischalten, indem Sie die Zahlungsdaten erneuern. Dies kann aus Sicherheitsgründen von Seiten Ihrer Bank nötig sein oder es gab einen Fehler in Ihren Eingaben.

Nach der Anmeldung in Ihrem Konto der sharee.bike App haben Sie die Möglichkeit Ihre Daten zu überprüfen und ggf. zu erneuern.
Kontaktieren Sie uns bitte falls Ihr Account für den Verleih nicht automatisch freigeschaltet wurde.

Für weitere Fragen wenden Sie sich bitte an unsere Buchhaltung unter: post\@sharee.bike oder telefonisch 0761/5158912 (Mo, Mi, Fr 9-12 Uhr)

<div>
Freundliche Grüße,
--
$ct->{txt09}
$ct->{txt04}
$ct->{txt05}

$ct->{txt08}
$ct->{txt11}
</div>

sharee.bike ist ein Angebot der TeilRad GmbH

EOF
;
$body  =~ s/\n/<br \/>/g;

}

}

 my $html = "<html><head><title></title></head><body style='text-align:left;border:0px solid silver;padding:15px;margin:2%;width:90%;'>\n";
 $html .= "<div>$body</div>\n";
 #$html .= "<div><img src=\"https://www2.tink-konstanz.de/img/TINK_Signatur.jpg\" \/></div>";
 $html .= "<div>$signature</div>\n";
 $html .= "</body></html>";


#-----------------------------------------------------------------

     
     if($hostname ne "ginger"){
	$email = $mailxconf{mailx_default}->{mail_to};
        $subject .= "* offline Test *";
     }
     my $message;


 if ($smtp->to($email)) {
  $smtp->data();
  $smtp->datasend("To: $email\n");
  $smtp->datasend("Subject: $subject\nMIME-Version: 1.0\nContent-Type: text/html; charset=UTF-8 \n\n");
  $smtp->datasend($html);
  $smtp->dataend();
 } else {
  print "Error: ", $smtp->message();
 }

 return "3. okay";
}



open(EMA, ">> $varenv{logdir}/newsletter_tink.log");
   print EMA "\n$today, done mailing\n";
   print EMA "'$today' '$basedir' '$wwwhost' '$todo' '$ctadr->{txt08}'\n";
   print EMA "\n\n";
close EMA;

$smtp->quit;

1;
