package Modalbox3;

# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH

use strict;
use warnings;

use CGI::Cookie ();
use CGI ':standard';
use CGI::Carp qw(fatalsToBrowser);
use Lib::Config;
use Mod::Buttons;
use Mod::DBtank;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

sub mobox3(){
 my ($node_meta,$users_dms,$mode,$return) = @_;
 #my ($return) = @_;
 my $q = new CGI;
 my $cf = new Config;
 my $dbt = new DBtank;
 my $but = new Buttons;
 $q->import_names('R');
 my @keywords = $q->param;
 my %varenv = $cf->envonline();
 my $script = $q->script_name();
 my $path = $q->path_info();
 my $coo = $q->cookie(-name=>'domcookie');
 my $dbh = "";

 if($node_meta->{main_id} > 100){

  my $bg_color = "white";
  my $bg_color2 = $node_meta->{bg_color} || "grey";
  my $title = "Content Editor \"$node_meta->{tpl_name}\"";
  $title = "Artikel Editor" if($node_meta->{ct_table} eq "content");
  $title = "Kunden Editor" if($node_meta->{ct_table} eq "contentadr");
  $title = "DMS-Account Zugriffsberechtigung" if($node_meta->{ct_table} eq "users");
  $title = "Service Editor" if($node_meta->{ct_table} eq "contentpos");
  $title = "Rental Editor" if($node_meta->{ct_table} eq "contenttranspos");
  my $height = $node_meta->{tpl_height} || "990";
  my $width = $node_meta->{tpl_width} || "990";

  if($mode eq "maintainer"){
    $title = "Node Editor";
    $height = "300";
    $width = "600";
  }elsif($mode eq "supervisor"){
    $title = "Datenfeld Eigenschaft bearbeiten";
    $height = "450";
    $width = "500";
  }elsif($R::rel_edit =~ /dialog4menu|dialog4content/){
    $title = "Relation Editor";
    $height = "300";
    $width = "550";
  }


print<<EOF
  <style>
  .ui-dialog .ui-dialog-content {
    background: $bg_color;
  }
  .ui-dialog > .ui-widget-header {
    color:black;
    font-weight:normal;
    border:1px solid $bg_color2;
    background: $bg_color2;
  }
  .ui-widget-overlay {
    background: #666 url("$varenv{metahost}/js/jquery-ui-1.12.1/images/ui-bg_diagonals-thick_20_666666_40x40.png") 50% 50% repeat;
    opacity: .5;
    filter: Alpha(Opacity=50);
  }
  </style>

  <script>
        \$(function() {
                \$( "#dialog-form3" )
                        .css("background-color","$bg_color")
                        .dialog({
                                height: $height,
                                width: $width,
                                closeOnEscape: true,
                                modal: true
                        });
               \$('.ui-widget-overlay').click(function() {
               \$(".ui-dialog-titlebar-close").trigger('click');});
                \$('.ui-dialog').css('z-index',9999);
});
  </script>
EOF
;

  print "<div id='dialog-form3' style='text-align:left;margin:auto;max-width:1400px;' title='$title'>\n";

  print $q->start_multipart_form(-name=>"editform");

  if($R::node2edit && $R::node2edit =~ /new_relation|edit_relation/){
   if($node_meta->{ct_table} eq "content" && $node_meta->{template_id} eq "205"){
    require "Mod/NodeEdit.pm";
    &NodeEdit::admin_tpl($node_meta,$users_dms,$mode,$return);
   }elsif($users_dms->{u_id} == $dbt->{copri_conf}->{superu_id}){
    require "Mod/NodeEdit.pm";
    &NodeEdit::admin_tpl($node_meta,$users_dms,$mode,$return,"only superu_id");
   }else{
    print $q->div("Dieses Menue ist zur Bearbeitung nicht freigegeben.");
   }
  }
  #elsif($node_meta->{ct_table} eq "contentpos"){
  #   require "Tpl/APIdialog.pm";
  #   &APIdialog::tpl($node_meta,$users_dms,$mode,$return);
  #}
  elsif($R::relate_dialog || $R::rel_edit =~ /dialog4menu|dialog4content/){
     require "Mod/RelationEdit.pm";
     &RelationEdit::tpl($node_meta,$users_dms,$mode,$return);
  }
  elsif(($R::node2edit && $R::node2edit =~ /editpart/) || ($R::base_edit && $R::base_edit !~ /delete/)){
    require "Tpl/BaseEdit.pm";
    &BaseEdit::tpl($node_meta,$users_dms,$mode,$return);
  }
  else{
	print $q->div("Zugriff verweigert.");
  }
  print $q->end_form;

  print "</div>\n";
 }
}
1;
