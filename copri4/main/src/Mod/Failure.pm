package Failure;
#
#      
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
use strict;
use warnings;
use CGI::Carp qw(fatalsToBrowser);
use CGI ':standard';
use Lib::Config;
use Mod::Buttons;
use Mod::Libenz;
use Mod::Libenzdb;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
  my ($main_id,$u_id,$lang,$bg_color1,$bg_color2,$level,$failure) = @_;

  my $q = new CGI;
  my $cf = new Config;
  my $lb = new Libenz;
  my $db = new Libenzdb;
  my $but = new Buttons;
  my %varenv = $cf->envonline();
  my $path = $q->path_info();
  $path =~ s/\/user|\/manager|\/admin//;
  my $script = $q->script_name();
  my %ib = $but->ibuttons();
  $failure =~ s/failure:://g;
  my ($link,$lname,$link2,$lname2);
  ($failure,$link,$lname,$link2,$lname2) = split(/::/,$failure);
  my $width="600";
  my $height="300";
  #my $bg_color = $varenv{background_color} || "white";
  #my $bg_color2 = $varenv{background_color2} || "";

  if($failure =~ /Newsletter Versandt/){
    $width="600";
    $height="700";
  }

print<<EOF
  <style>
  .ui-dialog .ui-dialog-content {
    background: $bg_color1;
  }
  .ui-dialog > .ui-widget-header {
    color:red;
    font-weight:normal;
    border:1px solid $bg_color2;
    background: $bg_color2;
  }
  </style>

  <script>
        \$(function() {
                \$( "#dialog-failure" )
			.css("background-color","$bg_color1")
		   	.dialog({
                                height: $height,
                                width: $width,
                                closeOnEscape: true,
                                modal: true
                        });
        });
  </script>
EOF
;

  my $title = "Achtung";
  my $back = "zurück";
  my $font_size = "";
  print "<div id='dialog-failure' style='$font_size;text-align:left;margin:auto;max-width:600px;' title='$title'>";


  print "<div style='padding:1em;background-color:white;'>\n";
  print $q->div("$failure");

  print "<style>.linknav2 { margin:3px; padding:1px 20px; border: 1px solid $bg_color2; background-color: $bg_color2; font-size:13px; text-decoration: none; } .linknav2:hover { color:#464433;}</style>\n";

  print $q->div({-style=>'float:left;padding:1em;'}, $q->a({-class=>"linknav2",-href=>"$link",-title=>''}, " $lname ")) if($lname);
  print $q->div({-style=>'float:left;padding:1em;'}, $q->a({-class=>"linknav2",-href=>"$link2",-title=>''}, " $lname2 ")) if($lname2);
  print $q->div({-style=>'float:left;padding:1em;'},$q->a({-class=>"linknav2",-href=>'javascript:history.back()'}, " $back "));
  print "</div>\n";

  print "</div>";
}
1;
