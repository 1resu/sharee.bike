package Premain;
#
#Deprecated module, please use Prelib.pm
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
use strict;
use warnings;
use POSIX;
use File::Path qw(make_path remove_tree);
use File::Copy;
use File::Copy::Recursive qw(fcopy rcopy dircopy fmove rmove dirmove);
use CGI::Carp qw(fatalsToBrowser);
use CGI ':standard';
use Scalar::Util qw(looks_like_number);
use Image::Magick;
use LWP::UserAgent;
use Lib::Config;
use Mod::Buttons;
use Mod::Libenz;
use Mod::Libenzdb;
use Mod::DBtank;
use Mod::Prelib;
use Data::Dumper;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

sub maininit(){
 my $self = shift;
 my $users_dms = shift;

 my $q = new CGI;
 my $pi = new Image::Magick;
 my @keywords = $q->param;
 my $cf = new Config;
 my $lb = new Libenz;
 my $db = new Libenzdb;
 my $but = new Buttons;
 my $ua = LWP::UserAgent->new;
 my $dbt = new DBtank;
 my $pl = new Prelib;
 my %ib = $but->ibuttons();
 my %varenv = $cf->envonline();
 my $script = $q->script_name();
 my $path_info = $q->path_info();
 my $path = $path_info;
 if("$varenv{metahost}"){
   $path = "$script" . "$path_info";
   $script="";
 }
 my $lang = "de";
 my $dbh = "";

 ###users global
 if($R::col_sort){
   $db->users_up("col_sort",$R::col_sort,$users_dms->{u_id});
 }
 if($R::sort_updown){
   $db->users_up("sort_updown",$R::sort_updown,$users_dms->{u_id});
 }
 if($R::cal_sort_updown){
   $db->users_up("cal_sort_updown",$R::cal_sort_updown,$users_dms->{u_id});
 }
 if($R::col_sort eq "off"){
   $db->users_up("col_sort","0",$users_dms->{u_id});
 }
 if("$R::rel_edit" =~ /_dnd/){
   $db->users_up("rel_edit","$R::rel_edit",$users_dms->{u_id});
 }
 if($R::cms_public =~ /^t$|^f$/){
   $db->users_up("cms_public","$R::cms_public",$users_dms->{u_id});
 }

 if($R::view_sort){
   $db->users_up("view_sort",$R::view_sort,$users_dms->{u_id});
 }

 ###
 my $time = time;
 my $now_time = strftime "%Y-%m-%d %H:%M", localtime;
 my $now_date = strftime "%Y-%m-%d", localtime;
 my $i_rows=0;
 my $u_rows=0;
 my $d_rows=0;

 #international buttons
 my ($key,$val,$ib_key);
 while (($key,$val) = each(%ib)) {
   $ib_key = $key if($R::rel_edit eq $val);
 }
 $ib_key = $R::rel_edit if(!$ib_key);

 my $ct_name = $q->escapeHTML("$R::ct_name");
 my $barcode = $q->escapeHTML("$R::barcode");
 my $txt11 = $q->escapeHTML("$R::txt11");
 my $teaserd_ct = $q->escapeHTML("$R::teaserd_ct");

 my $sort = $R::sort;
 $sort =~ s/,/./;
 #$sort = $R::sort4edit if($R::sort4edit eq "new$users_dms->{u_id}");
 $barcode=0 if($barcode !~ /^\d+$/);

 my ($c_idnew,$rel_idnew,$ct_last,$tt_last,$table,$tpl);
 $tpl = $db->get_tpl($R::template_id) if($R::template_id);

 #service_done (submit data via API)
 if($R::rel_edit eq "service_done"){
   my $httpReqServer = $varenv{api_server};
   my $request = {
	request => "service_done",
  	authcookie => $users_dms->{cookie},
   };
   if(looks_like_number($q->param('s_barcode'))){
     $request = { %$request, bike =>  $q->param('s_barcode') };
   }
   my $station = "";
   if(looks_like_number($q->param('s_int04'))){
     $station = 1000 + $q->param('s_int04');
     $request = { %$request, station =>  $station };
   }

   my $req = HTTP::Request->new(POST => "$httpReqServer");
   $req->content_type('application/x-www-form-urlencoded');

   my $post;
   foreach (keys (%$request)){
     $post .= "$_=$request->{$_}&";
   }
   #keep in mind, SubList Editor-button sends on submit also search-fields (s_)
   foreach(@keywords){
        my $val = $q->param($_);
	$post .= "$_=" . $q->escapeHTML($val) . "&" if($_ =~ /^txt\d+|^int\d+|^service_id/);
   }
   $req->content($post);
   $ua->agent("COPRI APIclient");
   my $res = $ua->request($req);
 
   # Check the outcome of the response
    if ($res->is_success) {
     #print $res->content;
     return $res->content;#JSON in $return
     #print $res->status_line, "\n";
    #}else {
     #print $res->status_line, "\n";
    }
 }#end service_done

 #new content
 if($ib_key =~ /new_user|new_pers|new_nel|new_time|new_adr|new_content/ && $R::main_id && $R::template_id){
   my $foreign_key = "";
   $ct_name = $q->escapeHTML("$R::s_ct_name") if($R::s_ct_name);#new over search
   $barcode = $q->escapeHTML("$R::s_barcode") if($R::s_barcode =~ /^\d+$/);#new over search
   $txt11 = $q->escapeHTML("$R::s_txt11") if($R::s_txt11);#new over search
   $ct_name = "$ct_name" || $barcode || "";

   if($ib_key eq "new_user"){
     $table = "contentuser";
     $foreign_key = "cu_id";
     $ct_last = $db->get_content2($table,$ct_name,$users_dms->{u_id});
   }elsif($ib_key eq "new_nel"){
     $table = "contentnel";
     $foreign_key = "cn_id";
     $ct_last = $db->get_content1($table,$R::c_id,$users_dms->{u_id});
   }elsif($ib_key eq "new_time"){
     $table = "timetable";
     $foreign_key = "ct_id";
   }elsif($ib_key eq "new_adr"){
     $table = "contentadr";
     $foreign_key = "ca_id";
     #if($ct_name && $ct_name ne "new$users_dms->{u_id}"){
     if($ct_name){
       $ct_last = $db->get_content2($table,$ct_name);
     #}else{
     # $ct_last = $db->get_content2multi($table,"","","","sort","new$users_dms->{u_id}");
     # if($ct_last->{c_id}){
	#$d_rows += $db->delete_content("$table","","","new$users_dms->{u_id}");
        #$ct_last->{c_id} = "";
       #}
     }
   }elsif($ib_key =~ /new_content/){
     $table = "content";
     $foreign_key = "cc_id";
     #new Nr. check
     $ct_last = $db->get_content4new($table,"$ct_name","$ct_name","$ct_name") if(!$ct_last->{c_id} && $ct_name =~ /^\d+$/);
     $ct_last = $db->get_content4new($table,"$barcode","$barcode","$barcode") if(!$ct_last->{c_id} && $barcode);
     $ct_last = $db->get_content4new($table,"$txt11","$txt11","$txt11") if(!$ct_last->{c_id} && $txt11);

     #if($ct_last->{c_id} && ("$ct_last->{sort}" eq "new$users_dms->{u_id}")){
	#$d_rows += $db->delete_content("$table","$ct_last->{c_id}");
	#$ct_last->{c_id} = "";
     #}
     my $ct_sort = $db->get_content3sort($table,$R::main_id);
     $sort = $ct_sort->{sort} + 1;# if(!$R::sort4edit);
   }
 
   #if(("$ct_last->{ct_name}" eq "$ct_name") && ("$ct_last->{ct_name}" ne "new$users_dms->{u_id}")){
   #  $ct_name = "new$users_dms->{u_id}";
   #  $ct_last->{c_id} = "";
   #}
   #print "$ib_key, $ct_last->{c_id}, $ct_name, && ($ct_last->{ct_name} !~ /new\d+/)";
   #exit 0;

   if($ct_last->{c_id} && !$ct_last->{ct_name}){
     return "failure::Abbruch, der Datensatz \"$ct_last->{ct_name} | $ct_last->{barcode} | $ct_last->{txt11}\" ist bereits vorhanden";
   }

   #Barcode 
   #hier sollen auch frei vom Anwedner definierte Nummern moeglich sein.
   my $leer; my $next_barcode;
   my $barcode_last;
   $barcode_last = $db->get_barcode("users","$table") if($users_dms->{$table});#hier gilt: $table=column in users
   $barcode_last->{$table} = 1000 if(!$barcode_last->{$table});
   #my $freenr = $lb->get_freenr($table,$barcode_last->{$table});
   my $lastnr = $db->get_content2($table);#get just last c_id
   my $freenr = $lastnr->{c_id};
   $freenr++;

   if(!$ct_name && !$barcode && $varenv{barcode}){
    ($ct_name,$next_barcode) = $lb->barcodeable($table,$freenr);#new nr routine
   }elsif(($ct_name =~ /^\d+$/) && !$barcode && $varenv{barcode}){
    ($leer,$next_barcode) = $lb->barcodeable($table,$freenr);
   }

   if($barcode > "999999"){
    	return "failure::Abbruch, die interne Barcode-Nummer muss kleiner als \"1000000\" sein.<br />Hier bitte keine EAN's verwenden";
   }

   #if($next_barcode =~ /^\d+$/ && $freenr eq $next_barcode){
   if($freenr > 0){
	#$barcode = $next_barcode;
	$barcode = $next_barcode || $freenr;
        $db->users_up("$table","$barcode","$users_dms->{u_id}");
   }

   my ($rel4e,$ctrel);
     $c_idnew = $db->insert_content($table,$ct_name,$users_dms->{u_id},$sort);
     $db->updater("$table","c_id",$c_idnew,"txt01","$now_date") if($varenv{dataflow} =~ /wiki/);
     $i_rows += 1 if($c_idnew > 0);
     $db->update_barcode($table,$c_idnew,$ct_name,$barcode);
     $rel4e = $db->get_rel4empty($R::main_id,$lang,$c_idnew,$R::template_id);
     $rel_idnew = $db->insert_relationlist($table,$R::main_id,$lang,$c_idnew,$R::template_id,$foreign_key);
     if((! -d "$varenv{data}/$R::main_id-resize/$c_idnew") && ($varenv{orga} eq "dms")){
       mkdir("$varenv{data}/$R::main_id/$c_idnew",0777);
       mkdir("$varenv{data}/$R::main_id-thumb/$c_idnew",0777);
       mkdir("$varenv{data}/$R::main_id-resize/$c_idnew",0777);
     }

   #nice alco for special table-columns
    my $tinfo = $db->table_info("relation");
    foreach my $rid (%$tinfo){
        if($tinfo->{$rid}->{attname} eq "template_right_id"){
          $db->updater("relation","main_id",$R::main_id,"template_right_id",$rel4e->{template_right_id});
        }
    }
   $db->users_up("rel_id4edit","0",$users_dms->{u_id});
   if(!$R::search_pattern){
     print redirect("$varenv{wwwhost}$script$path?node2edit=editpart\&rel_id=$rel_idnew\&return=$i_rows-$u_rows-$d_rows");
     exit 0;
   }
 }
 ###


 #save content
 #if($ib_key =~ /save/ && $R::c_id && $R::rel_id){
 my $c_id = $R::c_id || $c_idnew;#also over new
 my $rel_id = $R::rel_id || $rel_idnew;#also over new
 if(($ib_key =~ /save_content|save_user|save_adr/) || ($ib_key =~ /new_content/ && $R::search_pattern) && $c_id){

   if($ib_key eq "save_user"){
     $table = "contentuser";
   }elsif($ib_key eq "save_nel" || $ib_key eq "newsletter_mailman"){
     $table = "contentnel";
   }elsif($ib_key eq "save_adr"){
     $table = "contentadr";
   }elsif($ib_key =~ /save_content|new_content/){
     $table = "content";
   }

   my $freenr;
   if((!$ct_name && !$barcode) && ("$table" eq "contentadr" || "$table" eq "content")){
     my $barcode_last;

     $barcode_last = $db->get_barcode("users","$table") if($users_dms->{$table});#hier gilt: $table=column in users
     $barcode_last->{$table} = 1000 if(!$barcode_last->{$table});
     #$freenr = $lb->get_freenr($table,$barcode_last->{$table});
     my $lastnr = $db->get_content2($table);#get just last c_id
     $freenr = $lastnr->{c_id};
     $freenr++;

     $db->users_up("$table","$freenr","$users_dms->{u_id}") if($freenr);
     $ct_name = $freenr;
   }

   my $ct_exist1 = $db->get_content1($table,$c_id);
   my $IBAN_last = $ct_exist1->{txt22} || "";
   my $BIC_last = $ct_exist1->{txt23} || "";
   my $ct_exist2 = $db->get_content5($table,$ct_name,$c_id,"","not");
   if($table ne "contentadr" && ($ct_name && $ct_exist1->{c_id} && $ct_exist2->{c_id}) && ($ct_exist1->{c_id} ne $ct_exist2->{c_id}) && (("$ct_name" eq "$ct_exist2->{ct_name}") || ("$ct_name" eq "$ct_exist2->{barcode}"))){
	return "failure:: Konflikt, der Inhalt \"$ct_name ($ct_exist1->{c_id} ne $ct_exist2->{c_id})\" ist bereits vorhanden. Das speichern wurde verweigert";
   }
   #Mandatsreferenz / IBAN Konflikt
   if(($tpl->{tpl_order} =~ /Mandatsreferenz|Referenz/) && $ct_exist2->{txt22} && ($ct_name eq $ct_exist2->{ct_name}) && ($ct_exist1->{c_id} ne $ct_exist2->{c_id}) && ("$R::txt22" ne "$ct_exist2->{txt22}") && ("$table" eq "contentadr")){
	return "failure:: Konflikt, die Mandatsreferenz \"$ct_name\" existiert bereits mit der IBAN \"$ct_exist2->{txt22}\" (Code: $ct_exist1->{c_id} ne $ct_exist2->{c_id} && $R::txt22 ne $ct_exist2->{txt22}). Das speichern wurde verweigert";
   }

   #eMail txt08
   if($tpl->{tpl_id} =~ /202/ && $R::txt08){
     $ct_exist2 = $db->get_content7($table,"txt08",$R::txt08,"c_id","!=",$c_id);
     if($ct_exist2->{c_id} && ($c_id != $ct_exist2->{c_id}) && ("$R::txt08" eq "$ct_exist2->{txt08}")){
	return "failure:: Konflikt, die eMail Adresse \"$R::txt08\" existiert bereits (Code: $c_id != $ct_exist2->{c_id}) && $R::txt08 eq $ct_exist2->{txt08}). Das speichern wurde verweigert";
     }
    }

   #hidden select keys to delete first, marked with off_ (Project.pm)
   foreach(@keywords){
     if($_ =~ /off_(int\d+)/){
       $db->updater("$table","c_id",$c_id,"$1","null");
     }
   }


   my $img02;
   my $img03;
   my $img04;
   foreach(@keywords){
     my $val = $q->param($_);
     my $valxx = $q->escapeHTML("$val");
     #print "$_:$valxx<br>";
     $valxx =~ s/^\s+//; $valxx =~ s/\s+$//;
     if($_ =~ /^int|barcode/){
       $valxx =~ s/,/./g;
       $valxx = "null" if(!looks_like_number($valxx));#empty
     }

     if($_ =~ /^txt\d+|^int\d+|^time\d+|byte\d+/){
	if(1==1){
	   #on IBAN/BIC change set override Mandantsreferenz to c_id to trigger payone 
	   if($table eq "contentadr" && (($_ eq "txt22" && $valxx ne "null" && $valxx ne $ct_exist1->{txt22}) || ($_ eq "txt23" && $valxx ne "null" && $valxx ne $ct_exist1->{txt23}))){
            $u_rows += $db->updater("$table","c_id",$c_id,"ct_name","$c_id",$users_dms->{u_id});
	   }
	   if($table eq "contentadr" && $_ eq "txt22" && $valxx){
            my $currency = "EUR"; 
	    #$currency = "CHF" if($valxx =~ /^(CH)/i);
            $u_rows += $db->updater("$table","c_id",$c_id,"txt22","$valxx",$users_dms->{u_id});
            $u_rows += $db->updater("$table","c_id",$c_id,"txt24","$currency",$users_dms->{u_id});
	   }
	   elsif($table eq "contentadr" && $_ eq "txt15"){
	    $valxx = "" if($valxx eq "null");
            my $swk_code="";
            $swk_code = $lb->grep_filecontent("$varenv{basedir}/$varenv{ftp_getfile}","$valxx") if($valxx =~ /\d+/);
            if(looks_like_number($swk_code) && "$swk_code" eq "$valxx"){
              my $ct = $db->get_content6("contentadr","txt15","$swk_code","","","","","","","");
              if($ct->{c_id} && $ct->{c_id} != $c_id){
          	return "failure::Abbruch, die SWK Bonusnummer ist bereits vergeben.";
              }else{
                $u_rows = $db->updater("$table","c_id",$c_id,$_,$swk_code);
                my $konrad_tarif = 3430;#swk konrad tarif
                $u_rows = $db->updater("$table","c_id",$c_id,"int15","$konrad_tarif") if($ct_exist1->{int15});
                my $tink_tarif = 3432;#swk TINK tarif
                $u_rows = $db->updater("$table","c_id",$c_id,"int14","$tink_tarif") if($ct_exist1->{int14});
              }
            }elsif($valxx){
              $u_rows = $db->updater("$table","c_id",$c_id,$_,"");
                return "failure::Abbruch, die SWK Bonusnummer ($valxx) existiert nicht.";
            }else{
              $u_rows = $db->updater("$table","c_id",$c_id,$_,"");
            }
           }
	   #txt23 = Service Farbcode multiple select 
	   elsif($tpl->{tpl_id} == 205 && $_ eq "txt23"){
  	     my @val = $q->param($_);
      	     $valxx = $q->escapeHTML("@val");
             $u_rows += $db->updater("$table","c_id",$c_id,$_,$valxx,$users_dms->{u_id});
	   }
	   #txt07 station user_tour multiple select
	   elsif($tpl->{tpl_id} == 225 && $_ eq "txt07"){
  	     my @val = $q->param($_);
             #@txt18 = grep {!/null/} @txt18;
      	     $valxx = $q->escapeHTML("@val");
             $u_rows += $db->updater("$table","c_id",$c_id,$_,$valxx,$users_dms->{u_id});
	   }
	   #txt24 bike_node/station_group and txt25 bike_group multiple select
	   elsif($tpl->{tpl_id} == 225 && $_ eq "txt24"){
  	     my @val = $q->param($_);
	     my %station_filter_hash = ();
	     my @_valxx_filter = ();
    	     my $node = { template_id => 205,#Leihrad_liste
                 parent_id => 200013,#Waren
                 fetch  => "all",
                 keyfield => "main_id",
                 };
	     my $bike_nodes = {};
    	     $bike_nodes = $dbt->fetch_rel4tpl4nd($dbh,$node);

	     foreach my $bike_node (@val){
		foreach my $rid ( keys (%$bike_nodes)){
		  if($bike_node == $bike_nodes->{$rid}->{main_id} && $bike_nodes->{$rid}->{type_id}){
		    $station_filter_hash{$bike_nodes->{$rid}->{type_id}} = 1;
		  }
		}
	     }
	          
	     foreach my $type_id (keys (%station_filter_hash)){
          	push (@_valxx_filter, "$type_id");
             }

             $db->updater("$table","c_id",$c_id,"txt25","@_valxx_filter",$users_dms->{u_id});
      	     $valxx = $q->escapeHTML("@val");
             $u_rows += $db->updater("$table","c_id",$c_id,$_,$valxx,$users_dms->{u_id});
	   }
	   elsif($table eq "content" && $_ eq "txt06"){#GPS
	     if($valxx =~ /^(\d{1,2}\.\d+),\s?(\d{1,2}\.\d+)$/ || !$valxx || $valxx eq "null"){
        	$u_rows += $db->updater("$table","c_id",$c_id,$_,$valxx,$users_dms->{u_id});
	     }
	   }
	   elsif($table eq "content" && $_ eq "txt10"){#Verleih content Status
	     if($valxx){
        	$u_rows += $db->updater("$table","c_id",$c_id,$_,$valxx,$users_dms->{u_id});
	     }
	   }
	   elsif($table eq "content" && $_ =~ /byte/){
        	$u_rows += $db->updater("$table","c_id",$c_id,$_,"\\x$valxx",$users_dms->{u_id});
	   }
	   elsif($_ =~ /time\d+/){
     		return "failure::Formatfehler, Eingabe der Zeiteinheit bitte in Stunden:Minuten (hh:mm)." if($valxx !~ /\d{1,2}:\d{2}/);
        	$u_rows += $db->updater("$table","c_id",$c_id,$_,"$valxx",$users_dms->{u_id});
	   }
	   else{
 		$u_rows += $db->updater("$table","c_id",$c_id,$_,$valxx,$users_dms->{u_id});
	   }
	}
	else{
         $u_rows += $db->updater("$table","c_id",$c_id,$_,$valxx,$users_dms->{u_id});
	}
     }

     if($_ =~ /^img[\d+]/ && $table eq "contentuser"){
      #other img saved over save_media and imageresize!
      $u_rows += $db->updater("$table","c_id",$c_id,$_,$valxx,$users_dms->{u_id});
     }

     if($_ eq "sort"){
	$valxx = 0 if($valxx =~ /new/);
        $u_rows += $db->updater("$table","c_id",$c_id,$_,$valxx,$users_dms->{u_id});
     }
     if($_ =~ /date_time[\d+]/){
	my ($date_time,$chck) = $lb->checkdate($valxx);
        $u_rows += $db->updater("$table","c_id",$c_id,$_,$date_time,$users_dms->{u_id}) if(!$chck);
     }
     if($_ =~ /^ct_name|barcode/){
        $u_rows += $db->updater("$table","c_id",$c_id,$_,$valxx,$users_dms->{u_id});
        if($table eq "contentadr" && $varenv{syshost} eq "tinkdms" && $ct_exist1->{int03}){
	  my $nothing;#never change mandat/pseudocardpan
	}elsif($freenr && $freenr ne $valxx){
          $u_rows += $db->updater("$table","c_id",$c_id,$_,$freenr,$users_dms->{u_id});
	  $u_rows += $db->update_barcode($table,$c_id,$freenr,$freenr) if($table eq "contentadr");
	}
     }
     if($_ eq "barcode" && $varenv{barcode}){
        ($valxx,$barcode) = $lb->barcodeable($table,$valxx);
        $barcode = "null" if(!$barcode);#for empty
        $u_rows += $db->update_barcode($table,$c_id,$valxx,$barcode);
       my $price = $R::int02;
       $price =~ s/\./,/;
       $price = $price . ",00" if($price !~ /,\d/);
     }
   }#end foreach(@keywords)

    my $ct_info = $db->table_info("$table");
    foreach my $nid (%$ct_info){
	if(($ct_info->{$nid}->{attname} eq "content_public")){
	  my $ct_public = $R::content_public || 0;
          $u_rows += $db->updater("$table","c_id",$c_id,"content_public",$ct_public);
        }
    }

   #falls keine Teilenummer alias ct_name, s.o.
   $u_rows += $db->updater("$table","c_id",$c_id,"ct_name",$barcode,$users_dms->{u_id}) if(!$ct_name && $barcode);


   #Bild xx Auswahl|Select 
   if($ib_key =~ /save_content_/){
     #print redirect("$varenv{wwwhost}$script$path?$ib_key=1\&rel_id=$R::rel_id\&xpos=$R::xpos\&ypos=$R::ypos\&return=$i_rows-$u_rows-$d_rows");
   }elsif($varenv{orga} eq "dms" && $R::search_pattern){
     print redirect("$varenv{wwwhost}$script$path?node2edit=editpart\&rel_id=$rel_id\&return=$i_rows-$u_rows-$d_rows");
     exit 0;
   }

   if(1==1){
      if($ib_key =~ /save_adr/ && $c_id){
        $ct_exist1 = $db->get_content1($table,$c_id);
   	$db->updater("$table","c_id",$c_id,"int01","1",$users_dms->{u_id}) if($varenv{PI});#sync reset

	#2021-11-26 disabled because only users have to request mandate 
	#if($ct_exist1->{int03} && $ct_exist1->{int03} == 1 && ($IBAN_last ne "$R::txt22" || $BIC_last ne "$R::txt23")){
	#  use Mod::Payment;
	#  my $pay = new Payment;
	#  $pay->managemandate_main(\%varenv,$ct_exist1,"");
	#}

	if((!$ct_exist1->{int14} && !$ct_exist1->{int15}) || !$ct_exist1->{int03} || !$ct_exist1->{int04} || $ct_exist1->{int12}){
    	   my $deaktivated = "null";
           $deaktivated = 2 if($ct_exist1->{owner} >= 1000 && $ct_exist1->{int12} >= 1);
	   $db->updater("contentadr","c_id","$c_id","int12","$deaktivated");
	}else{
           $db->updater("contentadr","c_id","$c_id","int12","null");
	}

      }elsif($ib_key =~ /save_content/){
	      #require "Mod/KMLout.pm";
	      #my $kmlfile = Mod::KMLout::kmlGenerator("","");

	#copy sharee document for web CMS
	if($tpl->{tpl_id} =~ /227/ && $R::txt02 && $R::txt03){
	   copy("$varenv{data}/$R::main_id/$c_id/$R::txt02","$varenv{basedir}/site/$R::txt03");
	}
      }
   }

 }#end save_
 ###

 #new_relation
 if("$ib_key" =~ /new_relation/ && $R::parent_id && $R::main_id){
   my $prefix_id = "0";
   my $working_parent_id = $R::parent_id;
   $prefix_id = $1 if($R::main_id =~ /^(\d)/ && $R::main_id >= "100000");
   my $node_name = $R::node_name || "new$users_dms->{u_id}";
   $node_name = $q->escapeHTML("$node_name");
   my $return;
   $return = $lb->checkinput($node_name);
   return $return if($return =~ /failure/);

   #check multiple node_name
   my $check_name;
   $check_name = $db->get_node3($R::parent_id,$node_name,$lang);
   $check_name = $db->get_node3($R::main_id,$node_name,$lang) if(!$check_name->{node_name});

   if($check_name->{node_name} eq "$node_name"){
     return "failure::Abbruch, der Menuename \"$check_name->{node_name}\" ist bereits vorhanden. Bitte eindeutige Menuenamen verwenden.";
   }

   if("$ib_key" =~ /new_relation4sub/ || $R::new_submenu){
     $working_parent_id = $R::main_id;
     $prefix_id++;
   }

   my $tpl_id = $R::template_id; 
   my $new_main_id = $db->get_freenode($prefix_id);
   my $rc = $db->insert_node($working_parent_id,$new_main_id,$node_name,$lang,$R::n_sort,$tpl_id,$R::c_id,$users_dms->{u_id});

   $i_rows += 1 if($rc > 0);
   $u_rows += $db->updater("nodes","main_id",$new_main_id,"node_public","$R::node_public") if($R::node_public);
   $u_rows += $db->updater("nodes","main_id",$new_main_id,"footer","$R::footer") if($R::footer);
   $db->updater("relation","main_id",$new_main_id,"template_id","$tpl_id") if($tpl_id);

   if($varenv{orga} eq "dms"){
     mkdir("$varenv{data}/$new_main_id",0777);
     mkdir("$varenv{data}/$new_main_id-thumb",0777);
     mkdir("$varenv{data}/$new_main_id-resize",0777);
   }
   #nice alco for special table-columns
   my $tinfo = $db->table_info("relation");
   foreach my $rid (%$tinfo){
        $u_rows += $db->updater("relation","main_id",$R::main_id,"template_right_id",$R::template_right_id) if($tinfo->{$rid}->{attname} eq "template_right_id");
   }

   my $new_path;
   if("$ib_key" =~ /new_relation4sub/ || $R::new_submenu){
     $path =~ s/\/maintainer//;
     $new_path = "$path";
     #$new_path = "$path/$node_name";
   }else{
     my @pathseq = split(/\//,$path);
     pop  @pathseq if($path =~ /\/maintainer/);
     pop  @pathseq;
     foreach(@pathseq){
	$new_path .= "/$_" if($_ =~ /\w/);
	print "$new_path<br>";
     }
     $new_path .= "/$node_name";
   }
   $new_path =~ s/\/\//\//g;
   print "$new_path<br>";
   print redirect("$varenv{wwwhost}$script$new_path?node2edit=edit_relation\&main_id=$new_main_id\&tpl_id=$tpl_id\&return=$i_rows-$u_rows-$d_rows");
   exit 0;
 }

 #save_relation
 #TODO save bike relation with service contentpos tree
 if("$ib_key" eq "save_relation" && $R::main_id){
   my $working_parent_id = $R::parent_id;
   my $prefix_id = "0";
   $prefix_id = $1 if($R::main_id =~ /^(\d)/ && $R::main_id >= "100000");

   foreach(@keywords){
    my $val = $q->param($_);
    my $valxx = $q->escapeHTML("$val");
    #$_ =~ s/col_//;
    $valxx =~ s/^\s+//; $valxx =~ s/\s+$//;
    if(($_ eq "node_name") && $valxx){
 	#for sub-menue hack
	my $node_name = $valxx;
	my $node_path = $node_name;
 	#for splitting node_name?node_path
 	($node_name,$node_path) = split(/\|/,$node_name) if($node_name =~ /\|/);
	my $return;
   	$return = $lb->checkinput($node_name);
   	$return = $lb->checkinput($node_path);
	return $return if($return =~ /failure/);

        $u_rows += $db->updater("nodes","main_id",$R::main_id,"node_name",$node_name,"","parent_id");
        $u_rows += $db->updater("nodes","main_id",$R::main_id,"node_path",$node_path,"","parent_id");
    }
    if($_ =~ /(template_id)/ && $valxx){
 	my $rc = $db->update_relation3($R::main_id,$lang,$R::main_id,$valxx);

    }

    if($_ =~ /int|n_sort|owner/){
        $valxx =~ s/,/./;
        $valxx = "null" if(!$valxx && $valxx ne "0");#for empty
        $valxx = "0" if($valxx eq "0");
        #print "xxxxxxx $_ : $valxx |";
        $u_rows += $db->updater("nodes","main_id",$R::main_id,$_,$valxx) if($valxx =~ /^\d+$|null|0/);
    }
    if($_ =~ /txt01/){
        $u_rows += $db->updater("nodes","main_id",$R::main_id,$_,$valxx);
    }
    if((! -d "$varenv{data}/$R::main_id") && ($varenv{orga} eq "dms")){
     mkdir("$varenv{data}/$R::main_id",0777);
     mkdir("$varenv{data}/$R::main_id-thumb",0777);
     mkdir("$varenv{data}/$R::main_id-resize",0777);
    }
   }

   my $node_info = $db->table_info("nodes");
    foreach my $nid (%$node_info){
	if(($node_info->{$nid}->{attname} eq "node_public")){
	  my $node_public = $R::node_public || 0;
          $u_rows += $db->updater("nodes","main_id",$R::main_id,"node_public",$node_public);
        }
	if(($node_info->{$nid}->{attname} eq "footer")){
	  my $footer = $R::footer || 0;
          $u_rows += $db->updater("nodes","main_id",$R::main_id,"footer",$footer);
        }
   } 

   my $uri_path = $dbt->recurse_node($dbh,$R::main_id);
   print "$varenv{wwwhost}/$uri_path?return=$i_rows-$u_rows-$d_rows\n";
   print redirect("$varenv{wwwhost}/$uri_path?return=$i_rows-$u_rows-$d_rows");
   exit 0;
 }

 #relate or move content, multiple node-content relation
 #init from RelationEdit.pm (for _dnd look at Libenz.pm)
 if("$R::rel_edit" =~ /relate_content|move_content/ && $R::set_main_id && ($R::rel_id || ($R::main_id && $R::set_content_id && $R::template_id))){
   my $table = "content";
   my $foreign_key = "cc_id";
   if($ib_key =~ /relate_contentadr|move_contentadr/){
     $table = "contentadr";
     $foreign_key = "ca_id";
   }
   if($ib_key =~ /relate_contenttrans|move_contenttrans/){
     $table = "contenttrans";
     $foreign_key = "ct_id";
   }
   if($ib_key =~ /relate_contenttver|move_contenttver/){
     $table = "contenttver";
     $foreign_key = "cv_id";
   }
   if($ib_key =~ /relate_contentnel|move_contentnel/){
     $table = "contentnel";
     $foreign_key = "cn_id";
   }

   my $ctrel;
   if($R::rel_id){
     $ctrel = $db->get_ctrel($table,"",$lang,$R::rel_id);
     $R::main_id = $ctrel->{main_id};
     $R::set_content_id = $ctrel->{content_id};
     $R::template_id = $ctrel->{template_id};
   }

   my $return_rel_id=0;
   my $ctrel_ck;
   $ctrel_ck = $db->get_ctrel($table,$R::set_main_id,$lang,"",$R::set_content_id,$R::template_id);
 
    if($varenv{orga} eq "dms" && $varenv{wwwhost} =~ /wogedms|fsdm/){
      my @set_main_ids = ();
      my $set_main_ids = "";
      foreach(@keywords){
	if($_ =~ /set_main_id/){
   		@set_main_ids = $q->param($_);
 	}
      }
      $set_main_ids = join(',',@set_main_ids);

      my $ct4rel_ck = $db->collect_ct4rel("$table","","$lang","","","","","$R::template_id","","","$R::set_content_id","main_id");
      my $ctrel;
      foreach(@set_main_ids){
        if($_ != $ct4rel_ck->{$_}->{main_id}){
	  #print "insert: $table,$_,$lang,$R::set_content_id,$R::template_id,$foreign_key<br>";
	  $return_rel_id = $db->insert_relationlist($table,$_,$lang,$R::set_content_id,$R::template_id,$foreign_key) if($R::rel_edit =~ /relate_/ && $_);
	  $ctrel_ck = $db->get_ctrel($table,$_,$lang,"",$R::set_content_id,$R::template_id);

       	  if($ctrel_ck->{int02} == 1 && $return_rel_id && $R::template_id eq "202"){
         	`sudo $varenv{basedir}/src/scripts/mailman_member.pl add $return_rel_id $varenv{syshost}`;
       	  }
	  if( -d "$varenv{data}/$R::main_id/$R::set_content_id" ){
	    #print "$varenv{data}/$R::main_id/$R::set_content_id --> $varenv{data}/$_/$R::set_content_id";
            rcopy("$varenv{data}/$R::main_id/$R::set_content_id","$varenv{data}/$_/$R::set_content_id");
            rcopy("$varenv{data}/$R::main_id-thumb/$R::set_content_id","$varenv{data}/$_-thumb/$R::set_content_id");
            rcopy("$varenv{data}/$R::main_id-resize/$R::set_content_id","$varenv{data}/$_-resize/$R::set_content_id");
	  }
	}
      }
      foreach my $id (keys(%$ct4rel_ck)){
        if($ct4rel_ck->{$id}->{main_id} && $set_main_ids !~ /$ct4rel_ck->{$id}->{main_id}/){
	  #print "delete: $ct4rel_ck->{$id}->{main_id},$lang,$ct4rel_ck->{$id}->{rel_id}<br>";
       	  if($R::template_id eq "202"){
         	`sudo $varenv{basedir}/src/scripts/mailman_member.pl remove $ct4rel_ck->{$id}->{rel_id} $varenv{syshost}`;
       	  }
	  if($R::rel_edit =~ /relate_/ && $ct4rel_ck->{$id}->{c_id}){
    	    $db->delete_relation($ct4rel_ck->{$id}->{main_id},$lang,$ct4rel_ck->{$id}->{rel_id});
	    if( -d "$varenv{data}/$ct4rel_ck->{$id}->{main_id}/$ct4rel_ck->{$id}->{c_id}" ){
            	remove_tree("$varenv{data}/$ct4rel_ck->{$id}->{main_id}/$ct4rel_ck->{$id}->{c_id}");
            	remove_tree("$varenv{data}/$ct4rel_ck->{$id}->{main_id}-thumb/$ct4rel_ck->{$id}->{c_id}");
            	remove_tree("$varenv{data}/$ct4rel_ck->{$id}->{main_id}-resize/$ct4rel_ck->{$id}->{c_id}");
	    }
	  }
 	}
     }
     if($users_dms->{mandant_id} && $users_dms->{mandant_id} < "200000"){
	my $uri_path = $dbt->recurse_node($dbh,$R::set_main_id);
	print redirect("$varenv{wwwhost}/$uri_path?exit_box2=1\&rel_id=$return_rel_id\&return=$i_rows-$u_rows-$d_rows");
     	exit 0;
     }

   }elsif($varenv{orga} eq "dms"){
     my @ck_ids = $R::rel_id;
     @ck_ids = split(/\s/,$R::ck_ids) if($R::ck_ids);
     foreach(@ck_ids){
      #1.get ctrel
      my $ctrel = $db->get_ctrel("$table","","$lang","$_","","","");
      #2.1. update relation for move
      if($R::rel_edit =~ /move_/){
        $return_rel_id = $db->update_relation2("",$lang,$R::set_main_id,$R::set_template_id,$ctrel->{rel_id});
        #3.move medias
        if($return_rel_id){
         move("$varenv{data}/$R::main_id/$ctrel->{c_id}","$varenv{data}/$R::set_main_id/$ctrel->{c_id}");
         move("$varenv{data}/$R::main_id-thumb/$ctrel->{c_id}","$varenv{data}/$R::set_main_id-thumb/$ctrel->{c_id}");
         move("$varenv{data}/$R::main_id-resize/$ctrel->{c_id}","$varenv{data}/$R::set_main_id-resize/$ctrel->{c_id}");
        }
      }
      #2.2. insert relation for linking
      if($R::rel_edit =~ /relate_/){
       my $mastermain_id = $R::main_id;
       $return_rel_id = $db->insert_relationlist($table,$R::set_main_id,$lang,$R::set_content_id,$R::template_id,$foreign_key,$mastermain_id) if($R::rel_edit =~ /relate_/);
      }
     }
    }elsif($ctrel_ck->{rel_id} > 1){
     return "failure::Abbruch, die Content-Node Relation existiert bereits";
    }else{
      my $mastermain_id = $R::mastermain_id || $R::main_id;
      $return_rel_id = $db->insert_relationlist($table,$R::set_main_id,$lang,$R::set_content_id,$R::template_id,$foreign_key,$mastermain_id);
    }
    $i_rows += 1 if($return_rel_id > 0); 

    if($users_dms->{mandant_id} && $users_dms->{mandant_id} < "200000"){
     my $uri_path = $dbt->recurse_node($dbh,$R::set_main_id);
     print redirect("$varenv{wwwhost}/$uri_path?return=$i_rows-$u_rows-$d_rows");
     exit 0;
    }
 }

 ###copy (released at jbw)
 if($ib_key =~ /copy_tver|copy_nel|^copy_content|copy_adr/){
  if($R::main_id && $lang && $R::rel_id && $R::c_id && $R::template_id){
   my ($table,$foreign_key,$ctrel,$tpl);
   my @split_lates;
   if($ib_key eq "copy_content"){
    $table = "content";
    $foreign_key = "cc_id";
    $ctrel = $db->get_ctrel($table,$R::main_id,$lang,$R::rel_id,$R::c_id,$R::template_id);
    $tpl = $db->get_tpl($ctrel->{template_id});
    @split_lates = ($tpl->{tpl_order}); 
   }elsif($ib_key eq "copy_adr"){
    $table = "contentadr";
    $foreign_key = "ca_id";
    $ctrel = $db->get_ctrel($table,$R::main_id,$lang,$R::rel_id,$R::c_id,$R::template_id);
    my $edit_template = "$ctrel->{template_id}" . "000";
    $tpl = $db->get_tpl($edit_template);
    @split_lates = ($tpl->{tpl_order});
   }elsif($ib_key eq "copy_nel"){
    $table = "contentnel";
    $foreign_key = "cn_id";
    $ctrel = $db->get_ctrel($table,$R::main_id,$lang,$R::rel_id,$R::c_id,$R::template_id);
    my $edit_template01 = "$ctrel->{template_id}" . "001";
    $tpl = $db->get_tpl($edit_template01);
    @split_lates = ($tpl->{tpl_order});
   }elsif($ib_key eq "copy_tver"){
    $table = "contenttver";
    $foreign_key = "cv_id";
    $ctrel = $db->get_ctrel($table,$R::main_id,$lang,$R::rel_id,$R::c_id,$R::template_id);
    my $edit_template01 = "$ctrel->{template_id}" . "001";
    my $edit_template02 = "$ctrel->{template_id}" . "002";
    my $edit_template03 = "$ctrel->{template_id}" . "003";
    my $tpl01 = $db->get_tpl($edit_template01);
    my $tpl02 = $db->get_tpl($edit_template02);
    my $tpl03 = $db->get_tpl($edit_template03);
    @split_lates = ("$tpl01->{tpl_order}","$tpl02->{tpl_order}","$tpl03->{tpl_order}");
   }

   my $c_idnew;
   $c_idnew = $db->insert_content2($table,$ct_name,$users_dms->{u_id},"");
    if($c_idnew > 0){
     $i_rows += 1;
     my $rel_id = $db->insert_relationlist($table,$R::main_id,$lang,$c_idnew,$R::template_id,$foreign_key);
     $db->update_content4change($table,$c_idnew,"",$ctrel->{barcode},"barcode") if($ctrel->{barcode});
     #$db->update_users4trans($c_idnew,$R::template_id,$R::kind_of_trans,$users_dms->{u_id}) if($ib_key eq "copy_tver");
    foreach(@split_lates){
     my @tpl_order = split /,/,$_;
     foreach (@tpl_order){
      my ($key,$val) = split /=/,$_;
      if($key eq "txt01"){
      	$db->updater("$table","c_id",$c_idnew,"$key","$ctrel->{$key} (copy)");
      }elsif($ctrel->{$key} && $key !~ /owner|mtime|itime/){
      	$u_rows += $db->updater("$table","c_id",$c_idnew,"$key","$ctrel->{$key}");
      }
     }
    }
    if($ib_key =~ /copy_tver|copy_nel/){
      print redirect("$varenv{wwwhost}$script$path?ct_trans=open\&c_id4trans=$c_idnew\&tpl_id4trans=$R::template_id\&kind_of_trans=$users_dms->{kind_of_trans}\&owner=$users_dms->{u_id}\&xpos=$R::xpos\&ypos=$R::ypos\&return=$i_rows-$u_rows-$d_rows");
      exit 0;
    }else{
      print redirect("$varenv{wwwhost}$script$path?node2edit=editpart\&rel_id=$rel_id\&xpos=$R::xpos\&ypos=$R::ypos\&return=$i_rows-$u_rows-$d_rows");
      exit 0;
    }
   }
  }else{
    return "failure::Fehler, Datensatz kann nicht angelegt werden weil folgende Informationen fehlen: ($R::main_id && $lang && $R::rel_id && $R::c_id && $R::template_id)";
  }
 }
 #
 #copy (released at jvbasel), dialogs with RelationEdit.pm
 #3. fixed, cleaned for content
 elsif($R::rel_edit eq "copy_content" && $R::main_id && $R::set_main_id){
   my $table = "content";
   my $foreign_key = "cc_id";

   my @ck_ids = $R::rel_id;
   @ck_ids = split(/\s/,$R::ck_ids) if($R::ck_ids);
   foreach(@ck_ids){
    #1.get ctrel
    my $ctrel = $db->get_ctrel($table,"",$lang,$_);
    if($ctrel->{c_id}){
     my $columns;
     my $tinfo = $db->table_info("content");
     foreach my $rid (%$tinfo){
        if($tinfo->{$rid}->{attname} =~ /ct_name|barcode|txt|int|img/){
    	  $columns .= "$tinfo->{$rid}->{attname},";
	  #print "$tinfo->{$rid}->{attname}|";
        }
     }
     $columns =~ s/,$//; 
     my $ct_name = $ctrel->{ct_name};
     my $k=0;
     ($ct_name,$k) = split(/-/,$ctrel->{ct_name});
     $k++;
     my $ct_name_new = $ct_name . "-" . $k;
     my $c_idnew = $db->copy_content($table,"c_id",$ctrel->{c_id},"$columns");
     $u_rows += $db->updater("$table","c_id",$c_idnew,"ct_name","$ct_name_new",$users_dms->{u_id});
     my $rel_id = $db->insert_relationlist($table,$R::set_main_id,$lang,$c_idnew,$ctrel->{template_id},$foreign_key);
     $i_rows += 1 if($rel_id > 0); 
     #3.copy recursive medias
     if($rel_id){
       rcopy("$varenv{data}/$R::main_id/$ctrel->{c_id}","$varenv{data}/$R::set_main_id/$c_idnew");
       rcopy("$varenv{data}/$R::main_id-thumb/$ctrel->{c_id}","$varenv{data}/$R::set_main_id-thumb/$c_idnew");
       rcopy("$varenv{data}/$R::main_id-resize/$ctrel->{c_id}","$varenv{data}/$R::set_main_id-resize/$c_idnew");
     }
   }

   $db->users_up("rel_id4edit","0",$users_dms->{u_id});
   if($users_dms->{mandant_id} && $users_dms->{mandant_id} < "200000"){
      my $uri_path = $dbt->recurse_node($dbh,$R::set_main_id);
      print redirect("$varenv{wwwhost}/$uri_path?return=$i_rows-$u_rows-$d_rows");
      exit 0;
    }
   }
  ###
 } 
 ###

 ###PDFout Generator
 if("$ib_key" =~ /PDFout|Upload/){
  $db->users_up("time4csv","$time",$users_dms->{u_id});
  $users_dms = $db->select_users($users_dms->{u_id});
  my ($sysname, $nodename, $release, $version, $machine) = uname();
  #print "($sysname, $nodename, $release, $version, $machine)";
  my $topdf = "$varenv{basedir}/src/wkhtmltopdf-i386";
  $topdf = "$varenv{basedir}/src/wkhtmltopdf-amd64" if($machine =~ /_64/);
  $topdf = "$varenv{basedir}/src/wkhtmltopdf.app/Contents/MacOS/wkhtmltopdf" if($sysname =~ /Darwin/);
 
  #my @ck_ids = $R::rel_id;
  #foreach(@keywords){
  #  @ck_ids = $q->param($_) if($_ =~ /ckid/);#because of $R::ckid_rel as array
  #}
  #my $ck4ex = join(',',@ck_ids);
  my $ck4ex = "$users_dms->{checked4dragg}"; 
  if($ck4ex && $R::template_id && $R::main_id){
   my $psize="A4";
   my $wc_line=0;
   my $node = $db->get_node4multi($R::main_id,$lang);
   system(`$topdf --page-size $psize "$varenv{wwwhost}$script/Printpreview?printer_id=$R::printer_id\&main_id=$node->{main_id}\&template_id=$R::template_id\&ck4ex=$ck4ex\&ct_name2print=$node->{node_name}\&u_id=$users_dms->{u_id}\&wc=$wc_line" $varenv{pdf}/$users_dms->{u_id}-$users_dms->{time4csv}.pdf`);

   #print "$topdf --page-size $psize \"$varenv{wwwhost}$script/Printpreview?printer_id=$R::printer_id\&main_id=$node->{main_id}\&template_id=$R::template_id\&ck4ex=$ck4ex\&ct_name2print=$node->{node_name}\&u_id=$users_dms->{u_id}\&wc=$wc_line\" $varenv{pdf}/$users_dms->{u_id}-$users_dms->{time4csv}.pdf\n";
   if( -f "$varenv{pdf}/$users_dms->{u_id}-$users_dms->{time4csv}.pdf" && $varenv{metahost}){
     print redirect(-uri=>"$varenv{metahost}/pdf/$users_dms->{u_id}-$users_dms->{time4csv}.pdf", -target=>'_blank');
     exit 0;
   }
  }
 }
 ###

 ###CSV Generator
 if("$ib_key" =~ /XLSout/ && $R::template_id){
   $db->users_up("time4csv","$time",$users_dms->{u_id});
   my $table = "content";
   my $tpl_id = $R::template_id;
   if("$ib_key" =~ /XLSout_contentadr/ && "$R::ckid_rel"){
     $table = "contentadr";
     my $rel = $db->get_relation("","$lang","$R::ckid_rel");
     $tpl_id = $rel->{template_id};
   }

   my $scol = "txt01";
   $scol = $R::scol if($R::scol);

   my @ck_ids;
   foreach(@keywords){
    @ck_ids = $q->param($_) if($_ =~ /ckid_rel/);#because of fetching array
   }
   my $ck4ex = join(',',@ck_ids);

   $ck4ex = "$users_dms->{checked4dragg}" if($ck4ex !~ /\d/); 
   if($ck4ex && $tpl_id){
    my $ct4rel = $db->collect_ct4rel4nd("$table","","de","$scol","","","","","","$ck4ex","rel_id");
    #print "$ib_key|$tpl_id $ck4ex"; exit 0;
    my $tpl = $db->get_tpl($tpl_id);
    my @tpl_order = split /,/,$tpl->{tpl_order};
    #unshift(@tpl_order, "img00=Ordner");

    $users_dms = $db->select_users($users_dms->{u_id});
    open(CSV, "> $varenv{pdf}/$users_dms->{u_id}-$users_dms->{time4csv}.csv");
     #CSV Table Header
     my $col_header="";
     foreach(@tpl_order){
      my ($key,$val) = split /=/,$_;
      if($key eq "txt01" && $table eq "contentadr"){
        $col_header .= ";Name-01;Name-02;Name-03";
      }elsif($key !~ /mtime|owner/){
        $col_header .= ";$val";
      }
     }
     $col_header =~ s/^;//g;
     print CSV "$col_header\n";

     foreach my $id (sort { lc($ct4rel->{$a}->{$scol}) cmp lc($ct4rel->{$b}->{$scol}) } keys (%$ct4rel)){
      my $line="";
      foreach(@tpl_order){
       my ($key,$val) = split /=/,$_;
       $ct4rel->{$id}->{$key} = $q->unescapeHTML("$ct4rel->{$id}->{$key}");
       $ct4rel->{$id}->{$key} =~ s/:\d{2}\..*$// if($key =~ /time/);
       if($key eq "txt01" && $table eq "contentadr"){
          my ($n01,$n02,$n03) = split(/\n/,$ct4rel->{$id}->{$key});
          $n02 =~ s/^\s//;#jbw fix
          $line .= ";$n01;$n02;$n03";
        }elsif($key eq "date_time"){
          $ct4rel->{$id}->{start_time} =~ s/\s.*// if($ct4rel->{$id}->{start_time});
          $ct4rel->{$id}->{end_time} =~ s/\s.*// if($ct4rel->{$id}->{end_time});
          $line .= ";$ct4rel->{$id}->{start_time} - $ct4rel->{$id}->{end_time}";
        }elsif($key !~ /mtime|owner/){
          $line .= ";$ct4rel->{$id}->{$key}";
        }
       }

       $line =~ s/^;//g;
       $line =~ s/\n//g;
       $line =~ s/\r//g;
       print CSV "$line\n";
    }
   close CSV;

   $db->csv2xls($users_dms->{u_id},$users_dms->{time4csv});
  }
 }
 ###

 ###remove with question
 #delete ask, if content with multiples
 if("$ib_key" =~ /remove_chk4rel/){
  if($R::main_id && $R::c_id){
   my $rel4nd = $db->collect_rel4nodes("",$R::c_id,$R::template_id);
   my $tpl = $db->get_tpl($R::template_id);
   my $i=0;
   my $delete_key = "delete_content";
   $delete_key = "delete_adr" if($tpl->{ct_table} eq "contentadr");
   $delete_key = "delete_nel" if($tpl->{ct_table} eq "contentnel");

     foreach my $rid (sort { lc($rel4nd->{$a}->{node_name}) cmp lc($rel4nd->{$b}->{node_name}) } keys (%$rel4nd)){
	$i++;
     }

     if($i == 1 && ($varenv{orga} =~ /dms/ || $varenv{wwwhost} =~ /k9/)){
	return "failure::really delete ::?rel_edit=$delete_key\&exit_box2=1\&main_id=$R::main_id\&c_id=$R::c_id\&rel_id=$R::rel_id ::delete"
     }else{
        #until now, for copri3 we need it for k9
	return "failure::Es gibt hier zwei Moeglichkeiten. Nur die Relation loeschen oder den Content komplett loeschen. ::?rel_edit=delete_rel4ct\&exit_box2=1\&main_id=$R::main_id\&rel_id=$R::rel_id ::Relation loeschen ::?rel_edit=$delete_key\&exit_box2=1\&main_id=$R::main_id\&c_id=$R::c_id\&rel_id=$R::rel_id ::Content loeschen"
     }
  }elsif($R::ckid_rel || $R::rel_id){
   #delete ask for checkbox selected content
   my @ck_ids = $R::rel_id;
   foreach(@keywords){
    @ck_ids = $q->param($_) if($_ =~ /ckid/);#because of fetching array
   }
   return "failure::Wirklich loeschen. ::?rel_edit=delete_ckcontent\&exit_box2=1\&main_id=$R::main_id\&template_id=$R::template_id\&ck_ids=@ck_ids ::loeschen"
  }
 }
 ###

 ###delete stack of content and check if more related automatic
 if($R::rel_edit =~ /delete_ckcontent/ && $R::template_id && ($R::ck_ids || $R::rel_id)){
   my $table = "content";
   my $tpl = $db->get_tpl($R::template_id);
   $table = "contentadr" if($tpl->{ct_table} eq "contentadr");
   $table = "contentnel" if($tpl->{ct_table} eq "contentnel");
  my @ck_ids = $R::rel_id;
  @ck_ids = split(/\s/,$R::ck_ids);
  foreach(@ck_ids){
    #1.get ctrel
    my $ctrel;
    $ctrel = $db->get_ctrel("$table","","$lang","$_","","","");
    #2.only! delete relation
    $d_rows += $db->delete_relation($ctrel->{main_id},$lang,$ctrel->{rel_id}) if($ctrel->{c_id}); 
    $db->cleanup_users($users_dms->{u_id}) if($users_dms->{u_id});

    #3.get ctrel_check to check if some other relations with c_id available 
    my $ctrel_check;
    $ctrel_check = $db->get_ctrel("$table","","$lang","","$ctrel->{c_id}","$ctrel->{template_id}","");
    #4.delete media and content only if no further relation-content available
    if($ctrel->{c_id} && !$ctrel_check->{c_id}){
       remove_tree("$varenv{data}/$ctrel->{main_id}/$ctrel->{c_id}");
       remove_tree("$varenv{data}/$ctrel->{main_id}-thumb/$ctrel->{c_id}");
       remove_tree("$varenv{data}/$ctrel->{main_id}-resize/$ctrel->{c_id}");
       $table = "contentnel" if($ib_key eq "delete_nel");
       #5.delete also content if no further relations with same c_id
       $d_rows += $db->delete_content("$table","$ctrel->{c_id}") if(!$ctrel_check->{rel_id});
       my $tpl = $db->get_tpl($ctrel->{template_id});
       $db->delete_users("$ctrel->{c_id}","") if($tpl->{tpl_name} =~ /Mitarbeiter/);
    }
  }
 }
 ###

 #delete Only relation ... without content
 if("$ib_key" eq "delete_rel4ct" && $R::main_id && $R::rel_id){
   if($varenv{wwwhost} =~ /wogedms|fsdm/){
        `sudo $varenv{basedir}/src/scripts/mailman_member.pl remove $R::rel_id $varenv{syshost}` if($R::rel_id);
   }
   $d_rows += $db->delete_relation($R::main_id,$lang,$R::rel_id);

   $db->cleanup_users($users_dms->{u_id}) if($users_dms->{u_id});
 }

 #delete node-relation
 if("$ib_key" eq "delete_relation" && $R::main_id){
    my $subnode = $db->get_subnode($R::main_id,$lang);
    my $ctrel;
    $ctrel = $db->get_ctrel("content",$R::main_id,$lang);
   if($subnode->{main_id} || ($ctrel->{c_id} > 0)){
        return "failure::Abbruch, der Ordner enthält Daten. Für die referentielle Integrität ist es notwendig die Ordner Inhalte (content) und/oder Relationen des Ordners zu löschen. (code: $subnode->{main_id} || $ctrel->{c_id})";
    }elsif($R::main_id > "100" && !$subnode->{main_id}){
      $d_rows += $db->delete_node($R::main_id,$lang);
     $db->cleanup_users($users_dms->{u_id}) if($users_dms->{u_id});

      remove_tree("$varenv{data}/$R::main_id");
      remove_tree("$varenv{data}/$R::main_id-thumb");
      remove_tree("$varenv{data}/$R::main_id-resize");
      my $uri_path = $dbt->recurse_node($dbh,$R::main_id);
      $uri_path =~ s/\/\w+$//;
      print redirect("$varenv{wwwhost}/$uri_path?return=$i_rows-$u_rows-$d_rows");
      exit 0;
    }else{
	return "failure::Fehler, Relation kann nicht gelöscht werden ($R::main_id && $subnode->{main_id})";
    }
 }

 #delete content
 if($ib_key =~ /delete_user|delete_adr|delete_time|delete_content|delete_nel|delete_ttrans|delete_tver/ && $R::c_id && $R::rel_id){
    if($ib_key =~ /delete_user/){
      $table = "contentuser";
      $d_rows += $db->delete_content("$table","$R::c_id");
      $db->delete_users("$users_dms->{u_id}","$users_dms->{u_id}") if($users_dms->{u_id} > "100");
    }elsif($ib_key =~ /delete_adr/){
      $table = "contentadr";
      $d_rows += $db->delete_content("$table","$R::c_id");
    }elsif($ib_key =~ /delete_time/){
      $table = "timetable";
      $d_rows += $db->delete_time("$table","$R::t_id");
    }elsif($ib_key =~ /delete_content|delete_nel/){
       if($varenv{orga} eq "dms"){
          remove_tree("$varenv{data}/$R::main_id/$R::c_id");
          remove_tree("$varenv{data}/$R::main_id-thumb/$R::c_id");
          remove_tree("$varenv{data}/$R::main_id-resize/$R::c_id");
       }
       $table = "content";
       $table = "contentnel" if($ib_key eq "delete_nel");
       $d_rows += $db->delete_content("$table","$R::c_id");
    }elsif($ib_key =~ /delete_ttrans/){
      $table = "contenttrans";
      $d_rows += $db->delete_content("$table","$R::c_id");
      $db->cleanup_users($users_dms->{u_id}) if($users_dms->{u_id});
    }elsif($ib_key =~ /delete_tver/){
      $table = "contenttver";
      $d_rows += $db->delete_content("$table","$R::c_id");
      $db->cleanup_users($users_dms->{u_id}) if($users_dms->{u_id});
    }
   $db->users_up("rel_id4edit","0",$users_dms->{u_id});
 }

 if($ib_key eq "delete_media2" && $R::main_id && $R::c_id && $R::img_name){
     $R::img_name =~ s/\:.*//;
     $R::img_name =~ s/\s/\\ /g;
     #print "$varenv{data}/$R::main_id/$R::c_id/$R::img_name*";
     unlink glob "$varenv{data}/$R::main_id/$R::c_id/$R::img_name*";
     unlink glob "$varenv{data}/$R::main_id-thumb/$R::c_id/$R::img_name*";
     unlink glob "$varenv{data}/$R::main_id-resize/$R::c_id/$R::img_name*";
     $d_rows += 1;
 }

 #delete image or pdf via Image-Pdf-browser
 if($ib_key eq "delete_media" && $R::rel_id && $R::img_name){
  my $table = "content";
  my $ctrel = $db->get_ctrel($table,"",$lang,$R::rel_id);
  my $tpl = $db->get_tpl($ctrel->{template_id});
  my @ct4tpl = split (/,/,$tpl->{tpl_order});
  #foreach(@ct4tpl){
  # my $cttest = $db->get_contentimage("$_","$R::img_name") if($_ =~ /img|pdf/);
  # if($cttest->{c_id}){
  #  print $q->div({-id=>'Inhalttxt'}, "<br />Die Datei kann nicht geloescht werden, da es mit dem Content \"$cttest->{ct_name}\" referenziert ist", $q->p({-align=>'center'}, $q->a({-class=>"linknav",-href=>'javascript:history.back()'}, "[ back ]")));
  #  exit 0;
  # }
  #}
  unlink("$varenv{img4ct}/$R::img_name");
  unlink("$varenv{img4bg}/$R::img_name");
  unlink("$varenv{img4thumb}/$R::img_name");
  unlink("$varenv{img4bgthumb}/$R::img_name");
  unlink("$varenv{pdf4ct}/$R::img_name");
  $d_rows += 1;
 }
 
return "$i_rows-$u_rows-$d_rows";
}
1;



