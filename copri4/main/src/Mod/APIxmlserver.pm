package Mod::APIxmlserver;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
#Server for sharee xml api
#
##In DB context $q->escapeHTML must always done by API
#
#use lib qw(/var/www/copri-bike/shareeapp-lv/src);
use warnings;
use strict;
use Exporter;
our @ISA = qw (Exporter);

use POSIX;
use CGI;
use Apache2::Const -compile => qw(OK );
use Scalar::Util qw(looks_like_number);
use LWP::UserAgent;
use XML::Simple qw(:strict);

use Lib::Config;
use Mod::DBtank;
use Mod::Basework;
use Mod::Shareework;
use Mod::APIfunc;
use Digest::MD5 qw(md5 md5_hex);
use Data::Dumper;
use Sys::Hostname;
my $hostname = hostname;

sub handler {
 my ($r) = @_;
 my $q = new CGI;
 my $netloc = $q->url(-base=>1);
 #$q->import_names('R');
 my $cf = new Config;
 my $dbt = new DBtank;
 my $bw = new Basework;
 my $tk = new Shareework;
 my $apif = new APIfunc;

 my %varenv = $cf->envonline();
 my $oprefix = $dbt->{operator}->{$varenv{dbname}}->{oprefix};
 my $now_dt = strftime "%Y-%m-%d %H:%M:%S", localtime;
 my $lang="de";
 my $owner=199;#LastenVelo api (LV api)
 my @keywords = $q->param;
 my $debug=1;
 my $user_agent = $q->user_agent();
 my $dbh = "";

 if(1==1){
  foreach(@keywords){
   if(length($_) > 20 || length($q->param($_)) > 400){
     print "<text>Failure 19900: amount of characters in $_ exceeds</text>";
     return Apache2::Const::OK;
     exit 0;
   }
  }
 }


 $bw->log("APIxmlserver request:\n--> user-agent '$user_agent'",$q,"");

 print $q->header(-type => "application/xml", -charset => "utf-8", -'Access-Control-Allow-Origin' => "*");

 open(FILE,">>$varenv{logdir}/APIxmlserver.log") if($debug);
 print FILE "\n*** $now_dt user-agent: '$user_agent' to syshost: $varenv{syshost}\n" if($debug);
 print FILE "<=== q dump\n " . Dumper($q) . "\n" if($debug);
 print FILE "<=== DUMP postdata:\n " . Dumper($q->param('POSTDATA')) . "\n" if($debug);

 #print "Content-type: text/xml\n\n";
 if($q->param('POSTDATA')){
  my $xmlref = {};
  $xmlref = XMLin($q->param('POSTDATA'), ForceArray => ['sharee_LastenVelo'], KeyAttr => [ ] );

  $xmlref->{userID} =~ s/\s//g if($xmlref->{userID});
  $xmlref->{emailID} =~ s/\s//g if($xmlref->{emailID});
  if(ref($xmlref) eq "HASH" && $xmlref->{todo} && $xmlref->{emailID} && looks_like_number($xmlref->{userID}) && $xmlref->{userID} =~ /^\d+$/){

   #<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
   # <sharee_LastenVelo>
   #   <todo>add_LVuser</todo>
   #   <userID>12345678</userID>
   #   <emailID>ragu@gu-syems.de</emailID>
   #   <pwID>geheim123</pwID>
   #</sharee_LastenVelo>

   #für update:
   #<todo>update_LVuser</todo>
   #<userID>12345678</userID

   #für delete:
   #<todo>delete_LVuser</todo>
   #<userID>12345678</userID

    my $pref = {
        table           => "contentadr",
        fetch           => "one",
        template_id     => "202",
        int01           => "$xmlref->{userID}",
   };
   my $record = { c_id => "" };
   $record = $dbt->fetch_record($dbh,$pref);

   #select if email still exist
   my $pref_e = {
        table           => "contentadr",
        fetch           => "one",
        template_id     => "202",
        txt08           => "ilike::" . $q->escapeHTML($xmlref->{emailID}),
   };
   my $record_e = { c_id => "" };
   $record_e = $dbt->fetch_record($dbh,$pref_e);

   my $c_id = "";
   $bw->log("$xmlref->{todo}",$xmlref,"");
   print FILE "xmlref\n " . Dumper($xmlref) . "\n" if($debug);

   if($xmlref->{userID} && $xmlref->{emailID} && $xmlref->{pwID}){
    if($xmlref->{todo} =~ /add_LVuser|update_LVuser/ && !$record->{c_id} && !$record_e->{c_id}){

      my $teltime = time;
      my $pwmd5 = md5_hex($q->escapeHTML($xmlref->{pwID}));
      #$c_id = $tk->create_account($owner);
      my $insert = {
        table   =>      "contentadr",
	main_id	=>	"200011",
	template_id =>	"202",
        mtime   =>      'now()',
        atime   =>      'now()',
        owner   =>      "$owner",
	int01	=>	$q->escapeHTML($xmlref->{userID}),
 	txt08	=>	$q->escapeHTML($xmlref->{emailID}),
	txt11	=>	"$pwmd5",
	txt17	=>	"sharee_lv",
	int03	=>	"1",
	txt22	=>	"DE11111111111111111111",
	txt23	=>	"FRSPDE11111",
	int04	=>	"1",
	int13	=>	"1",
	txt30	=>	"5511",
	int05	=>	"1",
	int14	=>	"1",
	int16	=>	"null",
	txt01	=>	"no name",
	txt03	=>	"fake str. 1",
	txt06	=>	"79999 freiburg",
	txt07	=>	"$teltime",
	ct_name =>	"LV-12345678",
      };
      $c_id = $dbt->insert_contentoid($dbh,$insert);

    }elsif($xmlref->{todo} eq "update_LVuser" && $record_e->{c_id}){

      #keep all and add only LV userID  if user email still exist
      my $update = {
         table   =>     "contentadr",
	 txt17	=>	"sharee_lv",
	 txt30	=>	"5511",
	 mtime   =>     'now()',
       	 owner   =>     "$owner",
	 int01   =>     $q->escapeHTML($xmlref->{userID}),
	};
     	my $rows = $dbt->update_record($dbh,$update,$record_e);

    }elsif($xmlref->{todo} eq "update_LVuser" && $record->{c_id}){

      my $pwmd5 = md5_hex($xmlref->{pwID});
      my $update = {
         table   =>      "contentadr",
	 mtime   =>      'now()',
       	 owner   =>      "$owner",
         int01   =>      "$xmlref->{userID}",
         txt08   =>      "$xmlref->{emailID}",
         txt11   =>      "$pwmd5",
	 int04   =>      "1",
	 int13   =>      "1",
	 int14   =>      "1",
	};
     	my $rows = $dbt->update_record($dbh,$update,$record);

    }elsif($xmlref->{todo} eq "delete_LVuser"){
	$dbt->delete_content($dbh,"contentadr",$record->{c_id});
    }

    foreach my $item (keys(%$xmlref)){
     print "<$item>$xmlref->{$item}</$item>\n";
    }
   }

  }elsif(ref($xmlref) eq "HASH" && $xmlref->{todo} && $xmlref->{todo} eq "available" && $xmlref->{bikeID} =~ /\d+/){

	#<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
	#<sharee_LastenVelo>
     	#<todo>available</todo>
     	#<bikeID>17</bikeID>
	#</sharee_LastenVelo> 

    my $bike_id = $1 if($xmlref->{bikeID} =~ /(\d+)/);
       my $pref_cc = {
        table           => "content",
        fetch           => "one",
        template_id     => "205",
        barcode         => $bike_id,
	int10           => "!=::1",#if not available 
        };

    my $record_cc = $dbt->fetch_record($dbh,$pref_cc);

    my $update_cc = {
                table   =>      "content",
		int10	=>	"1",
                mtime   =>      "now()",
                owner   =>      "$owner",
    };

    $bw->log("APIxmlserver update to available",$update_cc,"");
    print FILE "update_cc\n " . Dumper($update_cc) . "\n" if($debug);
    $dbt->update_record($dbh,$update_cc,$record_cc) if($record_cc->{c_id});

  }elsif(ref($xmlref) eq "HASH" && $xmlref->{todo} && $xmlref->{todo} eq "requested" && $xmlref->{bikeID} =~ /\d+/){

    	#<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
	#<sharee_LastenVelo>
     	#<todo>requested</todo>
     	#<bikeID>17</bikeID>
     	#<userID>123456</userID>
     	#<emailID>mail@here.de</emailID>
	#</sharee_LastenVelo>

  }#end if(ref($xmlref)
  else{
   print "<text>Hossa, kein valides xml</text>";
  }
 }#end if($q->param('POSTDATA'))
 else{
  print "<text>NO DATA</text>";
 }

 close(FILE) if($debug);
 return Apache2::Const::OK;
}
1;
