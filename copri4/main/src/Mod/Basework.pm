package Basework;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#

use strict;
use warnings;
use POSIX;
use CGI; # only for debugging
use Lib::Config;

use Data::Dumper;
use Sys::Hostname;
my $hostname = hostname;
my $cf = new Config;
my $q = new CGI;

sub new {               
 my $class = shift;
 my $self = {};         
 bless($self,$class);   
 return $self;          
}

my $time = time;
my $now_dt = strftime "%Y-%m-%d %H:%M:%S", localtime;
my $now_date = strftime "%Y-%m-%d", localtime;


#logging
sub log {
 my $self = shift;
 my ($what,$message,$stdout) = @_;
 #my ($package, $filename, $line) = caller;
 my %varenv = $cf->envonline();

 $now_dt = strftime "%Y-%m-%d %H:%M:%S", localtime;
 my $logfile = "/var/log/copri4/$varenv{syshost}-process.log";
 #if($varenv{debug}){
 if(1==1){
  warn "$$ $what" . "\n" . Dumper($message) . "\n";#to apache2/error.log

  #2021-07-21 disabled. error.log is enough
  if(1==2){
   open(FILE,">> $logfile");
    print FILE "\n--- $now_dt $0 ---\n";
    print FILE "$what" . "\n" . Dumper($message) . "\n";
   close FILE;
  }
  #also to stdout
  if($stdout){
    #print "\n--- $now_dt $0 ---\n";
    print "$$ $what" . "\n" . Dumper($message) . "\n";
  }

 }

}

#return headline message
sub return_feedback(){
  my $self = shift;
  my $node_meta = shift;
  my $users_dms = shift || {};
  my $feedb = shift || {};

  my $return = "";
  if(ref($feedb) eq "HASH" && $feedb->{message}){
print<<EOF
  <script>
        \$(document).ready(function(){
         \$( "#retm" ).fadeOut(8000);
  })
  </script>
EOF
;
  my $debug = "";
  $debug = $feedb->{debug} if($users_dms->{u_id} eq "1842");

  if($feedb->{message} =~ /(failure::.*)/){
    $return = $1;
  }elsif($feedb->{message}){
    print $q->div({-id=>'retm'},"$feedb->{message} $debug"),"\n";
  }elsif($feedb->{u_rows}){
    print $q->div({-id=>'retm'},"$feedb->{u_rows} $node_meta->{node_name} aktualisiert $debug"),"\n";
  }
 }

 return $return;
}

sub battery_bars {
  my $self = shift;
  my $max_bars = shift || 0;
  my $current_percent = shift || 0;

  my $current_bars = 0;
  if($max_bars == 5){
	$current_bars = 1 if($current_percent >= 10);
	$current_bars = 2 if($current_percent >= 30);
	$current_bars = 3 if($current_percent >= 50);
	$current_bars = 4 if($current_percent >= 70);
	$current_bars = 5 if($current_percent >= 90);
  }

  return $current_bars;
}

sub battery_percent {
  my $self = shift;
  my $max_bars = shift || 0;
  my $current_bars = shift || 0;#by user input

  my $current_percent = 0;
  if($max_bars == 5){
        $current_percent = 10 if($current_bars >= 1);
        $current_percent = 30 if($current_bars >= 2);
        $current_percent = 50 if($current_bars >= 3);
        $current_percent = 70 if($current_bars >= 4);
        $current_percent = 100 if($current_bars >= 5);
  }

  return $current_percent;
}


1;
