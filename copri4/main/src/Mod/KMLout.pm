package Mod::KMLout;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
#without login only public Stations have to be viewable:
#https://shareeapp-primary.copri-bike.de/KMLout
#
#with login and valid autcookie:
#https://shareeapp-primary.copri-bike.de/KMLout?sessionid=a49aba2b5440be72816db2_rafo87znqx
#
#
use warnings;
use strict;
use POSIX;
use Exporter;
our @ISA = qw (Exporter);
use CGI::Cookie ();
use CGI ':standard';
use Apache2::Const -compile => qw(OK );
use Scalar::Util qw(looks_like_number);
use JSON;
use LWP::UserAgent;

use Lib::Config;
use Mod::DBtank;
use Mod::APIfunc;
use Mod::Basework;
use Data::Dumper;
my $bw = new Basework;

sub handler {
 my $r = shift;
 my $q = new CGI;
 $q->import_names('R');
 my $dbt = new DBtank;
 my $apif = new APIfunc;
 my $cf = new Config;
 my %varenv = $cf->envonline();

 my $coo = $q->cookie('domcookie') || $R::sessionid || "";
 my $users_sharee = { c_id => 0 };
 my $api_return = { authcookie => '' };

 ($api_return,$users_sharee) = $apif->auth_verify($q,$coo,"");

  if($dbt->{website}->{$varenv{syshost}}->{merchant_id}){
   $api_return->{authcookie} = $dbt->{website}->{$varenv{syshost}}->{merchant_id} if(!$api_return->{authcookie});
  }elsif($dbt->{operator}->{$varenv{dbname}}->{merchant_id}){
   $api_return->{authcookie} = $dbt->{operator}->{$varenv{dbname}}->{merchant_id} if(!$api_return->{authcookie});
  }elsif($dbt->{primary}->{$varenv{dbname}}->{merchant_id}){
   $api_return->{authcookie} = $dbt->{primary}->{$varenv{dbname}}->{merchant_id} if(!$api_return->{authcookie});
  }
  $users_sharee->{c_id} = "0" if(!$users_sharee->{c_id});

 $bw->log("kmlGenerator handler with api_return: ",$api_return,"");

 my $kmlfile = kmlGenerator($api_return,\%varenv,$users_sharee);

 #print $q->header();
 #print out kml file, used by Karte.pm js maps
 if(1==1){
    print $q->header(-type => "application/vnd.google-earth.kml+xml", -charset=>"utf-8");

    if (open(my $fh, '<', "$varenv{basedir}/xml/$kmlfile")) {
      while (my $row = <$fh>) {
	print "$row";
      }
    }
 }

 return Apache2::Const::OK;
}


sub kmlGenerator {
 my $authcookie = shift || { authcookie => '' };
 my $varenv = shift;
 my $users_sharee = shift || { c_id => 0 };
 $users_sharee->{c_id} = "0" if(!$users_sharee->{c_id});
 my $dbt = new DBtank;
 my $json = JSON->new->allow_nonref;

 my $project = "all";
 $project = $dbt->{website}->{$varenv->{syshost}}->{project} if($dbt->{website}->{$varenv->{syshost}}->{project});
 $project = $dbt->{operator}->{$varenv->{dbname}}->{project} if($dbt->{operator}->{$varenv->{dbname}}->{project});
 #print "$project | $varenv->{syshost} | $users_sharee->{c_id} | $authcookie->{authcookie}";
 my $icon_green = "Open_Green.png";
 my $icon_red = "Open_Red.png";
 my $icon_blue = "Open_Blue.png";
 if($project eq "Bayern"){
   $icon_green = "LastenradBay_Standortpfeil_gruen_03a.png";
   $icon_red = "LastenradBay_Standortpfeil_rot_03a.png";
   $icon_blue = "LastenradBay_Standortpfeil_blau_03a.png";
   #$icon_green = "LastenradBay-custom-marker-gruen.png";
   #$icon_red = "LastenradBay-custom-marker-rot.png";
   #$icon_blue = "LastenradBay-custom-marker-bayblau.png";
 }

 my $timestamp = strftime "%Y%m%d%H%M%S", localtime;
 my $kmlfile = "sharee-$timestamp-$users_sharee->{c_id}.kml";
 $bw->log("kmlGenerator with: ",$kmlfile,"");

 #request primary will jsonclient loop_sharees
 my $uri_request = $dbt->{primary}->{sharee_primary}->{primaryApp};

 #detect DMS, because  this will do it directly without loop_sharees
 $uri_request = $dbt->{operator}->{$varenv->{dbname}}->{operatorApp} if($varenv->{dbname} ne "sharee_primary" && $varenv->{syshost} =~ /shareedms-/);

 my $rest_stations = "request=stations_available&project=$project&authcookie=$authcookie->{authcookie}";
 my $rest_bikes = "request=bikes_available&project=$project&authcookie=$authcookie->{authcookie}";

 my $station_hash = {};
 my $bike_hash = {};
 my $lastenrad = 300101;
 my $e_lastenrad = 300102;
 my $stadtrad = 300103;

  my %place_name;
  my %place_desc;
  my %place_pin;
  my %place_longitude;
  my %place_latitude;

 #reading shareejson
 my $stations_json = fetch_primary_json("",$uri_request,$rest_stations);
 my $bikes_json = fetch_primary_json("",$uri_request,$rest_bikes);

 open(XML,">$varenv->{basedir}/xml/$kmlfile") || die "$0 can not write $!";

 print XML "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
 print XML "<kml xmlns=\"http://www.opengis.net/kml/2.2\">\n";
 print XML "<Document>\n";
 print XML "<name>sharee.bike</name>\n";
 print XML "<description>bike stations for $users_sharee->{c_id}</description>\n";


 if($stations_json && $bikes_json){
   #decode json to hash
   my $response_stations = decode_json($stations_json);
   my $response_bikes = decode_json($bikes_json);

   #BIG LOOP
   #loop stations hash
   foreach my $station (keys (%{ $response_stations->{shareejson}->{stations} })) {
    $station_hash->{$station} = $response_stations->{shareejson}->{stations}->{$station};

    $place_pin{$station} = $icon_red;
    my $station_desc = "Mietradstation $station";
    if($response_stations->{shareejson}->{stations}->{$station}->{description}){
      $station_desc = "Mietradstation: $response_stations->{shareejson}->{stations}->{$station}->{description} $station";
      #$bw->log("KMLout station_desc: ",$station_desc,"");
    }
    $place_name{$station} = "$station_desc";

    if($response_stations->{shareejson}->{stations}->{$station}->{state} eq "available" && $response_stations->{shareejson}->{stations}->{$station}->{gps}->{latitude} =~ /\d{1,2}\.\d+/ && $response_stations->{shareejson}->{stations}->{$station}->{gps}->{longitude} =~ /\d{1,2}\.\d+/){
     $place_latitude{$station} = $response_stations->{shareejson}->{stations}->{$station}->{gps}->{latitude};
     $place_longitude{$station} = $response_stations->{shareejson}->{stations}->{$station}->{gps}->{longitude};
     $place_desc{$station} = "";
     #print "\n--- Station $station hat folgende Räder ---\n";

     #loop station_group array
     foreach my $station_group (@{ $response_stations->{shareejson}->{stations}->{$station}->{station_group} }){
      #print "station_group ($station): " . $station_group . "\n";
      my $station_groupID = 0;

      #loop bikes hash
      foreach my $bike (keys (%{ $response_bikes->{shareejson}->{bikes} })) {
        $bike_hash->{$bike} = $response_bikes->{shareejson}->{bikes}->{$bike};

        #loop bike_group array
        foreach my $bike_group (@{ $response_bikes->{shareejson}->{bikes}->{$bike}->{bike_group} }){
          #print "bike_group ($bike): " . $bike_group . "\n";
          my $bike_groupID = 0;

          $station_groupID = $1 if($station_group =~ /(\d+)/);
          $bike_groupID = $1 if($bike_group =~ /(\d+)/);
 
	  #$bw->log("KMLout bike available if: $station_groupID == $bike_groupID && $station eq $response_bikes->{shareejson}->{bikes}->{$bike}->{station}","","");

          #if Lastenrad
          if($station_groupID == $lastenrad && $station_groupID == $bike_groupID && $station eq $response_bikes->{shareejson}->{bikes}->{$bike}->{station}){
                $place_pin{$station} = $icon_green;
		$place_desc{$station} .= "<p align='left'>&bull; Verfügbar: $response_bikes->{shareejson}->{bikes}->{$bike}->{description} $bike</p>";
		#print "($station) Lastenrad $bike\n";
          }
          #if E-Lastenrad
          if($station_groupID == $e_lastenrad && $station_groupID == $bike_groupID && $station eq $response_bikes->{shareejson}->{bikes}->{$bike}->{station}){
                $place_pin{$station} = $icon_green;
		$place_desc{$station} .= "<p align='left'>&bull; Verfügbar: $response_bikes->{shareejson}->{bikes}->{$bike}->{description} $bike</p>";
		#print "($station) E-Lastenrad $bike\n";
          }
          #if Stadtrad 
          if($station_groupID == $stadtrad && $station_groupID == $bike_groupID && $station eq $response_bikes->{shareejson}->{bikes}->{$bike}->{station}){
                $place_pin{$station} = $icon_green;
		$place_desc{$station} .= "<p align='left'>&bull; Verfügbar: $response_bikes->{shareejson}->{bikes}->{$bike}->{description} $bike</p>";
		#print "($station) Stadtrad $bike\n";
          }

        }
      }
     }

      print XML " <Style id=\"$place_pin{$station}\">\n";
      print XML "  <IconStyle>\n";
      print XML "   <Icon>\n";
      print XML "    <href>$varenv->{metahost}/img/$place_pin{$station}</href>\n";
      print XML "   </Icon>\n";
      print XML "  </IconStyle>\n";
      print XML " </Style>\n";
      print XML " <Placemark>\n";
      print XML "  <name>$place_name{$station}</name>\n";
      print XML "  <description><![CDATA[ $place_desc{$station} ]]></description>\n";
      print XML "  <styleUrl>#$place_pin{$station}</styleUrl>\n";
      print XML "  <Point>\n";
      print XML "    <coordinates>$place_longitude{$station}, $place_latitude{$station}</coordinates>\n";
      print XML "  </Point>\n";
      print XML " </Placemark>\n";

    }
   }#end BIG LOOP
 }#end if json

 print XML "</Document>\n";
 print XML "</kml>\n";
 close(XML);
 chmod 0666, "$varenv->{basedir}/xml/$kmlfile";

 return $kmlfile;
}#end kmlGenerator

#requestor
sub fetch_primary_json {
 my $self = shift;
 my $primary_server = shift || "";
 my $rest = shift || "";
 my $primary_request = "$primary_server/APIjsonserver?$rest";
 $bw->log("kmlGenerator primary_request: ",$primary_request,"");
 #print "GET_json >> " . $primary_request . "\n";

 my $ua = LWP::UserAgent->new;
 $ua->agent("sharee KMLout");

 my $req = HTTP::Request->new(GET => "$primary_request");
 $req->content_type('application/x-www-form-urlencoded');
 $req->content($rest);

 #Pass request to the user agent and get a response back
 my $res = $ua->request($req);
 # Check the outcome of the response
 if ($res->is_success) {
  #print $res->content;
  return $res->content;
  #print $res->status_line, "\n";
 }else {
  return "";
  #print $res->status_line, "\n";
 }
}


1;
