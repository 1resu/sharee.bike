package Mod::Indextest;
#      
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
#
use strict;
use warnings;

use POSIX;
use CGI::Cookie ();
use CGI ':standard';
use DateTime;
use DateTime::Format::Pg;
use DBI;
use Apache2::RequestUtil ();
use Apache2::RequestIO ();              
use Apache2::Const -compile => qw(OK);
use LWP::UserAgent;
use Digest::MD5 qw(md5 md5_hex);
use Scalar::Util qw(looks_like_number);
#use Encode;
#use URI::Encode qw(uri_encode uri_decode);

use Lib::Config;
use Mod::Buttons;
use Mod::Prelogic;
use Lib::Mlogic;
use Mod::Basework;
use Mod::Premain;
use Mod::DBtank;
use Mod::Libenzdb;
use Mod::APIfunc;
use Mod::Shareework;
use Mod::Prelib;
use Mod::Payment;
use Mod::Modalbox;
use Mod::Modalbox3;
use Data::Dumper;

sub handler {
 my $re = shift;
 my $q = new CGI();
 $q->import_names('R');
 my $cf = new Config;
 my $ml = new Mlogic;
 my $bw = new Basework;
 my $pre = new Prelogic;
 my $pm = new Premain;
 my $tk = new Shareework;
 my $dbt = new DBtank;
 my $db = new Libenzdb;
 my $apif = new APIfunc;
 my $but = new Buttons;
 my $pl = new Prelib;
 my $payone = new Payment;

 my %varenv = $cf->envonline();
 my $netloc = $q->url(-base=>1);
 my $dbh = "";
 my $script = $q->script_name();
 my $path = $q->path_info();
 $path = "$script" . "$path";
 my $refer = $ENV{HTTP_REFERER};
 my $now_dt = strftime "%Y-%m-%d %H:%M:%S", localtime;
 my $return = "";
 my $returnwww = "";
 my $html_charset = "utf-8";
 my $user_agent = $q->user_agent();
 $path =~ s/\.html//;

 #$mode is used to set GUI features like "maintainer" contextmenue 
 my $modes = $dbt->{shareedms_conf}->{modes};
 my $mode = "";
 if($R::mode && length($R::mode) >= 5 && $R::mode =~ /($modes)/){
   $mode = "$R::mode";
 }elsif($path && $path =~ /\/($modes)$/){
   $mode = $1;
 }elsif($path && $path =~ /\/($modes)\?\w/){
   $mode = $1;
 }

 #main datadir is main config directive like "shareeapp-kn" and catched by syshost name
 if($netloc =~ /:\/\/(sharee\w+-\w+)\.copri/){
   #$bw->log("Indexsharee merchant select by netloc:",$netloc,"");
   $varenv{syshost} = $1 if( -d "/var/www/copri-bike/$1");
   $mode = "manager" if($varenv{syshost} =~ /shareedms-/ && !$mode);
 }
 die "no configuration available" if(!$varenv{syshost});


 if($varenv{orga} ne "dms" && $path =~ /DMS|Waren|Kunden\/|Einstellung|journal|Faktur/i){
   print redirect("$varenv{wwwhost}");
   exit 0;   
 }

 my $lang = "de";
 my $dyn_js = "";
 my $users_dms = {};
 my $users_sharee = {};
 my $api_return = {};

 print $q->header(-charset=>"$html_charset");

 my $coo = $q->cookie('domcookie') || $R::sessionid || "";
 #Prio sessionid if also domcookie is set ... and s.u.
 if($R::sessionid  && $R::sessionid ne $q->cookie('domcookie')){
 	$coo = $q->param('sessionid');
    	my $cookie = CGI::Cookie->new(-name  => 'domcookie',-value => $coo);
    	print $q->header(-charset=>"$html_charset", -cookie=>$cookie);
 }

 (my $aowner,my $return_merchant) = $apif->fetch_merchant($q,\%varenv,$coo,$R::merchant_id);
 $varenv{merchant_id} = $return_merchant->{merchant_id};
 $bw->log("Indexsharee merchant select used with access_owner $aowner",$varenv{merchant_id},"");

 ($api_return,$users_sharee) = $apif->auth_verify($q,$coo,"");

 #login-screen should only be available if auth_verify fails 
 if($R::login_sharee || $R::login_dms){

    #1. logout
    $apif->authout($q,$coo);

    my $hw_id = unpack ('H*', pack('Nc', time, $$ % 0xff));#old $co
    #3. authorize
    my $author = $apif->authorization($q,$varenv{merchant_id},$hw_id,$aowner);#$co like browser hw_id
    #4. verify and get user values
    ($api_return,$users_sharee) = $apif->auth_verify($q,$author->{authcookie},"",$author->{new_authcoo});

    #5. domcookie by authcookie substr (cut first 15 chars), AND also sessionid 
    if($author->{authcookie} && length($author->{authcookie}) > 30){
	# take last 21 chars
    	$coo = substr $author->{authcookie}, 15;
    	my $cookie = CGI::Cookie->new(-name  => 'domcookie',-value => $coo);
    	print $q->header(-charset=>"$html_charset", -cookie=>$cookie);

    	#DMS login
    	if($users_sharee->{c_id} && $varenv{orga} eq "dms" && $R::login_dms eq "Login" && $coo && length($coo) > 20){
	  $users_dms = $dbt->select_users($dbh,$users_sharee->{c_id});
	  my $update_users = {
                table   => "users",
                owner   => $aowner,
                u_id    => $users_dms->{u_id}
          };
	  $dbt->update_one($dbh,$update_users,"cookie='$coo'") if($users_dms->{u_id});

     	  print redirect("$varenv{wwwhost}$path");
     	  exit 0;
    	}
     }
 }

 my $session="";
 my $session_and="";
 if(length($coo) > 20 && !$q->cookie(-name=>'domcookie')){
        $session = "?sessionid=$coo";
        $session_and = "&sessionid=$coo";
 }

 #DMS
 if($users_sharee->{c_id} && $varenv{orga} eq "dms" && $coo && length($coo) > 20){
     $users_dms = $dbt->select_users($dbh,$users_sharee->{c_id},"and cookie='$coo'");
 }


 #Save anyway on create ... and hopefully delete it later
 if($R::sharee_edit && $R::sharee_edit =~ /create_account/ && $R::txt04 && $R::txt04 =~ /\w+/ && $R::txt08 && $R::txt08 =~ /\w+\@\w+/){

    #1. logout
    $apif->authout($q,$coo);

   #create_account. 2. inserts contentadr
   my $tinkc_id = $tk->create_account($aowner);
   ($returnwww,$return) = $tk->save_account($tinkc_id,\%varenv,$aowner);

    #Like login_sharee, redundant
    my $hw_id = unpack ('H*', pack('Nc', time, $$ % 0xff));#old $co
    #3. authorize
    my $author = $apif->authorization($q,$varenv{merchant_id},$hw_id,$aowner);#$co like browser hw_id
    #print "3. authorize: " . $author->{authcookie} . " --  " . $q->param('authcookie') . " ++ " . $coo . "<br>"; 

    #4. verify and get user values
    ($api_return,$users_sharee) = $apif->auth_verify($q,$author->{authcookie},"",$author->{new_authcoo});
    #print "4. verifyize: " . $author->{authcookie} . " -- c_id: " . $users_sharee->{c_id} . " ++ " . $coo  . "<br>"; 

    #5. domcookie by authcookie substr (cut first 15 chars), AND also sessionid 
    if($author->{authcookie} && length($author->{authcookie}) > 30){
	# take last 21 chars
    	$coo = substr $author->{authcookie}, 15;
    	my $cookie = CGI::Cookie->new(-name  => 'domcookie',-value => $coo);
    	print $q->header(-charset=>"$html_charset", -cookie=>$cookie);
	#print "5. set cookie: " . $author->{authcookie} . " --  " . $q->param('authcookie') . " ++ " . $coo . "<br>"; 

        #2020-07-09 if user-pw authorized, then ignore conflict_ because it matches exist user-data
        if($tinkc_id && $returnwww && $returnwww =~ /conflict_txt07|conflict_txt08/){
          #delete user-pw conflict registration and going on with existing data
	  #$db->delete_content("contentadr",$tinkc_id);
          $return = $tk->delete_account($tinkc_id,$users_dms->{u_id});

          $apif->authout($q,$coo) if($coo);
          print redirect("$varenv{wwwhost}/$varenv{mandant}/Anmelden?conflict_failure=1");
          exit 0;
        }
        elsif(length($coo) > 20){
    	  #we need this to get $R::sessionid to FormEdit
    	  #if(length($coo) > 20 && !$q->cookie(-name=>'domcookie')){
    	  ($api_return,$users_sharee) = $apif->auth_verify($q,$author->{authcookie},"");
      	  if(($returnwww && $returnwww =~ /failure::(.*)/) || ($users_sharee->{txt31} && $users_sharee->{txt31} =~ /\w/)){
   		$returnwww =~ s/::/=/g;
      	  	print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_1}?sessionid=$coo\&$returnwww");
      	  	exit 0;
      	  }else{
      	  	print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_1_5}?sessionid=$coo");
      	  	exit 0;
      	 }
	}
      }
 }

  my $payable_check=0;
  if($users_sharee->{int03} && $users_sharee->{ct_name} && ($users_sharee->{int03} == 1 && $users_sharee->{ct_name} =~ /\w{2}-\d+/) || ($users_sharee->{int03} == 2 && length($users_sharee->{ct_name}) >= 19)){
	$payable_check=1;
  }


 # Logout
 if($mode =~ /logout/){
   if($mode =~ /logout_sharee/){
     $apif->authout($q,$coo);
   }else{
     $db->cook_out($coo);
   }
   print redirect("$varenv{wwwhost}");
   exit 0;
 }
 
 my $tpl = $dbt->get_tpl($dbh,"302001");#Kundendaten template
 $tpl->{tpl_order} .= ",txt04,txt08";


 if($R::login_sharee){
  if($users_sharee->{c_id} && (($users_sharee->{int03} != 1 && $users_sharee->{int03} != 2)|| ($users_sharee->{txt31} && $tpl->{tpl_order} =~ /$users_sharee->{txt31}/))){
      my $row = $db->updater("contentadr","c_id","$users_sharee->{c_id}","int12","1");#Vde
      print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_1}$session");
      exit 0;
  }elsif($users_sharee->{c_id} && !$payable_check){
      #print redirect("$varenv{wwwhost}/$varenv{mandant}/$varenv{profile}");
      #print redirect("$varenv{wwwhost}/$varenv{mandant}/$varenv{start}");
      print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_2}$session");
      exit 0;
  }elsif(!$users_sharee->{c_id} || ($users_sharee->{c_id} && $users_sharee->{c_id} !~ /^\d+$/)){
      print redirect("$varenv{wwwhost}/$varenv{mandant}/Anmelden?failure=1$session_and");
      exit 0;
  }else{
     print redirect("$varenv{wwwhost}/$varenv{mandant}/$varenv{profile}$session");
     exit 0;
  }
 }
 
 my $oncontextmenu="";
 $oncontextmenu="return false;";# if($mode =~ /maintainer/);
 $oncontextmenu="" if(!$users_dms->{u_id});
 

 #Menu & node_path handling
 my ($view,$view_post);
 my @viewsel = ("shareestart");
 if($path =~ /^\/(.*)/){
  @viewsel = split /\//,$1;
  if("$path" =~ /\/($modes)$/){
    pop @viewsel;
  }
  $view_post = $viewsel[-1] || "root";
  #$view_post = encode('iso-8859-1',decode('utf-8', $view_post));
  $view = $viewsel[0];
 }
 my $depth = scalar(@viewsel);
 $view = "root" if(!$view);

 my $node_meta = $dbt->get_node_meta($dbh,\@viewsel);

 #DMS & Tools
 if($varenv{orga} eq "dms" && $users_dms->{u_id}){
   if($R::cal_delta_start){
     use Date::Calc qw(Add_Delta_YMD);
     my $day = strftime "%d", localtime;
     my $mon = strftime "%m", localtime;
     my $year = strftime "%Y", localtime;
     $users_dms->{cal_start} =~ s/\s.*//;
     ($year,$mon,$day) = split(/-/,$users_dms->{cal_start}) if($users_dms->{cal_start});
     my ($year1,$mon1,$day1) = split(/:/,$R::cal_delta_start);
     my ($Dy,$Dm,$Dd) = Add_Delta_YMD($year,$mon,$day, $year1,$mon1,$day1);
     $db->users_up("cal_start","$Dy-$Dm-$Dd",$users_dms->{u_id});
   }
   if($R::cal_today){
     my $today4db = strftime("%Y-%m-%d %H:%M",localtime(time));
     $db->users_up("cal_start","$today4db",$users_dms->{u_id});
   }
   if($R::col_sort){
     $db->users_up("col_sort",$R::col_sort,$users_dms->{u_id});
   }
   if($R::sort_updown){
     $db->users_up("sort_updown",$R::sort_updown,$users_dms->{u_id});
   }
   if($R::cal_sort_updown){
     $db->users_up("cal_sort_updown",$R::cal_sort_updown,$users_dms->{u_id});
   }

   #base_edit implements new DMS methodes without Pre* things
   #permissions
   #DMS Kunden rw
   if($R::base_edit && $node_meta->{ct_table} eq "contentadr"){
    $bw->log("DMS action from $users_dms->{u_id} on $node_meta->{ct_table}|$R::c_id with: $R::base_edit","","");
    if($R::c_id && $R::base_edit eq "remove_chk4rel"){
	my $delete_key = "delete_content";
   	$delete_key = "delete_adr";
	$return = "failure::Datensatz wirklich löschen. ::?base_edit=$delete_key\&exit_box2=1\&c_id=$R::c_id ::löschen";
    }elsif($users_dms->{int02} == 2 && $R::c_id && $R::base_edit eq "save_adr"){
      ($returnwww,$return) = $tk->save_account($R::c_id,\%varenv,$users_dms->{u_id});
    }elsif($users_dms->{int02} == 2 && $R::c_id && $R::base_edit eq "delete_adr"){
	$return = $tk->delete_account($R::c_id,$users_dms->{u_id});
    }else{
	$return = "failure::Abbruch. Schreibender Zugriff \"Kunden Stammdaten\" verweigert.";
    }
   }

   #DMS users accounts
   if($R::base_edit && $node_meta->{ct_table} eq "users"){#DMS-Account rw
    $bw->log("DMS action from $users_dms->{u_id} on $node_meta->{ct_table} with: $R::base_edit","","");
    if($R::u_id && $R::base_edit eq "remove_chk4rel"){#users
	my $delete_key = "delete_dmsusers";
	$return = "failure::Datensatz wirklich löschen. ::?base_edit=$delete_key\&exit_box2=1\&u_id=$R::u_id ::löschen";
    }elsif($users_dms->{int07} == 2 && ($R::u_id || $R::c_idadr) && $R::base_edit =~ /_dmsusers/){
	my $u_id = $1 if($R::u_id && $R::u_id =~ /(\d+)/);
	$u_id = $1 if($R::c_idadr && $R::c_idadr =~ /(\d+)/ && $R::base_edit eq "new_dmsusers");
	$return = $tk->manage_dmsusers($R::base_edit,$u_id,$users_dms);
    }else{
	$return = "failure::Abbruch. Schreibender Zugriff \"DMS-Account\" verweigert.";
    }
   }

  #DMS Waren || Einstellung/Service* rw
  if($node_meta->{ct_table} eq "content" || $node_meta->{ct_table} eq "contentuser"){
   $bw->log("DMS action from $users_dms->{u_id} on $node_meta->{ct_table}|$R::main_id with: $R::rel_edit","","");
   if(($node_meta->{ct_table} eq "content" && $users_dms->{int01} == 2) || ($node_meta->{ct_table} eq "contentuser" && $users_dms->{int08} == 2)){
    if($R::rel_edit eq "save_relation" && $R::main_id && $R::main_id >= 200000){
     $return = $pl->save_relation($R::main_id,$users_dms->{u_id});
    }elsif($R::rel_edit eq "delete_relation" && $R::main_id && $R::main_id >= 200000){
     $return = $pl->delete_relation($R::main_id,$users_dms->{u_id});
    }elsif($R::rel_edit eq "new_relation" && $R::main_id && $R::main_id >= 200000){
     $return = $pl->new_relation($R::main_id,$users_dms->{u_id});
    }elsif($R::rel_edit && $R::main_id && $R::main_id >= 200000){
     $return = $pm->maininit($users_dms);
    }
   }elsif($R::rel_edit){
     $return = "failure::Abbruch. Schreibender Zugriff \"Waren Stammdaten\" verweigert.";
   }
  }

  #DMS Faktura
  if($node_meta->{ct_table} eq "contenttrans"){
    $bw->log("DMS action from $users_dms->{u_id} on $node_meta->{ct_table}|$R::c_id4trans,$R::tpl_id4trans with: $R::ct_trans","","");
    if($users_dms->{int03} == 1 && $R::ct_trans eq "open"){#DMS Faktura read
      $db->update_users4trans($R::c_id4trans,$R::tpl_id4trans,"",$users_dms->{u_id}) if(looks_like_number($R::c_id4trans) && looks_like_number($R::tpl_id4trans));
    }elsif($users_dms->{int03} == 2){#DMS Faktura rw
      $db->update_users4trans($R::c_id4trans,$R::tpl_id4trans,"",$users_dms->{u_id}) if(looks_like_number($R::c_id4trans) && looks_like_number($R::tpl_id4trans));
     if($R::ct_trans =~ /set_workflow2invoice|set_workflow2storno/){
       $return = $pl->set_workflow($users_dms,$R::c_id4trans,$R::set_main_id4workflow) if(looks_like_number($R::c_id4trans) && looks_like_number($R::tpl_id4trans) && looks_like_number($R::set_main_id4workflow));
     }else{
       $return .= "|";
       $return .= $pre->preinit($users_dms,$lang);#transactions logic
     }
    }elsif($R::ct_trans){
     $return = "failure::Abbruch. Schreibender Zugriff \"Faktura\" verweigert.";
    }
   }
  $users_dms = $dbt->select_users($dbh,$users_dms->{u_id},"");#just to get update after save

 #none DMS hosts ----------------------------------------------------
 }elsif($varenv{orga} ne "dms"){




  #save_account. 3. updates contentadr 
  if($users_sharee->{c_id} && $R::sharee_edit && $R::sharee_edit =~ /save_account/){
    ($returnwww,$return) = $tk->save_account($users_sharee->{c_id},\%varenv,$aowner);
  }

  if($users_sharee->{c_id} && $R::sharee_edit && $R::sharee_edit =~ /save_transact/){
    $returnwww  = $tk->save_transact($users_sharee->{c_id},$coo,$aowner);
  }

  if($returnwww && $returnwww =~ /failure::(.*)/){
   $returnwww =~ s/::/=/g;
   ($api_return,$users_sharee) = $apif->auth_verify($q,$coo,"");

   if($returnwww =~ /txt22|txt23/){
     print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_2}?cum=1$session_and\&$returnwww");
   }elsif($returnwww =~ /int03/){
     print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_1_5}?cum=1$session_and\&$returnwww");
   }elsif($returnwww =~ /txt09|txt16/){
     print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_3}?cum=1$session_and\&$returnwww");
   }elsif($returnwww =~ /int01/ && $R::radID){
     print redirect("$varenv{wwwhost}/?ask_radID=$R::radID\&failure=not-synced$session_and");
   }else{
     print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_1}?cum=1$session_and\&$returnwww");
   }
   exit 0;
  }else{
   ($api_return,$users_sharee) = $apif->auth_verify($q,$coo,"");


   #Payone Response POST (TransactionStatus)
   #payone response ($R::pseudocardpan || $R::status)
   if($users_sharee->{c_id} && ($R::pseudocardpan || $R::status)){
	my $payone_return;
   	 open(FILE,">>$varenv{logdir}/payone-return-post.log");
         print FILE "<--- $now_dt from Indexsharee.pm \nPayone return-way by ajaxCall: $R::status\n";
 	 my @keywords = $q->param;
   	 foreach(@keywords){
          my $val = $q->param($_);
          print FILE "$_=$val\n";
	  #TODO, check errormessages. At first we have do indicate what comes from payone!
	  $payone_return .= "$_=$val\n";# if($_ =~ /error|message/i);
   	 }
   	 close(FILE);

	my $update_adr = {
      		table   => "contentadr",
      		mtime   => "now()",
      		owner   => $aowner,
		c_id 	=> $users_sharee->{c_id}
 	};
	my $vde_on_fail = $users_sharee->{int12} || 3;#keep last or set 3

	#SEPA, done in payone Payment
	#CC
 	if($R::pseudocardpan && length($R::pseudocardpan) >= 19){#done by payone AJAX return 
 	 #if($R::status eq "APPROVED")
	  $update_adr->{txt22} = "";
	  $update_adr->{txt23} = "";
	  $update_adr->{ct_name} = $q->escapeHTML($R::pseudocardpan);
	  $update_adr->{txt27} = $q->escapeHTML($R::status);
	  $update_adr->{txt28} = "";
	  $update_adr->{int12} = 0;
	  $update_adr->{int03} = 2;
	  $dbt->update_record($dbh,$update_adr,$users_sharee) if($users_sharee->{c_id} > 0);
	  ($api_return,$users_sharee) = $apif->auth_verify($q,$coo,"");

     	   #define fictiv invoice to get 1 € test
     	   my $epoche = time();
     	   my $ctt = { 
             c_id => 1,
             int01 => 0,#capture amount
             int15 => 1,#preauth amount
             txt16 => "",
             reference => "$users_sharee->{c_id}_$epoche",
             payone_reset => ''
     	   };

     	   my $payone_txid = "";
	   ##preauthorization and/or capture needs to much time, must be done async!
	   #$payone_txid = $payone->preauthorizationCC_main(\%varenv,$users_sharee,$ctt,$aowner);
	   #if($payone_txid)
     	   if(1==1){

	     #$ctt->{txt16} = "$payone_txid";
	     #$payone_txid = $payone->captureCC_main(\%varenv,$users_sharee,$ctt,$aowner);
	     #int12=0 should be set after capture success in payment module

   	     if(($users_sharee->{int04} != 1) && ($users_sharee->{txt08} =~ /\w\@\w/)){
    		$tk->emailack($users_sharee->{c_id});
   	     }
   	     if(($users_sharee->{int13} != 1) && ($users_sharee->{txt07} =~ /\d{9}/ && length($users_sharee->{txt07}) <= 16)){
    		$tk->smsack($users_sharee);
   	     }

     	   }else{
             $dbt->update_one($dbh,$update_adr,"int12=$vde_on_fail");#Vde
     	   }

          $dbt->update_operatorsloop($varenv{dbname},$users_sharee->{c_id},"update");

     	  print redirect("$varenv{wwwhost}/$varenv{mandant}/$varenv{profile}?$returnwww$session_and");
     	  exit 0;
	}else{
          $update_adr->{txt28} = $q->escapeHTML($payone_return);
          $update_adr->{int12} = $vde_on_fail;
	  $dbt->update_record($dbh,$update_adr,$users_sharee) if($users_sharee->{c_id} > 0);
          $dbt->update_operatorsloop($varenv{dbname},$users_sharee->{c_id},"update");
	}
     }#end payone response with pseudocardpan
  }


  my $debug=0;
  $debug=1;
  #send confirm codes
  if($users_sharee->{c_id} && $users_sharee->{txt34} && length($users_sharee->{txt34}) > 20 && $payable_check && $R::sharee_edit && $R::sharee_edit =~ /save_account|send_email|send_sms/){

   if(($users_sharee->{int04} != 1 || $R::sharee_edit =~ /send_email/) && ($users_sharee->{txt08} =~ /\w\@\w/)){
    $tk->emailack($users_sharee->{c_id});
   }
   if(($users_sharee->{int13} != 1 || $R::sharee_edit =~ /send_sms/) && ($users_sharee->{txt07} =~ /\d{9}/ && length($users_sharee->{txt07}) <= 16)){
    $tk->smsack($users_sharee);
   }
  }#send confirm code

  #email and sms acknowledgments, check and save confirm states
  if($R::confirm_userid && $R::confirm_userid =~ /^\d+$/ && ($R::confirm_code && length($R::confirm_code) >= 5 || $R::confirm_smscode && length($R::confirm_smscode) >= 5)){
    
    #keep in mind, for now and just for testing confirm codes are just c_id
    open(FILE,">>$varenv{logdir}/confirm.log") if($debug);
    print FILE "\n\n*--> $now_dt done by $0\n" if($debug);
    print FILE "confirm_userid:$R::confirm_userid\nconfirm_code:$R::confirm_code\nconfirm_smscode:$R::confirm_smscode\n" if($debug);
    $R::confirm_code =~ s/\s//g;
    $R::confirm_smscode =~ s/\s//g;
    my $confirm_code = $q->escapeHTML($R::confirm_code);
    my $confirm_smscode = $q->escapeHTML($R::confirm_smscode);

    #confirm email
    if($confirm_code){
      my $authref = {
                table           => "contentadr",
                fetch           => "one",
                template_id     => "202",
                c_id            => $R::confirm_userid,
		txt34		=> "ilike::$confirm_code%",
      };
      my $confirmed_email = $dbt->fetch_record($dbh,$authref);

      #($api_return,$users_sharee) = $apif->auth_verify($q,"",$R::confirm_code);
      if($confirmed_email->{c_id}){
        $db->updater("contentadr","c_id","$confirmed_email->{c_id}","int04","1");
        #save verified email
        $db->updater("contentadr","c_id","$confirmed_email->{c_id}","txt32","$confirmed_email->{txt08}");
	print FILE "confirmed_email: $confirmed_email->{c_id} update because confirm_code:$confirm_code\n" if($debug);

	#after mailAck, delete all douple adr with no mailAck and no invoices
	my $ctadr = $db->collect_ct4rel3("contentadr","","","ilike","txt08","$confirmed_email->{txt08}","","","");
	foreach my $aid (keys(%$ctadr)){
	  if(!$ctadr->{$aid}->{int04}){
	    my $ctctt = $db->get_content6("contenttrans","int10",$ctadr->{$aid}->{c_id});
	    $db->delete_content("contentadr",$ctadr->{$aid}->{c_id}) if(!$ctctt->{c_id});	
	    print FILE "c_id $ctadr->{$aid}->{c_id} $confirmed_email->{txt08} delete because of dopplel\n" if($debug);
	  }
	}
      }
    }
    #confirm sms
    if($confirm_smscode){
      my $authref = {
                table           => "contentadr",
                fetch           => "one",
                template_id     => "202",
                c_id            => $R::confirm_userid,
		txt34		=> "ilike::%$confirm_smscode",
      };
      my $confirmed_sms = $dbt->fetch_record($dbh,$authref);

      #($api_return,$users_sharee) = $apif->auth_verify($q,"",$R::confirm_smscode);
      if($confirmed_sms->{c_id}){
      	$db->updater("contentadr","c_id","$confirmed_sms->{c_id}","int13","1");
	#save verified telnr
      	$db->updater("contentadr","c_id","$confirmed_sms->{c_id}","txt33","$confirmed_sms->{txt07}");
	print FILE "confirmed_sms: $confirmed_sms->{c_id} update because confirm_smscode:$confirm_smscode\n" if($debug);
      }
    }

    ($api_return,$users_sharee) = $apif->auth_verify($q,"",$users_sharee->{c_id}) if($users_sharee->{c_id});

    if($users_sharee->{int12} != 2 && $users_sharee->{int04} && $users_sharee->{int13}){
      my $row = $db->updater("contentadr","c_id","$users_sharee->{c_id}","int12","0");#Vde 
      $dbt->update_operatorsloop($varenv{dbname},$users_sharee->{c_id},"update");
     }else{
      my $field = "int13";
      $field = "int04" if(!$users_sharee->{int04});
      print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_3}?cum=3$session_and\&failure=$field#top");
      exit 0;
    }

    if(!$coo){
	print FILE "c_id: $users_sharee->{c_id} empty auth because of no cookie\n" if($debug);
        $users_sharee = { c_id => 0 };
    }
    close(FILE) if($debug);
    if($users_sharee->{c_id} && $users_sharee->{int04} && $users_sharee->{int13}){
      print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_3}?confirm_success=1");
      exit 0;
    }
  }#end confirm

  if($R::email && $R::sharee_edit =~ /password_forgotten/){
    	my $hw_id = unpack ('H*', pack('Nc', time, $$ % 0xff));#old $co
	$tk->send_password($R::email,$hw_id,$aowner);
  }

   #redirections 
   if($users_sharee->{c_id} && ($path =~ /$varenv{mandant}\/$varenv{profile}/ || $path =~ /$varenv{mandant}\/Account/)){

    if($R::sharee_edit =~ /save_account/){
     $returnwww =~ s/::/=/g if($returnwww && $returnwww =~ /success::\w+/);

     if((!$users_sharee->{int14}) || ($users_sharee->{txt31} && $tpl->{tpl_order} =~ /$users_sharee->{txt31}/)){
     #failure redirect should do the delete job
      print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_1}?failure=$users_sharee->{txt31}$session_and#top");
      exit 0;
     }
     elsif(!$users_sharee->{int03}){
      print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_1_5}$session");
      exit 0;
     }
     elsif($payable_check && (!$users_sharee->{int04} || !$users_sharee->{int13})){
      print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_3}?cum=2$session_and\&$returnwww");
      exit 0;
     }
     elsif($users_sharee->{int03} && (($users_sharee->{int03} == 1 && $users_sharee->{ct_name} !~ /\w{2}-\d+/) || ($users_sharee->{int03} == 2 && length($users_sharee->{ct_name}) < 19))){
      print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_2}$session");
      exit 0;
     }
     elsif($payable_check){
      print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_3}?cum=2$session_and\&$returnwww");
      exit 0;
     }
    }elsif($path =~ /$varenv{mandant}\/$varenv{profile}/){
     if((!$users_sharee->{int14}) || ($users_sharee->{txt31} && $tpl->{tpl_order} =~ /$users_sharee->{txt31}/)){
      print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_1}?failure=$users_sharee->{txt31}$session_and#top");
      exit 0;
     }elsif(!$payable_check){
      print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_1_5}$session");
      exit 0;
     }else{
      print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_3}?cum=2$session_and\&$returnwww");
      exit 0;
     }  
    }
   }elsif($users_sharee->{c_id} && ($path =~ /$varenv{mandant}\/Anmelden|$varenv{mandant}\/$varenv{profile}/)){
    if(!$users_sharee->{int14}){
      print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_1}?cum=2$session_and\&$returnwww");
      exit 0;
    }else{
      print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_3}?cum=2$session_and\&$returnwww");
      exit 0;
    }
   }
   ###
 }

 my $nodev = $db->get_node("$view_post","$lang");
 #Printpreview
 if($view =~ /Printpreview/){
  require "Mod/Printpreview.pm";
  &Printpreview::printpre();
  exit 0;
 }
 elsif($view =~ /PDFGenerator/){
  require "Mod/PDFGenerator.pm";
  &PDFGenerator::printpre();
  exit 0;
 }

 #else global REDIRECT. Availability check for redirect
 #elsif(!$nodev->{main_id} || ($nodev->{main_id} == 100 && $R::rel_edit eq "save_content")){
   #print redirect("$varenv{wwwhost}$session");
   #exit 0;
 #}


 #CSVout
 if($R::rel_edit && $R::rel_edit =~ /XLSout/){
  $users_dms = $dbt->select_users($dbh,$users_dms->{u_id});
  if(-f "$dbt->{copri_conf}->{basedir}/pdf/$users_dms->{owner}-$users_dms->{time4csv}.xls"){
   print $q->redirect(-uri=>"$varenv{metahost}/pdf/$users_dms->{owner}-$users_dms->{time4csv}.xls", -type=>"application/octet-stream", -target=>'_blank');
   exit 0;
  }
 }

 my $dyn_css = "";
 if(1==1){
  my $background = "";
  $background = "$varenv{metahost}/img/$varenv{background_image}" if($varenv{background_image});
  $dyn_css = "
  html,body {
   background-image:url('$background');
   background-repeat: $varenv{background_repeat};
   background-position: right bottom;
   background-attachment:fixed;
   background-size:$varenv{background_size};
   font-family: $varenv{font_family};
   font-size: $varenv{font_size};
   line-height: $varenv{line_height};
   margin: 0; padding: 0;
   text-align:$varenv{background_align};

   }\n";
 }


 my $url = "$varenv{wwwhost}/$path";
 #my $onload="onLoad();";
 my $onload="";

 my $local_style = "$varenv{metahost}/$dbt->{shareeapp_conf}->{local_style}";
 my $jquery = "";#"$varenv{metahost}/$dbt->{shareeapp_conf}->{jquery}";
 my $jquery_ui = "";
 my $style_jquery_ui = "";
 my $js_bootstrap = "$varenv{metahost}/$dbt->{shareeapp_conf}->{js_bootstrap}";
 my $style_bootstrap = "$varenv{metahost}/$dbt->{shareeapp_conf}->{style_bootstrap}";
 my $jquery_resize = "";

 if($varenv{wwwhost} =~ /shareedms/){
    $local_style = "$varenv{metahost}/$dbt->{shareedms_conf}->{local_style}";
    $jquery = "$varenv{metahost}/$dbt->{shareedms_conf}->{jquery}";
    $jquery_ui = "$varenv{metahost}/$dbt->{shareedms_conf}->{jquery_ui}";
    $style_jquery_ui = "$varenv{metahost}/$dbt->{shareedms_conf}->{style_jquery_ui}";
    $js_bootstrap = "$varenv{metahost}/$dbt->{shareedms_conf}->{js_bootstrap}";
    $style_bootstrap = "$varenv{metahost}/$dbt->{shareedms_conf}->{style_bootstrap}";
    $jquery_resize = "$varenv{metahost}/$dbt->{shareedms_conf}->{jquery_resize}";
    $dyn_js = "\$(function() {
         \$( '#datepicker1' ).datepicker({ dateFormat: 'dd.mm.yy', dayNamesMin: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'], monthNames: ['Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember']});
         \$( '#datepicker2' ).datepicker({ dateFormat: 'dd.mm.yy', dayNamesMin: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'], monthNames: ['Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember']});
         \$( '#datepicker3' ).datepicker({ dateFormat: 'dd.mm.yy', dayNamesMin: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'], monthNames: ['Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember']});
        });";
 }

 my $base_uri = "true";
 my $title = "$varenv{syshost} ";
 $title .= $dbt->{primary}->{$varenv{dbname}}->{pprefix} if($dbt->{primary}->{$varenv{dbname}}->{pprefix});
 $title .= $dbt->{operator}->{$varenv{dbname}}->{project} if($dbt->{operator}->{$varenv{dbname}}->{project});
 $title .= " " . $dbt->{operator}->{$varenv{dbname}}->{oprefix} if($dbt->{operator}->{$varenv{dbname}}->{oprefix});
 $title .= $dbt->{website}->{$varenv{syshost}}->{project} if($dbt->{website}->{$varenv{syshost}}->{project});
 $title .= " DEVEL $varenv{dbname}" if($dbt->{copri_conf}->{stage} eq "test");
 my $html5 = $q->start_html(-title=>"$title",
			-lang=>'de',
                        -onload=>"$onload",
			-oncontextmenu=>"$oncontextmenu",
			#-id=>"page-top", 
			#'-data-spy'=>"scroll", 
			#'-data-target'=>".navbar-fixed-top",
			-encoding=>"$html_charset",
			-base=>"$base_uri",
			-target=>"",
			-head=>[
				Link({
				-rel=>'shortcut icon', 
				-type=>'image/x-icon', 
				-href=>"$varenv{metahost}/css/favicon.ico"
				})
				],
			-meta=>{
				'viewport'=>"width=device-width,initial-scale=1,user-scalable=yes",
 			        'author'=>"Rainer Gümpelein",
				'publisher'=>"TeilRad GmbH",
				'copyright'=>"TeilRad GmbH",
				'keywords'=>"",
				'description'=>"sharee.bike Mietradmanagementsystem"
				},
			-script=>[
			 	{-language=>'JAVASCRIPT',
                                -src=>"$jquery"},
			 	{-language=>'JAVASCRIPT',
                                -src=>"$jquery_ui"},
			 	{-language=>'JAVASCRIPT',
                                -src=>"$jquery_resize"},
				{-language=>'JAVASCRIPT',
				-src=>"$varenv{metahost}/$dbt->{copri_conf}->{jsscript}"},
				{-language=>'JAVASCRIPT',
                                -code=>"$dyn_js"}
				],
			-STYLE=>{
				-code=>"$dyn_css",
				-src=>[
				"$local_style",
				"$style_bootstrap",
				"$varenv{metahost}/$dbt->{copri_conf}->{style_bootstrap_icons}",
				"$style_jquery_ui"
				],
				-verbatim=>"\@import url(\"$local_style\");",
				-media=>'screen'
				}
			);
 # CGI.pm doesn't support HTML5 DTD; replace the one it puts in.
 $html5 =~ s{<!DOCTYPE.*?>}{<!DOCTYPE html>}s;
 $html5 =~ s{<html.*?>}{<html lang='de'>}s;
 print $html5;

 my $sharee_agb_text = {};
  $sharee_agb_text->{ct_name} = "AGB";
  $sharee_agb_text->{txt01} = "<iframe src='$varenv{wwwhost}/site/agb.html' style='width:100%;height:8000px;border:none;' scrolling='auto'></iframe>";

  my $sharee_privacy_text = {};
  $sharee_privacy_text->{ct_name} = "Datenschutzhinweise";
  $sharee_privacy_text->{txt01} = "<iframe src='$varenv{wwwhost}/site/privacy_2.html' style='width:100%;height:8000px;border:none;' scrolling='auto'></iframe>";


print <<EOF

<div class="modal fade" id="sharee_agb" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">$sharee_agb_text->{ct_name}</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="text-default">$sharee_agb_text->{txt01}</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

 <!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#sharee_agb">
  Launch demo modal
</button>

EOF
;


 print "<script src='$js_bootstrap'></script>\n";

 print $q->end_html;
 return Apache2::Const::OK;
}
1;
