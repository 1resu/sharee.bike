package NodeEdit;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
#
use strict;
use warnings;
use CGI::Carp qw(fatalsToBrowser);
use CGI ':standard';
use Lib::Config;
use Mod::Buttons;
use Mod::Libenz;
use Mod::Libenzdb;
use Mod::Basework;
use Mod::DBtank;
use Mod::Relation;
use Data::Dumper;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub admin_tpl(){
 my $node_meta = shift;
 my $users_dms = shift;
 my $u_group = shift;
 my $return = shift;
 my $superu_id = shift || "";

 #print Dumper($node_meta);exit;

  my $q = new CGI;
  my @keywords = $q->param;
  my $cf = new Config;
  my $lb = new Libenz;
  my $db = new Libenzdb;
  my $bw = new Basework;
  my $dbt = new DBtank;
  my $but = new Buttons;
  my $relform = new Relation;
  my %varenv = $cf->envonline();
  my $script = $q->script_name();
  my $path = $q->path_info();
  $path = "$script" . "$path";
  my $lang = "de";

  #$path =~ s/\/login|\/user|\/manager|\/admin//;
  my $coo = $q->cookie(-name=>'domcookie');
  my %ib = $but->ibuttons();
  my $dbh = "";

  my $u_name = $node_meta->{owner};
  $u_name = $dbt->sys_username($dbh,$node_meta->{owner});

  my $change = $lb->time4de($node_meta->{change},"1") if($node_meta->{change});
  my $subid = $1 if($node_meta->{main_id} =~ /^(\d)/);

  #Node templates
  my $tpl_id = 97;#Standard for CMS
  #Selektierbare Tabellen Layout templates
  my $tpl_lists4selection;

  #if($node_meta->{ct_table} eq "content" || $node_meta->{ct_table} eq "contentpos"){
  if($node_meta->{ct_table} eq "content"){
    $tpl_id = 100;
    if($node_meta->{template_id} eq "205"){
      #$tpl_id = 102;
      $tpl_lists4selection =  205;
    }elsif($node_meta->{template_id} eq "225"){
      $tpl_lists4selection =  225;
    }elsif($node_meta->{template_id} eq "210"){
      $tpl_lists4selection =  210;
    }else{
      $tpl_lists4selection = "224,226,227,228,229";
    }
  }elsif($node_meta->{ct_table} eq "contenttrans"){
    $tpl_id = "101";
    $tpl_lists4selection =  "218";
  }elsif($node_meta->{ct_table} eq "contenttranspos"){
    $tpl_id = "101";
    $tpl_lists4selection =  "221,222";
  }elsif($node_meta->{ct_table} eq "contentadr" || $node_meta->{ct_table} eq "contentadrpos"){
    $tpl_id = "101";
    $tpl_lists4selection =  "401,402,403";
  }  

  #Defaults to tpl_id=97
  my $tpl = $db->get_tpl($tpl_id);
  my @tpl_order = ("");
  @tpl_order = split /,/,$tpl->{tpl_order};


  my @_templates; 
  my $tpl_all = $db->collect_tpl($tpl_lists4selection);
  foreach my $id (sort {$tpl_all->{$a}->{tpl_name} cmp $tpl_all->{$b}->{tpl_name}} keys (%$tpl_all)){
   push (@_templates, "$id:$tpl_all->{$id}->{tpl_name}") if($tpl_all->{$id}->{tpl_name} !~ /dummy/);
  }

  #reading mounted project data-dir
  #Test: mount /dev/sda2 data/Projekte
  my @_projectdir;
  #my $dir = "$varenv{data}/Projekte";
  my $dir = "$varenv{data}/fileserver";
  if( -d "$dir"){
      opendir(DIR, "$dir");
        foreach(sort(readdir(DIR))){
	  push (@_projectdir, "$_");
        }
      closedir DIR;
  }

  #content container
  print "<div id='Container_cms'>$superu_id";

  print $q->hidden(-name=>'last_node_name', -value=>"$node_meta->{node_name}");
  print $q->hidden(-name=>'owner', -value=>"$users_dms->{u_id}");
  print $q->hidden(-name=>'parent_id', -value=>"$node_meta->{parent_id}");
  print $q->hidden(-name=>'main_id', -value=>"$node_meta->{main_id}");
  #print $q->hidden(-name=>'template_id', -value=>"$node_meta->{template_id}");
  print $q->hidden(-name=>'mode', -value=>"admin");

  print $q->start_table({-style=>'margin-top:6px;', -border=>'0', -width=>'100%', -align=>'left', -cellpadding=>'3', -cellspacing=>'3'});
  print $q->Tr();
  if(1==1){
    if(($users_dms->{u_id} > 0) || ($u_group eq "admin")){
       print "<td style='background:$varenv{background_color2};margin:1px 0;padding:2px;' colspan='2' nowrap>\n";
	#print "<td style='margin:1px 0;padding:0px;' colspan='2' nowrap>\n";
       	 print $but->singlesubmit7("rel_edit","save_relation","$ib{save_relation}","","modal_position(xpos,ypos)");
	 #look at dialog4menu, it seems better. vise a verse
	   print $q->span({-style=>'margin:0 0.2em;'},"  ");
       	   print $but->singlesubmit7("rel_edit","delete_relation","$ib{delete_relation}","","modal_position(xpos,ypos)");
	  print $q->span({-style=>'margin:0 0.2em;'},"  ");
       	  print $but->singlesubmit7("rel_edit","new_relation","$ib{new_relation}","","modal_position(xpos,ypos)");
	  #$R::new_submenu = 1 if($subid < 4);
	  print $q->span({ -style=>'margin:4px 0 0 4px;'},$but->checkbox("1","new_submenu","$R::new_submenu"),"Submenu") if($subid < 3 && $node_meta->{template_id} !~ /205/);#no submenue if bikes);
	print $q->span({-style=>'margin-left:5em; font-size:0.91em;'}, "$u_name / $change") if($u_name);
       print "</td>\n";

       print $q->Tr();
       print $q->td({-colspan=>'2',-style=>'padding:5px;font-style:italic;'},"Path: $path");
       foreach (@tpl_order){
        my ($key,$des,$isize) = split /=/,$_;
	$des .= " ($key)" if($users_dms->{u_id} eq $varenv{superu_id});
	#print "$key, $des, $isize|";
        $isize = "20" if(!$isize);

	if($key =~ /node_public|footer/){
	  print $q->Tr();
	  print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
	  print $q->td({-class=>'content1_cms'},$but->checkbox("1","$key","$node_meta->{$key}")),"\n";
	}elsif($key =~ /txt01/ && $isize =~ /select/){
    	  print $q->Tr();
    	  print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
	  print $q->td({-class=>'content1_cms'},$but->selector("$key","250px","$node_meta->{$key}",@_projectdir)),"\n";
	
	}elsif($key =~ /tpl_name/){
    	  print $q->Tr();
    	  print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
	  print $q->td({-class=>'content1_cms'},$but->selector("template_id","200px",$node_meta->{template_id},@_templates)),"\n";
	}elsif($key eq "txt01" && $isize eq "select"){
          my @_valxx = ("");
	  #TODO
	  #foreach my $rid (sort { $bike_nodes->{$a}->{node_name} cmp $bike_nodes->{$b}->{node_name} } keys (%$bike_nodes)){
	  #  push (@_valxx, "$bike_nodes->{$rid}->{main_id}:$bike_nodes->{$rid}->{node_name} - $bike_nodes->{$rid}->{main_id}");
	  #}
          print $q->Tr();
          print $q->td({-class=>'left_italic_cms',-colspan=>'1'},"$des");
          print $q->td({-class=>'content1_cms',-colspan=>'1'},$but->selector_class("$key","eselect","width:250px;",$node_meta->{$key},@_valxx));
	}elsif($key =~ /node_name/){
    	  print $q->Tr();
    	  print $q->td({-class=>'left_italic_cms'},"$des");
          print $q->td({-class=>'content1_cms'},$q->textfield(-class=>'etxt',-name=>"$key",-default=>"$node_meta->{$key}",-override=>'1',-size=>"$isize",-maxlength=>120)),"\n";
	}elsif($key =~ /n_sort/){
    	  print $q->Tr();
    	  print $q->td({-class=>'left_italic_cms'},"$des");
          print $q->td({-class=>'content1_cms'},$q->textfield(-class=>'etxt',-name=>"$key",-default=>"$node_meta->{$key}",-override=>'1',-size=>"$isize",-maxlength=>120)),"\n";
	}
       }
    }
  }else{
    print $q->div({-style=>'text-align:left;margin:0.5em;padding:1em;background-color:white;'},"Im \"admin\" Modus können die Gruppen-Ordner verwaltet werden.<br />--> Dazu erst einen Gruppen-Ordner öffnen!");
  }

  print $q->end_table;

 my $debug;
 $debug = "(ct_table: $node_meta->{ct_table} | main_id: $node_meta->{main_id} | tpl_id: $node_meta->{template_id} | type_id: $node_meta->{type_id} | energy_id: $node_meta->{energy_id}" if($users_dms->{u_id} == $dbt->{copri_conf}->{superu_id});
  print $q->div({-style=>'position:absolute;bottom:2%;right:2%;z-index:10;font-size:13px;'},"$debug"),"\n";
 print "</div>";
}
1;
