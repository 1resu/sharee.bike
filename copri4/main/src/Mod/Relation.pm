package Relation;
#
##
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
use strict;
use warnings;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI ':standard';
use Mod::Buttons;
use Mod::Libenzdb;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
  my ($source,$main_id,$ct_name,$barcode,$u_id,$lang,$set_style,$colspan_left,$colspan_right) = @_;

  my $q = new CGI;
  my $db = new Libenzdb;
  my $but = new Buttons;
  my $users = $db->select_users($u_id);
  my %ib = $but->ibuttons();

  #collect recursive waren
  my $node_mandant = $db->get_node2($users->{fullurl},"$source",$lang);
  my $main_ids = $db->collect_noderec($node_mandant->{main_id},$lang,"nothing");
  my $parent_nodes = $db->collect_node($node_mandant->{main_id},$lang);
  my $nodes = $db->collect_node2("$main_ids");


  my $setpart_main_id=$main_id;
  my @_waren_rel;
  foreach my $pid (sort { lc($parent_nodes->{$a}->{node_name}) cmp lc($parent_nodes->{$b}->{node_name}) }  keys (%$parent_nodes)){
    my $parent_name = "/";
    $parent_name .= $parent_nodes->{$pid}->{node_name} if($parent_nodes->{$pid}->{node_name});
    push (@_waren_rel, "$pid:$parent_name") if($parent_name && "$parent_name" !~ /root/);
    foreach my $id (sort { lc($nodes->{$a}->{node_name}) cmp lc($nodes->{$b}->{node_name}) }  keys (%$nodes)){
      if($nodes->{$id}->{parent_id} eq $parent_nodes->{$pid}->{main_id}){
        push (@_waren_rel, "$id:$parent_name/$nodes->{$id}->{node_name}") if($nodes->{$id}->{node_name});
      }
    }  
  }

  print $q->hidden(-name=>'main_id', -value=>"$main_id");  
  print $q->Tr();
  print "<td colspan='9' style='font-size:1em;border: solid #dcd77f;'>\n";
  print $q->start_table({-style=>'',-border=>'0',-align=>'left', -cellpadding=>'0', -cellspacing=>'0'});

   print $q->Tr();
   print $q->td({-class=>'tdescr2',-colspan=>"$colspan_left",-style=>"$set_style"},"Kontext Kopie",$but->singlesubmit2("rel_edit","context_copy_content","$ib{context_copy_content}","$set_style"));
   if($barcode){
    $ct_name = "" if($barcode == $ct_name);
    print $q->hidden(-name=>'barcode', -value=>"$barcode");
    print $q->hidden(-name=>'empty_rel_id', -value=>"1");
    print $q->td({-class=>'tdval2',-colspan=>"$colspan_right",-style=>"$set_style"}, "Teilenummer:",$q->textfield(-class=>'etxt',-name=>"other_ct_name",-default=>"$ct_name",-size=>12,-maxlength=>15), "&nbsp; Barcode (intern): $barcode\n");
   }else{
    print $q->td({-class=>'tdval2',-colspan=>"$colspan_right",-style=>"$set_style;color:red"}, "Der Barcode (intern) muss vorhanden sein\n");
   }
   #print $q->Tr();
   #print $q->td({-class=>'tdescr2',-colspan=>"$colspan_left",-style=>"$set_style"},"Kopieren",$but->singlesubmit2("rel_edit","copy_content","$ib{copy_content}","$set_style"));
   #print $q->hidden(-name=>'', -value=>"");
   #print $q->td({-class=>'tdval2',-colspan=>"$colspan_right",-style=>"$set_style"}, "Barcode:",$q->textfield(-class=>'etxt',-name=>"new_barcode",-default=>"$barcode",-size=>12,-maxlength=>15), "\n");
   print $q->Tr();
   print $q->td({-class=>'tdescr2',-colspan=>"$colspan_left",-style=>"$set_style"},"Verschieben",$but->singlesubmit2("rel_edit","move_content","$ib{move_content}","$set_style"));
   print $q->td({-class=>'tdval2',-colspan=>"$colspan_right",-style=>"$set_style"}, "Gruppen-Ordner:",$but->selector("setpart_main_id","250px",$setpart_main_id,@_waren_rel),"\n");
  print $q->end_table;
  print "</td>"; 

}
1;
