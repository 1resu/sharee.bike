package Modalbox;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#

use strict;
use warnings;

use CGI::Cookie ();
use CGI ':standard';
use CGI::Carp qw(fatalsToBrowser);
use Scalar::Util qw(looks_like_number);
use Lib::Config;
use Mod::Buttons;
use Mod::Libenzdb;
use Mod::DBtank;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

sub mobox(){
 my ($node_meta,$users_dms,$mode,$return) = @_;
 my $q = new CGI;
 my $cf = new Config;
 my $but = new Buttons;
 my $db = new Libenzdb;
 my $dbt = new DBtank;
 $q->import_names('R');
 my @keywords = $q->param;
 my %varenv = $cf->envonline();
 my $script = $q->script_name();
 my $path = $q->path_info();
 my $lang = "de";
 my $dbh = "";

 #TODO do it BrowserTab save
 #my $kind_of_trans = $R::kind_of_trans || $users->{kind_of_trans} || "";
 #my $c_id4trans = $R::c_id4trans || $users->{c_id4trans} || "";
 #my $tpl_id4trans = $R::tpl_id4trans || $users->{tpl_id4trans} || "";
 #my $kind_of_trans = $users_dms->{kind_of_trans} || "";
 #my $c_id4trans = $users_dms->{c_id4trans} || "";
 #my $tpl_id4trans = $users_dms->{tpl_id4trans} || "";

 if($users_dms->{kind_of_trans} && looks_like_number($users_dms->{c_id4trans}) && looks_like_number($users_dms->{tpl_id4trans})){
  my $width = $node_meta->{tpl_width} || "990";
  my $bg_color = "white";
  my $bg_color2 = $varenv{term_active_color} || "";

  my $table = "contenttrans";
  my $ctt = $db->get_content1($table,$users_dms->{c_id4trans});
  my ($address_wc,$table_wc) = split(/\./,$ctt->{int04});
  my $rows = $address_wc + $table_wc;

  my $height = "600";
  if($varenv{orga} eq "dms"){
    $rows = $rows - 0;
  }else{
    $rows = $rows - 10;
  }
  $height += $rows * 15 if($rows > 0);
  my $debug;
  $debug = "(c_id: $users_dms->{c_id4trans} | tpl_id: $users_dms->{tpl_id4trans})" if($users_dms->{u_id} == $dbt->{copri_conf}->{superu_id});

print<<EOF
  <style>
  .ui-dialog .ui-dialog-content {
    background: $bg_color;
  }
  .ui-dialog > .ui-widget-header {
    color:$varenv{color};
    font-weight:normal;
    border:1px solid $bg_color2;
    background: $bg_color2;
  }
  .ui-widget-overlay {
    background: #666 url("$varenv{metahost}/js/jquery-ui-1.12.1/images/ui-bg_diagonals-thick_20_666666_40x40.png") 50% 50% repeat;
    opacity: .5;
    filter: Alpha(Opacity=50);
  }
  </style>

  <script>
        \$(function() {
                \$( "#dialog-form" )
                        .css("background-color","$bg_color")
			.dialog({
				height: $height,
				width: $width,
				//show: { effect: 'drop', direction: "up" } ,
				closeOnEscape: true,
				zIndex: 1010000000000,
				modal: true
			});
		\$('.ui-widget-overlay').click(function() {
		   \$(".ui-dialog-titlebar-close").trigger('click');
		});
		\$('.ui-dialog').css('z-index',9999);
        });
  </script>
EOF
;
  
  print "<div id='dialog-form' style='text-align:center;margin:0;padding:2px;max-width:1200px;' title='Terminal – $users_dms->{kind_of_trans} $debug'>";

  if($users_dms->{kind_of_trans} && looks_like_number($users_dms->{c_id4trans}) && looks_like_number($users_dms->{tpl_id4trans})){
   if($table eq "contenttrans" && $varenv{orga} eq "dms"){
    require "Tpl/Address3.pm";
    &Address3::tpl($node_meta,$users_dms,$return);
   }
  }else{
   print $q->div({-style=>"padding:0.1em;margin:0em;background-color:white;font-size:0.81em;"}, "Ein neues Formular kann im COPRI Hauptfenster geöffnet werden (Code: $users_dms->{kind_of_trans} && $users_dms->{c_id4trans} && $users_dms->{tpl_id4trans})",
  "\n");
  }
 
  print "</div>\n";
 }
}
1;
