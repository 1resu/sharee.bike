package PDFGenerator;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
#
use strict;
use warnings;

use POSIX;
use CGI::Cookie ();
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI ':standard';
use Lib::Config;
use Mod::Buttons;
use Mod::Libenz;
use Mod::Libenzdb;
use Mod::DBtank;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

sub printpre(){
 my $q = new CGI;
 my $cf = new Config;
 my $lb = new Libenz;
 my $db = new Libenzdb;
 my $dbt = new DBtank;
 my $but = new Buttons;
 my %varenv = $cf->envonline();
 my $path = $q->path_info();
 my $script = $q->script_name();
 my $user_agent = $q->user_agent();
 my %ib = $but->ibuttons();
 my $today = strftime("%d.%m.%Y %H:%M",localtime(time));
 my $lang="de";

 my $printer_id=$R::printer_id;
 my $mandant_main_id = $R::mandant_main_id;
 my $id=$R::id;

 my $payone_conf = $dbt->{operator}->{$varenv{dbname}}->{payone_conf} || $dbt->{payone_conf};
 my $ctf = $db->get_content1("contentuser","$mandant_main_id");
 my $ctrel = $db->get_ctrel("contentadr","",$lang,"",$id);

 print $q->start_html(-title=>'PDFGenerator');

 my $width="1799px";
if(1==1){
 print "<style type='text/css'><!--
body,html {
   background-color: white;
   margin: 1px; padding: 0;
   border: 0px solid black;
   text-align: left;
   font-family: Arial,sans-serif;
}
.Oline {
  border-bottom-width:1px;
  border-bottom-style:solid;
  border-color:black;
  margin:0.5em 0;
}
.tdname1 {
  font-size:1em;
  font-weight:bold;
  padding:0.1em;
  text-align: right;
}
.tdval1 {
  font-size:1em;
  font-weight:normal;
  padding:0.1em 1em;
  text-align: left;
}
.content1 {
  font-size:1em;
  font-weight:normal;
  padding:1em 0.1em;
  text-align: left;
}
//--></style>","\n";
}

  print $q->div({-style=>'font-size:1.2em;font-weight:bold;padding:1em 0.1em;'},"SEPA-Lastschriftmandat"),"\n";
  print $q->div({-class=>'Oline'},""),"\n";

  print $q->start_table({-border=>'0', -cellpadding=>'3', -cellspacing=>'0'}),"\n";
  print $q->Tr(),"\n";
  print $q->td({-class=>'tdname1'}, "Zahlungsempfänger"),"\n";
  print $q->td({-class=>'tdval1'}, "$payone_conf->{sepa_creditor_name}"),"\n";
  print $q->Tr(),"\n";
  print $q->td({-class=>'tdname1'}, "Gläubiger-Identifikationsnummer"),"\n";
  print $q->td({-class=>'tdval1'}, "$payone_conf->{sepa_creditor_id}"),"\n";
  print $q->Tr(),"\n";
  print $q->td({-class=>'tdname1'}, "Mandatsreferenz"),"\n";
  print $q->td({-class=>'tdval1'}, "$ctrel->{ct_name}"),"\n";
  print $q->end_table,"\n";

  print $q->div({-class=>'Oline'},""),"\n";
  print $q->div({-class=>'content1'},"Ich ermächtige den Zahlungsempfänger, Zahlungen von meinem Konto mittels Lastschrift einzuziehen. Zugleich weise ich mein Kreditinstitut an, die von dem Zahlungsempfänger auf mein Konto gezogenen Lastschriften einzulösen."),"\n";
  print $q->div({-class=>'content1'},"Hinweis: Ich kann innerhalb von acht Wochen, beginnend mit dem Belastungsdatum, die Erstattung des belasteten Betrages verlangen. Es gelten dabei die mit meinem Kreditinstitut vereinbarten Bedingungen."),"\n";
  print $q->div({-class=>'Oline'},""),"\n";
 
  print $q->start_table({-border=>'0',-cellpadding=>'3', -cellspacing=>'0'}),"\n";
  print $q->Tr(),"\n";
  print $q->td({-class=>'tdname1'}, "Name des Zahlungspflichtigen"),"\n";
  print $q->td({-class=>'tdval1'}, "$ctrel->{txt01}"),"\n";
  print $q->Tr(),"\n";
  print $q->td({-class=>'tdname1'}, "Straße Nr."),"\n";
  print $q->td({-class=>'tdval1'}, "$ctrel->{txt03}"),"\n";
  print $q->Tr(),"\n";
  print $q->td({-class=>'tdname1'}, "PLZ Ort"),"\n";
  print $q->td({-class=>'tdval1'}, "$ctrel->{txt06}"),"\n";
  print $q->Tr(),"\n";
  print $q->td({-class=>'tdname1'}, "Land"),"\n";
  print $q->td({-class=>'tdval1'}, "$ctrel->{txt10}"),"\n";
  print $q->Tr(),"\n";
  print $q->td({-class=>'tdname1'}, "E-Mail"),"\n";
  print $q->td({-class=>'tdval1'}, "$ctrel->{txt08}"),"\n";
  print $q->Tr(),"\n";
  print $q->td({-class=>'tdname1'}, "Swift BIC"),"\n";
  print $q->td({-class=>'tdval1'}, "$ctrel->{txt23}"),"\n";
  print $q->Tr(),"\n";
  print $q->td({-class=>'tdname1'}, "Bankkontonummer - IBAN"),"\n";
  print $q->td({-class=>'tdval1'}, "$ctrel->{txt22}"),"\n";

  print $q->end_table,"\n";

  print $q->div({-class=>'content1'},""),"\n";
  print $q->div({-class=>'content1'},"$ctrel->{txt06}, $today, $ctrel->{txt01}"),"\n";

 print $q->end_html;
 return Apache2::Const::OK;
}
1;

