package Mod::FileOut;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
use strict;
use warnings FATAL => 'all';

use CGI::Cookie ();
use CGI ':standard';
use Apache2::RequestUtil ();
use Apache2::RequestIO ();
use Apache2::Const -compile => qw(OK);

use File::Path qw(make_path remove_tree);
use File::Copy;
use File::Copy::Recursive qw(fcopy rcopy dircopy fmove rmove dirmove);

use Lib::Config;


sub handler {
  my $r = shift;
  my $q = new CGI();
  my $coo = $q->cookie(-name=>'domcookie') || $q->param('sessionid') || 'zvzfcxd578';

  my $cf = new Config;
  my %varenv = $cf->envonline();
  my $filesuff = $q->param('file');

  if($filesuff =~ /\.pdf/){
    rcopy("$varenv{basedir}/pdfinvoice/$filesuff","$varenv{basedir}/cache/$coo/$filesuff");
  }elsif($filesuff =~ /got_last\.csv/i){
    rcopy("$varenv{basedir}/ftp/SWK_codes/$filesuff","$varenv{basedir}/cache/$coo/$filesuff");
  }elsif($filesuff =~ /Statistik_\d{4}-\d{2}\.zip/i){
    rcopy("$varenv{basedir}/csv/$filesuff","$varenv{basedir}/cache/$coo/$filesuff");
  }
  print $q->redirect(-uri=>"$varenv{metahost}/cache/$coo/$filesuff", -type=>"application/octet-stream");
  #remove_tree("$varenv{basedir}/cache/$coo");
  #
  #my $file = $varenv{data} . "/" . $filesuff;
  #my $status = $r->sendfile($file);

  return Apache2::Const::OK;

}

1;
