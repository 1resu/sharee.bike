package MailTransport;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
# new emailing module
#
#perl -cw src/Mod/MailTransport.pm
#use lib "/var/www/copri4/shareeapp-operator/src";
#
use strict;
use warnings;
use POSIX;
use CGI ':standard';
use IO::All;
use Net::SMTP;
use MIME::Base64 qw( encode_base64 );
use Try::Tiny;
use Config::General;

use Mod::Basework;
use Mod::DBtank;
use Data::Dumper;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

my $q = new CGI;
my $bw = new Basework;
my $dbt = new DBtank;

sub mail_connect {
 my $self = shift;
 my $sendref = shift;

 my $mailx_file = "/var/www/copri4/shareeconf/mailx.cfg";
 my $conf = Config::General->new($mailx_file);
 my %mailxconf = $conf->getall;

 my $smtp = Net::SMTP->new($mailxconf{$sendref->{mailxcfg}}->{mail_gateway},
                        Port => 465,
                       Hello => 'sharee.bike',
                       Timeout => 30,
                       Debug   => 0,
                       SSL     => 1,
                      );

 $smtp->auth($mailxconf{$sendref->{mailxcfg}}->{sasl_username},$mailxconf{$sendref->{mailxcfg}}->{sasl_password});
 $smtp->mail($mailxconf{$sendref->{mailxcfg}}->{mail_from});
 
 return ($smtp,\%mailxconf);
}

sub mail_transport(){
  my $self = shift;
  my $smtp = shift;
  my $mailxconf = shift;
  my $sendref = shift;

  #print Dumper($smtp);
  my $ret = 1;
  my $now_dt = strftime "%Y-%m-%d %H:%M:%S", localtime;
  open(EMA, ">> $dbt->{copri_conf}->{logdir}/mailtransport.log");

  my $mail_from = $sendref->{mail_from} || $mailxconf->{$sendref->{mailxcfg}}->{mail_from};
  my $mail_to = $sendref->{mail_to} || $mailxconf->{$sendref->{mailxcfg}}->{mail_to};
  my $subject = $sendref->{subject};

  my ($buf, $picture);
  my $boundary = 'frontier';
  my $pdfpath = "$dbt->{copri_conf}->{basedir}/$sendref->{syshost}/pdf";
  my $attachBinaryFile = "";
  my $filesize = 0;

  if(-f "$pdfpath/$sendref->{attachment}"){
   $filesize = -s "$pdfpath/$sendref->{attachment}";
   $attachBinaryFile = "$sendref->{attachment}";
  }

  if($dbt->{copri_conf}->{stage} ne "live"){
   $mail_to = $mailxconf->{$sendref->{mailxcfg}}->{mail_to};
   $subject = "* offline Test * " . $sendref->{subject};
  }

  print EMA "\n$now_dt, start mailing to: $mail_to | subject: $subject\n";
  print EMA "attachment: $pdfpath/$sendref->{attachment} | filesize: $filesize\n";

  my $html = "<html><head><title>$subject</title></head><body style='text-align:left;border:0px solid silver;padding:15px;margin:2%;width:90%;'>\n";
  $html .= "<div>$sendref->{message}</div>\n";
  $html .= "<div>$sendref->{signature}</div>\n" if($sendref->{signature});
  $html .= "</body></html>";


  $bw->log("Trying send_mail by $0",$mail_to,"");

  if(ref($sendref) eq "HASH"){


 	if ($smtp->to($mail_to)) {
  	  $smtp->data();
  	  $smtp->datasend("To: $mail_to\n");
  	  $smtp->datasend("Subject: $subject\n");
  	  $smtp->datasend("MIME-Version: 1.0\n");
	  $smtp->datasend("Content-type: multipart/mixed;\n\tboundary=\"$boundary\"\n");
	  $smtp->datasend("\n");

	  $smtp->datasend("--$boundary\n");
	  #$smtp->datasend("Content-type: text/plain\n");
	  #$smtp->datasend("\nToday\'s files are attached:\n\nHave a nice day! :)\n");

	  $smtp->datasend("Content-Type: text/html; charset=UTF-8 \n\n");
	  $smtp->datasend($html);
	  $smtp->datasend("\n");

	  if($attachBinaryFile){
	    $smtp->datasend("--$boundary\n");
	    #$smtp->datasend("Content-Type: image/jpeg; name=\"$attachBinaryFile\"\n");
	    $smtp->datasend("Content-Type: application/pdf; name=\"$attachBinaryFile\"\n");

	    $smtp->datasend("Content-Transfer-Encoding: base64\n");
	    $smtp->datasend("Content-Disposition: attachment; filename=\"$attachBinaryFile\"\n");
	    $smtp->datasend("\n");

	   open(DAT, "$pdfpath/$attachBinaryFile") || die("Could not open $pdfpath/$attachBinaryFile");
   	    binmode(DAT);
   	    local $/=undef;
   	    while (read(DAT, $picture, 4104)) {
      	    $buf = &encode_base64( $picture );
     	     $smtp->datasend($buf);
   	    }
	    $smtp->datasend("\n");
	   close(DAT);
	  }

	  $smtp->datasend("--$boundary\r\n\r");
  	  $smtp->dataend();
	  $smtp->quit;
 	} else {
  	  print EMA $smtp->message();
 	}

	sleep 1;
	$ret = $?;
  }


  print EMA "done mailing with state: $?\n";
  print EMA "\n\n";
  close EMA;

  $bw->log("Success send_mail","","");
  return $ret;
}#end mail_send

1;
