package Mod::ajax_post;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#

use strict;
use warnings;
use POSIX;
use CGI ':standard';
use Apache2::Const -compile => qw(OK );
use Scalar::Util qw(looks_like_number);
use Lib::Config;
use Mod::Libenzdb;
use Mod::Libenz;

sub handler {
 my ($r) = @_;
 my $q = new CGI();
 $q->import_names('R');
 my @keywords = $q->param;
 my $cf = new Config;
 my $db = new Libenzdb;
 my $lb = new Libenz;
 my %varenv = $cf->envonline();
 my $now_dt = strftime "%Y-%m-%d %H:%M:%S", localtime;
 my $lang = "de";
 print $q->header(-charset=>'utf-8');

 open(FILE,">>$varenv{logdir}/ajax_post.log");
   print FILE "\n\nPost-start: $now_dt\n";
   foreach my $xkey (@keywords){
     my @val;
     my $matrix_val;
     my $tpl_id;
     my $xval;
     if($xkey =~ /^matrix_/){
      @val = $q->param($xkey);
      foreach(@val){
	my ($key,$des,$size,$interval,$service_type) = split /=/,$_;
        #print FILE "$key,$des,$size,$interval,$service_type\n";

	my $interval = 0; 
	$interval = $q->param("interval_$key") if(looks_like_number($q->param("interval_$key")));
	my $service_type = 0;
	$service_type = $q->param("servicetype_$key") if(looks_like_number($q->param("servicetype_$key")));
	print FILE "\n$key --> interval: $interval | service_type: $service_type\n";
	$matrix_val .= "$_=$interval=$service_type,";# if($interval =~ /(\d+)/);
      }
      $matrix_val =~ s/,$//;
      #print FILE "$xkey: @val\n";
      #print FILE "matrix_val: $matrix_val\n";
     #}elsif($_ =~ /^ckid_rel|ckid_main|edit_main|post_request/){
     }else{
      my @xval = $q->param($xkey);
      foreach(@xval){
        $xval .= "$_," if($_);
      }
      $xval =~ s/,$//;
      print FILE "$xkey: $xval (owner: $R::owner)\n";
     }

     if($xkey =~ /c_id4trans/ && looks_like_number($R::owner) && looks_like_number($xval)){
       $db->update_ajaxes("users","","","","c_id4trans","$xval","$R::owner");
     }

     if($xkey =~ /matrix_users/ && $R::owner){
       $matrix_val = "" if($R::u_group =~ /supervisor/);
       $db->update_ajaxes("users","","","","checkboxes","$matrix_val","$R::owner");
     }
     if($xkey =~ /matrix_template/ && $R::template_id){
       $db->update_ajaxes("template","tpl_id","=","$R::template_id","tpl_order","$matrix_val","");
       print FILE "update_ajaxes(\"template\",\"tpl_id\",\"=\",\"$R::template_id\",\"tpl_order\",\"$matrix_val\")\n";
     }
     if($xkey =~ /k9itemList_(\d+)/){
       $db->update_ajaxes("template","tpl_id","=","$1","tpl_order","$xval","");
     }

     #because of downward compatibility
     if($xkey =~ /^ckid_rel/ && $R::owner){
	$db->update_ajaxes("users","","","","checked4dragg","$xval","$R::owner");
     }
     if($xkey =~ /^ckid_main/ && $R::owner){
	$db->update_ajaxes("users","","","","view_list","$xval","$R::owner");
     }
     if($xkey =~ /^edit_main/ && $R::owner){
	$db->update_ajaxes("users","","","","edit_list","$xval","$R::owner");
     }


   }
 close(FILE);

 return Apache2::Const::OK;
}
1;
