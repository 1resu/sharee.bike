package Mod::ajax_json;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
use warnings;
use strict;
use POSIX;
use CGI;
use Apache2::Const -compile => qw(OK );
use DBI;
use JSON;
use Mod::Libenzdb;
use Lib::Config;
use Data::Dumper;

sub handler {
 my ($r) = @_;
 my $q = new CGI;
 $q->import_names('R');
 my $db = new Libenzdb;
 my $cf = new Config;
 my %varenv = $cf->envonline();
 my $now_dt = strftime "%Y-%m-%d %H:%M:%S", localtime;
 my $lang="de";
 my $main_id = $q->param('main_id');
 my $table = $q->param('table');
 my $search = $q->param('term');
 my @keywords = $q->param;
 my @query_output = ();
 my $debug=1;

 print $q->header(-type => "application/json", -charset => "utf-8");

 open(FILE,">>/var/log/copri-bike/ajax_json.log") if($debug);
 print FILE "$now_dt|$main_id\n" if($debug);

 foreach(@keywords){
      my @val = $q->param($_);
      my $valxx = $q->escapeHTML("@val");
      if($_ eq "c_idadr"){
      	$search = $valxx;
      }
      print FILE "$_: $valxx\n" if($debug);
 }


 if($table eq "content"){
     my $sth = $db->search_json("$table","$lang","$search","$main_id");
     while ( my $row = $sth->fetchrow_hashref ){
      push @query_output, $row;
     }
     print FILE Dumper(\@query_output) if($debug);
     print JSON::to_json(\@query_output);
 }elsif($table eq "contentadr"){
     my $sth = $db->search_jsonadr("$table","$lang","$search","$main_id","");
     while ( my $row = $sth->fetchrow_hashref ){
      push @query_output, $row;
     }
     print FILE Dumper(\@query_output) if($debug);
     print JSON::to_json(\@query_output);
 }
 close(FILE) if($debug);

 return Apache2::Const::OK;
}
1;

