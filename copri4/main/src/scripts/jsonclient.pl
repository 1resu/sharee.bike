#!/usr/bin/perl 

# Client
use JSON::RPC::Client;
 
my $client = new JSON::RPC::Client;
 
my $uri = 'http://www.example.com/jsonrpc/Test';
my $obj = {
   method  => 'sum', # or 'MyApp.sum'
   params  => [10, 20],
};
 
my $res = $client->call( $uri, $obj )
 
if($res){
   if ($res->is_error) {
       print "Error : ", $res->error_message;
   }
   else {
       print $res->result;
   }
}
else {
   print $client->status_line;
}
