#!/usr/bin/perl
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
#On this forking script, nothing will saved on script, else by execute APIsigclient methodes
#
# command line syntax
# sudo su www-data -c "./src/scripts/sig_client.pl shareeapp-sx 'todo' 'user_id' 'sig bikeId' 'pos_id'"
#
# sudo su www-data -c "./src/scripts/sig_client.pl shareeapp-sx bikes_available"
#
# sudo su www-data -c "./src/scripts/sig_client.pl shareeapp-sx reserve 1842 '380116b5-0522-43da-ab66-477744a731a3' ''"
# sudo su www-data -c "./src/scripts/sig_client.pl shareeapp-sx rental 1842 '380116b5-0522-43da-ab66-477744a731a3' ''"
#
use vars qw($syshost);

BEGIN {
  $syshost = $ARGV[0] || die 'syshost not defined';
}

use lib "/var/www/copri-bike/$syshost/src";
use strict;
use warnings;
use POSIX;
use CGI;
use Scalar::Util qw(looks_like_number);
use Mod::DBtank;
use Mod::APIsigclient;
use Data::Dumper;

my $q = new CGI;
my $cf = new Config;
my $dbt = new DBtank;
my $si = new APIsigclient;
my $bw = new Basework;
my $lang = "de";
my $now_dt = strftime "%Y-%m-%d %H:%M:%S", localtime;
my %varenv = $cf->envonline();
my $dbh = "";

my $todo = $ARGV[1] || "";
my $user_id = $ARGV[2] || "";
my $bike_id = $ARGV[3] || "";#from content.txt22
my $pos_id = $ARGV[4] || "";#from contenttranspos.c_id

 open(FILE,">>$varenv{logdir}/APIsigclient.log");
 print FILE "\n1. *** $now_dt 'sig_client fork with todo:$todo|user_id:$user_id|bike_id:$bike_id|pos_id:$pos_id' \n";

#get user and bike in copri by id
my $apref = {
        table           => "contentadr",
        fetch           => "one",
        template_id     => "202",
        c_id            => "$user_id",
        };
my $ctadr = { c_id => 0 };
$ctadr = $dbt->fetch_record($dbh,$apref) if($user_id =~ /^\d+$/ && $user_id > 0);

my $ct_bike = { txt22 => "$bike_id" };

my $posref = {
        table           => "contenttranspos",
        fetch           => "one",
        ca_id         => "$ctadr->{c_id}",
	int10           => "IN::('3','2')",
	c_id	=> "$pos_id",
        };
my $ctpos = { c_id => 0 };
$ctpos = $dbt->fetch_tablerecord($dbh,$posref) if($user_id =~ /^\d+$/ && $user_id > 0 && $pos_id && $pos_id =~ /^\d+$/);

print FILE "fetched from DB ctadr:$ctadr->{c_id}|ctpos:$ctpos->{c_id}\n";

#sig => copri key mapping
#bike id 	=> txt22
#reservationId 	=> txt10
#renatlId	=> txt11


#only for tests
if($todo eq "stations_available"){
 $q->param(-name=>'request',-value=>"stations_available");
 $ctadr = { c_id => 0 };
 my $return = $si->sig_available($q,\%varenv,$ctadr);
 #print $0 . Dumper($return) . "\n";
}

#only for tests
if($todo eq "bikes_available"){
 $q->param(-name=>'request',-value=>"bikes_available");
 #$ctadr = { c_id => 0 };
 my $return = $si->sig_available($q,\%varenv,$ctadr);
 #print $0 . Dumper($return) . "\n";
}

#only for tests. build in methode
elsif($todo eq "reserve"){
 #$ctadr = { c_id => 1842 };
 #$ct_bike = { txt22 => "380116b5-0522-43da-ab66-477744a731a3" };
 my $return = $si->sig_booking(\%varenv,$todo,$ctadr,$ct_bike,$ctpos);
 #print $0 . Dumper($return) . "\n";
}

#live! Fork rental request with bike "id"
elsif($todo eq "rental" && $ctadr->{c_id} > 0){
 #$ctadr = { c_id => 1842 };
 #$ct_bike = { txt22 => "380116b5-0522-43da-ab66-477744a731a3" };
 my $return = $si->sig_booking(\%varenv,$todo,$ctadr,$ct_bike,$ctpos);
 #print $0 . Dumper($return) . "\n";
}

#live! Fork unlock by lock_state=unlocking request with bike "id"
elsif($todo eq "unlock" && $ctadr->{c_id} > 0){
 #$ctadr = { c_id => 1842 };
 #$ct_bike = { txt22 => "380116b5-0522-43da-ab66-477744a731a3" };
 my $return = $si->sig_unlock(\%varenv,$todo,$ctadr,$ct_bike,$ctpos);
 #print $0 . Dumper($return) . "\n";
}

#live! Fork rental_end request with "rentalId"
elsif($todo eq "rental_end" && $ctadr->{c_id} > 0 && $ctpos->{txt11}){
 my $return = $si->sig_booking(\%varenv,$todo,$ctadr,$ct_bike,$ctpos);
 #print $0 . Dumper($return) . "\n";
}

#only for tests. build in methode
elsif($todo eq "rentals_running" && $ctadr->{c_id} > 0 && $ctpos->{txt22}){
 my $return = $si->sig_booking(\%varenv,$todo,$ctadr,$ct_bike,$ctpos);
 #print $0 . Dumper($return) . "\n";
}

else{
  print FILE "failure::sig_client request not defined: $todo && $ctadr->{c_id} > 0 && $ctpos->{txt22}\n";
  close(FILE);
  exit 1;
}

close(FILE);

