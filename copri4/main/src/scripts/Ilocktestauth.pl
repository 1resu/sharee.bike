#!/usr/bin/perl 
#
#Autor ragu@gnu-systems.de
#
#Ilockit authentify test
#
#sudo su www-data -c "./src/scripts/Ilocktestauth.pl shareeapp_operator 1003"
#
#
use vars qw($syshost);

BEGIN {
  $syshost = $ARGV[0] || exit 1;
}

use lib "/var/www/copri-bike/$syshost/src";

my $dbname = $ARGV[1] || "";
my $bike = $ARGV[2] || "";

use strict;
use warnings;
use POSIX;
use CGI ':standard';
use Lib::Config;
use Data::Dumper;

my $q = new CGI;
my $cf = new Config;
my %varenv = $cf->envonline("$syshost");
my $now_dt = strftime "%Y-%m-%d %H:%M:%S", localtime;


my @K_select = `cd /var/www/copri-bike/$varenv{syshost}/src/scripts && export CLASSPATH='.:/usr/share/java:/usr/share/java/postgresql.jar' && java Ilockauth $dbname $bike`;
  foreach(@K_select){
    my ($K_key,$K_val) = split(/ = /, $_);
    $K_val =~ s/\n//g;
    print "$K_key: $K_val\n";
  }

my $receiver_usb = "abcd";
if($receiver_usb =~ /(\w)(\w)$/){
 my $a = ord($1);
 my $b = ord($2);
 print "$1|$2" . "\n";
 print "$a|$b" . "\n";
 my $receiver_id   = sprintf( "%x", $a ) . sprintf( "%x", $b );
 print $receiver_id . "\n";
}

