#!/usr/bin/perl 
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
#SMS message if 24h,48h,72h occupied
#sudo su www-data -c "./src/scripts/sms_message.pl shareedms-fr01 '24h_occupied' '' ''"
#
#SMS message locking_progress after 60sec
#sudo su www-data -c "./src/scripts/sms_message.pl shareeapp-operator locking_progress '0179xxxx372' $pos_id"
#
#
use vars qw($syshost);

BEGIN {
  $syshost = $ARGV[0] || die 'syshost not defined';
}

use lib "/var/www/copri-bike/$syshost/src";

use strict;
use warnings;
use POSIX;
use DateTime;
use DateTime::Format::Pg;
use Data::Dumper;

use Mod::DBtank;
use Mod::SMSTransport;

my $dbt = new DBtank;
my $smstrans = new SMSTransport;
 
my $todo = $ARGV[1] || die 'todo not defined';
my $phone = $ARGV[2] || "";
my $pos_id = $ARGV[3] || "";
my $dbh = "";

#operator contact
   my $pref_cc = {
        table           => "contentuser",
        fetch           => "one",
        template_id     => "197",
        c_id    => 1,
   };
my $record_cc = { c_id => 0 };
$record_cc = $dbt->fetch_record($dbh,$pref_cc);
my $hotline = "";
$record_cc->{txt07} =~ s/\s//g;
$hotline = "Hotline $record_cc->{txt01} $record_cc->{txt07}";


#SMS message if 24h,48h,72h occupied
if($todo eq "24h_occupied"){
  #select booking pos if state=occupied and start_time > 24h
  my $search = {
        table  => "contenttranspos",
        int10  => 3,
	int34  => "null",#if not staff
	start_time_interval => "(now() - interval '1 day')",
  };
  my $update_pos = {
        table  => "contenttranspos",
  };

  my $dt1 = DateTime->now(time_zone => "Europe/Berlin");

  my $cttpos = { c_id => 0 };
  $cttpos = $dbt->collect_transpos($dbh,$search);

  foreach my $pid (sort { $cttpos->{$b}->{end_time} cmp $cttpos->{$a}->{end_time} } keys(%$cttpos)){
    if($cttpos->{$pid}->{int10} == 3){
	my $dt2 = "";
	$dt2 = DateTime::Format::Pg->parse_datetime($cttpos->{$pid}->{start_time});
	my $dt2_24h_occupied = $dt2->add( days => 1 );
	$dt2 = DateTime::Format::Pg->parse_datetime($cttpos->{$pid}->{start_time});
	my $dt2_48h_occupied = $dt2->add( days => 2 );
	$dt2 = DateTime::Format::Pg->parse_datetime($cttpos->{$pid}->{start_time});
	my $dt2_72h_occupied = $dt2->add( days => 3 );
	#print $dt1 . ">=" . $dt2_72h_occupied . "|" .  $dt1 . ">=" . $dt2_48h_occupied . "|" . $dt1 . ">=" . $dt2_24h_occupied . "\n";
	if($dt2){
	 if($dt1 >= $dt2_72h_occupied){
	  if($cttpos->{$pid}->{int33} != 3){ 
	    $todo = "72h_occupied";
	    print $dt1 . ">=" . $dt2_72h_occupied . "|$todo,$hotline,$cttpos->{$pid}->{phone},$cttpos->{$pid}->{ct_name}\n";
	    $smstrans->sms_message($todo,$hotline,$cttpos->{$pid}->{phone},$cttpos->{$pid}->{ct_name});
	    $dbt->update_one($dbh,$update_pos,"int33=3",$cttpos->{$pid}->{c_id});
	  }
	 }elsif($dt1 >= $dt2_48h_occupied){
	  if($cttpos->{$pid}->{int33} != 2){ 
	    $todo = "48h_occupied";
	    print $dt1 . ">=" . $dt2_48h_occupied . "|$todo,$hotline,$cttpos->{$pid}->{phone},$cttpos->{$pid}->{ct_name}\n";
            $smstrans->sms_message($todo,$hotline,$cttpos->{$pid}->{phone},$cttpos->{$pid}->{ct_name});
	    $dbt->update_one($dbh,$update_pos,"int33=2",$cttpos->{$pid}->{c_id});
	  }
  	 }elsif($dt1 >= $dt2_24h_occupied){
	  if($cttpos->{$pid}->{int33} != 1){ 
	    $todo = "24h_occupied";
	    print $dt1 . ">=" . $dt2_24h_occupied . "|$todo,$hotline,$cttpos->{$pid}->{phone},$cttpos->{$pid}->{ct_name}\n";
            $smstrans->sms_message($todo,$hotline,$cttpos->{$pid}->{phone},$cttpos->{$pid}->{ct_name});
	    $dbt->update_one($dbh,$update_pos,"int33=1",$cttpos->{$pid}->{c_id});
	  }
	 }
	}
	sleep 1;
    }
  }
}#end

#SMS message locking_progress after 60sec
if($todo eq "locking_progress" && $phone && $pos_id){
  sleep 60;
  #select booking pos if lock_state=locking still set
  my $booking_pos = {
        table           => "contenttranspos",
        fetch           => "one",
        c_id         => "$pos_id",
  };

  my $booking = { c_id => 0 };
  $booking = $dbt->fetch_tablerecord($dbh,$booking_pos);
  if($booking->{int20} == 3){
	$smstrans->sms_message($todo,$hotline,$phone,$booking->{ct_name});
  }
}#end

#SMS message fraud to me
if($todo eq "fraud_rental" && $pos_id){
  my $sms_to = "$dbt->{copri_conf}->{sms_to}";

  my $booking_pos = {
        table           => "contenttranspos",
        fetch           => "one",
        c_id         => "$pos_id",
  };

  my $booking = { c_id => 0 };
  $booking = $dbt->fetch_tablerecord($dbh,$booking_pos);

  $smstrans->sms_message($todo,"",$sms_to,$booking->{ct_name});

}#end
