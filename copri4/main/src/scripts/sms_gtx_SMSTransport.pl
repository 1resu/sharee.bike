#!/usr/bin/perl 
#
#sudo su www-data -c "./src/scripts/sms_gtx_SMSTransport.pl shareeapp-operator '+491799xxxx72'"
#
use vars qw($syshost);

BEGIN {
  $syshost = $ARGV[0] || die 'syshost not defined';
}

use lib "/var/www/copri4/$syshost/src";

use strict;
use warnings;
use POSIX;
use Mod::SMSTransport;

my $smstrans = new SMSTransport;

my $phone = $ARGV[1] || die 'phone not defined';
my $adrhash = { 
	txt07 => $phone,
	txt34 => "nureinlangertestdigest",
};

my $ret_json = $smstrans->sms_ack_digest($adrhash);
