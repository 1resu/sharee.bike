#!/usr/bin/perl 
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
#For pdf pw files
#"pdftotext -upw FksjxxxxxxfmC1h Teilrad_ISI_PLUS_BB01207.pdf"
#:1,$ s/^\n//g
#:1,$ s/\n/;/g
#:1,$ s/;C2-/\rC2-/g
#:1,$ s/;QR-/\rQR-/g
#
#
#check if UTF-8
#file Teilrad_ISI_PLUS_BB01207.txt
#iconv -t utf-8 -f iso-8859-1 Ilockitkeys_110520.csv -o Ilockitkeys_110520_utf8.cs
#
#sudo su www-data -c "./src/scripts/Ilockit_CSV2DB.pl shareedms-kn ../shareeconf/smartlock-keys/Ilockit-16-BB01742_1v2.csv"
#
#do it twice to set hex-key and bike-nr!
#
use vars qw($syshost);

BEGIN {
  $syshost = $ARGV[0] || exit 1;
}

use lib "/var/www/copri-bike/$syshost/src";

my $csv_file = $ARGV[1] || "";

use strict;
use warnings;
use POSIX;
use DBI;
use CGI ':standard';
use Lib::Config;
use Mod::DBtank;
use Scalar::Util qw(looks_like_number);
use Data::Dumper;

my $q = new CGI;
my $cf = new Config;
my %varenv = $cf->envonline("$syshost");
print "$varenv{wwwhost}\n";
my $dbt = new DBtank;
my $lang = "de";
my $now_dt = strftime "%Y-%m-%d %H:%M:%S", localtime;
my $dbh = "";
my $main_id = 300103;#TODO Flotten ID
my $template_id = 205;

###
#Start loop payone log and update userid ##################################
####
#reading CSV
 my $dbh_csv = DBI->connect("DBI:CSV:");
 $dbh_csv->{'csv_tables'}->{'ilockitcsv'} = {
                  'eol' => "\n",
                  'sep_char' => ";",
                  'quote_char' => undef,
		  #'quote_char' => "\"",
                  'escape_char' => undef,
                  'file' => $csv_file,
		  #'col_names' => ["serialnr","ilockit_id","ilockit_key","c1"]
		  'col_names' => ["serialnr","ilockit_id","ilockit_key","c1","bikeid","rahmen","stationid","bike_name"]
 };

 my $sth = $dbh_csv->prepare("SELECT * FROM ilockitcsv where serialnr like 'C2-%'");
 my $rc = $sth->execute();
 my $csv = $sth->fetchall_hashref("serialnr");

 my $i=0;
 foreach my $id (sort { $csv->{$a}->{serialnr} cmp $csv->{$b}->{serialnr} } keys (%$csv)){
   $i++;

   if($csv->{$id}->{serialnr}){
     $csv->{$id}->{ilockit_id} =~ s/\+/\-/;
     $csv->{$id}->{c1} =~ s/R/red /g;
     $csv->{$id}->{c1} =~ s/B/blue /g;
     $csv->{$id}->{c1} =~ s/G/green /g;
     $csv->{$id}->{c1} =~ s/\s$//;#empty last space
     $csv->{$id}->{bikeid} =~ s/THU//;
     $csv->{$id}->{stationid} =~ s/THU//;

     print "$i) $csv->{$id}->{serialnr}|\n";

     my $ct = &select_content($csv->{$id});

      	     my $data = {
                        table => "content",
			main_id => "$main_id",
			template_id => "$template_id",
                        txt22 => "$csv->{$id}->{serialnr}",
                        txt18 => "$csv->{$id}->{ilockit_id}",
                        byte01 => "\\x$csv->{$id}->{ilockit_key}",
                        barcode => "$csv->{$id}->{bikeid}",
			int04 => "$csv->{$id}->{stationid}",#station 
			txt01 => "$csv->{$id}->{bike_name}",
			txt11 => "$csv->{$id}->{rahmen}",
                        txt23 => "$csv->{$id}->{c1}",
			int10 => "1",#available
			int20 => "1",#locked
                        int11 => "2",#Ilockit 
                        txt04 => "", #Sonstiges
			mtime => "now()",
			owner => "1842",
                };

     if(ref($ct) eq "HASH" && $ct->{c_id} && ref($data) eq "HASH" && $csv->{$id}->{serialnr}){
	print "UPDATE $csv->{$id}->{serialnr} (c_id:$ct->{c_id})\n";
	$data->{barcode} = $ct->{c_id} if(!$ct->{barcode});
	$dbt->update_record($dbh,$data, { c_id => $ct->{c_id} });
	print Dumper($data) . "\n";
     }elsif(ref($data) eq "HASH" && $csv->{$id}->{serialnr}){
	print "INSERT $csv->{$id}->{serialnr}\n";
	$dbt->insert_contentoid($dbh,$data);
	print Dumper($data) . "\n";
     }else{
	print "nothing todo\n";
     }
  }
}

#content check if still exists
sub select_content{
 my $ct_hash = shift;
 my $fetch = {
                table           => "content",
		main_id		=> "$main_id",
		template_id	=> "$template_id",
		fetch           => "one",
        };
 if(ref($ct_hash) eq "HASH" && $ct_hash->{serialnr}){
   $fetch = { %$fetch , txt22 => "=::$ct_hash->{serialnr}" };
   #$fetch = { %$fetch , barcode => "<::100" };
   #$fetch = { %$fetch , int10 => "!=::3" };
   #$fetch = { %$fetch , int11 => "!=::2" };
 }

 my $ct = $dbt->fetch_record($dbh,$fetch);
 return $ct;
}


