import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Arrays;



public class Ilockauth {
   public static void main( String args[] ) {
      String db_id = args[0];
      String bike_id = args[1];
      Connection c = null;
      Statement stmt = null;
      try {
         Class.forName("org.postgresql.Driver");
         c = DriverManager
            .getConnection("jdbc:postgresql://localhost:5432/" + db_id,
            "postgres", "");
         c.setAutoCommit(false);
         //System.out.println("Opened database successfully");

         stmt = c.createStatement();
         ResultSet rs = stmt.executeQuery( "SELECT * FROM content where barcode=" + bike_id + " and int11=2;" );
         while ( rs.next() ) {

	    byte[] randomNum = new byte[16];

            byte[] K_int = rs.getBytes("byte01");

	    //example by byte[]
	    //byte[] K_int = {(byte) 0xA7, (byte) 0x99, (byte) 0x91, 0x12, 0x49, (byte) 0xE0, (byte) 0xBA, (byte) 0xD5, (byte) 0xF2, 0x48, (byte) 0x8D, (byte) 0xC1, 0x7E, 0x28, 0x2D, (byte) 0xD0};

            System.out.println( "K_int = " + Arrays.toString(K_int));
	    Ilockkeygen.generateKey(randomNum, K_int);

         }
         rs.close();
         stmt.close();
         c.close();
      } catch ( Exception e ) {
         System.err.println( e.getClass().getName()+": "+ e.getMessage() );
         System.exit(0);
      }
      //System.out.println("Operation done successfully");
   }
}
