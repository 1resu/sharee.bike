package Mlogic;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
use strict;
use warnings;
use CGI::Carp qw(fatalsToBrowser);
use CGI ':standard';
use Mod::DBtank;

use Data::Dumper;
my $q = new CGI;
my $dbt = new DBtank;


sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
 my $self = shift;
 my $node_meta = shift;
 my $users_dms = shift;
 my $mode = shift || "";
 my $varenv = shift;
 my $users_sharee = shift || "";
 my $return = shift || "";

 my $session="";
 my $session_and="";
 if($R::sessionid && length($R::sessionid) > 20 && !$q->cookie(-name=>'domcookie')){
        $session = "?sessionid=$R::sessionid";
        $session_and = "&sessionid=$R::sessionid";
 }
 my $bgcolor1 = $dbt->{primary}->{$varenv->{dbname}}->{bgcolor1};

 if($users_sharee->{c_id} && $R::sharee_edit ne "delete_account2" && ($users_sharee->{c_id} eq $varenv->{superu_id} || $dbt->{copri_conf}->{stage} eq "test" || $users_sharee->{txt08} eq "sigo\@sharee.bike")){
  my $coo = $q->cookie('domcookie') || $q->param('sessionid') || "";

  #my $api_test = "sharee_fr01"; my $bike="FR1538";
  #my $api_test = "sharee_fr01"; my $bike="FR1005";#E-Lastenrad (bike_group=300101, bike_node=300102)
  #my $api_test = "sharee_fr01"; my $bike="FR4781";#Tracking and BVB test
  my $api_test = "sharee_kn"; my $bike="KN1011";
  #my $api_test = "sharee_wue"; my $bike="WUE5525";
  #my $api_test = "sharee_sx"; my $bike="S3X1001";
  #my $api_test = "sharee_ren"; my $bike="REN2";
  
  print $q->div({-style=>'float:right;text-align:right;height:25px;padding:6px 15px;background-color:white'},$q->a({-style=>"background-color:#ffffff;color:#$bgcolor1;", -href=>"$varenv->{metahost}/src/scripts/tests/index.pl?sessionid=$coo\&api_test=$api_test\&bike=$bike", -target=>'_blank'}," [ tests --> $api_test ] "),"$users_sharee->{txt08}",$q->a({-style=>"color:#$bgcolor1;", -href=>"logout_sharee$session"},"logout")),"\n";

 }

  print "<div class='container'>\n";
    print "<div id='Contenttxt' style='margin-top:20px;padding-bottom:350px;'>\n";
      $self->tplselect($node_meta,$users_dms,$mode,$varenv,$users_sharee,$return);
    print "</div>\n";
  print "</div>\n";
  #print "<script src='$varenv->{js_bootstrap}'></script>\n";

}

#2021-05-05 changed to Mlogic
sub tplselect(){
 my $self = shift;
 my $node_meta = shift;
 my $users_dms = shift || "";
 my $mode = shift || "";
 my $varenv = shift;
 my $users_sharee = shift || "";
 my $return = shift || "";

 my $u_id = $users_dms->{u_id} || "";
 my $sort = "";
 my $lang = "de";
 my $tpl_id = $node_meta->{tpl_id};

 if($node_meta->{main_id}){
 if($tpl_id == 2){
   require "Tpl/Anmelden.pm";
   &Anmelden::tpl($node_meta,$users_dms,$mode,$varenv,$users_sharee,$return);

  }elsif($tpl_id == 302 || $tpl_id == 302008 || $tpl_id == 302004){#Adresse
   require "Tpl/FormEdit.pm";
   &FormEdit::tpl($node_meta,$users_dms,$mode,$varenv,$users_sharee,$return);
  }elsif($tpl_id == 308){
   require "Tpl/PayoneSelect.pm";
   &PayoneSelect::tpl($node_meta,$users_dms,$mode,$varenv,$users_sharee,$return);
  }elsif($tpl_id == 197){
   require "Tpl/Contact.pm";
   &Contact::tpl($node_meta,$users_dms,$mode,$varenv,$users_sharee,$return);
  #}elsif($tpl_id == 1 || $tpl_id == 3){
  # require "Tpl/Listing.pm";
  # &Listing::tpl($node_meta,$users_dms,$mode,$varenv,$users_sharee,$return);
  }
 }
  my $debug = "Mlogic --> (users_sharee->{c_id}: $users_sharee->{c_id} | ct_table: $node_meta->{ct_table} | parent_id: $node_meta->{parent_id} | main_id: $node_meta->{main_id} | tpl_id: $node_meta->{tpl_id} | u_id: $u_id | mode: $mode)";
 print $q->div({-style=>'position:fixed;bottom:0%;right:2%;z-index:10;font-size:13px;'},"$debug"),"\n" if($users_sharee->{c_id} eq $varenv->{superu_id});

 if($return && $return =~ /failure/){
   require "Mod/Failure.pm";
   &Failure::tpl("",$u_id,"","","","",$return);
 }
}

1;


