package Config;

#Deprecated config file
#Please use shareeconf/*

use strict;
use warnings;
use CGI;
use Config::General;
my $q = new CGI;

sub new { 
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self; 
}


sub envonline(){
 my $self = shift;

 my $server_type = "operator";
 my $server_key = "sharee_operator";

 my $globalconf_file = "/var/www/copri4/shareeconf/global.cfg";
 my $conf = Config::General->new($globalconf_file);
 my %globalconf = $conf->getall;

   my $basedir = "$globalconf{copri_conf}->{basedir}/$globalconf{$server_type}{$server_key}->{dir_app}";
   my $metahost = $globalconf{$server_type}{$server_key}->{operatorApp1};
   my %varenv = (    
		dbname => $globalconf{$server_type}{$server_key}{database}->{dbname},
		dbuser => $globalconf{$server_type}{$server_key}{database}->{user},
		dbpassw => $globalconf{$server_type}{$server_key}{database}->{passwd},
                dbhost => $globalconf{$server_type}{$server_key}{database}->{host},
                syshost => $globalconf{$server_type}{$server_key}->{dir_app},
                wwwhost => $globalconf{$server_type}{$server_key}->{operatorApp},
                praefix => $globalconf{$server_type}{$server_key}{database}->{dbname},
		systype => "sharee",#(azn)
                mandant => $globalconf{shareeapp_conf}->{parent_node},
		start => $globalconf{shareeapp_conf}->{start},
		profile => $globalconf{shareeapp_conf}->{profile},
		accounting_1 => $globalconf{shareeapp_conf}->{accounting_1},
		accounting_1_5 => $globalconf{shareeapp_conf}->{accounting_1_5},
		accounting_2 => $globalconf{shareeapp_conf}->{accounting_2},
		accounting_3 => $globalconf{shareeapp_conf}->{accounting_3},

                superu_id => $globalconf{copri_conf}->{superu_id},
                debug => $globalconf{copri_conf}->{debug},
                logdir => $globalconf{copri_conf}->{logdir},
                live_hostname => $globalconf{$server_type}{$server_key}->{live_hostname},
		metahost=>"$metahost",
		orga => "",
                style_font => "$metahost/img/OfficinaSansITCStd-Book.otf",
                font_family => "OfficinaSansITCStd-Book,Arial",
                background_image => "",
		background_size => "cover",
                background_color => "white",
                background_color2 => "#c7c8ca",
                background_align => "center",
		background_repeat => "no-repeat",
                font_size => "100.01%",
                line_height => "1",
		basedir => "$basedir",
                pdf => "$basedir/pdf",
                pdfinvoice => "$basedir/pdfinvoice",
                data => "$basedir/data",
		xmlfile => "$basedir/xml",
                Anrede => "Anrede,Frau,Herr,Firma",
                Einheit => "Stück,Paar,Satz,Meter,Liter",
		head_logo => "",
		barcode => "",
                printer => "PDF"
                );
   return %varenv;
}

1;

