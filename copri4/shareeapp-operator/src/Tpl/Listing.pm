package Listing;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
use strict;
use warnings;
use CGI ':standard';
use Lib::Config;
use Mod::Buttons;
use Mod::Libenz;
use Mod::Libenzdb;
use Mod::DBtank;
use Encode;
use Data::Dumper;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
 my $node_meta = shift;
 my $users_dms = shift || "";
 my $mode = shift || "";
 my $varenv = shift;
 my $users_sharee = shift || "";
 my $return = shift || "";

 my $q = new CGI;
 my $cf = new Config;
 my $but = new Buttons;
 my $lb = new Libenz;
 my $db = new Libenzdb;
 my $dbt = new DBtank;

 my $lang = "de";
 my $script = $q->script_name();
 my $path_info = $q->path_info();
 my $path = $path_info;
 #with meta_host, 
 if("$varenv->{metahost}"){
   $path = "$script" . "$path_info";
   $script="";
 }
 my $user_agent = $q->user_agent();
 my %ib = $but->ibuttons_arch();
 my @tpl_order = split /,/,$node_meta->{tpl_order};
 my $main_ids = $node_meta->{main_id};
 my @viewsel = split /\//,$1 if($path =~ /^\/(.*)/);

 my $session="";
 my $session_and="";
 if(length($R::sessionid) > 20){
        $session = "?sessionid=$R::sessionid";
        $session_and = "&sessionid=$R::sessionid";
 }

  my $ct4rel = {};
  my $scol = "sort";
  my $sort_updown = "up";
  if($path =~ /NEWS/){
    $scol = "date_time01";
    $sort_updown = "down";
    #$scol = "date_time01";
  }

  my $content2 = "content2";
  my $content_img = "pic-float";
  $ct4rel = $db->collect_ct4rel2("content",$main_ids,$lang,"","","","","",$users_dms->{u_id});

 print "<div id='Contentapp'>\n";

  #Content sequential
  #BIG LOOP content table
  foreach my $id (sort {
    if($sort_updown eq "down"){
        if ($scol =~ /barcode|int|sort/) {
                $ct4rel->{$b}->{$scol} <=> $ct4rel->{$a}->{$scol}
        }else{
                lc($ct4rel->{$b}->{$scol}) cmp lc($ct4rel->{$a}->{$scol})
        }
    }else{
        if ($scol =~ /barcode|int|sort/) {
                $ct4rel->{$a}->{$scol} <=> $ct4rel->{$b}->{$scol}
        }else{
                lc($ct4rel->{$a}->{$scol}) cmp lc($ct4rel->{$b}->{$scol})
        }
    }
  } keys(%$ct4rel)){

   my $j=0;
   my $date_time;
   my $uritxt_key;

   foreach (@tpl_order){
    my ($key,$des,$size) = split /=/,$_;
    $ct4rel->{$id}->{$key} = $q->unescapeHTML("$ct4rel->{$id}->{$key}");
    $ct4rel->{$id}->{$key} = $lb->newline($ct4rel->{$id}->{$key},"","");

    my $datamain_id = $ct4rel->{$id}->{main_id};
    my $dir_main = "$varenv->{data}/$datamain_id/$ct4rel->{$id}->{c_id}";
    my $dir_thumb = "$varenv->{data}/$datamain_id-thumb/$ct4rel->{$id}->{c_id}";
    my $dir_resize = "$varenv->{data}/$datamain_id-resize/$ct4rel->{$id}->{c_id}";

 
    if($key =~ /img/ && $ct4rel->{$id}->{rel_id}){
     #print "<div class=''>\n";
      if( -d "$dir_resize"){
       my @pics = $lb->read_dirfiles($dir_resize,"\.JPG|\.JPEG|\.PNG","file");
       foreach(@pics){
	print $q->img({-id=>"$content_img" ,-src=>"$varenv->{metahost}/data/$datamain_id-resize/$ct4rel->{$id}->{c_id}/$_"}),"\n" if($_ =~ /\w/);
       }
      }
     #print "</div>\n";
     print $q->div({-style=>'clear:both;'},""),"\n";
    }elsif($key =~ /pdf/){
     if( -d "$dir_main"){
       my @pdfs = $lb->read_dirfiles($dir_main,"\.JPG|\.JPEG|\.PNG","file","not");
       foreach(@pdfs){
	print $q->div($q->a({-href=>"$varenv->{wwwhost}/FileOut?file=$datamain_id/$ct4rel->{$id}->{c_id}/$_$session_and", -target=>'_default', -title=>'Download',-type=>'application/octet-stream'}, $q->img({-src=>"$varenv->{metahost}/glyphicons/file-any.png", -style=>'width:50px;'}), "$_")),"\n";

       }
     }


   }elsif($key =~ /date_time/ && $ct4rel->{$id}->{$key} =~ /\d{4}-\d{2}-\d{2}/){
	$date_time = $lb->time4de("$ct4rel->{$id}->{$key}","","Date_to_Text_Long");
	$date_time =~ s/^\w+,\s\w+//;
   }elsif($key =~ /node_name/){
	my $ct_users;
 	$ct_users = $db->get_owner($ct4rel->{$id}->{owner}) if($ct4rel->{$id}->{owner});
	$ct4rel->{$id}->{mtime} = $lb->time4de($ct4rel->{$id}->{mtime},"1") if($ct4rel->{$id}->{mtime});
        print $q->div({-class=>'content2_group'},"Absender: $ct_users->{u_name} | $ct4rel->{$id}->{mtime}"),"\n";
   }elsif($key =~ /ct_name/){
       my $debug;
       if($users_dms->{u_id}){
        print $q->div({-class=>'content_title2',-style=>"background-color:silver;padding:0.2em;"},$q->a({-class=>'editnav',-href=>"$path/manager?node2edit=edit_content\&rel_id=$ct4rel->{$id}->{rel_id}", -title=>"edit content"}, "$ct4rel->{$id}->{$key}"),$debug),"\n";
        print $q->div({-class=>'content_title2'},"$date_time"),"\n" if($date_time);
       }elsif(!$R::sharee_edit){
        print $q->div({-class=>'content_title2'},"$date_time"),"\n" if($date_time);
        print $q->div({-id=>"$ct4rel->{$id}->{c_id}",-class=>'content_title2'},"$ct4rel->{$id}->{$key}"),"\n" if($ct4rel->{$id}->{$key} =~ /[A-Za-z]/);
        print $q->div({-style=>""},"$debug"),"\n";
       }
    }elsif($key =~ /uri(\d+)/ && $ct4rel->{$id}->{$key} =~ /http/){
	$uritxt_key = "txt" . $1;
	print $q->div({-class=>'content_direct2',-style=>'font-size:14px;'},$q->a({-href=>"$ct4rel->{$id}->{$key}"},"zum Projekt")),"\n";
    }elsif($key =~ /txt|int/ && $ct4rel->{$id}->{$key} && "$key" ne "$uritxt_key"){
     if($key =~ /txt/ && $size =~ /area(\d+)/ && $ct4rel->{$id}->{$key}){
              #phone tag
              if($ct4rel->{$id}->{$key} =~ /Telefon|Mobile/){
                $ct4rel->{$id}->{$key} =~ s/([\s0-9-]+)/\<a href=\'tel:00$1\'\>$1\<\/a\>/;
              }
              #email tag with little coding against grabber
              if($ct4rel->{$id}->{$key} =~ /(\w+\@[\w-]+\.de)/){
                $ct4rel->{$id}->{$key} =~ s/(\w+\@[\w-]+\.de)/\<a href=\'mailto:$1\'\>$1\<\/a\>/;
                $ct4rel->{$id}->{$key} =~ s/\@/\&\#64\;/g;
                #$ct4rel->{$id}->{$key} =~ s/\.de/\&\#46\;de/g;
              }
              #Development
              if($ct4rel->{$id}->{$key} =~ /(www\.GNU-Systems\.de)/){
                $ct4rel->{$id}->{$key} =~ s/(www\.[\w-]+\.de)/\<a href=\'http:\/\/$1\' target=\'_blank\'\>$1\<\/a\>/g;
              }
	$ct4rel->{$id}->{$key} =~ s/\\//g;
	print $q->div({-class=>"$content2"}, "$ct4rel->{$id}->{$key}"),"\n";
     }elsif($ct4rel->{$id}->{$key} && $size !~ /checkbox/){
	print $q->div({-class=>"$content2"}, "$ct4rel->{$id}->{$key}"),"\n";
     }
    }
   }
   print $q->div({-class=>"$content2"},"&nbsp;"),"\n";
  }#end if
 print "</div>\n";
}
1;
