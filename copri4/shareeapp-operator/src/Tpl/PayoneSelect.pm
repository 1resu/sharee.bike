package PayoneSelect;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
use strict;
use warnings;
use POSIX;
use CGI ':standard';
use Lib::Config;
use Mod::Buttons;
use Mod::Libenz;
use Mod::Libenzdb;
use Tpl::AccountSubmenu;
use Tpl::PayoneSEPA;
use Tpl::PayoneCCclient;


sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
 my $node_meta = shift;
 my $users_dms = shift || "";
 my $mode = shift || "";
 my $varenv = shift;
 my $users_sharee = shift || "";
 my $return = shift || "";

 my $q = new CGI;
 my $cf = new Config;
 my $lb = new Libenz;
 my $db = new Libenzdb;
 my $but = new Buttons;
 my $submenu = new AccountSubmenu;
 my $paySEPA = new PayoneSEPA;
 my $payCC = new PayoneCCclient;

 $q->import_names('R');
 my @keywords = $q->param;

 my $script = $q->script_name();
 my $path_info = $q->path_info();
 my $path = $path_info;
 #with meta_host, 
 if("$varenv->{metahost}"){
   $path = "$script" . "$path_info";
   $script="";
 }
 my $time = time;
 my $now_dt = strftime "%Y-%m-%d %H:%M", localtime;

 my %ib = $but->ibuttons_arch();
 my @viewsel = split /\//,$1 if($path =~ /^\/(.*)/);

 my $ctrel = $users_sharee if(ref($users_sharee) eq "HASH" && $users_sharee->{c_id});
 my $tpl = $db->get_tpl($node_meta->{tpl_id});
 
  print "<div id='Contentapp'>\n";

  #subMenue--------
  $submenu->tpl($node_meta,$users_dms,$mode,$varenv,$users_sharee,$return);
  #-----------------

  #Selected Payment
  if($ctrel->{int03} == 1 || $ctrel->{int03} == 3){
    $paySEPA->tpl("309",$node_meta,$users_dms,$mode,$varenv,$users_sharee,$return);
  }elsif($ctrel->{int03} == 2){
    $payCC->tpl("310",$node_meta,$users_dms,$mode,$varenv,$users_sharee,$return);
  }

  print "</div>\n";

}
1;
