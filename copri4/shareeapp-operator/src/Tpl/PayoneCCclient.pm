package PayoneCCclient;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
use strict;
use warnings;
use CGI ':standard';
use Config::General;
use Mod::DBtank;


sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
 my $self = shift;
 my $tpl_id = shift;
 my $node_meta = shift;
 my $users_dms = shift || "";
 my $mode = shift || "";
 my $varenv = shift;
 my $users_sharee = shift || "";
 my $return = shift || "";

 my $q = new CGI;
 my $dbt = new DBtank;

 my $globalconf_file = "/var/www/copri4/shareeconf/global.cfg";
 my $conf = Config::General->new($globalconf_file);
 my %globalconf = $conf->getall;
 my $payone_conf = $globalconf{payone_conf};

 my $bgcolor1 = "009899";#sharee
 #my $bgcolor1 = "e2001a";

 $bgcolor1 = $dbt->{website}->{$varenv->{syshost}}->{bgcolor1} if($dbt->{website}->{$varenv->{syshost}}->{bgcolor1});
 $bgcolor1 = $dbt->{merchant_ids}->{$varenv->{merchant_id}}->{bgcolor1} if($dbt->{merchant_ids}->{$varenv->{merchant_id}}->{bgcolor1});

 print "<div class='form-group' style='clear:both;'>\n";

 print $q->div({-class=>'content_title3'},"Kreditkartendaten"),"\n";

 print $q->div({-class=>'content2'},"Kreditkartendaten werden von uns nicht gespeichert sondern direkt an unseren Zahlungsdienstleister Payone übermittelt. Deshalb sehen Sie hier nur leere Eingabefelder."),"\n";
 #print $q->div({-class=>'content2'}, "Zur Validierung Ihrer Zahlungsdaten wird eine 1,- € Testbuchung vorgenommen. Wir werden nach erfolgreicher Abbuchung den Betrag als Mietgutschrift in Ihrem Account hinterlegen."),"\n";
 if($users_sharee->{int03} && $users_sharee->{int03} == 2 && $users_sharee->{txt28} && $users_sharee->{txt28} =~ /\w/){
	print $q->div({-class=>'content2', -style=>'color:#c83434;'},"Ihre Zahlungsdaten konnten nicht erfolgreich validiert werden. Bitte überprüfen Sie Ihre Eingaben."),"\n";
 }
 
 my $ccerror="";
 #my $ccerror="Entschuldigung, die Zahlungsart Kreditkarten ist im Augenblick nicht verfügbar.";
 $ccerror = "Status not APPROVED" if($R::not_approved);
 print "<style type='text/css'>div#ccerror {color:#c83434;} </style>";
 print $q->div({-id=>'ccerror'},"$ccerror"),"\n";

if(1==1){

print<<EOF
<style type="text/css" media="screen, projection">
fieldset {
padding: 1em;
border: 1px solid #$bgcolor1;
width: 275px;
margin: 10px;
}
label {
margin-right: 10px;
float: left;
width: 80px;
padding-top: 0.3em;
text-align: left;
}
input, select{
font-size: 1em;
border: 1px solid black;
padding: 0.1em;
}
select {
margin-right: 10px;
}
input, .inputIframe, select {
display: block;
margin-bottom: 10px;
}
input {
width: 175px;
}
#paymentsubmit {
width: 100%;
font-size:1.2em;
padding: 5px;
margin-top:20px;
color:#FFF;
background-color:#$bgcolor1;
}
#errorOutput {
text-align: center;
color: #ff0000;
display: block;
}
</style>

<script type="text/javascript" src="https://secure.pay1.de/client-api/js/v1/payone_hosted_min.js"></script>
EOF
;

print<<EOF 
<form name="paymentform" action="" method="post">
<fieldset>

<input type="hidden" name="pseudocardpan" id="pseudocardpan">
<input type="hidden" name="truncatedcardpan" id="truncatedcardpan">

<!-- place your input fields -->
<label for="cardtypeInput">Kreditkartentyp:</label>
<span id="cardtype" class="inputIframe"></span>

<label for="cardpanInput">Kreditkartennummer:</label>
<span id="cardpan" class="inputIframe"></span>

<label for="cvcInput">CVC:</label>
<span id="cardcvc2" class="inputIframe"></span>

<label for="expireInput">gültig bis:</label>
<span id="expireInput" class="inputIframe">
<span id="cardexpiremonth"></span>
<span id="cardexpireyear"></span>
</span>

<label for="firstnameInput">Vorname:</label>
<input id="firstname" type="text" name="firstname" value="">

<label for="lastnameInput">Name:</label>
<input id="lastname" type="text" name="lastname" value="">

<div id="errorOutput"></div>

<input id="paymentsubmit" type="button" value="Speichern" onclick="check();">

</fieldset>
<input type="hidden" name="sessionid" value="$R::sessionid">

</form>

<div id="paymentform"></div>
<script>
var request, config;
config = {

 fields: {

  cardtype: {
   selector: "cardtype",	// put name of your div-container here
   cardtypes: ["V", "M"]	// define possible cardtypes in PAYONE iFrame
  },

  cardpan: {
   selector: "cardpan",		// put name of your div-container here
   type: "text",		// text (default), password, tel
   style: "font-size: 1em; border: 1px solid black;"
  },

  cardcvc2: {
   selector: "cardcvc2",	// put name of your div-container here
   type: "password",		// select(default), text, password, tel
   style: "font-size: 1em; border: 1px solid black;",
   size: "4",
   maxlength: "4",		// set max. length for CVC input; empty values possible
   length: { 
		"A": 4, "V": 3, "M": 3, "J": 0 
	} 			// set required CVC length per cardtype
   				// if set exact length required; 0=CVC input disabled
  },

  cardexpiremonth: {
   selector: "cardexpiremonth",	// put name of your div-container here
   type: "select",		// select(default), text, password, tel
   size: "2",
   maxlength: "2",

   iframe: {
    width: "50px"
   }
  },

  cardexpireyear: {
   selector: "cardexpireyear",	// put name of your div-container here
   type: "select",		// select(default), text, password, tel

   iframe: {
    width: "80px"
   }
  }

 },

 defaultStyle: {
  input: "font-size: 1em; border: 1px solid black; width: 175px;",
  select: "font-size: 1em; border: 1px solid black;",
  iframe: {
   height: "33px",
   width: "280px"
  },
  error: "errorOutput",
  language: Payone.ClientApi.Language.de
  				// area to display error-messages (optional)
  				// Language to display error-messages
  				// (default: Payone.ClientApi.Language.en)
 }};

 request = {
  aid: '$payone_conf->{aid}',			// your AID
  encoding: '$payone_conf->{encoding}',		// desired encoding
  mid: '$payone_conf->{mid}', 			// your MID
  mode: '$payone_conf->{mode}', 		// desired mode
  portalid: '$payone_conf->{portalid}',		// your PortalId
  request: 'creditcardcheck', 			// fixed value
  responsetype: '$payone_conf->{responsetype}', // fixed value
  storecarddata: '$payone_conf->{storecarddata}',// fixed value
  hash: '$payone_conf->{hash}'			// hash calculated over your request-parameter-values (alphabetical request-order) plus PMI portal key
  						// see Chapter 3.1.5.3
 };

var iframes = new Payone.ClientApi.HostedIFrames(config, request);

 function check() {
  // Function called by submitting PAY-button

  if (iframes.isComplete()) {
   iframes.creditCardCheck('checkCallback');
   // Perform "CreditCardCheck" to create and get a
   // PseudoCardPan; then call your function
   "checkCallback"
   } else { 
    \$('#ccerror').html('Die Kreditkartendaten konnten nicht validiert werden.');
    console.debug("not complete");
   }
 }

 function checkCallback(response) {
  console.debug(response);

  if (response.status === "VALID") {
   document.getElementById("pseudocardpan").value = response.pseudocardpan;
   document.getElementById("truncatedcardpan").value = response.truncatedcardpan;
   document.paymentform.submit();
  }else{
    \$('#ccerror').html('Die Kreditkartendaten konnten nicht validiert werden.');
  }
 }

</script>

EOF
;
}
print "</div>\n";


}
1;
