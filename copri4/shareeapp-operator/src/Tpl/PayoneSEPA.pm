package PayoneSEPA;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
use strict;
use warnings;
use POSIX;
use CGI ':standard';
use Lib::Config;
use Mod::Buttons;
use Mod::Libenz;
use Mod::Shareework;
use Mod::DBtank;
use Sys::Hostname;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
 my $self = shift;
 my $tpl_id = shift;
 my $node_meta = shift;
 my $users_dms = shift || "";
 my $mode = shift || "";
 my $varenv = shift;
 my $users_sharee = shift || "";
 my $return = shift || "";

 my $q = new CGI;
 my $cf = new Config;
 my $lb = new Libenz;
 my $tk = new Shareework;
 my $dbt = new DBtank;
 my $but = new Buttons;
 my $hostname = hostname;

 $q->import_names('R');
 my @keywords = $q->param;
 my $script = $q->script_name();
 my $path_info = $q->path_info();
 my $path = $path_info;
 #with meta_host, 
 if("$varenv->{metahost}"){
   $path = "$script" . "$path_info";
   $script="";
 }
 my $dbh = "";
 my $now_dt = strftime "%Y-%m-%d %H:%M", localtime;
 my @viewsel = split /\//,$1 if($path =~ /^\/(.*)/);
 my $bgcolor1 = "009899";#sharee
 $bgcolor1 = $dbt->{website}->{$varenv->{syshost}}->{bgcolor1} if($dbt->{website}->{$varenv->{syshost}}->{bgcolor1});
 $bgcolor1 = $dbt->{merchant_ids}->{$varenv->{merchant_id}}->{bgcolor1} if($dbt->{merchant_ids}->{$varenv->{merchant_id}}->{bgcolor1});

 my $coo = $q->cookie(-name=>'domcookie') || $R::sessionid;
 my $ctadr = $users_sharee if(ref($users_sharee) eq "HASH" && $users_sharee->{c_id});

 my $tpl = $dbt->get_tpl($dbh,$tpl_id);
 my @tpl_order = split /,/,$tpl->{tpl_order};

  print $q->start_form(),"\n";
  print $q->hidden(-name=>"sessionid",-override=>1,-value=>"$R::sessionid");
  print $q->hidden(-name=>"tinkc_id",-override=>1,-value=>"$ctadr->{c_id}");
  print $q->hidden(-name=>"request",-override=>1,-value=>"managemandate");

  #Standard parameter
  #foreach (keys (%$base_request)){
  #  print $q->hidden(-name=>"$_",-value=>"$base_request->{$_}"),"\n";
  #}

  print "<div class='form-group' style='clear:both;'>\n";

  print $q->div({-class=>'content_title3'},"Kontoverbindung"),"\n";
  print $q->div({-class=>'content2'}, "Durch das SEPA Mandat können wir die Leihrad Buchungen bequem einziehen."),"\n";
  #print $q->div({-class=>'content2'}, "Zur Validierung Ihrer Zahlungsdaten wird eine 1,- € Testbuchung vorgenommen. Wir werden nach erfolgreicher Abbuchung den Betrag als Mietgutschrift in Ihrem Account hinterlegen."),"\n";


  foreach (@tpl_order){
      my ($key,$des,$size) = split /=/,$_;
      $ctadr->{$key} = $q->unescapeHTML("$ctadr->{$key}");

      my $label_des="";
      my $red = "#c83434";
      my $required="required";
      if($key eq "txt22"){
	  if($R::IBAN){
            $label_des = "<span style=color:$red>IBAN Fehler</span>";
            $ctadr->{$key} = $R::IBAN;
	  }
          if($R::failure eq $key){
            $label_des = "<span style=color:$red>Bitte \"$des\" Angabe korrigieren</span>";
          }
          print $q->label({-for=>"$key", -style=>'padding-top:1.5em;'},"$label_des"),"\n";
	  print "<input id='$key' type='text' class='form-control' name='$key' override=1 value='$ctadr->{$key}' placeholder='$des' $required />\n";

      }elsif($key eq "txt23"){
          print $q->label({-for=>"$key", -style=>'padding-top:1.5em;'},"$label_des"),"\n";
	  print "<input id='$key' type='text' class='form-control' name='$key' override=1 value='$ctadr->{$key}' placeholder='$des' $required />\n";

      #------------------------
      }elsif($key =~ /int03/ && $size eq "checkbox"){

	if($ctadr->{$key} != 1){

	 my $int03 = "";
	 $int03 = $ctadr->{$key} if($ctadr->{$key} == 1);
	 print $q->div({-style=>'margin:10px 0;'},"Damit wir die SEPA-Lastschrift von Ihrem Konto einziehen können, benötigen wir von Ihnen ein SEPA-Lastschriftmandat. Das Mandat wird bei uns als PDF zu Ihren Zahldaten hinterlegt. Sie können nach der Anmeldung das Mandat als PDF herunterladen"),"\n";

         if($ctadr->{txt27} !~ /active|pending/ && $R::failure eq $key){
          $label_des = "<span style=color:$red>Bitte \"$des\" bestätigen</span>";
          print $q->label({-for=>"$key", -style=>'padding-top:10px;'},"$label_des"),"\n";
         }

         my $des = "Ich möchte das Mandat erteilen (elektronische Übermittlung)";
         print $q->div({-id=>"$key"},$but->checkbox("1","$key","$int03","","$required"), " $des"),"\n";
         #print $q->hidden(-name=>"$key",-override=>1,-value=>"3");

	}else{
	  #2022-08-08 disabled because of decoding, anyway, pdf should be enough 
	  if(1==2 && $ctadr->{txt28} =~ /SEPA-Lastschriftmandat/){
	   print $q->div({-class=>'content2'}, ""),"\n";
	   $ctadr->{txt28} = $q->unescapeHTML("$ctadr->{txt28}");
	   use URI::Encode qw(uri_encode uri_decode);
	   $ctadr->{txt28} = uri_decode($ctadr->{txt28});
	   $ctadr->{txt28} =~ s/\+/ /g;	
	   print $q->div({-style=>'padding:10px;margin:10px 0;border:1px solid silver;'},$ctadr->{txt28}),"\n";
	  }

	 #if payone fails/error
	 if($ctadr->{txt27} =~ /active|pending/){
	   #print $q->div({-style=>'padding:10px;margin:10px 0;border:1px solid silver;'}, "Das Mandat ist aktiviert"),"\n";
	   if($ctadr->{ct_name} && ($ctadr->{ct_name} =~ /PO-/ || $ctadr->{ct_name} =~ /TM-/) && $coo){
	     #print $q->div({-style=>'padding:10px;margin:10px 0;'}, "Download:", $q->a({-href=>"/PDFinvoice?session=$coo\&sepa=$ctadr->{ct_name}", -target=>'_blank'}, "SEPA-Lastschriftmandat.pdf")),"\n" if($coo);
	     #$varenv->{praefix} only defined in sharee
	     my $webtarget = "_blank";
	     my $dtext = "";
	     if($varenv->{syshost} =~ /app/){
	       $webtarget = "_self";
	       $dtext = "<br />(Der PDF download öffnet je nach System/Konfiguartion einen externen PDF-Viewer oder Webbrowser)";
     	     }
	     if( -f "$varenv->{basedir}/pdfinvoice/SEPA-Lastschriftmandat-$varenv->{praefix}-$ctadr->{ct_name}.pdf"){
               print $q->div({-style=>'padding:10px;margin:10px 0;'},$q->a({-href=>"$varenv->{wwwhost}/FileOut?file=SEPA-Lastschriftmandat-$varenv->{praefix}-$ctadr->{ct_name}.pdf&sessionid=$coo", -target=>"$webtarget" , -type=>'application/octet-stream', -style=>'text-decoration:underline;font-size:1.1em;'}, $q->img({-src=>"$varenv->{metahost}/glyphicons/file-any.png", -style=>'width:30px;'}), "SEPA-Lastschriftmandat.pdf"),"$dtext"),"\n";
	     }
	   }else{
	     print $q->div({-style=>'padding:10px;margin:10px 0;border:1px solid silver;color:#c83434;'},"Es gibt ein Problem! Das SEPA Mandat konnte nicht eingeholt werden. Bitte wenden Sie sich bei Bedarf an den unter \"Kontakt\" hinterlegten Support."),"\n";
	   }
	 }elsif($ctadr->{txt22}){ #if IBAN but no mandat_status
	   print $q->div({-style=>'padding:10px;margin:10px 0;border:1px solid silver;color:#c83434;'}, "Fehler. Das Mandat ist nicht aktiv. "),"\n";

      	   $ctadr->{txt28} = $q->unescapeHTML("$ctadr->{txt28}");
  	   $ctadr->{txt28} = $lb->newline($ctadr->{txt28},"","0");

	   #print $q->div({-style=>'padding:10px;margin:10px 0;border:1px solid silver;color:#c83434;'}, "Fehlermeldung: $ctadr->{txt28}"),"\n" if($ctadr->{txt28} !~ /SEPA-Lastschriftmandat/);
	 }
         print $q->hidden(-name=>"$key",-override=>1,-value=>"1");
	}
      }
      #--------------------
  }
  
  print $q->div({-style=>'margin-top:1em;text-align:center;'},"<button type='submit' name='sharee_edit' value='save_account' class='btn btn-primary btn-lg btn-block' style='border:1px solid #$bgcolor1;background-color:#$bgcolor1;'>Weiter</button>"),"\n";


  print "</div>\n";
  print $q->end_form,"\n";
}
1;
