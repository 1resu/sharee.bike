package Contact;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
use strict;
use warnings;
use CGI ':standard';
use Lib::Config;
use Mod::Buttons;
use Mod::Libenz;
use Mod::Libenzdb;
use Mod::DBtank;
use Encode;
use Data::Dumper;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
 my $node_meta = shift;
 my $users_dms = shift || "";
 my $mode = shift || "";
 my $varenv = shift;
 my $users_sharee = shift || "";
 my $return = shift || "";

 my $q = new CGI;
 my $cf = new Config;
 my $but = new Buttons;
 my $lb = new Libenz;
 my $db = new Libenzdb;
 my $dbt = new DBtank;

 my $lang = "de";
 my $script = $q->script_name();
 my $path_info = $q->path_info();
 my $path = $path_info;
 #with meta_host, 
 if($varenv->{metahost}){
   $path = "$script" . "$path_info";
   $script="";
 }
 my $user_agent = $q->user_agent();
 my %ib = $but->ibuttons_arch();
 my @tpl_order = split /,/,$node_meta->{tpl_order};
 my $main_ids = $node_meta->{main_id};
 my @viewsel = split /\//,$1 if($path =~ /^\/(.*)/);

 my $session="";
 my $session_and="";
 if(length($R::sessionid) > 20){
        $session = "?sessionid=$R::sessionid";
        $session_and = "&sessionid=$R::sessionid";
 }

 my $project = "";#project dependent operator contact match
 my $oprefix = "";#single operator contact match
 $project = "Freiburg" if($varenv->{syshost} eq "shareeweb-sharee");
 $project = "Bayern" if($varenv->{syshost} eq "shareeweb-bayern");
 $project = "Konstanz" if($varenv->{syshost} eq "shareeweb-konstanz");
 $oprefix = "REN" if($varenv->{syshost} eq "shareeweb-ren");


  my $sort_updown = "up";
  my $content2 = "content2_contact";
  my $content_img = "pic-contact";
  my $record_cc = { c_id => 0 }; 
   my $pref_cc = {
        table           => "contentuser",
        fetch           => "one",
        template_id     => "197",
	c_id	=> 1,
   };


  print "<div id='Contentapp'>\n";
  print $q->div({-class=>"$content2"},"Bei Fragen und Problemen bitte den örtlichen Betreiber kontaktieren"),"\n";

 #BIG LOOP content table
 #while (my ($mandant_conf, $value) = each %{ $dbt->{operator} }) {
 foreach my $m_id (sort { $dbt->{operator}->{$b}->{oprefix} cmp $dbt->{operator}->{$a}->{oprefix} } keys(%{ $dbt->{operator} })) {
  #if($dbt->{operator}->{$m_id}->{project} eq $project || $project eq "all"){
  if($dbt->{operator}->{$m_id}->{oprefix} eq $oprefix || $dbt->{operator}->{$m_id}->{project} eq $project){
   print $q->div({-class=>"$content2"},"&nbsp;"),"\n";

   my $sharee_operator = $dbt->{operator}->{$m_id}->{database}->{dbname} || "";
   my $dbh_operator = $dbt->dbconnect_extern($sharee_operator);
   $record_cc = $dbt->fetch_record($dbh_operator,$pref_cc);
   #print Dumper($node_meta->{tpl_order}) . "<br />\n";
  
   #handmade activation to show only realy live operators on web contact
   if($sharee_operator && $sharee_operator =~ /sharee_mak|sharee_sx|sharee_lec/){

   foreach (@tpl_order){
    my ($key,$des,$size) = split /=/,$_;
    $record_cc->{$key} = $q->unescapeHTML("$record_cc->{$key}");
    $record_cc->{$key} = $lb->newline($record_cc->{$key},"","");

    if($key =~ /ct_name/){
	#print $q->div({-id=>"$record_cc->{c_id}",-class=>'content_title2'},"&bull; $record_cc->{$key}"),"\n";
	my $title = "$record_cc->{$key}";
	$title = "$dbt->{operator}->{$m_id}->{title}";# if($dbt->{operator}->{$m_id}->{oprefix} eq $oprefix);
        print $q->div({-id=>"$record_cc->{c_id}",-class=>'content_title2'},"&bull; $title"),"\n";
     }elsif($key =~ /txt|int/ && $record_cc->{$key} && $key !~ /txt85/){
              #phone tag
              if($record_cc->{$key} =~ /\d+$/ && $des =~ /hotline|Telefon|phone/i){
                $record_cc->{$key} =~ s/([\s0-9-\/]+)/\<a href=\'tel:00$1\'\>$1\<\/a\>/;
              }
              #email tag with little coding against grabber
              if($record_cc->{$key} =~ /(\w+\@[\w-]+\.\w+)/){
                $record_cc->{$key} =~ s/([\w\.\-_]+\@[\w-]+\.\w+)/\<a href=\'mailto:$1\'\>$1\<\/a\>/;
                $record_cc->{$key} =~ s/\@/\&\#64\;/g;
              }
	$record_cc->{$key} =~ s/\\//g;
	if($des =~ /hotline|e-Mail|zeiten/i){
	  print $q->div({-class=>"$content2"}, "$des: $record_cc->{$key}"),"\n";
	}else{
	  print $q->div({-class=>"$content2"},"&nbsp;"),"\n" if($key eq "txt01");
	  print $q->div({-class=>"$content2"}, "$record_cc->{$key}"),"\n";
	}

     }
    }
   }
  }#end if
 }
 print "</div>\n";
}
1;
