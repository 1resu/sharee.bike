package FormEdit;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
use strict;
use warnings;
use POSIX;
use CGI ':standard';
use DateTime;
use DateTime::Format::Pg;

use Lib::Config;
use Mod::Buttons;
use Mod::Libenz;
use Mod::Libenzdb;
use Mod::DBtank;
use Mod::APIfunc;
use Mod::Pricing;
use Tpl::AccountSubmenu;
use Data::Dumper;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
 my $node_meta = shift;
 my $users_dms = shift || "";
 my $mode = shift || "";
 my $varenv = shift;
 my $users_sharee = shift || "";
 my $return = shift || "";

 my $q = new CGI;
 my $cf = new Config;
 my $lb = new Libenz;
 my $db = new Libenzdb;
 my $dbt = new DBtank;
 my $apif = new APIfunc;
 my $pri = new Pricing;
 my $but = new Buttons;
 my $submenu = new AccountSubmenu;
 my $script = $q->script_name();
 my $path_info = $q->path_info();
 my $path = $path_info;
 #with meta_host, 
 if("$varenv->{metahost}"){
   $path = "$script" . "$path_info";
   $script="";
 }
 my $dbh = "";#$dbt->dbconnect();
 my $user_agent = $q->user_agent();
 my %ib = $but->ibuttons_arch();
 my @viewsel = split /\//,$1 if($path =~ /^\/(.*)/);
 my $red = "red";

 my $coo = $q->cookie(-name=>'domcookie') || $R::sessionid;
 my $session="";
 my $session_and="";
 if($R::sessionid && length($R::sessionid) > 20 && !$q->cookie(-name=>'domcookie')){
        $session = "?sessionid=$R::sessionid";
        $session_and = "&sessionid=$R::sessionid";
 }

 my $bgcolor1 = "009899";#sharee
 $bgcolor1 = $dbt->{website}->{$varenv->{syshost}}->{bgcolor1} if($dbt->{website}->{$varenv->{syshost}}->{bgcolor1});
 $bgcolor1 = $dbt->{merchant_ids}->{$varenv->{merchant_id}}->{bgcolor1} if($dbt->{merchant_ids}->{$varenv->{merchant_id}}->{bgcolor1});

 #print "$dbt->{merchant_ids}->{$varenv->{merchant_id}}->{id}|$varenv->{merchant_id}|$coo";
 my $aowner = "198";
 $aowner = $dbt->{merchant_ids}->{$varenv->{merchant_id}}->{id} if($varenv->{merchant_id} && $dbt->{merchant_ids}->{$varenv->{merchant_id}}->{id});

 #TODO substitute aowner by project
 my $project = "all";
 $project = $dbt->{merchant_ids}->{$varenv->{merchant_id}}->{project} if($varenv->{merchant_id} && $dbt->{merchant_ids}->{$varenv->{merchant_id}}->{project});

 my $bonus_desc = "Bonusnummer (falls vorhanden)";
 my $bonus_ak = "Ihre Bonusnummer wurde akzeptiert. Bitte beachten Sie dass der Bonustarif im Kontext des Mietradbetreibers vergeben wird. Der Tarif wird bei der Mietrad Reservierung angezeigt.";
 my $bonus_avail = "Die Bonusnummer ist nicht vorhanden.";
 my $bonus_conflict = "Die Bonusnummer kann pro Registrierung nur einmal verwendet werden. So wird verhindert dass weitere Personen von den Freiminuten widerrechtlich Gebrauch nehmen.<br />
Weitere Personen aus Ihrem Haushalt profitieren jedoch ebenfalls, falls Sie weitere Produkte (Gas, SeeConnect, Fähre- oder Bus-Zeitkarten) nutzen. Dann könnte sich die zweite Person mit der weiteren Nummer registrieren.";	
 my $bonus_saved = "Aktiviert:";

 #sharee.bike text
 if($aowner == 186 || $aowner == 197){
   $bonus_desc = "Freischaltcode (falls vorhanden)";
   $bonus_ak = "Ihr Freischaltcode wurde angenommen. Bitte beachten sie dass der Freischaltcode mit einem Mietradtarif verbunden ist. Der Tarif wird bei der Mietrad Reservierung angezeigt.";
   $bonus_avail = "Der Freischaltcode ist nicht vorhanden.";
   $bonus_conflict = "Der Freischaltcode kann nur einmal verwendet werden.";
   $bonus_saved = "Aktiviert:";

 }

 my $ctrel = {};
 $ctrel = $users_sharee if(ref($users_sharee) eq "HASH" && $users_sharee->{c_id});
 #
 #collect rentals on operators and get operator hash with dbname->uri_operator
 #$q->param(-name=>'month',-value=>"18");#disabled because changing to not invoiced positions 
 my ($cttpos,$operator_hash) = $apif->user_rentals_history($q,$ctrel);
 #print Dumper($cttpos); 

 my $cttpos_count=0;
 foreach my $id (keys(%$cttpos)){
	$cttpos_count++;
 }

 #$ctt = $db->collect_content2("contenttrans","int10",$ctrel->{c_id}) if($ctrel->{c_id});
 #
 #loop operator hash to get invoices for each operator
 my $ctt_all = {};
 my $ctadrcoupon = {};
 foreach my $sharee_operator (keys (%$operator_hash)){
   my $dbh_operator = $dbt->dbconnect_extern("$sharee_operator");
   my $pref = {
        table    => "contenttrans",
        fetch    => "all",
	int10	 => "$ctrel->{c_id}",
        keyfield => "c_id",
        };


   if($ctrel->{c_id}){
     my $ctt = $dbt->fetch_tablerecord($dbh_operator,$pref);
     foreach my $id (keys(%$ctt)){
       $ctt->{$id}->{wwwhost} = "$operator_hash->{$sharee_operator}";
       $ctt->{$id}->{operator} = "$sharee_operator";
       $ctt->{$id}->{basedir} = "$dbt->{copri_conf}->{basedir}/$dbt->{operator}->{$sharee_operator}->{dir_app}";
       #print "$sharee_operator|$ctt->{$id}->{wwwhost}|$ctt->{$id}->{operator}|$ctt->{$id}->{basedir}<br>";
     }
     $ctt_all = { %$ctt_all, %$ctt };

     my $prefcoupon = {
        table   => "contentadr",
        fetch   => "one",
        txt15   => "~::\\w",
        c_id    => "$ctrel->{c_id}",
        };

     $ctadrcoupon->{$sharee_operator} = $dbt->fetch_tablerecord($dbh_operator,$prefcoupon);
   }
 }

 my $tpl_id = $node_meta->{tpl_id};
 #$tpl_id = "302004" if($viewsel[1] =~ /Mieten/ && $ctrel->{c_id});
 my $tpl = $db->get_tpl($tpl_id);
 my $tpl01 = $tpl;#If nothing else, because of tpl_name Title in split_lates
 my $tpl02 = "";
 my @split_lates = ("$tpl->{tpl_order}");
 my $template01 = "$tpl_id" . "001";
 my $template02 = "$tpl_id" . "002";
 if($tpl_id < 999){
  $tpl01 = $db->get_tpl($template01);
  $tpl02 = $db->get_tpl($template02);
  @split_lates = ("$tpl01->{tpl_order}","$tpl02->{tpl_order}");
 }
 my $now_dt = strftime "%Y-%m-%d %H:%M", localtime;
 my $start_date = strftime "%d.%m.%Y %H:%M", localtime;

         my ($end_date,$end_time) = split(/ /,$now_dt);
         my ($e_yy,$e_mo,$e_dd) = split(/-/,$end_date);
         my ($e_hh,$e_mi) = split(/\:/,$end_time);

  my $sharee_agb_text = {};
  $sharee_agb_text->{ct_name} = "AGB";
  $sharee_agb_text->{txt01} = "<iframe src='$varenv->{wwwhost}/site/agb_sharee_2.html' style='width:100%;height:10000px;border:none;' scrolling='auto'></iframe>";
  my $sharee_privacy_text = {};
  $sharee_privacy_text->{ct_name} = "Datenschutzhinweise";
  $sharee_privacy_text->{txt01} = "<iframe src='$varenv->{wwwhost}/site/privacy_sharee_2.html' style='width:100%;height:8000px;border:none;' scrolling='auto'></iframe>";

  if($project eq "Konstanz"){
    $sharee_agb_text->{txt01} = "<iframe src='$varenv->{wwwhost}/site/agb_konrad_1.html' style='width:100%;height:10000px;border:none;' scrolling='auto'></iframe>";
  }
  if($project eq "Bayern"){
    $sharee_agb_text->{txt01} = "<iframe src='$varenv->{wwwhost}/site/agb_lastenradbayern_220804.html' style='width:100%;height:11000px;border:none;' scrolling='auto'></iframe>";
    $sharee_privacy_text->{txt01} = "<iframe src='$varenv->{wwwhost}/site/privacy_lastenradbayern.html' style='width:100%;height:8000px;border:none;' scrolling='auto'></iframe>";
  }


  print "<div id='Contentapp'>\n";

print <<EOF
<!-- Modal -->
<div class="modal fade" id="sharee_agb" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">$sharee_agb_text->{ct_name}</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="text-default">$sharee_agb_text->{txt01}</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
    </div>
  </div>
</div>
EOF
;

print <<EOF
<!-- Modal -->
<div class="modal fade" id="sharee_privacy" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">$sharee_privacy_text->{ct_name}</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="text-default">$sharee_privacy_text->{txt01}</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
    </div>
  </div>
</div>
EOF
;



  my $debug=0;
  $debug=1 if($users_sharee->{c_id} eq $dbt->{copri_conf}->{superu_id} || $dbt->{copri_conf}->{stage} eq "test");
  my $debug_message = "";

  my $payable_check=0;
  if(($ctrel->{int03} == 1 && $ctrel->{ct_name} =~ /\w{2}-\d+/) || ($ctrel->{int03} == 2 && length($ctrel->{ct_name}) >= 19)){
        $payable_check=1;
 }

  #subMenue--------
  $submenu->tpl($node_meta,$users_dms,$mode,$varenv,$users_sharee,$return);
  #-----------------

  my $catch_failure=0;
  if(($R::failure && $R::failure =~ /\w+/) || ($ctrel->{txt31} && $ctrel->{txt31} =~ /\w/)){
	$debug_message = "($R::failure || $ctrel->{txt31})" if($debug);	
	$catch_failure=1;
	print $q->div({-class=>'content2', -style=>"clear:both;color:$red"}, "* Es liegt noch mindestens ein Fehler vor. $debug_message"),"\n";
  }

  #confirm code manage
  if($catch_failure && (!$ctrel->{int04} && !$ctrel->{int13}) && ($tpl_id =~ /^2$|302004/) && ($path =~ /$varenv->{accounting_3}/)){
	$debug_message = "$catch_failure && (!$ctrel->{int04} || !$ctrel->{int13}) && $tpl_id" if($debug);
        print $q->div({-class=>'content2'}, "Erst nachdem Ihre Profildaten vollständig sind erhalten Sie die Bestätigung. $debug_message"),"\n";
  }elsif((!$ctrel->{int04} || !$ctrel->{int13}) && ($tpl_id =~ /^2$|302004/) && ($path =~ /$varenv->{accounting_3}/)){

    print $q->start_form(-name=>'accountscreen', -action=>"/$varenv->{mandant}/Account/$varenv->{accounting_3}$session"),"\n";
    print $q->hidden(-name=>"sessionid",-override=>1,-value=>"$R::sessionid");
    my $required = "";
    $debug_message = "(!$ctrel->{int04} || !$ctrel->{int13}) && $tpl_id" if($debug);
    print $q->div({-class=>'content_title3',-style=>'clear:both;'}, "Fast geschafft $debug_message"),"\n";
    my $acktext = "Es wurden Bestätigungs-Codes an Ihre e-Mail Adresse und per SMS an Ihre Telefonnr. versandt.";
    $acktext = "Es wurde ein Bestätigungs-Code per SMS an Ihre Telefonnr. versandt." if($ctrel->{int04} && !$ctrel->{int13});
    $acktext = "Es wurde ein Bestätigungs-Code an Ihre e-Mail Adresse versandt." if(!$ctrel->{int04} && $ctrel->{int13});
    print $q->div({-class=>'content2'}, "$acktext Nach erfolgreicher Bestätigung können Sie unsere Mieträder nutzen. Bitte beachten Sie die Anweisungen."),"\n";
    if(!$ctrel->{int04}){
      my $des = "E-Mail Bestätigungscode";
      my $key = "confirm_code";
      my $label_des="* $des";
      $label_des = "<span style=color:$red>Bitte \"$des\" korrigieren</span>" if($R::failure);
      print $q->label({-for=>"$key", -style=>'padding-top:1em;'},"$label_des"),"\n";
      print "<input id='$key' type='text' class='form-control' name='$key' value='' $required autofocus/>\n";
      print $q->div({-class=>'content2'}, "Falls Sie keinen e-Mail Bestätigungs Code erhalten haben, überprüfen Sie bitte Ihre bei der Registrierung angegebene e-Mail Adresse und/oder den Spam Bereich in Ihrem e-Mail Programm. Hier können Sie erneut einen ",$q->a({-style=>"color:#$bgcolor1;",-href=>"$varenv->{wwwhost}?sharee_edit=send_email$session_and"},"e-Mail Code anfordern")),"\n";  
    }else{
      print $q->div({-class=>'content2', -style=>'color:gray;'}, "* E-Mail Bestätigungs-Code wurde bereits erfolgreich eingegeben."),"\n";
    }
    if(!$ctrel->{int13}){
      my $des = "SMS Bestätigungscode";
      my $key = "confirm_smscode";
      my $label_des="* $des";
      $label_des = "<span style=color:$red>Bitte \"$des\" korrigieren</span>" if($R::failure);
      print $q->label({-for=>"$key", -style=>'padding-top:1em;'},"$label_des"),"\n";
      print "<input id='$key' type='text' class='form-control' name='$key' value='' $required autofocus />\n";
      print $q->div({-class=>'content2'}, "Falls Sie keinen SMS Bestätigungs Code erhalten haben, überprüfen Sie bitte Ihre bei der Registrierung angegebene mobile Telefonnummer. Hier können Sie erneut einen ",$q->a({-style=>"color:#$bgcolor1;",-href=>"$varenv->{wwwhost}?sharee_edit=send_sms$session_and"},"SMS Code anfordern")),"\n";    
    }else{
      print $q->div({-class=>'content2', -style=>'color:gray;'}, "* SMS Bestätigungs-Code wurde bereits erfolgreich eingegeben."),"\n";
    }

    my $button_name = "Weiter";
    print $q->div({-style=>'margin-top:2em;text-align:center;'},"<button type='submit' name='confirm_userid' value='$ctrel->{c_id}' class='btn btn-primary btn-lg btn-block' style='border:1px solid #$bgcolor1;background-color:#$bgcolor1;'>$button_name</button>"),"\n";
    print $q->end_form,"\n";

  }else{
  
  my $postaction = "/$varenv->{mandant}/Account/$varenv->{accounting_2}";
  if($ctrel->{c_id} && $path =~ /$varenv->{accounting_2}|$varenv->{accounting_3}|$varenv->{profile}/){
    $postaction = "/$varenv->{mandant}/Account/$varenv->{accounting_3}";
  }elsif($path =~ /$varenv->{accounting_1}/){
    $postaction = "/$varenv->{mandant}/Account/$varenv->{accounting_1_5}";
  }elsif($ctrel->{c_id} && $path =~ /$varenv->{accounting_1}/){
    $postaction = "/$varenv->{mandant}/Account/$varenv->{accounting_2}";
  }

  print $q->start_form(-name=>'accountscreen', -action=>"$postaction$session"),"\n";
  print $q->hidden(-name=>"sessionid",-override=>1,-value=>"$R::sessionid") if($R::sessionid);


  print "<div class='form-group' style='clear:both;'>\n";

  my $i=0;
  my @tpl_order;
  foreach(@split_lates){
   $i++;
   if($i==1){
	if($R::confirm_success){
   	  print $q->div({-class=>'content_title3'},"Anmeldung bestätigt"),"\n";
	}else{
   	  print $q->div({-class=>'content_title3'},"$tpl01->{tpl_name}"),"\n";
	}

	if($path =~ /$varenv->{accounting_1}/){
		print $q->div({-class=>'content2'}, "Bitte füllen Sie dieses Formular vollständig aus und hinterlegen im nächsten Schritt wahlweise Ihre Lastschriftdaten (für Konten mit IBAN DE) oder Kreditkartendaten (VISA oder Mastercard). Bitte halten Sie die Daten immer auf dem neuesten Stand.<br /><br />
Nach Abschluss der Registrierung erhalten Sie sowohl auf die von Ihnen hinterlegte E-Mail als auch auf die Mobiltelefonnummer jeweils eine Nachricht, die Sie beide zwingend bestätigen müssen."),"\n";
	}
	if($path =~ /$varenv->{accounting_2}/){
	   if($tpl_id == 302008){
		print $q->div({-class=>'content2'}, "Wähle die Zahlungsart."),"\n";
	   }else{#old SEPA only style
		print $q->div({-class=>'content2'}, "Durch das SEPA Mandat können wir die Leihrad Buchungen bequem einziehen."),"\n";
	   }
	}

   }
   if($i==2){
   	print $q->div({-class=>'content_title3'},"$tpl02->{tpl_name}"),"\n";
	print $q->div({-class=>'content2'}, "Das Login besteht aus Ihrer e-Mail Adresse und einem mindestens 8 stelligen Passwort."),"\n";

   }
   @tpl_order = split /,/,$_;
    my $scol = "itime";
    foreach (@tpl_order){
      my ($key,$des,$size) = split /=/,$_;
      $ctrel->{$key} = $q->unescapeHTML("$ctrel->{$key}");
      $ctrel->{$key} = $lb->newline($ctrel->{$key},"","1");
     
      if($ctrel->{int03} && $path =~ /$varenv->{accounting_3}|$varenv->{profile}/){ 
       if(1==1){
	if($key =~ /ct_name/){
	 if($ctrel->{int12}){
	   print $q->div({-class=>'content2', -style=>'color:#c83434;'}, "Herzlich Willkommen. Wir mussten Ihren Zugang zu den Mieträdern sperren weil ein Problem mit Ihren Zahlungsdaten vorliegt. Bitte aktualisieren Sie Ihre Zahlungsdaten und kontaktieren Sie uns damit wir den Vorfall klären können."),"\n";
	 }else{
	   print $q->div({-class=>'content2'}, "Herzlich Willkommen. Sie können nach erfolgreicher Anmeldung unter dem Menue \"Fahrradstandortkarte\" ein Leihrad mieten."),"\n";
   	 }
	 print $q->div({-class=>'content2'}, "Nach der Mietradnutzung haben Sie hier die Möglichkeit die Mietvorgänge einzusehen und gebuchte Rechnungen als PDF herunterzuladen."),"\n";
	 print $q->div({-class=>'content2'}, "$bonus_ak"),"\n" if($R::success && $R::success eq "txt15");
	 print $q->div({-class=>'content2'}, "Aktuell liegen keine Buchungen vor."),"\n" if(!$cttpos_count);

     	}elsif($key =~ /barcode/ && $cttpos_count){

	  #print $q->div({-style=>'padding-top:1.5em;font-weight:bold;'},"Ihre Buchungsdaten der letzten 1 &frac12; Jahre im Überblick"),"\n"; 
    	  print $q->div({-style=>'padding-top:1.5em;font-weight:bold;'},"Folgende Positionen liegen zur nächsten Abbuchung vor"),"\n"; 

	  my @tpl_posorder = ("txt01=Beschreibung","int04=Station","ct_name=(Rad) Nummer","int26=CO2","int02=Betrag");

	  my $j=0;
	  my $nx=0;
	  my $sum = 0;
	  print "<div style='clear:both;'>\n";
  	  print $q->start_table({-style=>'margin:15px 0;', -border=>'0', -width=>'auto',-align=>'left', -cellpadding=>'3', -cellspacing=>'0'}),"\n";
	  print $q->Tr(),"\n";

  	  foreach my $id (sort { lc($cttpos->{$b}->{$scol}) cmp lc($cttpos->{$a}->{$scol}) } keys(%$cttpos)){
	   $j++;
	   $nx++;

  	   my $pricing = {};
   	   my $counting = {};
   	   my $rental_feed = {};
   	   if($cttpos->{$id}->{int35} && $cttpos->{$id}->{start_time} && $cttpos->{$id}->{end_time}){
    		($pricing,$counting) = $pri->counting_rental($varenv,$cttpos->{$id},"calc_price");
    		$rental_feed = $pri->fetch_rentalfeed($varenv,$cttpos->{$id},$counting);
   	   }

	  print $q->Tr(),"\n";
          foreach (@tpl_posorder){
           my ($key,$val) = split /=/,$_;

	    my $occupied_style = "background-color:#fcfdfb;";
	    $occupied_style = "background-color:#f4f1ee;" if($nx %= 2);
	    #$occupied_style = "color:#ff1493;" if($cttpos->{$id}->{txt10} =~ /occupied|requested/);
   	    $occupied_style = "color:#ff1493;" if($cttpos->{$id}->{int10} == 2 || $cttpos->{$id}->{int10} == 3);

	   if($key eq "txt01"){
	    #print $q->td({-class=>'tdtxt', -style=>"$occupied_style"},"$cttpos->{$id}->{$key}");
	    print "<td class='tdtxt' style='$occupied_style'>\n";
	    if($cttpos->{$id}->{txt01}){
            	$cttpos->{$id}->{$key} =~ s/\<br \/\>/; &nbsp; /g;
            	print "$cttpos->{$id}->{$key}<br />\n";
	    }
	    if($cttpos->{$id}->{start_time} && $cttpos->{$id}->{end_time}){
	      $cttpos->{$id}->{start_time} = $lb->time4de($cttpos->{$id}->{start_time},"1");
	      $cttpos->{$id}->{end_time} = $lb->time4de($cttpos->{$id}->{end_time},"1");
	      print $q->span("&rarr; $cttpos->{$id}->{start_time}<br />&larr; $cttpos->{$id}->{end_time}");
	    }
	    print "</td>\n";
	   }elsif($key =~ /int04/){
            if($cttpos->{$id}->{int09}){#if Tarifnr then bike
		#print $q->td({-class=>'tdint', -style=>"$occupied_style"},"Station $cttpos->{$id}->{$key}");
		print "<td class='tdtxt' style='$occupied_style'>\n";
		my $return_station = "";
		$return_station = "&larr; $cttpos->{$id}->{txt13}$cttpos->{$id}->{int04}" if($cttpos->{$id}->{txt13} && $cttpos->{$id}->{int04});
		print $q->span("Station<br />&rarr; $cttpos->{$id}->{txt12}$cttpos->{$id}->{int06}<br />$return_station");
		print "</td>\n";
	    }else{
	    	print $q->td({-class=>'tdint', -style=>"$occupied_style"},"$cttpos->{$id}->{$key}");
	    }
	   }elsif($key =~ /ct_name/){
            if($cttpos->{$id}->{int09}){#if Tarifnr then bike
	    	print $q->td({-class=>'tdint', -style=>"$occupied_style"},"Rad<br />$cttpos->{$id}->{$key}");
	    }else{
	    	print $q->td({-class=>'tdint', -style=>"$occupied_style"},"$cttpos->{$id}->{$key}");
	    }
	   }elsif($key eq "int26"){
	    my $co2saving = "";
	    if($cttpos->{$id}->{int26}){
	      $co2saving = "Einsparung</br>";
	      my $co2diff = $pri->co2calc($cttpos->{$id});
	      #my $sprit_price = $pri->sprit2calc($cttpos->{$id});
	      $co2saving .= "$co2diff kg CO&sup2;<br />";
	      #$co2saving .= "$sprit_price EUR<br />" if($sprit_price !~ /-/);
	      $cttpos->{$id}->{int26} =~ s/\./,/;
	      $co2saving .= "bei $cttpos->{$id}->{int26} KM";
	    }
	    print $q->td({-class=>'tdint', -style=>"$occupied_style", -nowrap=>1},"$co2saving");
	   }elsif($key eq "int02"){
            if($cttpos->{$id}->{int35} && $cttpos->{$id}->{start_time} && $cttpos->{$id}->{end_time}){
	      my $gesamt = 0;
	      $sum += $pricing->{total_price};
	      $gesamt = $lb->round($pricing->{total_price});
	      $gesamt = sprintf('%.2f', $gesamt);
	      $gesamt =~ s/\./,/;
	      my $rabatt = "";
	      $rabatt = "$pricing->{discount}" if($pricing->{discount});
              print $q->td({-class=>'tdint', -style=>"$occupied_style", -nowrap=>1},"$rabatt $gesamt €"),"\n";
            }else{
              my $gesamt = 0;
              my $rabatt = "";
              ($gesamt,$rabatt) = $pri->price2calc($cttpos->{$id});
              $sum += $gesamt;
              $gesamt = $lb->round($gesamt);
	      $gesamt = sprintf('%.2f',$gesamt);
	      print $q->td({-class=>'tdint', -style=>"$occupied_style", -nowrap=>1},"$rabatt $gesamt €");
    	    }
	   }
	  }
	  }
    	  if($j==0){
	    print $q->Tr(),"\n";
	    print $q->td({-class=>'tdint'},"Keine Daten vorhanden");
	  }else{
	    $sum = $lb->round($sum);
	    $sum = sprintf('%.2f',$sum);
	    print $q->Tr(),"\n";
	    print $q->td({-class=>'tdint', -colspan=>3},"");
	    print $q->td({-class=>'tdint'},"Gesamt");
	    print $q->td({-class=>'tdint'},$q->b("$sum €"));
	  }
	  print $q->end_table;
	  print "</div>\n";

	  my $i=0;
          my $dtext = "";
	  print "<div style='clear:both;'>\n";
	  foreach my $id (sort { $ctt_all->{$b}->{ct_name} cmp $ctt_all->{$a}->{ct_name} } keys(%$ctt_all)){

 	    if( -f "$ctt_all->{$id}->{basedir}/pdfinvoice/Rechnung-$ctt_all->{$id}->{operator}-$ctt_all->{$id}->{ct_name}.pdf" && $ctt_all->{$id}->{ct_name} =~ /\d/ && $coo){
	     $i++;
	     my $invoice_time = $ctt_all->{$id}->{invoice_time} || $ctt_all->{$id}->{mtime};
	     $invoice_time = $lb->time4de($invoice_time,0);
             #$varenv->{operator} only defined in sharee
             my $webtarget = "_blank";
	     my $dtext = "";
             if($varenv->{syshost} =~ /app/){
               $webtarget = "_self";
               $dtext = "(Der PDF download öffnet je nach System/Konfiguartion einen externen PDF-Viewer oder Webbrowser)";
             }

	     if($i==1){
               print $q->div({-id=>'Rechnungen',-style=>'font-weight:bold;'},"Ihre Rechnungen"),"\n";
               print $q->div({-style=>'padding:0.5em;'},"$dtext"),"\n";
	     }
             print $q->div({-style=>'font-size:1em;padding:0.5em;border:0px solid #cccccc;'},$q->a({-href=>"$ctt_all->{$id}->{wwwhost}/FileOut?file=Rechnung-$ctt_all->{$id}->{operator}-$ctt_all->{$id}->{ct_name}.pdf&sessionid=$coo", -target=>"$webtarget" , -type=>'application/octet-stream', -style=>'text-decoration:underline;'}, $q->img({-src=>"$varenv->{metahost}/glyphicons/file-any.png", -style=>'width:30px;'}), "Rechnung $ctt_all->{$id}->{ct_name}.pdf"), "&nbsp;&nbsp; ($invoice_time)"),"\n";
	   }

	  }
	  print "</div>\n";
       }
      }
     }
     #------------------------------------------------------
	
      my $label_des="* $des";
      my $autofocus="";
      if($key =~ /txt15|txt19/){
        $label_des="$des";
      }

      if($key =~ /txt/ && $size eq "select" && $des eq "Land"){
	my $country = $lb->country_code();
	$ctrel->{$key} = "DE" if(!$ctrel->{$key});
	my @_valxx;
	foreach (sort { $country->{$a} cmp $country->{$b} } keys (%$country)){
   	  push @_valxx, "$_:$country->{$_}";
 	}
    	print $q->label({-for=>"$key",-style=>'padding-top:15px;'},"$label_des"),"\n";
	print $but->selector_class("$key","form-control","","$ctrel->{$key}",@_valxx);

      }elsif($key =~ /txt/ && $size eq "select"){
	my @_valxx = split(/,/,$varenv->{$des});
    	print $q->label({-for=>"$key",-style=>'padding-top:15px;'},"$label_des"),"\n";
	print $but->selector_class("$key","form-control","","$ctrel->{$key}",@_valxx);

      #}elsif($key =~ /txt/ && !$R::confirm_success){
      }elsif($key =~ /txt/){
        my $required="required";

	#start failure messages
	#Gutschein 
	if($key =~  /txt16/ && $ctrel->{c_id}){
         if($R::failure =~ /^txt16/){
            $autofocus = "autofocus";
            $label_des = "<span style=color:$red>Der Gutscheincode ist nicht vorhanden.</span>";
         }
	 elsif($R::failure =~ /conflict_txt16/){
            $autofocus = "autofocus";
            $label_des = "<span style=color:$red>Der Gutscheincode wurde bereits hinterlegt.</span>";
	 }else{
            $required="";
	    $label_des = "<span style='font-weight:normal;'>$des</span>";
	 }
	}
	elsif($key eq "txt08" && $R::failure && $R::failure =~ /conflict_txt08/){
	  $autofocus = "autofocus";
	  my ($failkey,$failval) = split(/=/,$R::failure);
	  $label_des = "<span style=color:$red>Achtung, es existiert bereits ein Account mit der e-Mail Adresse: $failval</span>";
	  $ctrel->{$key} = $R::conflict_txt08;
	}
	elsif(($R::failure && $R::failure =~ /^$key/) || ($ctrel->{txt31} && $ctrel->{txt31} =~ /$key/)){
	  $autofocus = "autofocus";
	  $label_des = "<span style=color:$red>Bitte \"$des\" Angabe korrigieren</span>";
	  $label_des = "<span style=color:$red>Für das \"$des\" liegt ein Fehler vor.</span>" if($key eq "txt04");
	  $label_des = "<span style=color:$red>Bitte mobile Telefon Nr. mit Ländervorwahl, Beispiel: +49 170 12345678</span>" if($key eq "txt07");
	}#end failure messages

        if($key eq "txt15"){
	  if($R::failure =~ /^txt15/){
	    $autofocus = "autofocus";
	    $label_des = "<span style=color:$red>$bonus_avail</span>";
	  }
	  if($R::failure =~ /conflict_txt15/){
	   $label_des = "<span style=color:$red>$bonus_conflict</span>";
	  }

	  my $saved_coupon = "";
	  $saved_coupon = "$ctadrcoupon->{sharee_kn}->{txt15}" if($ctadrcoupon->{sharee_kn}->{txt15});
    	  print $q->label({-for=>"$key", -style=>'padding-top:1.5em;'},"$bonus_desc"),"\n";
	  print "<input id='$key' type='text' class='form-control' name='$key' value='$saved_coupon' override $autofocus />\n";
	  print $q->div("&nbsp");
	  foreach my $opid (keys(%$ctadrcoupon)){
	   if($ctadrcoupon->{$opid}->{txt15} && ($aowner != 195 && $aowner != 185 && $aowner != 176)){
 	     print $q->div("$bonus_saved $ctadrcoupon->{$opid}->{txt15}"),"\n";
	   }
	  }
	
	}
	elsif($key eq "txt04"){
         if($R::failure && $R::failure =~ /confirm_txt04/){
	    $autofocus = "autofocus";
	    $label_des = "<span style=color:$red>Die Passwort Wiederholung ist fehlerhaft. Bitte korrigieren Sie Ihre Eingabe.</span>";
	 }
	 my $pw = "xxxxxxxx";
	 $pw = "" if(!$ctrel->{c_id});
    	 print $q->label({-for=>"$key", -style=>'padding-top:1.5em;'},"$label_des"),"\n";
	 print "<input id='$key' type='password' class='form-control' name='$key' value='$pw' override $required $autofocus /><br />\n";
         print $q->label({-for=>"confirm_$key"},"* Passwort wiederholen"),"\n";
	 print "<input id='confirm_$key' type='password' class='form-control' name='confirm_$key' value='$pw' override $required />\n";
	 print $q->div({-style=>'text-align:left;color:grey;'}, "<input type='checkbox' onclick='show_passwd()'>", "Passwort anzeigen"),"\n" if($pw ne "xxxxxxxx");
	}else{
	 #all other input textfields
    	 print $q->label({-for=>"$key", -style=>'padding-top:1.5em;'},"$label_des"),"\n";
	 print "<input id='$key' type='text' class='form-control' name='$key' value='$ctrel->{$key}' override $required $autofocus />\n";
	}
      }
      #end $key =~ /txt/

      #all int checkboxes disabled because of AGB downunder
      elsif($key =~ /int/ && $size eq "checkbox"){
        if(($R::failure && $R::failure =~ /^$key/) || ($ctrel->{txt31} && $ctrel->{txt31} =~ /$key/)){
	  $autofocus = "autofocus";
	  $label_des = "<span style=color:$red>Bitte \"$des\" bestätigen</span>";
        }
	my $required="";

        #sharee AGB global new
        if($key eq "int14" && $size eq "checkbox"){
	  $required="";
	  #bootstrap 5
	  my $sharee_agb = "<button type='button' class='btn btn-primary ' style='padding:1px 40px;border:1px solid #$bgcolor1;background-color:#$bgcolor1;' data-bs-toggle='modal' data-bs-target='#sharee_agb'>$des</button>\n";

	  print $q->label({-for=>"$key", -style=>'padding-top:20px;'},"$label_des"),"\n";
          print $q->div({-id=>"$key"},$but->checkbox("1","$key","$ctrel->{$key}","","$required","$autofocus"), "&nbsp; $sharee_agb"),"\n";
	  print $q->hidden(-name=>"$key",-override=>1,-value=>"null");

	  my $sharee_privacy = "<button type='button' class='btn btn-primary ' style='text-decoration:underline;color:black;border:1px solid white;background-color:white;' data-bs-toggle='modal' data-bs-target='#sharee_privacy'>Datenschutzhinweise</button>\n";
	  print $q->div({-style=>'padding-top:10px;'},"$sharee_privacy"),"\n";


        }elsif(1==2 && $key =~ /int02/){#newsletter
    	  	print $q->label({-for=>"$key", -style=>'padding-top:10px;'},""),"\n";
          	print $q->div({-id=>"$key"},$but->checkbox("1","$key","$ctrel->{$key}","","$required"), " $des"),"\n";
          	print $q->hidden(-name=>"$key",-override=>1,-value=>"0");
        }else{
    	  print $q->label({-for=>"$key", -style=>'padding-top:10px;'},""),"\n";
          print $q->div({-id=>"$key"},$but->checkbox("1","$key","$ctrel->{$key}","","$required"), " $des"),"\n";
          print $q->hidden(-name=>"$key",-override=>1,-value=>"0");
	}
     
      }elsif($key =~ /int/){
        if(($R::failure && $R::failure =~ /^$key/) || ($ctrel->{txt31} && $ctrel->{txt31} =~ /$key/)){
	   $autofocus = "autofocus";
	   $label_des = "<span style=color:$red>Bitte \"$des\" bestätigen</span>";
	   print $q->label({-for=>"$key", -style=>'padding-top:10px;'},"$label_des"),"\n";
        }

	if($key =~ /int03/){

	          print $q->div({-class=>'content2'}, "
	Wählen Sie Ihre Zahlungsart aus.<br/>
       	Das Abbuchungsverfahren (Erteilung eines SEPA Mandats) kann nur mit einem Girokonto in Deutschland erfolgen (IBAN beginnt mit DE). Es werden nur folgende Kreditkarten akzeptiert: VISA und MasterCard.<br/> 
	Die Nutzungsgebühren werden gesammelt i.d.R. wöchentlich mit dem Verwendungszweck „Mietrad“ von unserem Zahlungsdienstleister payone eingezogen.
	"),"\n";

          my $incasso="1";
          my $creditcard="2";
          my $checked_incasso;
          my $checked_creditcard;
          $checked_incasso=1 if($ctrel->{$key} == 1 || $ctrel->{$key} == 3);
          $checked_creditcard=2 if($ctrel->{$key} == 2);
          print $q->div({ -class=>'radio', -style=>'padding-top:20px;' },$but->radiobox_vertical("$key","$incasso","$checked_incasso",$q->img({-id=>"pic-payment" ,-style=>'height:50px;', -title=>'Abbuchung',-src=>"$varenv->{metahost}/img/payment-incasso2.png"}))),"\n";
          print $q->div({ -class=>'radio', -style=>'padding-bottom:20px;' },$but->radiobox_vertical("$key","$creditcard","$checked_creditcard",$q->img({-id=>"pic-payment" ,-style=>'height:30px;', -title=>'Kreditkarte', -src=>"$varenv->{metahost}/img/payment-creditcard2.png"}))),"\n";
    	  print $q->hidden(-name=>"$key",-override=>1,-value=>"null");
	}
      }

      if($key eq "txt16" && $ctrel->{c_id}){
        print $q->div({-style=>'margin-top:1em;text-align:center;'},"<button type='submit' name='sharee_edit' value='save_transact' class='btn btn-primary btn-lg btn-block' style='border:1px solid #$bgcolor1;background-color:#$bgcolor1;'>Speichern</button>"),"\n";
      }
 
     }
  }

   if(($R::failure && $R::failure =~ /\w+/ && $R::failure !~ /txt15|txt16/) || ($ctrel->{txt31} && $ctrel->{txt31} =~ /\w/)){
	print $q->div({-class=>'content2', -style=>"color:$red"}, "* Achtung!<br />Bitte überprüfen Sie Ihre Profildaten, damit wir das Fahrradmietsystem freischalten können."),"\n";	
   } 

  if($path !~ /$varenv->{accounting_3}|$varenv->{profile}/){
   if($ctrel->{c_id}){
    my $button_name = "Speichern";
    $button_name = "Weiter" if($path =~ /$varenv->{accounting_1_5}|$varenv->{accounting_2}/);
    print $q->hidden(-name=>"tinkc_id",-override=>1,-value=>"$ctrel->{c_id}");
    print $q->div({-style=>'margin-top:1em;text-align:center;'},"<button type='submit' name='sharee_edit' value='save_account' class='btn btn-primary btn-lg btn-block' style='border:1px solid #$bgcolor1;background-color:#$bgcolor1;'>$button_name</button>"),"\n";
    if($path =~ /$varenv->{accounting_1}/){
    	print $q->div({-style=>'margin-top:3em;text-align:center;'},$q->a({-style=>"color:#$bgcolor1;font-size:1.2em;", -role=>"button", -href=>"/$viewsel[0]/Account?sharee_edit=delete_account1$session_and"}, "Delete account?")),"\n";
    }
   }else{
    print $q->div({-style=>'margin-top:1em;text-align:center;'},"<button type='submit' name='sharee_edit' value='create_account' class='btn btn-primary btn-lg btn-block' style='border:1px solid #$bgcolor1;background-color:#$bgcolor1;'>Weiter</button>"),"\n";
   }
  }


  }#ende e-Mail iif

  #print $q->div({-class=>'content2'}, "Bei Problemen während der Registrierung bitte unsere Account-hotline <a href='mailto:hotline\@sharee.bike'>hotline\@sharee.bike</a> kontaktieren."),"\n" if($project eq "Bayern" && $path =~ /$varenv->{accounting_1}|$varenv->{accounting_3}/);
  print $q->div({-class=>'content2', -style=>'margin-top:2em;'}, "Bei Problemen während der Registrierung bitte unsere Account-hotline hotline\@sharee.bike kontaktieren."),"\n" if($project eq "Bayern" && $path =~ /$varenv->{accounting_1}|$varenv->{accounting_3}/);

  print "</div>\n";
  print $q->end_form,"\n";

  print $q->div({-style=>'position:fixed;bottom:2%;right:2%;z-index:10;font-size:13px;'},"--> $varenv->{syshost} | $varenv->{merchant_id} | $bgcolor1 | template -> $node_meta->{tpl_name},$tpl_id ($template01,$template02) | $users_sharee->{c_id}"),"\n" if($users_sharee->{c_id} eq $dbt->{copri_conf}->{superu_id} || $dbt->{copri_conf}->{stage} eq "test");

  print "</div>";
}
1;
