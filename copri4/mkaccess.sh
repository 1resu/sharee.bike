#!/bin/bash

#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH

chgrp -R www-data *
chmod go-w *
chmod -R o-rwx *
for i in $(find . -type d -and -name csv); do chmod -R ug+rwx $i;done
for i in $(find . -type d -and -name data); do chmod -R ug+rwx $i;done
for i in $(find . -type d -and -name ftp); do chmod -R ug+rwx $i;done
for i in $(find . -type d -and -name cache); do chmod -R ug+rwx $i;done
for i in $(find . -type d -and -name pdf); do chmod -R ug+rwx $i;done
for i in $(find . -type d -and -name xml); do chmod -R ug+rwx $i;done
for i in $(find . -type d -and -name site); do chmod -R ug+rwx $i;done
for i in $(find . -type d -and -name apk); do chmod -R ug+rwx $i;done

for i in $(find . -type d -and -name sql); do chmod -R go-rwx $i;done
for i in $(find . -type d -and -name src); do chmod -R go-w $i;done
for i in $(find . -type d -and -name src); do chmod -R ug+x $i;done
for i in $(find . -type d -and -name img); do chmod -R go-w $i;done
for i in $(find . -type d -and -name apache); do chmod -R go-w $i;done
for i in $(find . -type d -and -name css); do chmod -R go-w $i;done
for i in $(find . -type d -and -name js); do chmod -R go-w $i;done
for i in $(find . -type f -and -iname README*); do chmod -R go-rwx $i;done
for i in $(find . -type d -and -name docs); do chmod -R go-rwx $i;done
for i in $(find . -type f -and -name .htaccess); do chmod -R go-wx $i;done
for i in $(find . -type f -and -name startup.pl); do chmod -R go-w $i;done
for i in $(find . -type f -and -name robots.txt); do chmod -R go-wx $i;done
for i in $(find . -type d -and -name cronjobs); do chmod -R g-w $i/*;done
for i in $(find . -type d -and -name cronjobs); do chmod -R o+r $i/*;done
for i in $(find . -type d -and -name cronjobs); do chown -R root:root $i/*;done



live_sharee="ginger" 
echo "You are on Server with hostname: $(hostname)";
echo "on main gitlab sourcen there is no live config";

#sharee
echo "KEEP in mind, SHAREE autoLinking is only done if $(hostname) == ${live_sharee}";

if [[ $(hostname) == ${live_sharee} ]]
then

  #shareedms-operator
  echo "";
  for i in $(find . -type l -and -name shareedms-operator.conf); do rm $i;done
  for i in $(find . -type f -and -name shareedms-operator_live.conf)
   do (
        echo $(dirname $(realpath $i));
        cd $(dirname $(realpath $i));
        ln -s $(basename $i) shareedms-operator.conf;
	ls -al
   )
  done

  #shareeapp-operator
  echo "";
  for i in $(find . -type l -and -name shareeapp-operator.conf); do rm $i;done
  for i in $(find . -type f -and -name shareeapp-operator_live.conf)
   do (
        echo $(dirname $(realpath $i));
        cd $(dirname $(realpath $i));
        ln -s $(basename $i) shareeapp-operator.conf;
	ls -al
   )
  done


  #shareeweb-project
  echo "";
  for i in $(find . -type l -and -name shareeweb-project.conf); do rm $i;done
  for i in $(find . -type f -and -name shareeweb-project_live.conf)
   do (
        echo $(dirname $(realpath $i));
        cd $(dirname $(realpath $i));
        ln -s $(basename $i) shareeweb-project.conf;
	ls -al
   )
  done

fi


