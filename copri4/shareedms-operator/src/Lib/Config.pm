package Config;

#Deprecated config file
#Please use shareeconf/*

use strict;
use warnings;
use CGI;
use Config::General;
my $q = new CGI;

sub new { 
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self; 
}

sub envonline(){
 my $self = shift;

 my $server_type = "operator";
 my $server_key = "sharee_operator";

 my $globalconf_file = "/var/www/copri4/shareeconf/global.cfg";
 my $conf = Config::General->new($globalconf_file);
 my %globalconf = $conf->getall;

   my $basedir = "$globalconf{copri_conf}->{basedir}/$globalconf{$server_type}{$server_key}->{dir_dms}";
   my $metahost = $globalconf{$server_type}{$server_key}->{operatorDMS1};

   my %varenv = (    
                dbname => $globalconf{$server_type}{$server_key}{database}->{dbname},
                dbuser => $globalconf{$server_type}{$server_key}{database}->{user},
                dbpassw => $globalconf{$server_type}{$server_key}{database}->{passwd},
                dbhost => $globalconf{$server_type}{$server_key}{database}->{host},
                syshost => $globalconf{$server_type}{$server_key}->{dir_dms},
                wwwhost => $globalconf{$server_type}{$server_key}->{operatorDMS},
                praefix => $globalconf{$server_type}{$server_key}{database}->{dbname},
                systype => "sharee",#(azn)
                mandant => $globalconf{shareedms_conf}->{parent_node},
               	start => $globalconf{shareedms_conf}->{start},
                profile => $globalconf{shareedms_conf}->{profile},

                superu_id => $globalconf{copri_conf}->{superu_id},
                debug => $globalconf{copri_conf}->{debug},
                logdir => $globalconf{copri_conf}->{logdir},
                live_hostname => $globalconf{$server_type}{$server_key}->{live_hostname},
		metahost=>"$metahost",
		orga => "dms",
		term_active_color => "#$globalconf{shareedms_conf}->{term_active_color}",
                background_image => "",
		background_size => "cover",
                background_color => "#5c5c5c",
                background_color2 => "#a7a18f",
                background_align => "center",
		background_repeat => "no-repeat",
                font_family => "Arial",
                font_size => "100.01%",
                line_height => "1",
		basedir => "$basedir",
                csv => "$basedir/csv",
                pdf => "$basedir/pdf",
                data => "$basedir/data",
		xmlfile => "$basedir/xml",
                Anrede => "Anrede,Frau,Herr,Firma",
                Einheit => "Stück,Paar,Satz,Meter,Liter",
		map_activ => "",
		min_width => "1100",
		head_logo => "$metahost/img/sharee_bike_Logo.jpg",
		barcode => "",
		limit => "200",
		waren_main_id => $globalconf{shareedms_conf}->{waren},
		Zahlungsweise => "SEPA-Lastschrift (payone)|Kreditkarte (payone)|Zahlungseingang (payone txid)|Abbuchung|Bar|EC-Karte|Überweisung|Zahlungsausfall",
		order_state => "|angerufen|eMail gesendet|in Arbeit|an FiBu delegiert|txid renewed|cronjob fail|payone Mahnung|COPRI Mahnung",
		'Ziel Datei' => "privacy.html|agb.html|impress.html|tariff_info.html|bike_info.html",
                printer => "PDF"
                );
   return %varenv;

}

1;


