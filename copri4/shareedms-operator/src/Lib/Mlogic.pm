package Mlogic;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
#
use strict;
use warnings;
use CGI::Carp qw(fatalsToBrowser);
use CGI ':standard';
use Config::General;
use Mod::Buttons;
use Mod::Basework;
use Mod::Libenz;
use Mod::Libenzdb;
use Mod::DBtank;
use POSIX;
use Data::Dumper;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
 my $self = shift;
 my $node_meta = shift;
 my $users_dms = shift;
 my $mode = shift || "";
 my $varenv = shift;
 my $users_sharee = shift || "";
 my $return = shift || "";

 my $q = new CGI;
 my $bw = new Basework;
 my $lb = new Libenz;
 my $db = new Libenzdb;
 my $dbt = new DBtank;
 my $but = new Buttons;
 my %ib = $but->ibuttons();
 my $script = $q->script_name();
 my $path_info = $q->path_info();
 my $path = $path_info;
 if("$varenv->{metahost}"){
   $path = "$script" . "$path_info";
   $script="";
 }
 my $coo = $q->cookie(-name=>'domcookie');
 my $debug = "";
 my $lang = "de";
 my @viewsel = split /\//,$1 if($path =~ /^\/(.*)/);
 my $view_root = $viewsel[0] || "";
 my $mandant = $db->get_node("$view_root","$lang");
 my $parent_id = $node_meta->{parent_id};
 my $main_id = $node_meta->{main_id};

 my $title = "";
 $title .= $dbt->{primary}->{$varenv->{dbname}}->{title} if($dbt->{primary}->{$varenv->{dbname}}->{title});
 $title .= $dbt->{operator}->{$varenv->{dbname}}->{title} if($dbt->{operator}->{$varenv->{dbname}}->{title});
 $title .= " | " . $dbt->{operator}->{$varenv->{dbname}}->{oprefix} if($dbt->{operator}->{$varenv->{dbname}}->{oprefix});
 $title .= $dbt->{website}->{$varenv->{syshost}}->{title} if($dbt->{website}->{$varenv->{syshost}}->{title});
 $title .= " (devel $varenv->{dbname})" if($dbt->{copri_conf}->{stage} eq "test");


 ###User Panel
 if($users_dms->{u_id}){

  ###header start
  print "<div style='position:fixed;z-index:10;left:0px;width:100%;height:24px;background-color:$varenv->{background_color};'>\n";
  print "<div style='position:fixed;z-index:10;right:0px;'>\n";

  #cleanup
  #if(!$mandant->{node_name} || "$mandant->{node_name}" ne "$users_dms->{fullurl}"){
  # $db->cleanup_users($users_dms->{owner}) if($users_dms->{owner});
  #}

  if($users_dms->{c_id4trans} && $users_dms->{tpl_id4trans}){
     my $table = "contenttrans";

    my $limit = $R::limit || $varenv->{limit};
    my $offset = $R::offset || "0";
    if($R::go && $R::go eq "backward_list"){
        $offset -= $limit if($offset >= $limit);
    }elsif($R::go && $R::go eq "forward_list"){
        $offset += $limit;
    }
     my $ctrel = $db->get_ctrel($table,"",$lang,"",$users_dms->{c_id4trans},$users_dms->{tpl_id4trans});
     print $q->div({-id=>'Headerlogin',-style=>"background-color:$varenv->{term_active_color};"},$q->a({-class=>'elinkbutton1',-title=>"Faktura Terminal öffnen",-href=>"$varenv->{wwwhost}/DMS/Faktura?ct_trans=open\&c_id4trans=$users_dms->{c_id4trans}\&tpl_id4trans=$users_dms->{tpl_id4trans}\&owner=$users_dms->{owner}\&offset=$offset\&limit=$limit\&relids=$R::relids\&no_redirect=1"}," Faktura \#$ctrel->{ct_name} $ctrel->{txt01}", $q->span({-id=>"c_id4trans", -style=>"color:$varenv->{term_active_color}"}, "$users_dms->{c_id4trans}"))) if($ctrel->{ct_name});
  }

   print $q->div({-id=>'Headerlogin'},"$users_sharee->{txt08} ", $q->span({-id=>"owner", -style=>"color:silver"}, "($users_dms->{u_id})")),"\n";
   print $q->div({-id=>'Headerlogin'},$q->a({-href=>"logout"},$q->img({-style=>'padding:0px 10px;',-src=>"$varenv->{metahost}/img/logout.svg"}))),"\n";
  print "</div>";
  print "</div>";
 }
 #end user panel
 ###header end


 #Mainlogin
 if(!$users_dms->{u_id}){

  print "<div id='ContentLogin'>\n";

  print $q->start_multipart_form(-id=>'authform', -name=>'loginscreen'),"\n";
  print $q->hidden(-name=>"merchant_id",-override=>1,-value=>"$varenv->{merchant_id}") if($varenv->{merchant_id});
  print $q->hidden(-name=>"sessionid",-override=>1,-value=>"$R::sessionid") if($R::sessionid);

  print "<div class='form-group'>\n";
   #Login
    print $q->div({-class=>'content2', -nowrap=>1}, $q->img({ -style=>"height:25px",-src=>"$varenv->{head_logo}"}),"Operator Login – $title"),"\n";
    print $q->div({-style=>'color:#c83434'},"Login failure"),"\n" if("$R::login_dms" eq "Login" && !$users_dms->{u_id});
    print $q->label({-for=>'Userid'},""),"\n";
    print $q->textfield(-class=>'form-control', -name=>'user_id', -value=>'', -override=>1, -type=>'email',-class=>'form-control', -id=>'Userid', -placeholder=>'NutzerIn', -required=>1, -autofocus=>1),"\n";
    print $q->label({-for=>'PW'},""),"\n";
    print $q->password_field(-class=>'form-control', -name=>'user_pw', -value=>'', -override=>1, -type=>'password',-class=>'form-control', -id=>'PW', -placeholder=>'Passwort', -required=>1),"\n";
    print $q->div({-style=>'margin-top:1em;'},"<button type='submit' name='login_dms' value='Login' class='btn btn-primary btn-lg btn-block'>Login</button>"),"\n";

  print "</div>\n";
  print $q->end_form,"\n";
  #print $q->span({-style=>'color:white;'},"merchant_id:$varenv->{merchant_id}"),"\n";
  print "</div>";

  exit 0;
 }

 if($users_dms->{u_id} && $main_id){
      
   if(ref($return) eq "HASH"){
      $return = $bw->return_feedback($node_meta,$users_dms,$return);
   }else{
      #deprecated
      $return = $lb->return_feedback($return,$node_meta->{node_name},$users_dms->{owner}) if($return !~ /shareejson/);
   }

   if($main_id >= "100000"){
    my $mstyle=""; 

    my $dbh_primary = $dbt->dbconnect_extern("sharee_primary");
    my $users_dms_primary = { u_id => 0 };
    $users_dms_primary = $dbt->select_users($dbh_primary,$users_dms->{u_id},"and int02 >= 1");
    #print Dumper($users_dms_primary);

    my $mod_active = "$dbt->{shareedms_conf}->{mod_active}";
    if($users_dms_primary->{u_id} && $users_dms_primary->{int02} >= 1){
      $mod_active .= "|Kunden|Faktura";
    }else{
      $mod_active .= "|App-feedback";
    }

    ###Top Menu
    #bootstrap menue
    print "<nav class='navbar navbar-expand-md navbar-dark bg-dark fixed-top' style='top:23px;opacity:0.85;'>\n";
    print "<div class='container-fluid'>\n";
    print "<button class='navbar-toggler' type='button' data-bs-toggle='collapse' data-bs-target='#navbarsDefault' aria-controls='navbarsDefault' aria-expanded='false' aria-label='Toggle navigation'>\n";
      print "<span class='navbar-toggler-icon'></span>\n";
    print "</button>\n";
    print $q->div({-id=>'Headerlogo'},$q->a({-class=>'navbar-brand',-style=>"color:black;",-href=>"$varenv->{wwwhost}"}, $q->img({ -style=>"height:25px",-src=>"$varenv->{head_logo}"}), "c o p r i" ,$q->div({-style=>'font-size:0.71em;'}, "$title"))),"\n";
    my $node = $db->collect_noderel($dbt->{shareedms_conf}->{parent_id},$lang,$users_dms->{u_id});

    print "<div class='collapse navbar-collapse' id='navbarsDefault'>\n";
    print "<ul class='navbar-nav me-auto mb-2 mb-md-0' style='padding:0 20px;text-align:left;'>\n";
    #print "<ul class='nav navbar-nav'>\n";

    foreach my $id (sort {$node->{$a}->{n_sort} <=> $node->{$b}->{n_sort}} keys (%$node)){
      $node->{$id}->{node_path} = $node->{$id}->{node_name} if(!$node->{$id}->{node_path});
      my $lmenu0 = "";
      my $url = "";
      my $mclass = "";
      my $mstyle = "";
      if("$node->{$id}->{node_name}" eq "$viewsel[1]"){
      	$mclass = "active";
	$mstyle = "color:white;";
        $parent_id = $node->{$id}->{parent_id};
        $main_id = $node->{$id}->{main_id};
      }

      if($node->{$id}->{main_id} && $node->{$id}->{node_name} =~ /$mod_active/){
        my $topath = "/$viewsel[0]/$node->{$id}->{node_path}";
	#
	#0. menue
	my $aclass = "dropdown-item";
	$aclass = "nav-link" if($node->{$id}->{node_name} =~ /Mietjourna|Alarmjournal|Karte/);
	$aclass = "nav-link" if($node->{$id}->{node_name} =~ /Mietjourna|Alarmjournal|Karte|App-feedback/ && $mod_active !~ /Kunden/);
	if($users_dms->{u_id} && ($users_dms->{int01} == 2 && $node_meta->{ct_table} =~ /content$/) || ($users_dms->{u_id} == $dbt->{copri_conf}->{superu_id})){
		my $url = "$topath/maintainer?node2edit=edit_relation\&main_id=$node->{$id}->{main_id}";
		$lmenu0 = $but->event_button("$topath","$node->{$id}->{node_name}","$node->{$id}->{main_id}","$aclass $mclass","","$users_dms->{u_id}","$url");
	}else{
              	$lmenu0 = $but->lo_button("$topath","$node->{$id}->{node_name}","$node->{$id}->{main_id}","$aclass $mclass","","$users_dms->{u_id}");
	}

        #1. submenu
        my $subs1=0;
        my $node1 = $db->collect_noderel($node->{$id}->{main_id},$lang,$users_dms->{u_id});
	foreach my $id1 (sort {$node1->{$a}->{n_sort} <=> $node1->{$b}->{n_sort}} keys (%$node1)){
	    $subs1=1 if($node1->{$id1}->{main_id});
	}
                
	#1. submenue
	if($subs1){
          print "<li class='nav-item dropdown'>\n";#with integrated event_button
	   print "<a class='nav-link dropdown-toggle $mclass' href='#' style='' id='navbarDropdown' role='button' data-bs-toggle='dropdown' aria-expanded='false'> $node->{$id}->{node_name} </a>\n";
          print "<ul class='dropdown-menu' aria-labelledby='navbarDropdown'>\n";
          print "<li class='nav-item'> $lmenu0 </li><li><hr class='dropdown-divider'></li>\n";
          my $lmenu1 = "";
          foreach my $id1 (sort {$node1->{$a}->{n_sort} <=> $node1->{$b}->{n_sort}} keys (%$node1)){
	   #hide Faktura config or be admin
	   if(($node1->{$id1}->{template_id} !~ /196|201/) || ($users_dms_primary->{u_id} && $users_dms_primary->{int02} >= 1)){
            my $node2 = $db->collect_noderel($node1->{$id1}->{main_id},$lang,$users_dms->{u_id});
            $topath = "/$viewsel[0]/$node->{$id}->{node_name}";
     	    $mclass = "";
            $mstyle = "";
      	    if("$node1->{$id1}->{node_name}" eq "$viewsel[2]"){
        	$mclass = "active";
        	$mstyle = "color:white;";
	    }
	    if($users_dms->{u_id} && ($users_dms->{int01} == 2 && $node_meta->{ct_table} =~ /content$/) || ($users_dms->{u_id} == $dbt->{copri_conf}->{superu_id})){
		my $url = "$topath/$node1->{$id1}->{node_path}/maintainer?node2edit=edit_relation\&main_id=$node1->{$id1}->{main_id}";
		$lmenu1 = $but->event_button("$topath/$node1->{$id1}->{node_path}","$node1->{$id1}->{node_name}","$node1->{$id1}->{main_id}","dropdown-item $mclass","","$users_dms->{u_id}","$url");
	    }else{
              	$lmenu1 = $but->lo_button("$topath/$node1->{$id1}->{node_path}","$node1->{$id1}->{node_name}","$node1->{$id1}->{main_id}","dropdown-item $mclass","","$users_dms->{u_id}");
	    }

	    print "<li class='nav-item'> $lmenu1 </li>\n";

	    #2. submenu (services content)
	    if(ref($node2) eq "HASH"){
	      my $lmenu2;
              foreach my $id2 (sort {$node2->{$a}->{n_sort} <=> $node2->{$b}->{n_sort}} keys (%$node2)){
		my $parent_node4rel = $db->get_node4rel($node2->{$id2}->{parent_id},"","","null");
		#because of don't view Service Attribut on Einste.
		if($parent_node4rel->{template_id} =~ /205|225/){
		#if(1==1){
              	  $mclass = "";
                  $mstyle = "";
                  if("$node2->{$id2}->{node_name}" eq "$viewsel[3]"){
                    $mclass = "active";
                    $mstyle = "color:white;";
                  }
                  if($users_dms->{u_id} && ($users_dms->{int01} == 2 && $node_meta->{ct_table} =~ /content$/) || ($users_dms->{u_id} == $dbt->{copri_conf}->{superu_id})){
                    my $url = "$topath/$node1->{$id1}->{node_path}/$node2->{$id2}->{node_path}/maintainer?node2edit=edit_relation\&main_id=$node2->{$id2}->{main_id}";
                    $lmenu2 = $but->event_button("$topath/$node1->{$id1}->{node_path}/$node2->{$id2}->{node_path}","$node2->{$id2}->{node_name}","$node2->{$id2}->{main_id}","dropdown-item $mclass","","$users_dms->{u_id}","$url");
                  }else{
                    $lmenu2 = $but->lo_button("$topath/$node1->{$id1}->{node_path}/$node2->{$id2}->{node_path}","$node2->{$id2}->{node_name}","$node2->{$id2}->{main_id}","dropdown-item $mclass","","$users_dms->{u_id}");
                  }
                  print "<div class='nav-item' style='margin-left:25px;'> $lmenu2 </div>\n";
		} 
	      }
	    }
	   }
	  }
          print "</ul>\n";
          print "</li>\n";
          #end submenu
	#}elsif($node->{$id}->{node_name} !~ /Einstellung/ || $users_dms->{u_group} =~ /maintainer/){
        }elsif(1==1){
          print "<li class='nav-item'> $lmenu0 </li>\n";
        }
      }
    }
   }#end foreach lmenue0
   print "</ul>\n";
   print "</div></div></nav>\n";
   ###end bootstrap menue

   #node_name=DMS
   if($varenv->{dbname} eq "sharee_primary" && $main_id == "100002"){
    my $big2menu="";
    #while (my ($key, $value) = each %{ $dbt->{operator} }) {
    foreach my $key (sort keys  (%{ $dbt->{operator} })) {
      $big2menu .= $but->lia_button("$dbt->{operator}->{$key}->{operatorDMS}","DMS $key","","","color:#$dbt->{operator}->{$key}->{bgcolor1};","") if($dbt->{operator}->{$key}->{merchant_id});
    }
    foreach my $key (sort keys  (%{ $dbt->{website} })) {
      $big2menu .= $but->lia_button("$dbt->{website}->{$key}->{operatorWeb}","Web $key","","","color:#$dbt->{website}->{$key}->{bgcolor1};","") if($dbt->{website}->{$key}->{merchant_id});
    }
    foreach my $key (sort keys  (%{ $dbt->{appsframe} })) {
      $big2menu .= $but->lia_button("$dbt->{primary}->{sharee_primary}->{primaryApp}?sessionid=$dbt->{appsframe}->{$key}->{merchant_id}","App $key","","","color:#$dbt->{merchant_ids}->{$dbt->{appsframe}->{$key}->{merchant_id}}->{bgcolor1};","");
    }

    print $q->div({-style=>'width:100%;margin:0;padding-top:70px;color:white;'}, ""),"\n";
    print $q->div({-style=>'width:100%;margin:0;'}, $q->ul({-id=>'BigNavi'},$big2menu)),"\n";
   }
  
  $debug = "syshost: $varenv->{syshost}, merchant_id: $varenv->{merchant_id}, (c_id4trans:$users_dms->{c_id4trans} && tpl_id4trans:$users_dms->{tpl_id4trans}) $node_meta->{tpl_name},$node_meta->{tpl_id},$node_meta->{ct_table},$parent_id,$main_id, permissions: ($users_dms->{int01},$users_dms->{int02},$users_dms->{int03},$users_dms->{int07},$users_dms->{int08},$users_dms->{int09})" if($users_dms->{u_id} == $dbt->{copri_conf}->{superu_id});
  print $q->div({-style=>'position:fixed;bottom:0%;right:1%;z-index:10;padding:2px;font-size:13px;'},"$debug",$q->a({-style=>'color:black;text-decoration: none;',-href=>'https://sharee.bike',-target=>'_blank'},"sharee.bike &copy; TeilRad GmbH 2022")),"\n";


#update c_id4trans on changing browser tab 2019-10-08
if(1==2 && $mode eq "manager"){
print<<EOF
<script>

function postc_id4trans(c_val,o_val) {
                \$.ajax({
                    url: '$varenv->{wwwhost}/ajax_post',
                    type: 'POST',
                    async: true,
                    cache: false,
                    data: { c_id4trans: c_val, owner: o_val },
                });
}

document.addEventListener('visibilitychange', function () {
 if(document.visibilityState != 'hidden') {
  var c_id4trans = document.getElementById("c_id4trans").innerText;
  var owner = document.getElementById("owner").innerText;
  if(c_id4trans){
    postc_id4trans(c_id4trans,owner);
  }
  console.log("postc_id4trans:" + c_id4trans + " " + owner);
 }
}, false);

</script>

EOF
;
}
   print $q->div({-style=>'padding: 30px 0 0 0;'}, ""), "\n";
   $self->tplselect($node_meta,$users_dms,$mode,$varenv,$return);
 print "</div>\n";
 }#end logedin environment

}

#2021-05-05 changed to Mlogic
sub tplselect(){
 my $self = shift;
 my $node_meta = shift;
 my $users_dms = shift;
 my $mode = shift || "";
 my $varenv = shift;
 my $return = shift || "";

 my $sort = "";
 my $lang = "de";
 my $tpl_name = $node_meta->{tpl_name};
 my $tpl_id = $node_meta->{tpl_id};
 my $ct_table = $node_meta->{ct_table};
 my $parent_id = $node_meta->{parent_id};
 my $main_id = $node_meta->{main_id};
 my $u_id = $users_dms->{u_id};
 #print Dumper($node_meta);
 #exit;

print <<EOF
 <script>
  \$(function(){
     \$('.autos').autosize({append: '\\n'});
  });
 </script>
EOF
;

 if($node_meta->{tpl_id} =~ /195|199/ && $node_meta->{ct_table} eq "contentuser"){
  require "Tpl/SubListe.pm";
  $return = &SubListe::tpl($node_meta,$users_dms,$mode,$return);
 }elsif($node_meta->{ct_table} eq "contentuser"){
  require "Tpl/MandantConf.pm";
  $return = &MandantConf::tpl($node_meta,$users_dms,$mode,$return);
 }elsif($node_meta->{tpl_id} == 1 && $node_meta->{main_id} == 200017){
  require "Tpl/Karte_osm.pm";
  &Karte_osm::tpl($node_meta,$users_dms,$mode,$varenv,"",$return);
 }elsif($node_meta->{ct_table} =~ /contentpos|contentadrpos|users$/){#contentpos or contentadrpos
  require "Tpl/SubListe.pm";
  $return = &SubListe::tpl($node_meta,$users_dms,$mode,$return);
 }elsif($node_meta->{ct_table} =~ /content$|contentadr$|contenttrans$/){
  require "Tpl/Liste3.pm";
  $return = &Liste3::tpl($node_meta,$users_dms,$mode,$return);
 }elsif($node_meta->{ct_table} =~ /contenttranspos|contenttheftpos/){
  require "Tpl/Calorin.pm";
  &Calorin::tpl($node_meta,$users_dms,$mode,$return);
 }
##Modalbox things##
 if($return !~ /failure/){
  if(($R::ct_trans !~ /close/) && ($ct_table =~ /contenttrans/) && ($R::ct_trans || $R::trans2edit || $R::ctpos_activ || $R::select_part || $R::set_main_id)){
   &Modalbox::mobox($node_meta,$users_dms,$mode,$return);
  }elsif(($R::ct_trans !~ /close/) && ($ct_table =~ /content$|contentadr|contentuser|contentpos|users$|contenttranspos/ && ($R::ct_trans || $R::rel_edit || $R::base_edit)) || ($R::node2edit && $R::node2edit =~ /edit/)){
   &Modalbox3::mobox3($node_meta,$users_dms,$mode,$return) if($R::rel_edit !~ /delete|save/ && $R::ct_trans !~ /delete/ && $R::base_edit !~ /delete|save/ && !$R::service_id);
  }
 }

 if($return =~ /failure/){
   require "Mod/Failure.pm";
   &Failure::tpl("",$u_id,"","","","",$return);
 }
}

1;


